# PHPMUD

A MUD engine built in PHP. 

The reason to choose PHP is because it's very quick to get things done in PHP, there's an incredible number of libraries available and it's much easier to get started with than a lower level language. This might be a poor excuse for a choice of language but the wider spread and easier usage means there will be less trouble finding capable (enough) hands in the future for this engine to continue to move forward. Developers with knowledge of the inner workings of the lower level languages are becoming more and more rare, while this isn't something I'd regularly advocate I do think facilitating for more easier development will be a bonus for these type of games.

## So how does it work?

Getting started with the codebase is fairly straight forward, a Vagrant instance has been provided that comes set up with all the goodies that development and running the engine needs.


    $ vagrant up    
    $ vagrant ssh
    $ cd /vagrant
    $ ./server.php
    

## Tools used
* **The repository is git based** - 
  See the [git book](https://git-scm.com/book/az/v1/Git-Basics-Getting-a-Git-Repository) for some basics. A google or two will also be very helpful, it's all fairly straightforward.
  
* **The code is written in PHP** -
  Reasons explained above, see [php quickref](http://php.net/manual/en/funcref.php) with it's search at the top in the menu.
  
* **Composer** -
  [Composer](https://getcomposer.org/) is a library manager for PHP, it links to [Packagist](https://packagist.org/), an online package repository with PHP libraries.
  
* **Vagrant** - 
  [Vagrant](https://www.vagrantup.com/) is used to provision a system that will contain all the needed elements for running the game and optionally developing it. Though development can also be done on the host machine using a more advanced IDE like Netbeans or PhpStorm.
  
## Some specifics
PHP doesn't allow for a graceful reload system, or actually it does, but sockets can't be persisted. In C we can write the sockets to a file temporarily and then reload them after the codebase is reloaded. We're using PHP's runkit to somewhat simulate these features. If the game does crash, we crash. There'll be a safeguard script that brings it back up after a second or so but the connections will be lost. Runkit should provide us with capabilities to reload the codebase when new features are to be implemented. 

## Codebase structure

    app // contains the application elements
     - config    // contains the configuration file(s)
     - modules   // contains the game modules
     - game      // contains the base game framework 
    var // storage for variable items like logs
    
    server.php // game startup script
    game.php   // actual game server
    
    .vagrant // storage folder for the vagrant data
    ansible  // storage folder for the puphpet vagrant provisioner (this sets up the vagrant instant with everything needed)
    vendor   // contains the libraries pulled in through composer
    
    .gitignore    // contains files that shouldn't end up in the git repository, variable files, configuration files, etc
    composer.json // the basic composer definitions/requirements
    composer.lock // the installed versions of libraries
    README.md     // markdown based readme file
    Vagrantfile   // base vagrant file
   
    
## Structure of a module
To be defined..
