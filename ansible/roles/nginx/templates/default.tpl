server {
    listen 80;
    
    server_name {{ nginx.servername }};

    root {{ nginx.docroot }};

    error_log /var/log/nginx/curves_error.log;
    rewrite_log on;


    location / {
        index index.html index.php;
        try_files $uri $uri/ @handler;
    }
 
    location @handler {
        rewrite / /index.php;
    }
 
    location ~ .php/ {
        rewrite ^(.*.php)/ $1 last;
    }
 
    # Handle the exectution of .php files.
    location ~ .php$ {
        if (!-e $request_filename) {
            rewrite / /index.php last;
        }
        expires off;
        fastcgi_pass unix:///run/php/php5.6-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param MAGE_RUN_CODE default;
        fastcgi_param MAGE_RUN_TYPE store;
        include fastcgi_params;
    }
}
