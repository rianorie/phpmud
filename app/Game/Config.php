<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game;

use JetBrains\PhpStorm\Pure;

/**
 * Config library, used for retrieval of the config elements
 *
 * @package Game
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-23
 */
class Config
{
	/**
	 * @var object
	 */
	private $config;

    /**
     * Construct the Config handler
     *
     * @param string $path Path to the config json file
     */
	public function __construct(string $path)
	{
		$this->config = json_decode(file_get_contents($path));
	}

    /**
     * Retrieve a configuration element
     *
     * @param string $path The config path
     *
     * @return mixed
     */
	#[Pure] public function get(string $path) : mixed
    {
		$parts = explode('.', $path);

		$data = $this->config;
		foreach($parts as $part) {
			if ( ! isset($data->{$part})) {
				return null;
			}
			$data = $data->{$part};
		}

		return $data;
	}
}
