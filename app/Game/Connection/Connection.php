<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Connection;

use Socket;

/**
 * Connection handler class that provides (remote) socket interactions
 *
 * @author  Rian Orie
 * @version 0.5
 */
class Connection implements Interfaces\Connection
{
    /**
     * Socket handle storage
     * @var Socket
     */
    private Socket $handle;

    /**
     * Use compression
     *
     * @var bool
     */
    private bool $compression = false;

    /**
     * Lock for writing output to the socket
     *
     * @var bool
     */
    private bool $writelock = false;

    /**
     * Byte read limit
     * @var int
     */
    private int $readLimit = 1024;

    /**
     * Data storage
     * @var array
     */
    private array $data = [];

    /**
     * @var Interfaces\Manager
     */
    private Interfaces\Manager $manager;

    /**
     * Construct a new connection
     *
     * @param Socket $socket  Socket handle
     * @param Interfaces\Manager  $manager The connection manager
     *
     */
    public function __construct(Socket $socket, Interfaces\Manager $manager)
    {
        $this->manager = $manager;
        $this->handle  = $socket;
    }

    /**
     * Retrieve the connection manager
     *
     * @return Interfaces\Manager
     */
    public function getManager() : Interfaces\Manager
    {
        return $this->manager;
    }

    /**
     * Set data for the connection
     *
     * @param string $key  The key
     * @param mixed  $data The data to set
     */
    public function setData(string $key, mixed $data)
    {
        $this->data[$key] = $data;
    }

    /**
     * Load data from the connection
     *
     * @param null|string $key Load data by this key
     *
     * @return mixed
     */
    public function getData($key = null) : mixed
    {
        if (null != $key) {

            if (isset($this->data[$key])) {
                return $this->data[$key];
            }

            return null;
        }

        return $this->data;
    }

    /**
     * Get the identifier for the connection object
     *
     * @return int
     */
    public function getId() : int
    {
        return spl_object_id($this->getSocket());
    }

    /**
     * Return the socket in the object
     *
     * @return Socket
     */
    public function getSocket() : Socket
    {
        return $this->handle;
    }

    /**
     * Read from a socket
     *
     * @param int $limit Optional read limit length
     *
     * @return string
     */
    public function read($limit = null) : string
    {
        if (is_null($limit)) {
            $limit = $this->readLimit;
        }
        return socket_read($this->getSocket(), $limit);
    }

    /**
     * Set the write lock state for this connection, if locked only forceful output is pushed to the socket
     *
     * @param boolean $state
     */
    public function writeLock(bool $state)
    {
        $this->writelock = $state;
    }

    /**
     * Write to a socket
     *
     * @param string  $message The message to write
     * @param boolean $force   Force writing of the output regardless of the writelock state
     *
     * @return int
     */
    public function write(string $message, bool $force = false) : int
    {
        if ($this->getSocket() instanceof Socket) {

            if ($this->compression) {
                $message = $this->getCompressed($message);
            }

            if ( ! $this->writelock || ($this->writelock && $force)) {
                $mixOut = socket_write($this->getSocket(), $message, strlen($message));

                if ($mixOut === false) {
                    $this->close();
                }

                return $mixOut;
            }
        }

        return false;
    }

    /**
     * Block a socket
     *
     * @return bool
     */
    public function block() : bool
    {
        if ( ! $this->getSocket() instanceof Socket) {
            return false;
        }

        return socket_set_block($this->getSocket());
    }

    /**
     * Unblock a socket
     *
     * @return bool
     */
    public function unblock() : bool
    {
        if ( ! $this->getSocket() instanceof Socket) {
            return false;
        }
        return socket_set_nonblock($this->getSocket());
    }

    /**
     * Close a socket
     *
     * @return void
     */
    public function close()
    {
        if ($this->getSocket() instanceof Socket) {
            if (@socket_write($this->getSocket(), '{close}', 7) === false) {
                @socket_close($this->getSocket());
            }
        }
    }

    /**
     * Set the compression state
     *
     * @param bool $state The compression state
     */
    public function setCompression(bool $state)
    {
        $this->compression = $state;
    }

    /**
     * Compress a string using
     *
     * @param string $message The message to compress
     *
     * @return bool|string
     */
    private function getCompressed(string $message) : bool|string
    {
        if (class_exists('HttpDeflateStream')) {
            $stream = new \HttpDeflateStream(\HttpDeflateStream::FLUSH_SYNC);
            return $stream->update($message);
        }

        return $message;
    }
}
