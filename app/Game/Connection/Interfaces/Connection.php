<?php

namespace Game\Connection\Interfaces;

use Socket;

interface Connection
{
    public function __construct(Socket $socket, Manager $manager);

    /**
     * Retrieve the connection manager
     *
     * @return Manager
     */
    public function getManager() : Manager;

    /**
     * Set data for the connection
     *
     * @param string $key  The key
     * @param mixed  $data The data to set
     */
    public function setData(string $key, mixed $data);

    /**
     * Load data from the connection
     *
     * @param null|string $key Load data by this key
     *
     * @return mixed
     */
    public function getData($key = null) : mixed;

    /**
     * Get the identifier for the connection object
     *
     * @return int
     */
    public function getId() : int;

    /**
     * Return the socket in the object
     *
     * @return resource
     */
    public function getSocket();

    /**
     * Read from a socket
     *
     * @param int $limit Optional read limit length
     *
     * @return string
     */
    public function read($limit = null) : string;

    /**
     * Set the write lock state for this connection, if locked only forceful output is pushed to the socket
     *
     * @param boolean $state
     */
    public function writeLock(bool $state);

    /**
     * Write to a socket
     *
     * @param string  $message The message to write
     * @param boolean $force   Force writing of the output regardless of the writelock state
     *
     * @return int
     */
    public function write(string $message, bool $force = false) : int;

    /**
     * Block a socket
     *
     * @return bool
     */
    public function block() : bool;

    /**
     * Unblock a socket
     *
     * @return bool
     */
    public function unblock() : bool;

    /**
     * Close a socket
     *
     * @return void
     */
    public function close();
}
