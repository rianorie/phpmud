<?php

namespace Game\Connection\Interfaces;

use Game\Config;
use Game\Connection\Interfaces\Connection as ConnectionInterface;
use Game\Connection\Exception;
use Game\Event;
use Psr\Log\LoggerInterface;
use Socket;

interface Manager
{
    public function __construct(Config $config, Event $event, LoggerInterface $logger);

    /**
     * Factory method, otherwise we'd have an entire factory for this one piece of functionality
     *
     * @param Socket $socket The socket resource to base the connection object off of
     *
     * @return ConnectionInterface
     */
    public function create(Socket $socket) : ConnectionInterface;

    /**
     * Register a connection in the manager
     *
     * @param ConnectionInterface $connection
     *
     * @throws Exception
     */
    public function add(ConnectionInterface $connection);

    /**
     * Remove a connection from the manager
     *
     * @param ConnectionInterface $connection
     *
     * @throws Exception
     */
    public function remove(ConnectionInterface $connection);

    /**
     * Retrieve all currently active connections
     *
     * @return ConnectionInterface[]
     */
    public function getAll() : array;

    /**
     * Retrieve all connections except one
     *
     * @param ConnectionInterface $connection
     *
     * @return ConnectionInterface[]
     */
    public function getAllExcept(ConnectionInterface $connection) : iterable;
}
