<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Connection;

use Game\Config,
	Game\Event;
use Psr\Log\LoggerInterface;
use Socket;

/**
 * Serves as a repository for the socket connections
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-29
 * @version 1.0
 */
class Manager implements Interfaces\Manager
{
	/**
	 * @var Interfaces\Connection[]
	 */
	private array $connections = [];

	/**
	 * @var Config
	 */
	private Config $config;

	/**
	 * @var Event
	 */
	private Event $event;

	/**
	 * @var LoggerInterface
	 */
	private LoggerInterface $logger;

	/**
	 * Initialize the connection manager, feeding it the needed dependencies
	 *
	 * @param Config $config The config object
	 * @param Event  $event  Event handler
	 * @param LoggerInterface $logger Logging system
	 */
	public function __construct(Config $config, Event $event, LoggerInterface $logger)
	{
		$this->config = $config;
		$this->event  = $event;
		$this->logger = $logger;
	}

    /**
     * Factory method, otherwise we'd have an entire factory for this one piece of functionality
     *
     * @param Socket $socket The socket resource to base the connection object off of
     *
     * @return Connection
     */
	public function create(Socket $socket) : Connection
    {
		$this->logger->info('New connection initialized.');
		return new Connection($socket, $this);
	}

	/**
	 * Register a connection in the manager
	 *
	 * @param Interfaces\Connection $connection
	 *
	 * @throws Exception
	 */
	public function add(Interfaces\Connection $connection)
	{
		foreach($this->connections as $current) {
			if ($current === $connection) {
				throw new Exception('Already a connection registered with the same resource.');
			}
		}

		$this->connections[] = $connection;
		$this->logger->info('New connection registered. External id: '.$connection->getData('socket.external'));
	}

	/**
	 * Remove a connection from the manager
	 *
	 * @param Interfaces\Connection $connection
	 *
	 * @throws Exception
	 */
	public function remove(Interfaces\Connection $connection)
	{
		foreach($this->connections as $pos => $existing) {
			if ($existing === $connection) {
				unset($this->connections[$pos]);
				$this->logger->info('Connection removed.');
				return;
			}
		}

		throw new Exception('Trying to remove a connection that doesn\'t exist.');
	}

	/**
	 * Retrieve all currently active connections
	 *
	 * @return Interfaces\Connection[]
	 */
	public function getAll() : array
	{
		return $this->connections;
	}

	/**
	 * Retrieve all connections except one
	 *
	 * @param Interfaces\Connection $connection
	 *
	 * @return Interfaces\Connection[]
	 */
	public function getAllExcept(Interfaces\Connection $connection) : iterable
	{
		foreach($this->connections as $existing) {
			if ($existing !== $connection) {
				yield $existing;
			}
		}
	}
}
