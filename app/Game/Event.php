<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game;

use Game\Module\Interfaces\Observer;
use JetBrains\PhpStorm\Pure;

/**
 * Event class, handles the event related features
 *
 * @package Game
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 0.5
 * @created 2015-07-23
 */
class Event
{
    /**
     * Definition a callback needs to return when it wants to stop processing
     */
    const STOP = -1000;

    /**
     * Storage for the observers in the system
     *
     * @var array
     */
    private array $observers;

    /**
     * Event dispatcher - handles the triggering of events in the system
     *
     * @param string $event The event to dispatch
     * @param array  $data  Data to feed the event
     */
    public function dispatch(string $event, array $data = [])
    {
        $data = (object) $data;

        // See if there are any observers registered for this event
        if (isset($this->observers[$event])) {

            // if there are, loop through them
            foreach ($this->observers[$event] as $sort => $callbacks) {

                foreach ($callbacks as $callback) {

                    // and execute them with the given arguments. BUT, if the callback
                    // returns the stop sign, stop processing any other callbacks.
                    if ($callback->fire($data) == self::STOP) {
                        return;
                    }
                }
            }
        }
    }

    /**
     * Register an observer
     *
     * @param string   $event    The event to hook into
     * @param Observer $callback Anonymous function or class registration to execute on event fire
     * @param int      $position The optional position of the callback in the queue, defaults to 50, takes any numeric
     *                           definition
     *
     * @return string
     */
    public function observe(string $event, Observer $callback, int $position = 50) : string
    {
        // grab an unique id to find the event with later
        $id = $this->getRandomId();

        // add the handler to the list
        $this->observers[$event][$position][$id] = $callback;

        // sort this new observer right into it's proper place
        $this->prepareObserverList();

        // and give the id back to handler
        return $id;
    }

    /**
     * Remove a registered observer from the system
     *
     * @param string $event    The event to observe
     * @param string $id       The id of the observer
     * @param int    $position Position the observer is registered at
     *
     * @return bool
     *
     * @throws Exception
     */
    public function removeObserver(string $event, string $id, int $position = 50) : bool
    {
        // check to make sure the observer exists
        if ( ! isset($this->observers[$event][$position][$id])) {
            throw new Exception('Trying to remove unregistered observer');
        }

        // unset it
        unset($this->observers[$event][$position][$id]);

        // and return the result of a final verification
        return ( ! isset($this->observers[$event][$position][$id]));
    }

    /**
     * Generate a random string
     *
     * @return string
     */
    #[Pure] protected function getRandomId() : string
    {
        return uniqid('', true);
    }

    /**
     * Do some cleaning and sorting on the observer list, keeps it nice and neat
     */
    protected function prepareObserverList()
    {
        // sort the events
        ksort($this->observers);

        // go through the defined events with their observers
        foreach ($this->observers as $event => $void) {

            // sorting the event position definition
            ksort($this->observers[$event]);

            // clean out anything that shouldn't be there
            if ( ! is_array($this->observers[$event]) || count($this->observers[$event]) == 0) {
                unset($this->observers[$event]);

            } else {

                // one more level of checking and cleaning and then we're done
                foreach ($this->observers[$event] as $order => $observers) {
                    if ( ! is_array($this->observers[$event][$order]) || count($this->observers[$event][$order]) == 0) {
                        unset($this->observers[$event][$order]);
                    }
                }
            }

        }
    }
}
