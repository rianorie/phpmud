<?php

namespace Game\Helper;

class Output
{
    private static $textLength = 70;

    /**
     * Get output in columns
     *
     * @param array    $data
     * @param int      $columns
     * @param bool     $left
     * @param bool|int $fixedWidth
     *
     * @return mixed
     */
    public function getColumnized(array $data, $columns = 4, $left = false, $fixedWidth = false)
    {
        $output = '';

        $maxlength = max(array_map('strlen', $data));

        $width = floor(self::$textLength / $columns) - 2;

        if ($maxlength > $width) {
            $width = $maxlength;
        }

        if ($fixedWidth !== false) {
            $width = $fixedWidth;
        }

        $part = '%' . $width . 's  ';
        if ($left) {
            $part = '%-' . $width . 's ';
        }

        for ($i = 0; $i < count($data); $i++) {

            if ($i != 0 && $i % $columns == 0) {
                $output .= "\n";
            }

            $output .= $part;
        }

        return call_user_func_array('sprintf', array_merge([$output], $data));
    }

    /**
     * Format a long piece of text into multiple lines
     *
     * @param string $string Input that needs to be formatted
     *
     * @return string
     */
    public function format(string $string) : string
    {
        return wordwrap($string, self::$textLength, "\n");
    }
}
