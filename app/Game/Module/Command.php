<?php

namespace Game\Module;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Interfaces\Command as CommandInterface;
use Game\Output;
use Game\Module\Entity\Interfaces\Manager as EntityManager;

abstract class Command implements CommandInterface
{
    /**
     * @var EntityManager
     */
    protected EntityManager $entityManager;

    /**
     * @var Input
     */
    protected Input $input;

    /**
     * @var Output
     */
    protected Output $output;

    /**
     * Command constructor.
     *
     * @param EntityManager $entityManager
     * @param Input         $input
     * @param Output        $output
     */
    public function __construct(EntityManager $entityManager, Input $input, Output $output)
    {
        $this->entityManager = $entityManager;
        $this->input = $input;
        $this->output = $output;
    }

    abstract public function execute(Connection $connection, string $input, string $command, string $raw);

    protected function getEntityManager() : EntityManager
    {
        return $this->entityManager;
    }

    protected function getInput() : Input
    {
        return $this->input;
    }

    protected function getOutput() : Output
    {
        return $this->output;
    }
}
