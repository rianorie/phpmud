<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module;

/**
 * Trait that implements a dynamic way to set and get elements of a class
 *
 * @package Game\Module
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-31
 */
trait DataTrait
{
	/**
	 * Storage for the key map
	 *
	 * @var array
	 */
	private $keycache;

	/**
	 * Overload a couple of methods
	 *
	 * @param string $method
	 * @param array $data
	 *
	 * @return bool|mixed|void
	 */
	public function __call($method, $data)
	{
		if (strpos($method, 'get') === 0) {
			$method = substr($method, 3);
			return $this->getData($this->underscoreKey($method));

		} elseif(strpos($method, 'set') === 0) {
			$method = substr($method, 3);
			return $this->setData($this->underscoreKey($method), (isset($data[0]) ? $data[0] : null));

		} elseif(strpos($method, 'uns') === 0) {
			$method = substr($method, 3);
			return $this->unsetData($this->underscoreKey($method));

		} elseif(strpos($method, 'has') === 0) {
			$method = substr($method, 3);
			return isset($this->data[$this->underscoreKey($method)]);
		}
	}

	/**
	 * Converts field names for setters and getters
	 *
	 * $this->setMyField($value) === $this->setData('my_field', $value)
	 * Uses cache to eliminate unnecessary preg_replace
	 *
	 * @param string $key
	 * @return string
	 */
	protected function underscoreKey($key)
	{
		if (isset($this->keycache[$key])) {
			return $this->keycache[$key];
		}

		$result = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $key));
		$this->keycache[$key] = $result;

		return $result;
	}

	/**
	 * Reformat a key into camelCase
	 *
	 * @param string $key The key to reformat
	 *
	 * @return string
	 */
	protected function camelCaseKey($key)
	{
		if (strpos($key, '_') !== false) {
			$key = str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
		}

		return $key;
	}
}