<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module\Entity\Adapter;
use Game\Module\Entity\Interfaces\Entity;
use Game\Module\Entity\Exception;

/**
 * Entity Manager Adapter interface. Defines the base requirements for the adapters in the entity manager
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-31
 * @version 1.0
 */
interface AdapterInterface
{
    /**
     * Load entity data from the system
     *
     * @param Entity     $entity The entity to hydrate
     * @param int|string $id     Value of the field of the data record to load
     * @param string     $field  Optionally define a different field to load the record by
     *
     * @return Entity
     *
     * @throws Exception
     */
	public function load(Entity $entity, int|string $id, $field = 'id') : Entity;

    /**
     * Push one or several fields to the database
     *
     * @param Entity       $entity The entity to store
     * @param array|string $field  The field(s) to update
     *
     * @return boolean
     *
     * @throws Exception
     */
	public function update(Entity $entity, array|string $field) : bool;

	/**
	 * Save an entire entity to the database, new or existing
	 *
	 * @param Entity $entity
	 *
	 * @return boolean
	 *
	 * @throws Exception
	 */
	public function save(Entity $entity) : bool;

	/**
	 * Remove an entity from the database
	 *
	 * @param Entity $entity
	 *
	 * @return boolean
	 *
	 * @throws Exception
	 */
	public function delete(Entity $entity) : bool;

	/**
	 * Find a number of entities by flexible arguments
	 *
	 * @param Entity $entity The entity to find
	 * @param array  $fields The optional fields that define the search
	 * @param int    $limit  The number of records to retrieve
	 * @param int    $offset The starting point
	 *
	 * @return Entity[]
	 */
	public function find(Entity $entity, array $fields = [], $limit = -1, $offset = 0) : bool|array|\Generator;

    /**
     * Count the number of entities by flexible arguments
     *
     * @param Entity $entity The entity to find
     * @param array  $fields The optional fields that define the search
     * @param int    $limit  The number of records to retrieve
     * @param int    $offset The starting point
     *
     * @return int
     */
    public function count(Entity $entity, array $fields = [], $limit = -1, $offset = 0) : int;
}
