<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module\Entity\Adapter;

use Game\Module\Entity\Interfaces\Entity as EntityInterface;
use Game\Module\Entity\Entity;
use Game\Module\Entity\Exception;

/**
 * PDO adapter for the entity manager
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-31
 * @version 1.0
 */
class Pdo implements AdapterInterface
{
    /**
     * @var \PDO
     */
    private \PDO $db;

    /**
     * Construct the PDO Adapter
     *
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Load entity data from the system
     *
     * @param EntityInterface $entity The entity to hydrate
     * @param int|string      $id     Value of the field of the data record to load
     * @param string          $field  Optionally define a different field to load the record by
     *
     * @return Entity
     *
     * @throws Exception
     */
    public function load(EntityInterface $entity, int|string $id, $field = 'id') : EntityInterface
    {
        try {

            $statement = $this->db->prepare("SELECT * FROM {$entity->getTable()} WHERE {$field}=:id");
            $statement->setFetchMode(\PDO::FETCH_INTO, $entity);
            $statement->execute([':id' => $id]);
            $statement->fetch();

            if ( ! $entity->getId()) {
                throw new Exception('Failed to load the entity.', 0);
            }

            return $entity;

        } catch (\Exception $e) {
            throw new Exception('Failed to load the entity.', 0, $e);
        }
    }

    /**
     * Push one or several fields to the database
     *
     * @param EntityInterface $entity The entity to store
     * @param array|string    $field  The field(s) to update
     *
     * @return boolean
     *
     * @throws Exception
     */
    public function update(EntityInterface $entity, array|string $field) : bool
    {
        if (is_null($entity->getId())) {
            throw new Exception('Updating only works on existing entities!');
        }

        try {

            // sanitize the input
            if (is_string($field)) {
                $field = [$field];
            }

            /** @noinspection SqlWithoutWhere */
            $sql = "UPDATE {$entity->getTable()} SET ";

            $placeholders = [':id' => $entity->getId()];
            foreach ($field as $key) {
                $placeholders[':' . $key] = $entity->$key;
                $sql .= sprintf('`%s`=:%s, ', $key, $key);
            }

            $sql = rtrim($sql, ', '); // trims off the last ", "

            $sql .= ' WHERE `id`=:id';

            $statement = $this->db->prepare($sql);

            return $statement->execute($placeholders);


        } catch (\Exception $e) {
            throw new Exception('Failed to update entity.', 0, $e);
        }
    }

    /**
     * Save an entire entity to the database, new or existing
     *
     * @param EntityInterface $entity
     *
     * @return boolean
     *
     * @throws Exception
     */
    public function save(EntityInterface $entity) : bool
    {
        // if the entity already exists, feed it to the update method instead
        if ( ! is_null($entity->getId())) {
            return $this->update($entity, $entity->getColumns());
        }

        try {
            $placeholders = $keys = $fields = [];

            foreach ($entity->getColumns() as $key) {
                if ( ! is_null($entity->$key)) {
                    $placeholders[':' . $key] = $entity->$key;
                    $keys[]                   = sprintf('`%s`', $key);
                    $fields[]                 = sprintf(':%s', $key);
                }
            }

            $keys   = implode(', ', $keys);
            $fields = implode(', ', $fields);

            $sql = "INSERT INTO {$entity->getTable()} ({$keys}) VALUES({$fields});";

            // clean up
            unset($fields, $keys);

            $statement = $this->db->prepare($sql);

            $result = $statement->execute($placeholders);

            if ( ! $result) {
                ob_start();
                var_dump($sql, $statement->errorInfo());
                file_put_contents('/vagrant/var/log/db.log', ob_get_clean(), FILE_APPEND);
            }

            $entity->id = $this->db->lastInsertId();

            return $result;

        } catch (\Exception $e) {
            throw new Exception('Failed to save entity.', 0, $e);
        }
    }

    /**
     * Remove an entity from the database
     *
     * @param EntityInterface $entity
     *
     * @return boolean
     *
     * @throws Exception
     */
    public function delete(EntityInterface $entity) : bool
    {
        if (is_null($entity->getId())) {
            throw new Exception('Deleting only works on existing entities!');
        }

        try {
            /** @noinspection SqlResolve */
            $statement = $this->db->prepare("DELETE FROM {$entity->getTable()} WHERE `id`=:id LIMIT 1");
            return $statement->execute([':id' => $entity->getId()]);

        } catch (\Exception $e) {
            throw new Exception('Failed to save entity.', 0, $e);
        }
    }

    /**
     * Find a number of entities by flexible arguments
     *
     * @param EntityInterface $entity The entity to find
     * @param array           $fields The optional fields that define the search
     * @param int             $limit  The number of records to retrieve
     * @param int             $offset The starting point
     *
     * @return bool|array|\Generator
     *
     * @throws Exception
     */
    public function find(EntityInterface $entity, array $fields = [], $limit = -1, $offset = 0) : bool|array|\Generator
    {
        try {

            $statement = $this->query($entity, $fields, $limit, $offset);

            while ($item = $statement->fetch()) {
                yield $item;
            }

            return false;

        } catch (\Exception $e) {
            throw new Exception('Failed to load the entity.', 0, $e);
        }
    }

    /**
     * Count the number of entities by flexible arguments
     *
     * @param EntityInterface $entity The entity to find
     * @param array           $fields The optional fields that define the search
     * @param int             $limit  The number of records to retrieve
     * @param int             $offset The starting point
     *
     * @return int
     *
     * @throws Exception
     */
    public function count(EntityInterface $entity, array $fields = [], $limit = -1, $offset = 0) : int
    {
        try {

            $statement = $this->query($entity, $fields, $limit, $offset);

            return $statement->rowCount();

        } catch (\Exception $e) {
            throw new Exception('Failed to count the entities.', 0, $e);
        }
    }

    /**
     * Count the number of entities by flexible arguments
     *
     * @param EntityInterface $entity The entity to find
     * @param array           $fields The optional fields that define the search
     * @param int             $limit  The number of records to retrieve
     * @param int             $offset The starting point
     *
     * @return bool|\PDOStatement
     *
     * @throws \Exception
     */
    public function query(EntityInterface $entity, array $fields = [], $limit = -1, $offset = 0) : bool|\PDOStatement
    {
        try {

            $sql = "SELECT * FROM {$entity->getTable()} WHERE 1";

            $placeholders = [];
            foreach ($fields as $key => $value) {
                if ( ! is_array($value) && ! is_null($value)) {
                    $placeholders[':' . $key] = $value;
                }

                if (is_array($value)) {

                    $type = strtolower(array_key_first($value));

                    switch ($type) {
                        case '>':
                            $sql                      .= ' AND `' . $key . '` > :' . $key;
                            $placeholders[':' . $key] = $value[$type];
                            break;

                        case '<':
                            $sql                      .= ' AND `' . $key . '` < :' . $key;
                            $placeholders[':' . $key] = $value[$type];
                            break;

                        case '>=':
                            $sql                      .= ' AND `' . $key . '` >= :' . $key;
                            $placeholders[':' . $key] = $value[$type];
                            break;

                        case '<=':
                            $sql                      .= ' AND `' . $key . '` <= :' . $key;
                            $placeholders[':' . $key] = $value[$type];
                            break;

                        case '!=':
                            $sql                      .= ' AND `' . $key . '` != :' . $key;
                            $placeholders[':' . $key] = $value[$type];
                            break;

                        case 'not like':
                            $sql                      .= ' AND `' . $key . '` NOT LIKE :' . $key;
                            $placeholders[':' . $key] = $value[$type];
                            break;

                        case 'not in':

                            $sql .= ' AND `' . $key . '` NOT IN (';

                            $i = 0;
                            foreach ($value[$type] as $loopval) {
                                $placeholders[':' . $key . $i] = $loopval;
                                $sql .= ':' . $key . $i . ', ';
                                $i++;
                            }
                            $sql = rtrim($sql, ', ');

                            $sql .= ')';

                            break;

                        case 'in':
                        default:

                            $sql .= ' AND `' . $key . '` IN (';

                            $i = 0;
                            foreach ($value[$type] as $loopval) {
                                $placeholders[':' . $key . $i] = $loopval;
                                $sql .= ':' . $key . $i . ', ';
                                $i++;
                            }
                            $sql = rtrim($sql, ', ');

                            $sql .= ')';

                            break;

                    }

                } else if (is_numeric($value)) {
                    $sql .= ' AND `' . $key . '`=:' . $key;

                } else if (is_null($value)) {
                    $sql .= ' AND `' . $key . '` IS NULL';

                } else {
                    $sql .= ' AND `' . $key . '` LIKE :' . $key;
                }
            }

            // LIMIT 0, 50  (start at 0 (offset), limit to 50)
            $sql .= sprintf(' LIMIT %d, %d', $offset, ($limit == -1 ? PHP_INT_MAX : $limit));

            $statement = $this->db->prepare($sql, [\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL]);
            $statement->setFetchMode(\PDO::FETCH_CLASS, get_class($entity), [$entity->getEntityManager()]);
            $statement->execute($placeholders);

            return $statement;

        } catch (\Exception $e) {
            throw new Exception('Failed to load the entities.', 0, $e);
        }
    }
}
