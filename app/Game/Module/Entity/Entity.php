<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module\Entity;

use JetBrains\PhpStorm\Pure;

/**
 * Entity base class
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
abstract class Entity implements Interfaces\Entity
{
	/**
	 * @var int
	 */
	public ?int $id = null;

	/**
	 * Storage for the extra data
	 *
	 * @var array
	 */
	protected array $data = [];

	/**
	 * @var Manager
	 */
	protected ?Interfaces\Manager $manager;

    /**
     * Construct the entity
     *
     * @param ?Interfaces\Manager $entityManager
     */
	public function __construct(?Interfaces\Manager $entityManager = null)
	{
		$this->manager = $entityManager;
	}

	/**
	 * Set the module entity manager
	 *
	 * @param Manager $entityManager
	 */
	public function setEntityManager(Interfaces\Manager $entityManager)
	{
		$this->manager = $entityManager;
	}

	/**
	 * Retrieve the entity manager
	 *
	 * @return ?Interfaces\Manager
	 */
	public function getEntityManager() : ?Interfaces\Manager
	{
		return $this->manager;
	}

    /**
     * Retrieve the entity id
     *
     * @return int|null
     */
	final public function getId() : ?int
    {
		return $this->id;
	}

	/**
	 * Retrieve the table name
	 *
	 * @return string
	 */
	abstract public function getTable() : string;

	/**
	 * Return an array of the entity database columns
	 *
	 * @return array
	 */
	abstract public function getColumns() : array;

	/**
	 * Retrieve data from the extra data set
	 *
	 * @param string|null $key
	 *
	 * @return mixed
	 */
	#[Pure] public function getData($key = null) : mixed
    {
		if ( ! is_null($key)) {
			return (isset($this->data[$key]) ? $this->data[$key]: null);
		} else {
			return $this->data;
		}
	}

    /**
     * Add data to the dataset
     *
     * @param string $key
     * @param mixed  $mixed
     *
     * @return $this
     */
	public function setData(string $key, mixed $mixed) : self
    {
		$this->data[$key] = $mixed;

		return $this;
	}

    /**
     * Remove data from the dataset
     *
     * @param string $key
     *
     * @return $this
     */
	public function unsetData(string $key) : self
	{
		unset($this->data[$key]);

		return $this;
	}
}
