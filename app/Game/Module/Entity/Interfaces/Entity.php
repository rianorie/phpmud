<?php

namespace Game\Module\Entity\Interfaces;

/**
 * Entity base definition
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
interface Entity
{
	/**
	 * Retrieve the database table for this entity
	 *
	 * @return string
	 */
	public function getTable() : string;

	/**
	 * Returns the database columns for this entity
	 *
	 * @return array
	 */
	public function getColumns() : array;

    /**
     * Retrieve the entity id
     *
     * @return int|null
     */
	public function getId() : ?int;

	/**
	 * Set the module entity manager
	 *
	 * @param Manager $entityManager
	 */
	public function setEntityManager(Manager $entityManager);

	/**
	 * Retrieve the entity manager
	 *
	 * @return Manager
	 */
	public function getEntityManager() : ?Manager;

	public function getData(string $key = null);

	public function setData(string $key, $mixed);

	public function unsetData(string $key);
}
