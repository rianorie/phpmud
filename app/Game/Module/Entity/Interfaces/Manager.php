<?php

namespace Game\Module\Entity\Interfaces;

use Game\Module\Entity\Adapter\AdapterInterface;

interface Manager extends AdapterInterface
{}
