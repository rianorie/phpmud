<?php

namespace Game\Module\Entity\Interfaces;

interface Table
{
    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition);

    /**
     * Return the full table
     *
     * @return array
     */
    public function all();
}
