<?php

namespace Game\Module\Entity\Interfaces;

interface Validator
{
    public function validate($input) : bool;
}
