<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module\Entity;

use Game\Module\Entity\Interfaces\Entity as EntityInterface;

/**
 * Entity manager, handles the persistence and loading of entities
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-31
 * @version 1.0
 */
class Manager implements Interfaces\Manager
{
	/**
	 * @var Adapter\AdapterInterface
	 */
	private Adapter\AdapterInterface $adapter;

	/**
	 * Construct the entity manager
	 *
	 * @param Adapter\AdapterInterface $adapter
	 */
	public function __construct(Adapter\AdapterInterface $adapter)
	{
		$this->adapter = $adapter;
	}

    /**
     * Hydrate entity with data from the system
     *
     * @param EntityInterface $entity The entity to hydrate
     * @param int|string      $id     Value of the field of the data record to load
     * @param string          $field  Optionally define a different field to load the record by
     *
     * @return Entity
     *
     * @throws Exception
     */
	public function load(EntityInterface $entity, int|string $id, $field = 'id') : EntityInterface
	{
		return $this->adapter->load($entity, $id, $field);
	}

    /**
     * Push one or several fields to the database
     *
     * @param EntityInterface $entity The entity to store
     * @param array|string    $field  The field(s) to update
     *
     * @return boolean
     *
     * @throws Exception
     */
	public function update(EntityInterface $entity, array|string $field) : bool
	{
		return $this->adapter->update($entity, $field);
	}

    /**
     * Save an entire entity to the database, new or existing
     *
     * @param EntityInterface $entity
     *
     * @return boolean
     *
     * @throws Exception
     */
	public function save(EntityInterface $entity) : bool
	{
		return $this->adapter->save($entity);
	}

    /**
     * Remove an entity from the database
     *
     * @param EntityInterface $entity
     *
     * @return boolean
     *
     * @throws Exception
     */
	public function delete(EntityInterface $entity) : bool
	{
		return $this->adapter->delete($entity);
	}

    /**
     * Find a number of entities by flexible arguments
     *
     * @param EntityInterface $entity The entity to find
     * @param array           $fields The optional fields that define the search
     * @param int             $limit  The number of records to retrieve
     * @param int             $offset The starting point
     *
     * @return bool|array|\Generator
     */
	public function find(EntityInterface $entity, array $fields = [], $limit = -1, $offset = 0) : bool|array|\Generator
	{
		return $this->adapter->find($entity, $fields, $limit, $offset);
	}

    /**
     * Count the number of entities by flexible arguments
     *
     * @param EntityInterface $entity The entity to find
     * @param array           $fields The optional fields that define the search
     * @param int             $limit  The number of records to retrieve
     * @param int             $offset The starting point
     *
     * @return int
     */
    public function count(EntityInterface $entity, array $fields = [], $limit = -1, $offset = 0) : int
    {
        return $this->adapter->count($entity, $fields, $limit, $offset);
    }

    public function query(EntityInterface $entity, array $fields = [], $limit = -1, $offset = 0)
    {
        return $this->adapter->query($entity, $fields, $limit, $offset);
    }
}
