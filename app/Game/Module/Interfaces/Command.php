<?php

namespace Game\Module\Interfaces;

use Game\Connection\Interfaces\Connection;

interface Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw);
}
