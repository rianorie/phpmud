<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module\Interfaces;

use \Game\Event,
    \Game\Input,
    \Game\Output,
    \Game\Config;
use Monolog\Logger;

/**
 * Module requirements
 *
 * @package Game\Structure
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-22
 */
interface Module
{
    /**
     * Initiate the module, setting up the basics
     *
     * @param array                       $data          The module data
     * @param \Game\Config                $config        The configuration data
     * @param \Game\Event                 $event         The event manager
     * @param \Game\Input                 $input         The input manager
     * @param \Game\Output                $output        The output manager
     * @param \Game\Module\Entity\Manager $entityManager The database handle
     * @param \Game\Module\Manager        $manager       The module manager
     * @param Logger                      $logger        The system logger
     *
     * @return mixed
     */
    public function __construct(
        array $data,
        Config $config,
        Event $event,
        Input $input,
        Output $output,
        \Game\Module\Entity\Manager $entityManager,
        \Game\Module\Manager $manager,
        Logger $logger
    );

    /**
     * And initialize the different elements, hooking events and registering commands
     *
     * @return void
     */
    public function init();

    /**
     * Retrieve module data
     *
     * @param string $key The optional data element to retrieve
     *
     * @return mixed
     */
    public function getData($key = null);
}
