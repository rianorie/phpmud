<?php

namespace Game\Module\Interfaces;

use stdClass;

interface Observer
{
    public function fire(stdClass $data);
}
