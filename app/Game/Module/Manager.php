<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module;

use Game\Config;
use Game\Event;
use Game\Input;
use Game\Output;
use Psr\Log\LoggerInterface;

/**
 * Module Manager, handles the loading, unloading and retrieval of modules
 *
 * @package Game\Module
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-30
 */
class Manager
{
	/**
	 * Loaded modules array
	 *
	 * @var Interfaces\Module[]
	 */
	private $modules = [];

	/**
	 * @var Config
	 */
	private $config;

	/**
	 * @var  Input
	 */
	private $input;

	/**
	 * @var Output
	 */
	private $output;

	/**
	 * @var Event
	 */
	private $event;

	/**
	 * @var Entity\Manager
	 */
	private $entity;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * Construct the module manager
	 *
	 * @param Config          $config
	 * @param Input           $input
	 * @param Output          $output
	 * @param Event           $event
	 * @param Entity\Manager  $entityManager
	 * @param LoggerInterface $logger
	 */
	public function __construct(Config $config, Input $input, Output $output, Event $event, Entity\Manager $entityManager, LoggerInterface $logger)
	{
		$this->config = $config;
		$this->input  = $input;
		$this->output = $output;
		$this->event  = $event;
		$this->entity = $entityManager;
		$this->logger = $logger;
	}

	/**
	 * Load all modules in the system
	 *
	 * @throws Exception
	 */
	public function loadAll()
	{
		foreach($this->getAvailableModules() as $module) {
			$this->loadModule($module);
		}
	}

	/**
	 * Load a module
	 *
	 * @param string $data
	 */
	public function loadModule($data)
	{
		// if we were fed the module name, load up all the module
		// data that goes with it
		if (is_string($data)) {
			$data = $this->getModuleData($data);
		}

		$this->logger->info('Loading module '.$data->name);

		// run the migrations for the module, if any
		$this->migrateModule($data);

		// construct the module class name
		$class = 'Modules\\'.$data->namespace.'\\Module';

		// construct the module
		$this->modules[$data->id] = new $class((array) $data, $this->config, $this->event, $this->input, $this->output, $this->entity, $this, $this->logger);

		$this->logger->info('Initializing module '.$data->name);

		// and finally, initialize is
		$this->modules[$data->id]->init();
	}

	/**
	 * Unload a module from the system
	 *
	 * @param string $module

	public function unloadModule($module)
	{

		$files = $this->modules[$module]->getFileList();
		$namespace = $this->modules[$module]->getData('namespace');

		foreach($files as $file) {
			if (substr($file, -4) == '.php') {

				// create the class definition based on the file path
				$parts = explode(DIRECTORY_SEPARATOR, str_replace('.php', '', $file));
				$class = $namespace.implode('\\', $parts);

				// unset the methods
				foreach(get_class_methods($class) as $method) {
					runkit_method_remove($class, $method);
				}
			}
		}
	}
	*/

	/**
	 * Return a list of the loaded modules
	 *
	 * @param bool|false $full Return the modules with their information, or just the module names
	 *
	 * @return Interfaces\Module[]
	 */
	public function getModules($full = false)
	{
		if ( ! $full) {
			return array_keys($this->modules);
		}

		return $this->modules;
	}

	/**
	 * Retrieve all the available modules
	 *
	 * @return array
	 *
	 * @throws Exception
	 */
	public function getAvailableModules()
	{
		if (is_null($this->config->get('modules.folder'))) {
			throw new Exception('Module folder needs to be defined in the config!');
		}

		// find the available module folders
		$moduleFolders = scandir(HOMEDIR.$this->config->get('modules.folder'));

		// get rid of the default "." and ".." paths
		unset($moduleFolders[0], $moduleFolders[1]);

		$data = [];
		foreach($moduleFolders as $module) {
			$data[] = $this->getModuleData($module);
		}

		return $data;
	}

	/**
	 * Retrieve the base definitions for a module
	 *
	 * @param string $module The module to retrieve data for
	 * @param bool $unloaded Force retrieving the unloaded information
	 *
	 * @return object
	 * @throws Exception
	 */
	public function getModuleData($module, $unloaded = false)
	{
		// if the module isn't set/loaded or we're forcing the retrieval of the unloaded module
		if ((count($this->modules) > 0 && isset($this->modules[$module])) || $unloaded) {
			return $this->modules[$module]->getData();
		}

		// construct the definition path
		$moduleBase = $this->getModulePath($module);

		// check that the definition exists
		if ( ! file_exists($moduleBase.'/module.json')) {
			throw new Exception('Module '.$module.' doesn\'t have a module.json file!');
		}

		// load the definition info
		$data = json_decode(file_get_contents($moduleBase.'/module.json'));

		// add the module folder as the id
		$data->id = $module;

		// make sure the basic requirements are there
		$requiredFields = ['name', 'version', 'namespace', 'description', 'dependencies'];
		foreach($requiredFields as $field) {
			if ( ! isset($data->{$field})) {
				throw new Exception('Module in directory '.$module.' is missing the '.$field.' in its definition!');
			}
		}

		// make sure there's a loader we can reach
		if ( ! file_exists($moduleBase.'/Module.php')) {
			throw new Exception('Module '.$data->name.' is missing it\'s base class.');
		}

		return $data;
	}

	/**
	 * Construct and return the module path
	 *
	 * @param string $module
	 *
	 * @return string
	 */
	public function getModulePath($module)
	{
		return HOMEDIR.$this->config->get('modules.folder').$module;
	}

	/**
	 * Run the module migrations
	 *
	 * @param object|string $data The module data
	 *
	 * @throws Exception
	 */
	private function migrateModule($data)
	{
		// if we were fed the module name, load up all the module
		// data that goes with it
		if (is_string($data)) {
			$data = $this->getModuleData($data);
		}

		// grab the migrations path
		$path = $this->getModulePath($data->id).'/migrations/';
		if (is_dir($path)) {

			// Construct a config array for the migration manager
			$config = ['paths' => ['migrations' => $path],
			           'environments' => [
				           'default_migration_table' => $this->config->get('database.migrationTable'),
				           'default_database' => 'production',
				           'production' => [
					           'adapter' => 'mysql',
					           'host' => $this->config->get('database.host'),
					           'name' => $this->config->get('database.name'),
					           'user' => $this->config->get('database.user'),
					           'pass' => $this->config->get('database.pass'),
					           'port' => $this->config->get('database.port'),
					           'charset' => $this->config->get('database.charset'),
				           ]
			           ]];

			$stream = fopen('php://temp', 'w+');

			// set up the migrations manager
			$migrationManager = new \Phinx\Migration\Manager(
				new \Phinx\Config\Config($config),
				new \Symfony\Component\Console\Output\StreamOutput($stream)
			);

			$this->logger->info('Executing migrations for '.trim($data->name));

			// and execute!
			$migrationManager->migrate('production');

			// Get the output of the command and close the stream, which will
			// destroy the temporary file.
			$result = stream_get_contents($stream, -1, 0);
			fclose($stream);

			$this->logger->info($result);
		}
	}
}
