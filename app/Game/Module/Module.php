<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game\Module;

use \Game\Event,
    \Game\Input,
    \Game\Output,
    \Game\Config;
use JetBrains\PhpStorm\Pure;
use Monolog\Logger;

/**
 * Base class for the module to extend from, provides all the basic features
 *
 * @package Game\Module
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-28
 */
abstract class Module implements Interfaces\Module
{
    /**
     * Data storage
     *
     * @var array
     */
    private array $data;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var Event
     */
    private Event $eventHandler;

    /**
     * @var Input
     */
    private Input $inputHandler;

    /**
     * @var Output
     */
    private Output $outputHandler;

    /**
     * @var Entity\Manager
     */
    private Entity\Manager $entityManager;

    /**
     * @var Manager
     */
    private Manager $manager;

    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * {@inheritdoc}
     *
     * @param Event $event
     * @param Input $input
     */
    public function __construct(
        array $data,
        Config $config,
        Event $event,
        Input $input,
        Output $output,
        Entity\Manager $entityManager,
        Manager $manager,
        Logger $logger
    )
    {
        $this->config        = $config;
        $this->data          = $data;
        $this->eventHandler  = $event;
        $this->inputHandler  = $input;
        $this->outputHandler = $output;
        $this->manager       = $manager;
        $this->entityManager = $entityManager;
        $this->logger        = $logger;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    abstract public function init();

    /**
     * Retrieve data from the module
     *
     * @param string $key The data key to retrieve
     *
     * @return mixed
     */
    #[Pure] public function getData($key = null) : mixed
    {
        if ( ! is_null($key)) {
            if (isset($this->data[$key])) {
                return $this->data[$key];
            } else {
                return null;
            }
        }

        return $this->data;
    }

    /**
     * Get the config handler
     *
     * @return Config
     */
    public function getConfig() : Config
    {
        return $this->config;
    }

    /**
     * Grab the event handler
     *
     * @return Event
     */
    public function getEvent() : Event
    {
        return $this->eventHandler;
    }

    /**
     * Grab the input handler
     *
     * @return Input
     */
    public function getInput() : Input
    {
        return $this->inputHandler;
    }

    /**
     * Grab the output handler
     *
     * @return Output
     */
    public function getOutput() : Output
    {
        return $this->outputHandler;
    }

    /**
     * Load the module manager
     *
     * @return Manager
     */
    public function getModuleManager() : Manager
    {
        return $this->manager;
    }

    /**
     * Grab the entity manager
     *
     * @return Entity\Manager
     */
    public function getEntityManager() : Entity\Manager
    {
        return $this->entityManager;
    }

    /**
     * Grab the logger
     *
     * @return Logger
     */
    public function getLogger() : Logger
    {
        return $this->logger;
    }
}
