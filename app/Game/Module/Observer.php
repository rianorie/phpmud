<?php

namespace Game\Module;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use stdClass;

abstract class Observer implements Interfaces\Observer
{
    /**
     * @var EntityManager
     */
    protected EntityManager $entityManager;

    /**
     * @var Input
     */
    protected Input $input;

    /**
     * @var Output
     */
    protected Output $output;

    /**
     * Command constructor.
     *
     * @param EntityManager $entityManager
     * @param Input         $input
     * @param Output        $output
     */
    public function __construct(EntityManager $entityManager, Input $input, Output $output)
    {
        $this->entityManager = $entityManager;
        $this->input = $input;
        $this->output = $output;
    }

    abstract public function fire(stdClass $data);

    protected function getEntityManager() : EntityManager
    {
        return $this->entityManager;
    }

    protected function getInput() : Input
    {
        return $this->input;
    }

    protected function getOutput() : Output
    {
        return $this->output;
    }
}
