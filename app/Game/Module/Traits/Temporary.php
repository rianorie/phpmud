<?php

namespace Game\Module\Traits;

use Game\Module\Exception;

trait Temporary
{
    private array $temporary = [];

    /**
     * Retrieve temporary data
     *
     * @param string|null $key
     *
     * @return mixed
     */
    public function getTemporary($key = null) : mixed
    {
        try {
            return $this->getTemporaryOrFail($key);

        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Retrieve temporary data or throw an exception when it's not available
     *
     * @param string|null $key
     *
     * @return mixed
     * @throws Exception
     */
    public function getTemporaryOrFail(?string $key = null) : mixed
    {
        if (is_null($key)) { return $this->temporary; }

        if (isset($this->temporary[$key])) {
            return $this->temporary[$key];
        }

        throw new Exception('Trying to access non-existent temporary data');
    }

    /**
     * Set temporary data
     *
     * @param string $key
     * @param mixed  $data
     * @param bool   $overwrite
     *
     * @return bool
     */
    public function setTemporary(string $key, mixed $data, bool $overwrite = true) : bool
    {
        if ($overwrite || ! isset($this->temporary[$key])) {
            $this->temporary[$key] = $data;
            return true;
        }

        return false;
    }
}
