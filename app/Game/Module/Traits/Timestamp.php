<?php

namespace Game\Module\Traits;

trait Timestamp
{
    /**
     * @var string
     */
    public string $created_at;

    /**
     * @var string
     */
    public ?string $updated_at;

    public function getCreatedAt() : string
    {
        return $this->created_at;
    }

    public function getUpdatedAt() : string
    {
        return $this->updated_at;
    }
}
