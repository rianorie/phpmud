<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game;

use \Game\Connection\Interfaces\Connection;

/**
 * Output handler
 *
 * @package Game
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-26
 */
class Output
{
	/**
	 * @var Event
	 */
	private Event $event;

	/**
	 * Construct the input class
	 *
	 * @param Event $event
	 */
	public function __construct(Event $event)
	{
		$this->event = $event;
	}

    /**
     * Write the output to the connection, doing some small handling beforehand
     *
     * @param Connection   $connection The connection to write to
     * @param array|string $data       The data to write
     * @param bool         $newline    Add an extra newline at the end of the output
     * @param bool         $force      Force writing the output to the connection
     *
     * @return bool
     */
	public function write(Connection $connection, array|string $data, $newline = false, $force = false) : bool
    {
		// we're expecting an array, sanitize the input so that it is.
		if ( ! is_array($data)) { $data = [$data]; }

		// before doing anything with the output, throw it into an observer
		$this->event->dispatch('output.before_process', ['connection' => $connection, 'data' => &$data]);

		// if the argument is empty, stop here. No point in writing empty data
		if (empty($data)) {
			return false;
		}

		// generate the output
		$output = call_user_func_array('sprintf', $data);

		// before doing anything with the output, throw it into an observer
		$this->event->dispatch('output.before_write', ['connection' => $connection, 'output' => &$output]);

		return ($connection->write($output.($newline ? "\r\n" : ""), $force) > 0);
	}
}
