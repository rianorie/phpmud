<?php
namespace Game;

class Registry
{
    private static $register = [];

    public static function init()
    {
        self::$register['start'] = time();
    }

    public static function set($key, $data)
    {
        self::$register[$key] = $data;
    }

    /**
     * @param $key
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public static function get($key)
    {
        if ( ! isset(self::$register[$key])) {
            throw new \Exception('No such key set in the registry');
        }

        return self::$register[$key];
    }

    public static function exists($key)
    {
        return isset(self::$register[$key]);
    }
}
