<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Game;

use Psr\Log\LoggerInterface;
use Socket;

/**
 * Game server class, fires up all the different elements and binds them together
 *
 * @package Game
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-20
 */
class Server
{
    const PULSE_MICROSECONDS = 250000; // 0.25 sec

    const PULSE_PER_SECOND   = 4;

    const PULSE_VIOLENCE     = 3 * self::PULSE_PER_SECOND;

    const PULSE_HOUR         = 3600 * self::PULSE_PER_SECOND;

    const PULSE_MINUTE       = 60 * self::PULSE_PER_SECOND;

    const PULSE_SLOW         = 120 * self::PULSE_PER_SECOND;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * Path to store the unix socket at
     *
     * @var string
     */
    private string $path;

    /**
     * @var Socket
     */
    private Socket $handle;

    /**
     * @var Event
     */
    private Event $event;

    /**
     * @var Input
     */
    private Input $input;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var Module\Manager
     */
    private Module\Manager $moduleManager;

    /**
     * @var Connection\Manager
     */
    private Connection\Manager $connectionManager;

    /**
     * Construct the server
     *
     * @param Config             $config
     * @param Event              $event
     * @param Input              $input
     * @param Connection\Manager $connectionManager
     * @param Module\Manager     $moduleManager
     * @param LoggerInterface    $logger
     */
    public function __construct(Config $config, Event $event, Input $input,
                                Connection\Manager $connectionManager, Module\Manager $moduleManager,
                                LoggerInterface $logger)
    {
        $this->config            = $config;
        $this->event             = $event;
        $this->input             = $input;
        $this->logger            = $logger;
        $this->connectionManager = $connectionManager;
        $this->moduleManager     = $moduleManager;
    }

    /**
     * Set the socket path
     *
     * @param string $path The path to store the unix socket at
     */
    public function setSocketPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * Run the server
     *
     * @throws Connection\Exception
     * @throws Module\Exception
     * @throws Exception
     */
    public function run()
    {
        // Load the game modules
        $this->moduleManager->loadAll();

        // fire event
        $this->event->dispatch('game.before_bind');

        // bind the socket
        $this->bind();

        // fire event
        $this->event->dispatch('game.after_bind');

        $tick = 0;

        // Loop continuously
        while (true) {

            // Setup clients listen socket for reading
            $socketRead = [$this->handle];

            $map = [];

            // Loop through the clients to push into the socket reader
            foreach ($this->connectionManager->getAll() as $connection) {
                $socketRead[]              = $connection->getSocket();
                $map[$connection->getId()] = $connection;
            }

            $write = $except = null;

            // tick every .25 seconds
            $this->event->dispatch('game.pulse', ['connections' => $this->connectionManager->getAll()]);
            $tick++;

            if ($tick > 0 && ($tick % self::PULSE_VIOLENCE == 0)) {
                $this->event->dispatch('game.pulse.violence', ['connections' => $this->connectionManager->getAll()]);
            }

            if ($tick > 0 && ($tick % self::PULSE_MINUTE == 0)) {
                $this->event->dispatch('game.pulse.tick', ['connections' => $this->connectionManager->getAll()]);
            }

            if ($tick > 0 && ($tick % self::PULSE_SLOW == 0)) {
                $this->event->dispatch('game.pulse.slow_tick', ['connections' => $this->connectionManager->getAll()]);
            }

            if ($tick > 0 && ($tick % self::PULSE_HOUR == 0)) {
                $this->event->dispatch('game.pulse.hour', ['connections' => $this->connectionManager->getAll()]);

                // tick is reset here because PULSE_HOUR is the longest running one
                $tick = 0;
            }

            // Set up a blocking call to socket_select()
            if (@socket_select($socketRead, $write, $except, 0) < 1) {
                usleep(self::PULSE_MICROSECONDS);
                continue;
            }


            $handleId = $this->getSocketResourceId($this->handle);
            $readSocketIds = [];
            foreach($socketRead as $readSocket) {
                $readSocketIds[] = $this->getSocketResourceId($readSocket);
            }

            // Create a new connection if requested
            if (in_array($handleId, $readSocketIds)) {

                $this->event->dispatch('game.before_connection');

                // instantiate a new connection
                $connection = $this->connectionManager->create(socket_accept($this->handle));


//				// since we're using a socket wrapper that will help us keep things in the air
//				// we need to be forwarded the connection details. If that fails, don't accept
//				// the connection. Since we're also getting a write/read action in before the
//				// regular interaction from the mud clients we can be certain this will be the
//				// expected json string.
//
//				// there is quirkiness about the reading and writing of the sockets, it seems
//				// that without this write and read action on the other end by large the json
//				// data wont get transferred properly.
//
//
//				// first grab the first three characters, these will always be the length of the
//				// json string
//				$read = '';
//				while (strlen($read) < 3 || intval($read) <= 0) {
//					$read .= $connection->read(3);
//					printf("READ: %s\n", $read);
//				}
//
//				$length = intval(substr($read, 0, 3));
//
//				// then grab the actual json string based on the previously given length
//				$read = $connection->read($length);

                $id = $this->getSocketResourceId($connection->getSocket());
                $connection->write($id);

                while ( ! file_exists(HOMEDIR . 'var/socketdata/' . $id . '.json')) {
                    usleep(5000);
                    $this->logger->alert('Socket data file not available yet.');
                }

                // wait for the socket data file to contain a full and proper json string
                $file = file_get_contents(HOMEDIR . 'var/socketdata/' . $id . '.json');
                $data = json_decode($file);

                // decode the json string and move on with the show as regular
                if ( ! $data) {
                    $this->logger->alert('Connection failed to provide proper json data on connect.');
                    $connection->close();
                } else {

                    $connection->setData('connected', time());
                    $connection->setData('address', $data->address);
                    $connection->setData('socket.external', $data->external);
                    $connection->setData('socket.internal', $data->internal);
                    $connection->setData('socket.new', $data->new);

                    // add the new connection to the manager
                    $this->connectionManager->add($connection);

                    $this->event->dispatch('game.after_connection', ['connection' => $connection]);
                }

                unlink(HOMEDIR . '/var/socketdata/' . $id . '.json');

                // remove the server from the read handle so that it wont end up in the foreach
                unset($socketRead[0]);
            }

            // If a client is trying to write - handle it now
            foreach ($socketRead as $socket) {
                /** @var Connection\Connection $connection */
                $connection = $map[$this->getSocketResourceId($socket)];

                $read = $connection->read();
                if ($read === false) {
                    $this->event->dispatch('game.dropped_connection', ['connection' => $connection]);
                } else {
                    try {
                        $read  = rtrim($read);
                        $lines = explode("\r\n", $read);

                        foreach ($lines as $line) {
                            $this->input->handle($connection, $line);
                        }

                    } catch (\Exception $e) {
                        $this->logger->critical($e->getMessage());
                    }
                }
            }

        } // end infinite while
    }

    /**
     * Retrieve the system id for a socket resource
     *
     * @param Socket $socket
     *
     * @return int
     */
    private function getSocketResourceId(Socket $socket) : int
    {
        return spl_object_id($socket);
    }

    /**
     * Bind to a socket on the server
     *
     * @return void
     *
     * @throws Exception
     */
    private function bind()
    {
        if (is_null($this->path)) {
            throw new Exception('Socket path is empty!');
        }

        // set up the TCP socket
        $this->handle = socket_create(AF_UNIX, SOCK_STREAM, 0);

        if ($this->handle == false) {
            throw new Exception('Failed to create socket! Error: ' . socket_strerror(socket_last_error()));
        }

        socket_set_option($this->handle, SOL_SOCKET, SO_REUSEPORT, 1);

        $this->logger->info('Socket created');

        @unlink($this->path);
        socket_bind($this->handle, $this->path);
        $this->logger->info('Socket bound to ' . $this->path);

        socket_listen($this->handle);
//        socket_set_nonblock($this->handle);
    }

    public function __destruct()
    {
        printf("WE CRASHED!\n");
    }
}
