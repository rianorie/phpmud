<?php

namespace Modules\Boards\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use JetBrains\PhpStorm\Pure;
use Modules\Boards\Entity\Board as BoardEntity;
use Modules\Boards\Helper\Permissions;
use Modules\World\Character;
use Modules\World\Entity\Character as CharacterEntity;

class Board extends Command
{
    protected Permissions $permissionsHelper;

    #[Pure] public function __construct(EntityManager $entityManager, Input $input, Output $output, Permissions $permissionsHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->permissionsHelper = $permissionsHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        if (empty(trim($input))) {
            $this->showBoards($connection);
            return;
        }

        $this->switchBoards($connection, $input);
    }

    protected function showBoards(Connection $connection)
    {
        /** @var CharacterEntity $character */
        $character = $connection->getData('character');

        $output = "{RNum          Name Unread Description\n".
                  "{r==== ============ ====== ==========={x\n";

        /** @var BoardEntity[] $boardsCollection */
        $boardsCollection = $this->getEntityManager()->find(new BoardEntity());
        foreach ($boardsCollection as $board) {
            if ($board) {
                $output .= sprintf(
                    "{W[{w%2d{W]{y %12s {W[%s%4d{W]{y %s\n",
                    $board->getId(),
                    $board->getLabel(),
                    (rand(0, 1) ? '{w' : '{R'),
                    0,
                    $board->getDescription()
                );
            }
        }

        $output .= "{x\n";

        $currentBoard = $this->getCurrentBoard($character);
        $output .= sprintf(
            "Your current board is %s.\n%s\n",
            $currentBoard->getLabel(),
            $this->getBoardPermissions($currentBoard, $character)
        );

        $this->getOutput()->write($connection, $output);
    }

    protected function switchBoards(Connection $connection, string $input)
    {
        /** @var CharacterEntity $character */
        $character = $connection->getData('character');

        if (is_numeric($input)) {

            try {
                /** @var BoardEntity $board */
                $board = $this->getEntityManager()->load(new BoardEntity(), $input);

                $character->setTemporary('current_board', $board->getId());

                $this->getOutput()->write($connection, sprintf(
                    "Current board changed to %s. %s",
                    $board->getLabel(),
                    $this->getBoardPermissions($board, $character)
                ));
                return;

            } catch (Exception $e) {
                // nothing to do here
            }

        } else {
            try {

                /** @var BoardEntity $board */
                $board = $this->getEntityManager()->find(new BoardEntity(), ['keyword' => ['like' => $input.'%']], 1);
                if ($board) {

                    $character->setTemporary('current_board', $board->getId());

                    $this->getOutput()->write($connection, sprintf(
                        "Current board changed to %s. %s",
                        $board->getLabel(),
                        $this->getBoardPermissions($board, $character)
                    ));
                    return;
                }

            } catch (Exception $e) {
                // nothing to do here
            }
        }

        $this->getOutput()->write($connection, 'No such board.');
    }

    /**
     * Get a textual representation of someones board rights
     *
     * @param BoardEntity     $board
     * @param CharacterEntity $character
     *
     * @return string
     */
    #[Pure] protected function getBoardPermissions(BoardEntity $board, CharacterEntity $character) : string
    {
        $canRead  = $this->permissionsHelper->canRead($character, $board);
        $canWrite = $this->permissionsHelper->canWrite($character, $board);

        if ($canRead && $canWrite) {
            return 'You can both read and write here.';

        } else if ($canWrite) {
            return 'you can only write here.';

        } else {
            return 'You can only read here.';
        }
    }

    /**
     * Retrieve the current board the character is on
     *
     * @param CharacterEntity $character
     *
     * @return BoardEntity
     */
    protected function getCurrentBoard(CharacterEntity $character) : BoardEntity
    {
        $currentBoard = $character->getTemporary('current_board');

        $firstBoard = null;

        /** @var BoardEntity[] $boardsCollection */
        $boardsCollection = $this->getEntityManager()->find(new BoardEntity());
        foreach($boardsCollection as $board) {

            if (is_null($firstBoard)) {
                $firstBoard = $board;
            }

            if ($board && (! $currentBoard || $board->getId() == $currentBoard)) {
                return $board;
            }
        }

        return $firstBoard;
    }
}
