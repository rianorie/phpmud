<?php

namespace Modules\Boards\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;

class Note extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        # no argument -> read current unread, if none, forward to next board

        # "list" -> list current notes on this board
        # "read %d" -> Read the note with that id
        # "write" -> Write a note on the current board
    }
}
