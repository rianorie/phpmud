<?php

namespace Modules\Boards\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * Item entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class Board extends Entity
{
    use Timestamp;

    public string $label;

    public string $description;

    public string $keyword;

    public string $read;

    public string $write;

    public function getTable() : string
    {
        return 'boards';
    }

    public function getColumns() : array
    {
        return [
            'id', 'label', 'description',
            'keyword', 'read', 'write',
            'created_at', 'updated_at'
        ];
    }

    public function getLabel() : string
    {
        return $this->label;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getKeyword() : string
    {
        return $this->keyword;
    }

    public function getRead() : string
    {
        return $this->read;
    }

    public function getWrite() : string
    {
        return $this->write;
    }
}
