<?php

namespace Modules\Boards\Helper;

use Modules\Boards\Entity\Board;
use Modules\World\Entity\Character;

class Permissions
{
    public function canRead(Character $character, Board $board) : bool
    {
        return true;
    }

    public function canWrite(Character $character, Board $board) : bool
    {
        return true;
    }
}
