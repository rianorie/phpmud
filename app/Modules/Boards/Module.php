<?php

namespace Modules\Boards;

class Module extends \Game\Module\Module
{
    public function init()
    {
        $permissionHelper = new Helper\Permissions();

        $this->getInput()->registerCommand(
            'board',
            new Command\Board($this->getEntityManager(), $this->getInput(), $this->getOutput(), $permissionHelper)
        );
    }
}
