<?php

use Phinx\Migration\AbstractMigration;

class Boards010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('boards');

        $table
            ->addColumn('label', 'string')
            ->addColumn('description', 'string')
            ->addColumn('keyword', 'string')
            ->addColumn('read', 'string')
            ->addColumn('write', 'string')
            ->addTimestamps()
            ->addIndex('label', ['unique' => true])
            ->addIndex('keyword', ['unique' => true])
            ->save();

        $table = $this->table('board_notes');

        $table
            ->addColumn('board_id', 'integer')
            ->addColumn('to', 'string')
            ->addColumn('from', 'integer')
            ->addColumn('subject', 'string')
            ->addColumn('body', 'text')
            ->addColumn('expire', 'datetime')
            ->addTimestamps()
            ->save();
    }
}
