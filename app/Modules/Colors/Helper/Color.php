<?php

namespace Modules\Colors\Helper;

class Color
{
    /**
     * Determine the length of a string without the colors in it
     *
     * @param string $text
     *
     * @return int
     */
    public function strlen(string $text) : int
    {
        $colors = substr_count($text, '{');

        $xterm  = (substr_count($text, '{+') * 5); // xterm colors are 5 positions long
        $ansi   = ($colors - $xterm) * 2; // ansi colors are 2 positions long

        return strlen($text) - $xterm - $ansi;
    }

    /**
     * Return the string cleaned from any color characters
     *
     * @param string $text
     *
     * @return string
     */
    public function strip(string $text) : string
    {
        $clean = '';
        $chars = str_split($text);

        $skip = 0;
        for($i = 0; $i < count($chars); $i++) {
            if ($chars[$i] == '{' && $chars[$i+1] != '+') {
                $skip = 2;
            } elseif ($chars[$i+1] == '+') {
                $skip = 5;
            }
            if ($skip > 0) {
                $skip--;
            } else {
                $clean .= $chars[$i];
            }
        }

        return $clean;
    }
}
