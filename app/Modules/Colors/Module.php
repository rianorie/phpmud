<?php

namespace Modules\Colors;

use Game\Exception;
use Modules\Colors\Observer\AddColorsToOutput;
use Modules\Toggle\Utility as ToggleUtility;

class Module extends \Game\Module\Module
{
    /**
     * Set up the colors module
     *
     * @throws Exception
     */
    public function init()
    {
        ToggleUtility::registerToggle('Colors', 'General', 'Colors', true);
        ToggleUtility::registerToggle('256Colors', 'General', 'Xterm colors', true);

        $this->getEvent()->observe(
            'output.before_write',
            new AddColorsToOutput($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            1000
        );
    }
}
