<?php

namespace Modules\Colors\Observer;

use Game\Module\Observer;
use Modules\Toggle\Utility as ToggleUtility;
use stdClass;

class AddColorsToOutput extends Observer
{
    protected $ansi;

    protected $xterm;

    public function fire(stdClass $data)
    {
        $this->constructColors();

        $character = $data->connection->getData('character');

        // define the color replacements
        $colorKeys = array_keys($this->ansi);
        $colorKeys = array_merge($colorKeys, array_keys($this->xterm));

        // if color is enabled, replace the color codes with the actual color
        if ( ! $character || ToggleUtility::getToggle($character, 'colors')) {

            // Grab the ansi color values
            $colorValues = array_values($this->ansi);

            // if 256 is enabled, use the xterm color id
            if ( ! $character || ToggleUtility::getToggle($character, '256colors')) {
                $colorValues = array_merge($colorValues, array_column($this->xterm, 0));

                // if 256 is disabled, use the fallback color
            } else {
                $colorValues = array_merge($colorValues, array_column($this->xterm, 1));
            }

            // do the actual replacement. $output is loaded by reference so updating it
            // here will update it outside of the function too
            $data->output = str_replace($colorKeys, $colorValues, $data->output);

            // if color is not enabled, replace the color codes with nothing
        } else {
            $data->output = str_replace($colorKeys, '', $data->output);
        }
    }

    protected function constructColors()
    {
        $this->ansi = [
            '{b' => chr(27) . '[0;34m',
            '{B' => chr(27) . '[1;34m',
            '{c' => chr(27) . '[0;36m',
            '{C' => chr(27) . '[1;36m',
            '{D' => chr(27) . '[1;30m',
            '{g' => chr(27) . '[0;32m',
            '{G' => chr(27) . '[1;32m',
            '{m' => chr(27) . '[0;35m',
            '{M' => chr(27) . '[1;35m',
            '{r' => chr(27) . '[0;31m',
            '{R' => chr(27) . '[1;31m',
            '{w' => chr(27) . '[0;37m',
            '{W' => chr(27) . '[1;37m',
            '{x' => chr(27) . '[0;36m',
            '{y' => chr(27) . '[0;33m',
            '{Y' => chr(27) . '[1;33m'
        ];

        $this->xterm = [
            "{+000" => [chr(27) . '[38;5;000m', chr(27) . '[1;30m'], /* Black */
            "{+001" => [chr(27) . '[38;5;001m', chr(27) . '[0;31m'], /* Red */
            "{+002" => [chr(27) . '[38;5;002m', chr(27) . '[0;32m'], /* Green */
            "{+003" => [chr(27) . '[38;5;003m', chr(27) . '[0;33m'], /* Yellow */
            "{+004" => [chr(27) . '[38;5;004m', chr(27) . '[0;34m'], /* Blue */
            "{+005" => [chr(27) . '[38;5;005m', chr(27) . '[0;35m'], /* Magenta */
            "{+006" => [chr(27) . '[38;5;006m', chr(27) . '[0;36m'], /* Cyan */
            "{+007" => [chr(27) . '[38;5;007m', chr(27) . '[0;37m'], /* White */
            "{+008" => [chr(27) . '[38;5;008m', chr(27) . '[1;30m'], /* Bold dark grey */
            "{+009" => [chr(27) . '[38;5;009m', chr(27) . '[1;31m'], /* Bold bright red */
            "{+010" => [chr(27) . '[38;5;010m', chr(27) . '[1;32m'], /* Bold bright green */
            "{+011" => [chr(27) . '[38;5;011m', chr(27) . '[1;33m'], /* Bold bright yellow */
            "{+012" => [chr(27) . '[38;5;012m', chr(27) . '[1;34m'], /* bold bright blue */
            "{+013" => [chr(27) . '[38;5;013m', chr(27) . '[1;35m'], /* Bold bright magento */
            "{+014" => [chr(27) . '[38;5;014m', chr(27) . '[1;36m'], /* Bold bright cyan */
            "{+015" => [chr(27) . '[38;5;015m', chr(27) . '[1;37m'], /* Bold bright white */
            "{+016" => [chr(27) . '[38;5;016m', chr(27) . '[1;30m'], /* Grey0 */
            "{+017" => [chr(27) . '[38;5;017m', chr(27) . '[0;34m'], /* NavyBlue */
            "{+018" => [chr(27) . '[38;5;018m', chr(27) . '[0;34m'], /* DarkBlue */
            "{+019" => [chr(27) . '[38;5;019m', chr(27) . '[0;34m'], /* Blue3 */
            "{+020" => [chr(27) . '[38;5;020m', chr(27) . '[1;34m'], /* Blue3 */
            "{+021" => [chr(27) . '[38;5;021m', chr(27) . '[1;34m'], /* Blue1 */
            "{+022" => [chr(27) . '[38;5;022m', chr(27) . '[0;32m'], /* DarkGreen */
            "{+023" => [chr(27) . '[38;5;023m', chr(27) . '[0;32m'], /* DeepSkyBlue4 */
            "{+024" => [chr(27) . '[38;5;024m', chr(27) . '[0;34m'], /* DeepSkyBlue4 */
            "{+025" => [chr(27) . '[38;5;025m', chr(27) . '[1;34m'], /* DeepSkyBlue4 */
            "{+026" => [chr(27) . '[38;5;026m', chr(27) . '[1;34m'], /* DodgerBlue3 */
            "{+027" => [chr(27) . '[38;5;027m', chr(27) . '[1;34m'], /* DodgerBlue2 */
            "{+028" => [chr(27) . '[38;5;028m', chr(27) . '[0;32m'], /* Green4 */
            "{+029" => [chr(27) . '[38;5;029m', chr(27) . '[0;32m'], /* SpringGreen4 */
            "{+030" => [chr(27) . '[38;5;030m', chr(27) . '[0;36m'], /* Turquoise4 */
            "{+031" => [chr(27) . '[38;5;031m', chr(27) . '[0;36m'], /* DeepSkyBlue3 */
            "{+032" => [chr(27) . '[38;5;032m', chr(27) . '[1;34m'], /* DeepSkyBlue3 */
            "{+033" => [chr(27) . '[38;5;033m', chr(27) . '[1;34m'], /* DodgerBlue1 */
            "{+034" => [chr(27) . '[38;5;034m', chr(27) . '[0;32m'], /* Green3 */
            "{+035" => [chr(27) . '[38;5;035m', chr(27) . '[0;32m'], /* SpringGreen3 */
            "{+036" => [chr(27) . '[38;5;036m', chr(27) . '[0;32m'], /* DarkCyan */
            "{+037" => [chr(27) . '[38;5;037m', chr(27) . '[0;36m'], /* LightSeaGreen */
            "{+038" => [chr(27) . '[38;5;038m', chr(27) . '[0;36m'], /* DeepSkyBlue2 */
            "{+039" => [chr(27) . '[38;5;039m', chr(27) . '[0;36m'], /* DeepSkyBlue1 */
            "{+040" => [chr(27) . '[38;5;040m', chr(27) . '[0;32m'], /* Green3 */
            "{+041" => [chr(27) . '[38;5;041m', chr(27) . '[0;32m'], /* SpringGreen3 */
            "{+042" => [chr(27) . '[38;5;042m', chr(27) . '[0;32m'], /* SpringGreen2 */
            "{+043" => [chr(27) . '[38;5;043m', chr(27) . '[1;36m'], /* Cyan3 */
            "{+044" => [chr(27) . '[38;5;044m', chr(27) . '[1;36m'], /* DarkTurquoise */
            "{+045" => [chr(27) . '[38;5;045m', chr(27) . '[1;36m'], /* Turquoise2 */
            "{+046" => [chr(27) . '[38;5;046m', chr(27) . '[1;32m'], /* Green1 */
            "{+047" => [chr(27) . '[38;5;047m', chr(27) . '[1;32m'], /* SpringGreen2 */
            "{+048" => [chr(27) . '[38;5;048m', chr(27) . '[1;32m'], /* SpringGreen1 */
            "{+049" => [chr(27) . '[38;5;049m', chr(27) . '[1;32m'], /* MediumSpringGreen */
            "{+050" => [chr(27) . '[38;5;050m', chr(27) . '[1;36m'], /* Cyan2 */
            "{+051" => [chr(27) . '[38;5;051m', chr(27) . '[1;36m'], /* Cyan1 */
            "{+052" => [chr(27) . '[38;5;052m', chr(27) . '[0;31m'], /* DarkRed */
            "{+053" => [chr(27) . '[38;5;053m', chr(27) . '[0;35m'], /* DeepPink4 */
            "{+054" => [chr(27) . '[38;5;054m', chr(27) . '[0;35m'], /* Purple4 */
            "{+055" => [chr(27) . '[38;5;055m', chr(27) . '[0;34m'], /* Purple4 */
            "{+056" => [chr(27) . '[38;5;056m', chr(27) . '[0;34m'], /* Purple3 */
            "{+057" => [chr(27) . '[38;5;057m', chr(27) . '[1;34m'], /* BlueViolet */
            "{+058" => [chr(27) . '[38;5;058m', chr(27) . '[0;33m'], /* Orange4 */
            "{+059" => [chr(27) . '[38;5;059m', chr(27) . '[1;30m'], /* Grey37 */
            "{+060" => [chr(27) . '[38;5;060m', chr(27) . '[0;34m'], /* MediumPurple4 */
            "{+061" => [chr(27) . '[38;5;061m', chr(27) . '[0;34m'], /* SlateBlue3 */
            "{+062" => [chr(27) . '[38;5;062m', chr(27) . '[0;34m'], /* SlateBlue3 */
            "{+063" => [chr(27) . '[38;5;063m', chr(27) . '[1;34m'], /* RoyalBlue1 */
            "{+064" => [chr(27) . '[38;5;064m', chr(27) . '[0;33m'], /* Chartreuse4 */
            "{+065" => [chr(27) . '[38;5;065m', chr(27) . '[0;32m'], /* DarkSeaGreen4 */
            "{+066" => [chr(27) . '[38;5;066m', chr(27) . '[0;36m'], /* PaleTurquoise4 */
            "{+067" => [chr(27) . '[38;5;067m', chr(27) . '[0;36m'], /* SteelBlue */
            "{+068" => [chr(27) . '[38;5;068m', chr(27) . '[1;34m'], /* SteelBlue3 */
            "{+069" => [chr(27) . '[38;5;069m', chr(27) . '[1;34m'], /* CornflowerBlue */
            "{+070" => [chr(27) . '[38;5;070m', chr(27) . '[0;32m'], /* Chartreuse3 */
            "{+071" => [chr(27) . '[38;5;071m', chr(27) . '[0;32m'], /* DarkSeaGreen4 */
            "{+072" => [chr(27) . '[38;5;072m', chr(27) . '[0;32m'], /* CadetBlue */
            "{+073" => [chr(27) . '[38;5;073m', chr(27) . '[0;36m'], /* CadetBlue */
            "{+074" => [chr(27) . '[38;5;074m', chr(27) . '[0;36m'], /* SkyBlue3 */
            "{+075" => [chr(27) . '[38;5;075m', chr(27) . '[0;36m'], /* SteelBlue1 */
            "{+076" => [chr(27) . '[38;5;076m', chr(27) . '[0;32m'], /* Chartreuse3 */
            "{+077" => [chr(27) . '[38;5;077m', chr(27) . '[0;32m'], /* PaleGreen3 */
            "{+078" => [chr(27) . '[38;5;078m', chr(27) . '[0;32m'], /* SeaGreen3 */
            "{+079" => [chr(27) . '[38;5;079m', chr(27) . '[1;36m'], /* Aquamarine3 */
            "{+080" => [chr(27) . '[38;5;080m', chr(27) . '[1;36m'], /* MediumTurquoise */
            "{+081" => [chr(27) . '[38;5;081m', chr(27) . '[1;36m'], /* SteelBlue1 */
            "{+082" => [chr(27) . '[38;5;082m', chr(27) . '[0;32m'], /* Chartreuse2 */
            "{+083" => [chr(27) . '[38;5;083m', chr(27) . '[1;32m'], /* SeaGreen2 */
            "{+084" => [chr(27) . '[38;5;084m', chr(27) . '[1;32m'], /* SeaGreen1 */
            "{+085" => [chr(27) . '[38;5;085m', chr(27) . '[1;32m'], /* SeaGreen1 */
            "{+086" => [chr(27) . '[38;5;086m', chr(27) . '[1;36m'], /* Aquamarine1 */
            "{+087" => [chr(27) . '[38;5;087m', chr(27) . '[1;36m'], /* DarkSlateGray2 */
            "{+088" => [chr(27) . '[38;5;088m', chr(27) . '[0;31m'], /* DarkRed */
            "{+089" => [chr(27) . '[38;5;089m', chr(27) . '[0;35m'], /* DeepPink4 */
            "{+090" => [chr(27) . '[38;5;090m', chr(27) . '[0;35m'], /* DarkMagenta */
            "{+091" => [chr(27) . '[38;5;091m', chr(27) . '[0;35m'], /* DarkMagenta */
            "{+092" => [chr(27) . '[38;5;092m', chr(27) . '[0;35m'], /* DarkViolet */
            "{+093" => [chr(27) . '[38;5;093m', chr(27) . '[0;34m'], /* Purple */
            "{+094" => [chr(27) . '[38;5;094m', chr(27) . '[0;33m'], /* Orange4 */
            "{+095" => [chr(27) . '[38;5;095m', chr(27) . '[0;33m'], /* LightPink4 */
            "{+096" => [chr(27) . '[38;5;096m', chr(27) . '[0;35m'], /* Plum4 */
            "{+097" => [chr(27) . '[38;5;097m', chr(27) . '[0;35m'], /* MediumPurple3 */
            "{+098" => [chr(27) . '[38;5;098m', chr(27) . '[0;35m'], /* MediumPurple3 */
            "{+099" => [chr(27) . '[38;5;099m', chr(27) . '[1;34m'], /* SlateBlue1 */
            "{+100" => [chr(27) . '[38;5;100m', chr(27) . '[0;33m'], /* Yellow4 */
            "{+101" => [chr(27) . '[38;5;101m', chr(27) . '[0;33m'], /* Wheat4 */
            "{+102" => [chr(27) . '[38;5;102m', chr(27) . '[1;30m'], /* Grey53 */
            "{+103" => [chr(27) . '[38;5;103m', chr(27) . '[1;30m'], /* LightSlateGrey */
            "{+104" => [chr(27) . '[38;5;104m', chr(27) . '[0;35m'], /* MediumPurple */
            "{+105" => [chr(27) . '[38;5;105m', chr(27) . '[0;34m'], /* LightSlateBlue */
            "{+106" => [chr(27) . '[38;5;106m', chr(27) . '[0;33m'], /* Yellow4 */
            "{+107" => [chr(27) . '[38;5;107m', chr(27) . '[0;33m'], /* DarkOliveGreen3 */
            "{+108" => [chr(27) . '[38;5;108m', chr(27) . '[0;33m'], /* DarkSeaGreen */
            "{+109" => [chr(27) . '[38;5;109m', chr(27) . '[0;36m'], /* LightSkyBlue3 */
            "{+110" => [chr(27) . '[38;5;110m', chr(27) . '[0;36m'], /* LightSkyBlue3 */
            "{+111" => [chr(27) . '[38;5;111m', chr(27) . '[0;36m'], /* SkyBlue2 */
            "{+112" => [chr(27) . '[38;5;112m', chr(27) . '[0;32m'], /* Chartreuse2 */
            "{+113" => [chr(27) . '[38;5;113m', chr(27) . '[0;32m'], /* DarkOliveGreen3 */
            "{+114" => [chr(27) . '[38;5;114m', chr(27) . '[0;32m'], /* PaleGreen3 */
            "{+115" => [chr(27) . '[38;5;115m', chr(27) . '[0;36m'], /* DarkSeaGreen3 */
            "{+116" => [chr(27) . '[38;5;116m', chr(27) . '[1;36m'], /* DarkSlateGray3 */
            "{+117" => [chr(27) . '[38;5;117m', chr(27) . '[1;36m'], /* SkyBlue1 */
            "{+118" => [chr(27) . '[38;5;118m', chr(27) . '[1;32m'], /* Chartreuse1 */
            "{+119" => [chr(27) . '[38;5;119m', chr(27) . '[1;32m'], /* LightGreen */
            "{+120" => [chr(27) . '[38;5;120m', chr(27) . '[1;32m'], /* LightGreen */
            "{+121" => [chr(27) . '[38;5;121m', chr(27) . '[1;32m'], /* PaleGreen1 */
            "{+122" => [chr(27) . '[38;5;122m', chr(27) . '[1;36m'], /* Aquamarine1 */
            "{+123" => [chr(27) . '[38;5;123m', chr(27) . '[1;36m'], /* DarkSlateGray1 */
            "{+124" => [chr(27) . '[38;5;124m', chr(27) . '[0;31m'], /* Red3 */
            "{+125" => [chr(27) . '[38;5;125m', chr(27) . '[0;31m'], /* DeepPink4 */
            "{+126" => [chr(27) . '[38;5;126m', chr(27) . '[0;31m'], /* MediumVioletRed */
            "{+127" => [chr(27) . '[38;5;127m', chr(27) . '[0;35m'], /* Magenta3 */
            "{+128" => [chr(27) . '[38;5;128m', chr(27) . '[0;35m'], /* DarkViolet */
            "{+129" => [chr(27) . '[38;5;129m', chr(27) . '[0;35m'], /* Purple */
            "{+130" => [chr(27) . '[38;5;130m', chr(27) . '[0;31m'], /* DarkOrange3 */
            "{+131" => [chr(27) . '[38;5;131m', chr(27) . '[0;31m'], /* IndianRed */
            "{+132" => [chr(27) . '[38;5;132m', chr(27) . '[0;31m'], /* HotPink3 */
            "{+133" => [chr(27) . '[38;5;133m', chr(27) . '[0;35m'], /* MediumOrchid3 */
            "{+134" => [chr(27) . '[38;5;134m', chr(27) . '[0;35m'], /* MediumOrchid */
            "{+135" => [chr(27) . '[38;5;135m', chr(27) . '[0;35m'], /* MediumPurple2 */
            "{+136" => [chr(27) . '[38;5;136m', chr(27) . '[0;33m'], /* DarkGoldenrod */
            "{+137" => [chr(27) . '[38;5;137m', chr(27) . '[0;33m'], /* LightSalmon3 */
            "{+138" => [chr(27) . '[38;5;138m', chr(27) . '[0;33m'], /* RosyBrown */
            "{+139" => [chr(27) . '[38;5;139m', chr(27) . '[0;35m'], /* Grey63 */
            "{+140" => [chr(27) . '[38;5;140m', chr(27) . '[0;35m'], /* MediumPurple2 */
            "{+141" => [chr(27) . '[38;5;141m', chr(27) . '[0;35m'], /* MediumPurple1 */
            "{+142" => [chr(27) . '[38;5;142m', chr(27) . '[0;33m'], /* Gold3 */
            "{+143" => [chr(27) . '[38;5;143m', chr(27) . '[0;33m'], /* DarkKhaki */
            "{+144" => [chr(27) . '[38;5;144m', chr(27) . '[0;33m'], /* NavajoWhite3 */
            "{+145" => [chr(27) . '[38;5;145m', chr(27) . '[1;30m'], /* Grey69 */
            "{+146" => [chr(27) . '[38;5;146m', chr(27) . '[1;30m'], /* LightSteelBlue3 */
            "{+147" => [chr(27) . '[38;5;147m', chr(27) . '[1;30m'], /* LightSteelBlue */
            "{+148" => [chr(27) . '[38;5;148m', chr(27) . '[0;33m'], /* Yellow3 */
            "{+149" => [chr(27) . '[38;5;149m', chr(27) . '[0;33m'], /* DarkOliveGreen3 */
            "{+150" => [chr(27) . '[38;5;150m', chr(27) . '[0;33m'], /* DarkSeaGreen3 */
            "{+151" => [chr(27) . '[38;5;151m', chr(27) . '[0;37m'], /* DarkSeaGreen2 */
            "{+152" => [chr(27) . '[38;5;152m', chr(27) . '[0;36m'], /* LightCyan3 */
            "{+153" => [chr(27) . '[38;5;153m', chr(27) . '[0;36m'], /* LightSkyBlue1 */
            "{+154" => [chr(27) . '[38;5;154m', chr(27) . '[0;32m'], /* GreenYellow */
            "{+155" => [chr(27) . '[38;5;155m', chr(27) . '[0;32m'], /* DarkOliveGreen2 */
            "{+156" => [chr(27) . '[38;5;156m', chr(27) . '[1;32m'], /* PaleGreen1 */
            "{+157" => [chr(27) . '[38;5;157m', chr(27) . '[0;37m'], /* DarkSeaGreen2 */
            "{+158" => [chr(27) . '[38;5;158m', chr(27) . '[1;36m'], /* DarkSeaGreen1 */
            "{+159" => [chr(27) . '[38;5;159m', chr(27) . '[1;36m'], /* PaleTurquoise1 */
            "{+160" => [chr(27) . '[38;5;160m', chr(27) . '[1;31m'], /* Red3 */
            "{+161" => [chr(27) . '[38;5;161m', chr(27) . '[1;31m'], /* DeepPink3 */
            "{+162" => [chr(27) . '[38;5;162m', chr(27) . '[0;35m'], /* DeepPink3 */
            "{+163" => [chr(27) . '[38;5;163m', chr(27) . '[0;35m'], /* Magenta3 */
            "{+164" => [chr(27) . '[38;5;164m', chr(27) . '[0;35m'], /* Magenta3 */
            "{+165" => [chr(27) . '[38;5;165m', chr(27) . '[0;35m'], /* Magenta2 */
            "{+166" => [chr(27) . '[38;5;166m', chr(27) . '[0;31m'], /* DarkOrange3 */
            "{+167" => [chr(27) . '[38;5;167m', chr(27) . '[0;31m'], /* IndianRed */
            "{+168" => [chr(27) . '[38;5;168m', chr(27) . '[0;31m'], /* HotPink3 */
            "{+169" => [chr(27) . '[38;5;169m', chr(27) . '[0;35m'], /* HotPink2 */
            "{+170" => [chr(27) . '[38;5;170m', chr(27) . '[0;35m'], /* Orchid */
            "{+171" => [chr(27) . '[38;5;171m', chr(27) . '[0;35m'], /* MediumOrchid1 */
            "{+172" => [chr(27) . '[38;5;172m', chr(27) . '[0;33m'], /* Orange3 */
            "{+173" => [chr(27) . '[38;5;173m', chr(27) . '[0;33m'], /* LightSalmon3 */
            "{+174" => [chr(27) . '[38;5;174m', chr(27) . '[0;31m'], /* LightPink3 */
            "{+175" => [chr(27) . '[38;5;175m', chr(27) . '[1;35m'], /* Pink3 */
            "{+176" => [chr(27) . '[38;5;176m', chr(27) . '[1;35m'], /* Plum3 */
            "{+177" => [chr(27) . '[38;5;177m', chr(27) . '[1;35m'], /* Violet */
            "{+178" => [chr(27) . '[38;5;178m', chr(27) . '[0;33m'], /* Gold3 */
            "{+179" => [chr(27) . '[38;5;179m', chr(27) . '[0;33m'], /* LightGoldenrod3 */
            "{+180" => [chr(27) . '[38;5;180m', chr(27) . '[0;33m'], /* Tan */
            "{+181" => [chr(27) . '[38;5;181m', chr(27) . '[0;33m'], /* MistyRose3 */
            "{+182" => [chr(27) . '[38;5;182m', chr(27) . '[1;35m'], /* Thistle3 */
            "{+183" => [chr(27) . '[38;5;183m', chr(27) . '[1;35m'], /* Plum2 */
            "{+184" => [chr(27) . '[38;5;184m', chr(27) . '[0;33m'], /* Yellow3 */
            "{+185" => [chr(27) . '[38;5;185m', chr(27) . '[0;33m'], /* Khaki3 */
            "{+186" => [chr(27) . '[38;5;186m', chr(27) . '[0;33m'], /* LightGoldenrod2 */
            "{+187" => [chr(27) . '[38;5;187m', chr(27) . '[0;33m'], /* LightYellow3 */
            "{+188" => [chr(27) . '[38;5;188m', chr(27) . '[0;37m'], /* Grey84 */
            "{+189" => [chr(27) . '[38;5;189m', chr(27) . '[0;37m'], /* LightSteelBlue1 */
            "{+190" => [chr(27) . '[38;5;190m', chr(27) . '[0;33m'], /* Yellow2 */
            "{+191" => [chr(27) . '[38;5;191m', chr(27) . '[0;33m'], /* DarkOliveGreen1 */
            "{+192" => [chr(27) . '[38;5;192m', chr(27) . '[1;33m'], /* DarkOliveGreen1 */
            "{+193" => [chr(27) . '[38;5;193m', chr(27) . '[0;33m'], /* DarkSeaGreen1 */
            "{+194" => [chr(27) . '[38;5;194m', chr(27) . '[0;37m'], /* Honeydew2 */
            "{+195" => [chr(27) . '[38;5;195m', chr(27) . '[0;37m'], /* LightCyan1 */
            "{+196" => [chr(27) . '[38;5;196m', chr(27) . '[1;31m'], /* Red1 */
            "{+197" => [chr(27) . '[38;5;197m', chr(27) . '[1;31m'], /* DeepPink2 */
            "{+198" => [chr(27) . '[38;5;198m', chr(27) . '[1;35m'], /* DeepPink1 */
            "{+199" => [chr(27) . '[38;5;199m', chr(27) . '[1;35m'], /* DeepPink1 */
            "{+200" => [chr(27) . '[38;5;200m', chr(27) . '[1;35m'], /* Magenta2 */
            "{+201" => [chr(27) . '[38;5;201m', chr(27) . '[1;35m'], /* Magenta1 */
            "{+202" => [chr(27) . '[38;5;202m', chr(27) . '[1;31m'], /* OrangeRed1 */
            "{+203" => [chr(27) . '[38;5;203m', chr(27) . '[1;31m'], /* IndianRed1 */
            "{+204" => [chr(27) . '[38;5;204m', chr(27) . '[1;31m'], /* IndianRed1 */
            "{+205" => [chr(27) . '[38;5;205m', chr(27) . '[0;35m'], /* HotPink */
            "{+206" => [chr(27) . '[38;5;206m', chr(27) . '[1;35m'], /* HotPink */
            "{+207" => [chr(27) . '[38;5;207m', chr(27) . '[1;35m'], /* MediumOrchid1 */
            "{+208" => [chr(27) . '[38;5;208m', chr(27) . '[0;31m'], /* DarkOrange */
            "{+209" => [chr(27) . '[38;5;209m', chr(27) . '[0;31m'], /* Salmon1 */
            "{+210" => [chr(27) . '[38;5;210m', chr(27) . '[0;31m'], /* LightCoral */
            "{+211" => [chr(27) . '[38;5;211m', chr(27) . '[0;31m'], /* PaleVioletRed1 */
            "{+212" => [chr(27) . '[38;5;212m', chr(27) . '[1;35m'], /* Orchid2 */
            "{+213" => [chr(27) . '[38;5;213m', chr(27) . '[1;35m'], /* Orchid1 */
            "{+214" => [chr(27) . '[38;5;214m', chr(27) . '[0;33m'], /* Orange1 */
            "{+215" => [chr(27) . '[38;5;215m', chr(27) . '[0;33m'], /* SandyBrown */
            "{+216" => [chr(27) . '[38;5;216m', chr(27) . '[0;33m'], /* LightSalmon1 */
            "{+217" => [chr(27) . '[38;5;217m', chr(27) . '[1;35m'], /* LightPink1 */
            "{+218" => [chr(27) . '[38;5;218m', chr(27) . '[1;35m'], /* Pink1 */
            "{+219" => [chr(27) . '[38;5;219m', chr(27) . '[1;35m'], /* Plum1 */
            "{+220" => [chr(27) . '[38;5;220m', chr(27) . '[0;33m'], /* Gold1 */
            "{+221" => [chr(27) . '[38;5;221m', chr(27) . '[0;33m'], /* LightGoldenrod2 */
            "{+222" => [chr(27) . '[38;5;222m', chr(27) . '[0;33m'], /* LightGoldenrod2 */
            "{+223" => [chr(27) . '[38;5;223m', chr(27) . '[0;33m'], /* NavajoWhite1 */
            "{+224" => [chr(27) . '[38;5;224m', chr(27) . '[0;33m'], /* MistyRose1 */
            "{+225" => [chr(27) . '[38;5;225m', chr(27) . '[0;37m'], /* Thistle1 */
            "{+226" => [chr(27) . '[38;5;226m', chr(27) . '[1;33m'], /* Yellow1 */
            "{+227" => [chr(27) . '[38;5;227m', chr(27) . '[1;33m'], /* LightGoldenrod1 */
            "{+228" => [chr(27) . '[38;5;228m', chr(27) . '[1;33m'], /* Khaki1 */
            "{+229" => [chr(27) . '[38;5;229m', chr(27) . '[0;33m'], /* Wheat1 */
            "{+230" => [chr(27) . '[38;5;230m', chr(27) . '[0;37m'], /* Cornsilk1 */
            "{+231" => [chr(27) . '[38;5;231m', chr(27) . '[1;37m'], /* Grey100 */
            "{+232" => [chr(27) . '[38;5;232m', chr(27) . '[1;30m'], /* Grey3 */
            "{+233" => [chr(27) . '[38;5;233m', chr(27) . '[1;30m'], /* Grey7 */
            "{+234" => [chr(27) . '[38;5;234m', chr(27) . '[1;30m'], /* Grey11 */
            "{+235" => [chr(27) . '[38;5;235m', chr(27) . '[1;30m'], /* Grey15 */
            "{+236" => [chr(27) . '[38;5;236m', chr(27) . '[1;30m'], /* Grey19 */
            "{+237" => [chr(27) . '[38;5;237m', chr(27) . '[1;30m'], /* Grey23 */
            "{+238" => [chr(27) . '[38;5;238m', chr(27) . '[1;30m'], /* Grey27 */
            "{+239" => [chr(27) . '[38;5;239m', chr(27) . '[1;30m'], /* Grey30 */
            "{+240" => [chr(27) . '[38;5;240m', chr(27) . '[1;30m'], /* Grey35 */
            "{+241" => [chr(27) . '[38;5;241m', chr(27) . '[1;30m'], /* Grey39 */
            "{+242" => [chr(27) . '[38;5;242m', chr(27) . '[1;30m'], /* Grey42 */
            "{+243" => [chr(27) . '[38;5;243m', chr(27) . '[0;37m'], /* Grey46 */
            "{+244" => [chr(27) . '[38;5;244m', chr(27) . '[0;37m'], /* Grey50 */
            "{+245" => [chr(27) . '[38;5;245m', chr(27) . '[0;37m'], /* Grey54 */
            "{+246" => [chr(27) . '[38;5;246m', chr(27) . '[0;37m'], /* Grey58 */
            "{+247" => [chr(27) . '[38;5;247m', chr(27) . '[0;37m'], /* Grey62 */
            "{+248" => [chr(27) . '[38;5;248m', chr(27) . '[0;37m'], /* Grey66 */
            "{+249" => [chr(27) . '[38;5;249m', chr(27) . '[0;37m'], /* Grey70 */
            "{+250" => [chr(27) . '[38;5;250m', chr(27) . '[0;37m'], /* Grey74 */
            "{+251" => [chr(27) . '[38;5;251m', chr(27) . '[1;37m'], /* Grey78 */
            "{+252" => [chr(27) . '[38;5;252m', chr(27) . '[1;37m'], /* Grey82 */
            "{+253" => [chr(27) . '[38;5;253m', chr(27) . '[1;37m'], /* Grey85 */
            "{+254" => [chr(27) . '[38;5;254m', chr(27) . '[1;37m'], /* Grey89 */
            "{+255" => [chr(27) . '[38;5;255m', chr(27) . '[1;37m'], /* Grey93 */
        ];
    }
}
