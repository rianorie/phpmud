<?php

namespace Modules\CommandNotFound\Command;

use Game\Connection\Interfaces\Connection;

use Game\Input;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;

use Game\Helper\Output as OutputHelper;
use Game\Module\Command;

class Commands extends Command
{
    /**
     * @var OutputHelper
     */
    protected $outputHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $outputHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->outputHelper = $outputHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $this->getOutput()->write($connection, $this->outputHelper->getColumnized($this->getInput()->getCommands(), 6, true));
    }
}
