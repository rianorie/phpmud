<?php

namespace Modules\CommandNotFound;

use Game\Helper\Output as OutputHelper;

class Module extends \Game\Module\Module
{
    /**
     * Set up the command not found module
     *
     * @throws \Game\Exception
     */
    public function init()
    {
        $outputHelper = new OutputHelper();

        $this->getEvent()->observe(
            'input.not_found',
            new Observer\NotFound($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            100
        );

        $this->getEvent()->observe(
            'input.empty',
            new Observer\NoInput($this->getEntityManager(), $this->getInput(), $this->getOutput())
        );

        $this->getInput()->registerCommand(
            'commands',
            new Command\Commands($this->getEntityManager(), $this->getInput(), $this->getOutput(), $outputHelper),
            $this->getInput()::PRIORITY_LOW
        );
    }
}
