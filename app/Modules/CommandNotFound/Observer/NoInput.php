<?php

namespace Modules\CommandNotFound\Observer;

use Game\Module\Observer;
use stdClass;

class NoInput extends Observer
{
    public function fire(stdClass $data)
    {
        $this->getOutput()->write($data->connection, "");
    }
}
