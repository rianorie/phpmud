<?php

namespace Modules\CommandNotFound\Observer;

use Game\Module\Observer;
use stdClass;

class NotFound extends Observer
{
    public function fire(stdClass $data)
    {
        $output = [
            'Wait, what? I have no idea what that means.',
            'Soo.. that doesn\'t make any kind of sense.',
            'Command not found.',
            "I could not find that command.\nCheck your spelling and check the help files."
        ];

        $this->getOutput()->write($data->connection, ["%s\n", $output[rand(0, 3)]]);
    }
}
