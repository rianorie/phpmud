<?php

namespace Modules\Communication\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Game\Registry;
use Modules\Toggle\Utility as ToggleUtility;
use Modules\World\Helper\Character as CharacterHelper;

class Ooc extends Command
{
    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $char = $connection->getData('character');

        $last = [];

        try {
            if (Registry::exists('communication.ooc.last')) {
                $last = Registry::get('communication.ooc.last');
            }
        } catch (\Exception $e) {
            // this will never happen. Exists check prevents the exception from get but
            // phpstorm is a dick and I want to get rid of the notice
        }

        if ($input == 'last') {
            $output = '';

            foreach($last as $lastOoc) {
                $output .= $lastOoc."\n";
            }

            $this->getOutput()->write($connection, $output);
            return;
        }

        if ( ! ToggleUtility::getToggle($char, 'ooc')) {
            $this->getOutput()->write($connection, "You need to turn OOC on to participate on the channel.");
            return;
        }

        $last[] = sprintf("{y[".date('H:i')." OOC {Y({y%s{Y){y]: {Y%s{x", $char->getName(), $input);
        $last = array_slice($last, -20);

        Registry::set('communication.ooc.last', $last);

        $online = $this->characterHelper->getCharacters();
        foreach($online as $other) {
            if (ToggleUtility::getToggle($other, 'ooc')) {
                $this->getOutput()->write($other->getConnection(), ["{y[OOC {Y({y%s{Y){y]: {Y%s{x", $char->getName(), $input]);
            }
        }
    }
}
