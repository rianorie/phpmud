<?php

namespace Modules\Communication\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;

class Osay extends Command
{
    /**
     * @var \Modules\World\Helper\Character
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $text = 'say';

        if (substr($input, -1) == '?') {
            $text = 'ask';
        } else if (substr($input, -1) == '!') {
            $text = 'exclaim';
        }

        /** @var Character $char */
        $char = $connection->getData('character');

        $this->getOutput()->write($connection, ["{cYou %s oocly, {C'{c%s{C'{x", $text, $input]);

        // grab the online characters
        $online = $this->characterHelper->getOnlineRoomCharacters($char->getData('room'), $char);
        foreach ($online as $other) {
            if ($other) {
                $this->getOutput()->write(
                    $other->getConnection(),
                    ["{c%s %ss oocly, {C'{c%s{C'{x", $char->getName(), $text, $input]
                );
            }
        }
    }
}
