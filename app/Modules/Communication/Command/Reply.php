<?php

namespace Modules\Communication\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;

class Reply extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        if (empty($input)) {
            $this->getOutput()->write($connection, "Reply what, exactly?");
            return;
        }

        if (strtolower($input) == 'who') {
            if (is_null($connection->getData('character')->getData('last_tell_from'))) {
                $this->getOutput()->write($connection, "You're not replying anything to anyone right now.");
                return;
            } else {
                $this->getOutput()->write($connection, ["Replying to %s.", $connection->getData('character')->getData('last_tell_from')]);
                return;
            }
        }

        $this->getInput()->handle(
            $connection,
            sprintf('tell %s %s', $connection->getData('character')->getData('last_tell_from'), $input)
        );
    }
}
