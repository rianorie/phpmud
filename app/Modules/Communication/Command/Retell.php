<?php

namespace Modules\Communication\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;

class Retell extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        if (empty($input)) {
            $this->getOutput()->write($connection, "Tell what, exactly?");
            return;
        }

        if (is_null($connection->getData('character')->getData('last_tell_to'))) {
            $this->getOutput()->write($connection, "Retell to who? You didn't send any tells yet.");
            return;
        }

        // forward to the tell command
        $this->getInput()->handle(
            $connection,
            sprintf('tell %s %s', $connection->getData('character')->getData('last_tell_to'), $input)
        );
    }
}
