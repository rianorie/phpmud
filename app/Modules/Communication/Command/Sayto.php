<?php

namespace Modules\Communication\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Helper\Character as CharacterHelper;

class Sayto extends Command
{
    /**
     * @var CharacterHelper
     */
    protected CharacterHelper $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $text = 'say to';
        $otherText = 'says to';

        $name = substr($input, 0, strpos($input, ' '));

        $target = $this->characterHelper->getCharacterByName(ucfirst($name));
        if ( ! $target) {
            $this->getOutput()->write($connection, ["They aren't here."]);
            return;
        }

        $input = substr($input, strpos($input, ' ') + 1);


        if (substr($input, -1) == '?') {
            $text = 'ask';
            $otherText = 'asks';
        } else if (substr($input, -1) == '!') {
            $text = 'exclaim at';
            $otherText = 'exclaims at';
        }

        /** @var Character $char */
        $char = $connection->getData('character');

        $this->getOutput()->write($connection, ["{gYou %s %s, {G'{g%s{G'{x", $text, $target->getName(), $input]);

        // grab the online characters
        $online = $this->characterHelper->getOnlineRoomCharacters($char->getData('room'), $char);
        foreach ($online as $other) {
            if ($other) {
                if ($other == $target) {

                    $this->getOutput()->write(
                        $other->getConnection(),
                        ["{g%s %s you, {G'{g%s{G'{x", $char->getName(), $otherText, $input]
                    );

                } else {
                    $this->getOutput()->write(
                        $other->getConnection(),
                        ["{g%s %s %s, {G'{g%s{G'{x", $char->getName(), $otherText, $target->getName(), $input]
                    );
                }
            }
        }
    }
}
