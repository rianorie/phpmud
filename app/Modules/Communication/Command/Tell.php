<?php

namespace Modules\Communication\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\Toggle\Utility as ToggleUtility;

class Tell extends Command
{
    /**
     * @var \Modules\World\Helper\Character
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $charQuiet = ToggleUtility::getToggle($connection->getData('character'), 'quiet');
        $charTells = ToggleUtility::getToggle($connection->getData('character'), 'tells');

        if ($charQuiet) {
            $this->getOutput()->write($connection, "You must turn off quiet mode first.");
            return;
        }

        if (! $charTells) {
            $this->getOutput()->write($connection, "You must turn on tells first.");
            return;
        }

        $names = explode(',', substr($input, 0, strpos($input, ' ')));
        $input = substr($input, strpos($input, ' ')+1);

        foreach($names as $name) {
            $char = $this->characterHelper->getCharacterByName(ucfirst($name));
            if ($char) {

                $charQuiet = ToggleUtility::getToggle($char, 'quiet');
                $charTells = ToggleUtility::getToggle($char, 'tells');

                if ($charQuiet || ! $charTells) {
                    $this->getOutput()->write($connection, ["%s is not receiving tells right now.", $char->getName()]);

                } else {
                    $this->getOutput()->write($char->getConnection(), ["{G%s tells you '%s'{x", $connection->getData('character')->getName(), $input]);
                    $this->getOutput()->write($connection, ["{GYou tell %s '%s'", $char->getName(), $input]);
                    $char->setData('last_tell_from', $connection->getData('character')->getName());
                    $connection->getData('character')->setData('last_tell_to', $char->getName());
                }
            } else {
                $this->getOutput()->write($connection, "They aren't here.");
            }
        }
    }
}
