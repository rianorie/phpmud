<?php

namespace Modules\Communication;

use Modules\Toggle\Utility as ToggleUtility;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Room as RoomHelper;

class Module extends \Game\Module\Module
{
    /**
     * Set up the communication module
     *
     * @throws \Game\Exception
     */
    public function init()
    {
        ToggleUtility::registerToggle('Quiet', 'Communication', 'Quiet mode', false);
        ToggleUtility::registerToggle('Tells', 'Communication', 'Tells', true);
        ToggleUtility::registerToggle('OOC', 'Communication', 'OOC', true);

        $characterHelper = new CharacterHelper(new RoomHelper($this->getEntityManager()));

        $this->getInput()->registerCommands([
            'say'    => new Command\Say($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper),
            'sayto'  => new Command\Sayto($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper),
            'osay'   => new Command\Osay($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper),
            'reply'  => new Command\Reply($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            'tell'   => new Command\Tell($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper),
            'retell' => new Command\Retell($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            'ooc'    => new Command\Ooc($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper),
        ]);
    }
}
