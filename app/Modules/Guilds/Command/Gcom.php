<?php

namespace Modules\Guilds\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use Modules\Guilds\Entity\Group;
use Modules\Guilds\Entity\Guild;
use Modules\World\Entity\Character;

class Gcom extends Command
{
    /**
     * @var \Modules\Guilds\Helper\Guild
     */
    protected $guildHelper;

    /**
     * @var \Modules\World\Helper\Character
     */
    protected $characterHelper;

    /**
     * @var \Game\Helper\Output
     */
    protected $outputHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $guildHelper, $characterHelper, $outputHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->guildHelper  = $guildHelper;
        $this->outputHelper = $outputHelper;
        $this->characterHelper = $characterHelper;
    }

    /**
     * @param Connection $connection
     * @param string     $input
     * @param string     $command
     * @param string     $raw
     *
     * @return void
     */
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        try {
            /** @var Character $character */
            $character = $connection->getData('character');


            $guild = $character->getMetadata('guild');
            if ( ! empty($guild)) {
                /** @var Guild $guild */
                $guild = $this->guildHelper->getGuildByName($guild);
            }

            if (empty($input)) {
                $output = "Guild related commands are grouped under this command. All syntax\n" .
                          "is provided here but limited to a particular rank.\n\n" .
                          "Syntax: gcom list                     Lists the online guild members\n" .
                          "Syntax: gcom all                      Lists all the available guilds in the game\n" .
                          "Syntax: gcom show [guild]             Shows the information for a specific guild\n";

                if ($guild && ($this->guildHelper->isGuildLeader($character, $guild)
                               || $this->guildHelper->isRecruiter($character, $guild))) {

                    $output .=
                        "\n" .
                        "Syntax: gcom add [name] [guild]       Recruit someone into the given guild\n" .
                        "Syntax: gcom remove [name]            Remove someone from the guild [GL only]\n" .
                        "Syntax: gcom recruiter [name]         Set or unset someone as a recruiter [GL only]\n" .
                        "Syntax: gcom blademaster [name]       Set someone as a blademaster [GL only]\n" .
                        "Syntax: gcom promote [name] [rank]    Sets the rank of a guild member\n".
                        "Syntax: gcom ranks [guild]            Shows the ranks of a guild\n";
                }

                $this->getOutput()->write($connection, $output);
                return;
            }

            $sub  = $input;
            $text = '';
            if (strpos($input, ' ') !== false) {
                // determine the input
                $sub  = substr($input, 0, strpos($input, ' '));
                $text = substr($input, strpos($input, ' ') + 1);
            }


            // listing online guild members is handled in the glist command
            if (stripos('list', $input) !== false) {
                $this->getInput()->handle($connection, 'glist');
                return;
            }

            // if all is called, output all the info there is
            if (stripos('all', $input) !== false) {
                $this->subcommandAll($connection);
                return;
            }

            // show outputs info for one specific guild
            if (stripos('show', $sub) !== false) {
                $this->subcommandShow($connection, $text);
                return;
            }

            // add allows a guildleader or recruiter to add someone to their guild
            if (stripos('add', $sub) !== false) {
                $this->subcommandAdd($connection, $character, $guild, $text);
                return;
            }

            // remove allows a guildleader remove someone from their guild
            if (stripos('remove', $sub) !== false) {
                $this->subcommandRemove($connection, $character, $text);
                return;
            }

            // toggle the recruiter status for a member
            if (stripos('recruiter', $sub) !== false) {
                $this->subcommandRecruiter($connection, $character, $text);
                return;
            }

            // show the ranks for a guild
            if (stripos('ranks', $sub) !== false) {
                $this->subcommandRanks($connection, $character, $text);
                return;
            }

            // adjust someone's rank in the guild
            if (stripos('promote', $sub) !== false) {
                $this->subcommandPromote($connection, $character, $text);
                return;
            }

            // toggle the blademaster status for a member
            if (stripos('blademaster', $sub) !== false) {
                $this->subcommandBlademaster($connection, $character, $text);
                return;
            }

        } catch (\Exception $exception) {

        }
    }

    /**
     * @param Connection $connection
     *
     * @throws \Game\Module\Entity\Exception
     */
    protected function subcommandAll(Connection $connection)
    {
        $output = '';

        /** @var Group[] $groups */
        $groups = $this->guildHelper->getGroups();
        foreach ($groups as $group) {
            if ($group) {
                $output .= $this->generateGuildInfo($group);
                $output .= "{x\n\n";
            }
        }

        $this->getOutput()->write($connection, $output);
    }

    /**
     * @param Connection $connection
     * @param string     $guild
     *
     * @throws \Game\Module\Entity\Exception
     */
    protected function subcommandShow(Connection $connection, string $guild)
    {
        $output = '';

        /** @var Guild[] $searchResult */
        $searchResult = $this->getEntityManager()->find(new Guild(), ['name' => $guild.'%'], 1);
        foreach ($searchResult as $search) {
            if ($search) {

                /** @var Group[] $groups */
                $groups = $this->guildHelper->getGroupsByGuild($search);

                foreach($groups as $group) {
                    if ($group) {
                        $output .= $this->generateGuildInfo($group);
                        $output .= "{x\n\n";
                    }
                }
            }
        }

        $this->getOutput()->write($connection, $output);
    }

    /**
     * @param Connection $connection
     * @param Character  $character
     * @param Guild      $guild
     * @param string     $text
     *
     * @throws \Game\Module\Entity\Exception
     */
    protected function subcommandAdd(Connection $connection, Character $character, Guild $guild, string $text)
    {
        // no argument? what are we even doing?
        if (empty($text)) {
            $this->getOutput()->write($connection, "Recruit who into your guild?");
            return;
        }

        // can we guild anyone at all?
        if (    ! $this->guildHelper->isGuildLeader($character, $guild)
             && ! $this->guildHelper->isRecruiter($character, $guild)) {
            $this->getOutput()->write($connection, "You're not allowed to guild into that guild.");
            return;
        }

        $name      = $text;
        $guildname = '';

        $parts     = explode(' ', $text);
        if (count($parts) > 1) {
            $name      = $parts[0];
            $guildname = $parts[1];
        }

        if (empty($name)) {
            $this->getOutput()->write($connection, "There doesn't seem to be anyone by that name.");
            return;
        }

        // try and find the actual target to be recruited
        $target = false;

        $onlineCharacters = $this->characterHelper->getOnlineRoomCharacters($character->getData('room'), $character);
        foreach($onlineCharacters as $onlineCharacter) {
            if (stripos($onlineCharacter->getName(), $name) !== false) {
                $target = $onlineCharacter;
                break;
            }
        }

        // if we still can't find anyone, stop here
        if ( ! $target) {
            $this->getOutput()->write($connection, "Who? There doesn't seem to be anyone by that name.");
            return;
        }


        $targetGuild = false;

        // if we're trying to guild into a defined guild, check if we can
        if ( ! empty($guildname)) {

            $allowedGuilds = $this->guildHelper->getCharacterStaffPositions($character);
            foreach($allowedGuilds as $allowedGuild) {
                if (stripos($allowedGuild->getName(), $guildname) !== false) {
                    $targetGuild = $allowedGuild;
                    break;
                }
            }

        // otherwise grab the guild the recruiter is in
        } else {
            $targetGuild = $guild;
        }

        // if no guild is found, stop
        if ( ! $targetGuild) {
            $this->getOutput()->write($connection, "You're not allowed to guild into that.");
            return;
        }

        if ( ! empty($target->getMetadata('guild'))) {

            $currentTargetGuild = $target->getMetadata('guild');

            $reguildAllowed = false;

            $allowedGuilds = $this->guildHelper->getCharacterStaffPositions($character);
            foreach($allowedGuilds as $allowedGuild) {
                if ($allowedGuild->getName() == $currentTargetGuild) {
                    $reguildAllowed = true;
                    break;
                }
            }

            if ( ! $reguildAllowed) {
                $this->getOutput()->write($connection, "They are already guilded.");
                return;
            }
        }

        $target->setMetadata('guild', $targetGuild->getName());
        $this->getEntityManager()->update($target, 'metadata');

        $this->getOutput()->write($connection, ['%s is now a part of the %s guild!', $target->getName(), $targetGuild->getName()]);
        $this->getOutput()->write($target->getConnection(), ['You are now a %s!', $targetGuild->getName()]);
    }

    /**
     * @param Connection $connection
     * @param Character  $character
     * @param string     $text
     *
     * @throws \Exception
     */
    protected function subcommandRemove(Connection $connection, Character $character, string $text)
    {
        // no argument? what are we even doing?
        if (empty($text)) {
            $this->getOutput()->write($connection, "Remove who from your guild?");
            return;
        }

        // try and find the actual target to be recruited
        $target = false;

        $onlineCharacters = $this->characterHelper->getCharacters();
        foreach($onlineCharacters as $onlineCharacter) {
            if (stripos($onlineCharacter->getName(), $text) !== false) {
                $target = $onlineCharacter;
                break;
            }
        }

        // if we still can't find anyone, stop here
        if ( ! $target) {
            $this->getOutput()->write($connection, "Who? There doesn't seem to be anyone by that name.");
            return;
        }

        $currentGuild = $target->getMetadata('guild');
        if (empty($currentGuild)) {
            $this->getOutput()->write($connection, "They're not in a guild.");
            return;
        }

        $deguildAllowed = false;

        $allowedGuilds = $this->guildHelper->getCharacterStaffPositions($character);
        foreach($allowedGuilds as $allowedGuild) {
            if ($allowedGuild->getName() == $currentGuild) {
                $deguildAllowed = $this->guildHelper->isGuildLeader($character, $allowedGuild);
                break;
            }
        }

        if ( ! $deguildAllowed) {
            $this->getOutput()->write($connection, "You're not allowed to deguild them.");
            return;
        }

        // if a character is in a subguild, move that guild to the
        // first position. Otherwise make them a loner
        $subguild = $target->getMetadata('subguild');

        if ( ! empty($subguild)) {
            $target->setMetadata('guild', $subguild);
            $target->unsetMetadata('subguild');

            $this->getOutput()->write($target->getConnection(), ['%s is now your primary guild.', $subguild]);

        } else {
            $target->unsetMetadata('guild');
        }

        $this->getEntityManager()->update($target, 'metadata');

        $this->getOutput()->write($connection, ['%s was deguilded from %s!', $target->getName(), $currentGuild]);
        $this->getOutput()->write($target->getConnection(), ['You were deguilded from %s!', $currentGuild]);
    }

    /**
     * @param Connection $connection
     * @param Character  $character
     * @param string     $text
     *
     * @throws \Game\Module\Entity\Exception
     */
    protected function subcommandRecruiter(Connection $connection, Character $character, string $text)
    {
        // no argument? what are we even doing?
        if (empty($text)) {
            $this->getOutput()->write($connection, "Set whom as a recruiter?");
            return;
        }

        // try and find the actual target
        $target = false;

        /** @var Character[] $targetSearchGenerator */
        $targetSearchGenerator = $this->getEntityManager()->find(new Character(), ['name' => $text]);
        foreach($targetSearchGenerator as $targetSearch) {
            if ($targetSearch) {
                $target = $targetSearch;
                break;
            }
        }

        // if we still can't find anyone, stop here
        if ( ! $target) {
            $this->getOutput()->write($connection, "Who? There doesn't seem to be anyone by that name.");
            return;
        }

        $currentGuild = $target->getMetadata('guild');
        if (empty($currentGuild)) {
            $this->getOutput()->write($connection, "They're not in a guild.");
            return;
        }

        $adjustAllowed = false;

        $allowedGuilds = $this->guildHelper->getCharacterStaffPositions($character);
        foreach($allowedGuilds as $allowedGuild) {
            if ($allowedGuild->getName() == $currentGuild) {
                $currentGuild = $allowedGuild;
                $adjustAllowed = $this->guildHelper->isGuildLeader($character, $allowedGuild);
                break;
            }
        }

        if ( ! $adjustAllowed) {
            $this->getOutput()->write($connection, "You're not allowed to toggle their recruiter status.");
            return;
        }

        if ($this->guildHelper->isRecruiter($target, $currentGuild)) {
            $this->guildHelper->removeRecruiter($target, $currentGuild);
            $this->getOutput()->write($connection, ['They are no longer a recruiter for %s.', $currentGuild->getName()]);
        } else {
            if ($this->guildHelper->addRecruiter($target, $currentGuild)) {
                $this->getOutput()->write($connection, ['They are now a recruiter for %s.', $currentGuild->getName()]);
            } else {
                $this->getOutput()->write($connection, ['Failed to set them as a recruiter for %s.', $currentGuild->getName()]);
            }
        }
    }

    /**
     * Toggle the blademaster status for a character
     *
     * @param Connection $connection
     * @param Character  $character
     * @param string     $text
     *
     * @throws \Game\Module\Entity\Exception
     */
    protected function subcommandBlademaster(Connection $connection, Character $character, string $text)
    {
        // no argument? what are we even doing?
        if (empty($text)) {
            $this->getOutput()->write($connection, "Set whom as a blademaster?");
            return;
        }

        // try and find the actual target
        $target = false;

        $characters = $this->characterHelper->getCharacters();
        foreach($characters as $character) {
            if (stripos($character->getName(), $text) !== false) {
                $target = $character;
                break;
            }
        }

        // if we still can't find anyone, stop here
        if ( ! $target) {
            $this->getOutput()->write($connection, "Who? There doesn't seem to be anyone online by that name.");
            return;
        }

        // check if the flagging character is a guild leader
        if ( ! $this->guildHelper->isGuildLeader($character)) {
            $this->getOutput()->write($connection, "You're not allowed to set their blademaster status.");
            return;
        }

        if (empty($target->getMetadata('blademaster'))) {
            $target->setMetadata('blademaster', true);
            $this->getOutput()->write($connection, ['%s is now a blademaster!', $target->getName()]);
            $this->getOutput()->write($target->getConnection(), ['You now an officially acknowledged blademaster!']);

        } else {
            $this->getOutput()->write($connection, ['%s is already a blademaster.', $target->getName()]);
            return;
        }

        $target->setTitle($this->guildHelper->getGuildTitle($target));

        $this->getEntityManager()->update($target, ['metadata', 'title']);
    }

    /**
     * List the ranks for a guild sorted by position
     *
     * @param Connection $connection
     * @param Character  $character
     * @param string     $text
     *
     * @throws \Game\Module\Entity\Exception
     */
    protected function subcommandRanks(Connection $connection, Character $character, string $text)
    {
        $guildname = $character->getMetadata('guild');

        if ( ! empty($text)) {
            $guildname = $text;
        }

        $targetGuild = false;

        // if we're trying to guild into a defined guild, check if we can
        if ( ! empty($guildname)) {

            $allowedGuilds = $this->guildHelper->getCharacterStaffPositions($character);
            foreach($allowedGuilds as $allowedGuild) {
                if (stripos($allowedGuild->getName(), $guildname) !== false) {
                    $targetGuild = $allowedGuild;
                    break;
                }
            }

        // otherwise grab the guild the character is in
        } else {
            $targetGuild = $this->guildHelper->getGuildByName($guildname);
        }

        if ( ! $targetGuild) {
            $this->getOutput()->write($connection, 'Failed to find an allowed guild to list ranks for.');
            return;
        }

        $output = sprintf("Ranks for %s\n{b--------------------{x\n", $targetGuild->getLabel());

        $ranks = $this->guildHelper->getRanksForGuild($targetGuild);
        foreach($ranks as $rank) {
            $output .= sprintf("{c[{w%3d{c] %s{c\n", $rank->getPosition(), $rank->getName());
        }

        $this->getOutput()->write($connection, $output);
    }

    /**
     * Update a characters rank
     *
     * @param Connection $connection
     * @param Character  $character
     * @param string     $text
     *
     * @throws \Game\Module\Entity\Exception
     */
    protected function subcommandPromote(Connection $connection, Character $character, string $text)
    {

        $parts = explode(' ', $text);
        if (count($parts) !== 2 ||  ! is_numeric($parts[1])) {
            $this->getOutput()->write($connection, "Check the syntax for the command.");
            return;
        }

        $name     = $parts[0];
        $position = $parts[1];

        $target = false;

        $onlineCharacters = $this->characterHelper->getCharacters();
        foreach($onlineCharacters as $onlineCharacter) {
            if (stripos($onlineCharacter->getName(), $name) !== false) {
                $target = $onlineCharacter;
                break;
            }
        }

        if ( ! $target) {
            $this->getOutput()->write($connection, "Can't find them.");
            return;
        }

        $guild = $this->guildHelper->getGuildByName($target->getMetadata('guild'));
        if (empty($guild)) {
            $this->getOutput()->write($connection, "They're not in a guild.");
            return;
        }

        // can we guild anyone at all?
        if (    ! $this->guildHelper->isGuildLeader($character, $guild)
             && ! $this->guildHelper->isRecruiter($character, $guild)) {
            $this->getOutput()->write($connection, "You can't promote for that guild.");
            return;
        }

        $newRank = false;

        $ranks = $this->guildHelper->getRanksForGuild($guild);
        foreach($ranks as $rank) {
            if ($rank->getPosition() == $position) {
                $newRank = $rank;
                break;
            }
        }

        if ( ! $newRank) {
            $this->getOutput()->write($connection, "Couldn't find that rank in the guild.");
            return;
        }

        $target->setMetadata('rank', $newRank->getName());
        $target->setTitle($this->guildHelper->getGuildTitle($target));

        if ($this->getEntityManager()->update($target, ['metadata', 'title'])) {

            $this->getOutput()->write($connection, ['%s is now a %s!', $target->getName(), $rank->getName()]);
            $this->getOutput()->write($target->getConnection(), ['You are now a %s!', $rank->getName()]);
        } else {
            $this->getOutput()->write($connection, "Something went wrong.");
        }

        return;
    }

    /**
     * @param Group $group
     *
     * @return string
     * @throws \Game\Module\Entity\Exception
     */
    protected function generateGuildInfo(Group $group)
    {
        $guilds = [];
        $guildleaders = [];
        $recruiters = [];

        /** @var Guild[] $guildCollection */
        $guildCollection = $this->guildHelper->getGuildsByGroup($group);
        foreach ($guildCollection as $guild) {
            if ($guild) {
                $guilds[] = $guild->getLabel();
            }
        }

        $leaderCollection = $this->guildHelper->getGuildLeadersByGroup($group);
        foreach($leaderCollection as $character) {
            if ($character) {
                $guildleaders[] = $character->getName();
            }
        }

        $recruiterCollection = $this->guildHelper->getRecruitersByGroup($group);
        foreach($recruiterCollection as $character) {
            if ($character) {
                $recruiters[] = $character->getName();
            }
        }

        $output = '';

        if ( ! empty($guilds)) {
            $output .= sprintf("{w%s %s\n{b--------------------{x\n", $group->getLabel(), $group->getTag());
            $output .= $this->outputHelper->getColumnized($guilds, 4, true);
            $output .= "\n\n";

            if ( ! empty($guildleaders)) {
                $output .= sprintf("{cGuild leader(s): {w%s{x\n", implode(', ', $guildleaders));
            }

            if ( ! empty($recruiters)) {
                $output .= sprintf("{cRecruiter(s): {w%s{x\n", implode(', ', $recruiters));
            }
        }

        return $output;
    }
}
