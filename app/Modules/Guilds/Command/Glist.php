<?php

namespace Modules\Guilds\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\Colors\Helper\Color;
use Modules\Guilds\Entity\Group;
use Modules\Guilds\Helper\Guild;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Entity\Character;

class Glist extends Command
{
    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * @var Guild
     */
    protected $guildHelper;

    /**
     * @var Color
     */
    protected $colorHelper;

    public function __construct(
        EntityManager $entityManager,
        Input $input,
        Output $output,
        $characterHelper,
        $guildHelper,
        $colorHelper
    )
    {
        parent::__construct($entityManager, $input, $output);
        $this->characterHelper = $characterHelper;
        $this->guildHelper = $guildHelper;
        $this->colorHelper = $colorHelper;
    }

    /**
     * @param Connection $connection
     * @param string     $input
     * @param string     $command
     * @param string     $raw
     */
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var Character $character */
        $character = $connection->getData('character');

        // grab the guilds the character is a part off
        $guild    = $character->getMetadata('guild');
        $subguild = $character->getMetadata('subguild');

        // if they're not in a guild, we can stop here
        if ( ! $guild) {
            $this->getOutput()->write($connection, "You're not in any guilds.");
            return;
        }

        try {
            // grab the groups the guilds are a part off
            $groups    = $this->guildHelper->getGroupsByGuildName($guild);

            $processList = [];

            /** @var Group $group */
            foreach($groups as $group) {
                if ($group && $group->getVisible()) {
                    $processList[] = $group;
                }
            }

            if ($subguild) {

                /** @var Group[] $subgroups */
                $subgroups = $this->guildHelper->getGroupsByGuildName($subguild);
                foreach($subgroups as $subgroup) {
                    if ($subgroup && $subgroup->getVisible()) {
                        $processList[] = $subgroup;
                    }
                }
            }

            // grab the online characters
            $online = $this->characterHelper->getCharacters();

            $ouput = [];

            foreach($processList as $group) {
                $ouput[$group->getId()] = sprintf("{w%s %s\n{b--------------------{x\n", $group->getLabel(), $group->getTag());

                foreach($online as $char) {

                    $charGuilds = [
                        $char->getMetadata('guild'),
                        $char->getMetadata('subguild')
                    ];

                    foreach($charGuilds as $charGuild) {
                        if ($charGuild) {

                            // load the guild and groups entities
                            $charGuild  = $this->guildHelper->getGuildByName($charGuild);
                            $charGroups = $this->guildHelper->getGroupsByGuild($charGuild);

                            /** @var Group $charGroup */
                            foreach($charGroups as $charGroup) {
                                if ($charGroup && $charGroup->getId() == $group->getId()) {

                                    $length = $this->colorHelper->strlen($charGuild->getLabel());
                                    $diff = 15 - $length;

                                    $ouput[$group->getId()] .= sprintf(
                                        "{c[{w%s%{$diff}s{c]{w %s\n",
                                        $charGuild->getLabel(),
                                        ' ',
                                        $this->characterHelper->getPublicInfo($char)
                                    );
                                }
                            }
                        }
                    }
                }
            }

            $this->getOutput()->write($connection, implode("\n", $ouput).'{x');

        } catch (\Exception $exception) {

            $this->getOutput()->write($connection, ['FAIL %s', $exception->getMessage()]);

            // log this, I guess?
        }
    }
}
