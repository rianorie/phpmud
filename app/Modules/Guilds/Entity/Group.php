<?php

namespace Modules\Guilds\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

class Group extends Entity
{
    use Timestamp;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $tag;

    /**
     * @var integer
     */
    public $visible;

    public function getTable() : string
    {
        return 'guild_groups';
    }

    public function getColumns() : array
    {
        return ['id', 'name', 'label', 'tag', 'visible', 'created_at', 'updated_at'];
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLabel() : string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label) : void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getTag() : string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag(string $tag) : void
    {
        $this->tag = $tag;
    }

    /**
     * @return bool
     */
    public function getVisible() : bool
    {
        return (bool) $this->visible;
    }

    /**
     * @param bool $visible
     */
    public function setVisible(bool $visible) : void
    {
        $this->visible = (int) $visible;
    }
}
