<?php

namespace Modules\Guilds\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

class Guild extends Entity
{
    use Timestamp;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $label;

    public function getTable() : string
    {
        return 'guilds';
    }

    public function getColumns() : array
    {
        return ['id', 'name', 'label', 'created_at', 'updated_at'];
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLabel() : string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label) : void
    {
        $this->label = $label;
    }
}
