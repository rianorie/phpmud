<?php

namespace Modules\Guilds\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

class Rank extends Entity
{
    use Timestamp;

    /**
     * @var integer
     */
    public $guild_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var integer
     */
    public $position;

    public function getTable() : string
    {
        return 'guild_ranks';
    }

    public function getColumns() : array
    {
        return ['id', 'guild_id', 'name', 'position', 'created_at', 'updated_at'];
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getGuildId() : int
    {
        return $this->guild_id;
    }

    /**
     * @param int $guild_id
     */
    public function setGuildId(int $guild_id) : void
    {
        $this->guild_id = $guild_id;
    }

    /**
     * @return int
     */
    public function getPosition() : int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position) : void
    {
        $this->position = $position;
    }
}
