<?php

namespace Modules\Guilds\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

class Relation extends Entity
{
    use Timestamp;

    /**
     * @var integer
     */
    public $group_id;

    /**
     * @var integer
     */
    public $guild_id;

    public function getTable() : string
    {
        return 'guild_relations';
    }

    public function getColumns() : array
    {
        return ['id', 'group_id', 'guild_id', 'created_at', 'updated_at'];
    }

    /**
     * @return int
     */
    public function getGroupId() : int
    {
        return $this->group_id;
    }

    /**
     * @param int $group_id
     */
    public function setGroupId(int $group_id) : void
    {
        $this->group_id = $group_id;
    }

    /**
     * @return int
     */
    public function getGuildId() : int
    {
        return $this->guild_id;
    }

    /**
     * @param int $guild_id
     */
    public function setGuildId(int $guild_id) : void
    {
        $this->guild_id = $guild_id;
    }
}
