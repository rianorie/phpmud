<?php

namespace Modules\Guilds\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

class Staff extends Entity
{
    use Timestamp;

    const GUILDLEADER = 1;

    const RECRUITER = 2;

    /**
     * @var integer
     */
    public $group_id;

    /**
     * @var integer
     */
    public $character_id;

    /**
     * @var integer
     */
    public $position;

    public function getTable() : string
    {
        return 'guild_staff';
    }

    public function getColumns() : array
    {
        return ['id', 'group_id', 'character_id', 'position', 'created_at', 'updated_at'];
    }

    /**
     * @return int
     */
    public function getCharacterId() : int
    {
        return $this->character_id;
    }

    /**
     * @param int $character_id
     */
    public function setCharacterId(int $character_id) : void
    {
        $this->character_id = $character_id;
    }

    /**
     * @return int
     */
    public function getPosition() : int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position) : void
    {
        if ( ! in_array($position, [self::GUILDLEADER, self::RECRUITER])) {
            throw new \InvalidArgumentException('Position should be one of the available ones in the entity.');
        }

        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getGroupId() : int
    {
        return $this->group_id;
    }

    /**
     * @param int $group_id
     */
    public function setGroupId(int $group_id) : void
    {
        $this->group_id = $group_id;
    }
}
