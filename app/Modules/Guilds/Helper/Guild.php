<?php

namespace Modules\Guilds\Helper;

use Game\Module\Entity\Entity;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Modules\Guilds\Entity\Group as GuildGroup;
use Modules\Guilds\Entity\Guild as GuildEntity;
use Modules\Guilds\Entity\Rank;
use Modules\Guilds\Entity\Relation;
use Modules\Guilds\Entity\Staff;
use Modules\World\Entity\Character;

class Guild
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Guild Item constructor
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getGuildTitle(Character $character)
    {
        $guild = $character->getMetadata('guild');
        if (empty($guild)) {
            return '';
        }

        /** @var GuildEntity $guild */
        $guild = $this->getGuildByName($guild);

        $guildGroup = false;

        /** @var GuildGroup[] $groupsGenerator */
        $groupsGenerator = $this->getGroupsByGuild($guild);
        foreach($groupsGenerator as $group) {
            if ($group && $group->getVisible()) {
                $guildGroup = $group;
            }
        }

        $blademaster = $character->getMetadata('blademaster');
        $blademasterTag = '';
        if ($blademaster) {
            $blademasterTag = '[BM]';

            if ($character->getMetadata('blademaster.tag')) {
                $blademasterTag = $character->getMetadata('blademaster.tag');
            }
        }

        return sprintf(
            "%s%s%s",
            $character->getMetadata('rank'),
            ($guildGroup ? ' '.$guildGroup->getTag() : ''),
            ($blademaster ? ' '.$blademasterTag : '')
        );
    }

    /**
     * Retrieve the guild record from the database
     *
     * @param string $name
     *
     * @return Entity|GuildEntity|null
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function getGuildByName(string $name) : ?GuildEntity
    {
        return $this->entityManager->load(new GuildEntity(), $name, 'name');
    }

    /**
     * Load up a list of the guild groups
     *
     * @return Entity[]|GuildGroup[]
     */
    public function getGroups()
    {
        return $this->entityManager->find(new GuildGroup);
    }

    /**
     * Load up a list of the guilds
     *
     * @return Entity[]|GuildEntity[]
     */
    public function getGuilds()
    {
        return $this->entityManager->find(new GuildEntity());
    }

    /**
     * Retrieve a list of the guilds associated with a group
     *
     * @param GuildGroup $group
     *
     * @return bool|\Generator|GuildEntity[]
     * @throws \Game\Module\Entity\Exception
     */
    public function getGuildsByGroup(GuildGroup $group)
    {
        /** @var Relation[] $relations */
        $relations = $this->entityManager->find(new Relation(), ['group_id' => $group->getId()]);
        foreach ($relations as $relation) {
            if ($relation) {
                yield $this->entityManager->load(new GuildEntity(), $relation->getGuildId());
            }
        }

        return false;
    }

    /**
     * Get the groups a guild is a part off
     *
     * @param string $guild
     *
     * @return Entity|GuildGroup|false
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function getGroupsByGuildName(string $guild)
    {
        /** @var GuildEntity $guild */
        $guild = $this->getGuildByName($guild);

        return $this->getGroupsByGuild($guild);
    }

    /**
     * Get the groups a guild is a part off
     *
     * @param GuildEntity $guild
     *
     * @return Entity|GuildGroup|false
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function getGroupsByGuild(GuildEntity $guild)
    {
        /** @var Relation[] $relations */
        $relations = $this->entityManager->find(new Relation(), ['guild_id' => $guild->getId()]);
        foreach ($relations as $relation) {
            if ($relation) {
                yield $this->entityManager->load(new GuildGroup(), $relation->getGroupId());
            }
        }

        return false;
    }

    /**
     * Retrieve the guildleaders for a group
     *
     * @param GuildGroup $group
     *
     * @return bool|\Generator|Character[]
     * @throws \Game\Module\Entity\Exception
     */
    public function getGuildLeadersByGroup(GuildGroup $group)
    {
        /** @var Staff[] $staffCollection */
        $staffCollection = $this->entityManager->find(new Staff(), ['group_id' => $group->getId(), 'position' => Staff::GUILDLEADER]);
        foreach($staffCollection as $staff) {
            if ($staff) {
                yield $this->entityManager->load(new Character(), $staff->getCharacterId());
            }
        }

        return false;
    }

    /**
     * Retrieve the recruiters for a group
     *
     * @param GuildGroup $group
     *
     * @return bool|\Generator|Character[]
     * @throws \Game\Module\Entity\Exception
     */
    public function getRecruitersByGroup(GuildGroup $group)
    {
        /** @var Staff[] $staffCollection */
        $staffCollection = $this->entityManager->find(new Staff(), ['group_id' => $group->getId(), 'position' => Staff::RECRUITER]);
        foreach($staffCollection as $staff) {
            if ($staff) {
                yield $this->entityManager->load(new Character(), $staff->getCharacterId());
            }
        }

        return false;
    }

    /**
     * @param GuildEntity $guild
     *
     * @return Entity[]|Rank[]
     */
    public function getRanksForGuild(GuildEntity $guild)
    {
        $ranks = [];

        /** @var Rank[] $rankGenerator */
        $rankGenerator = $this->entityManager->find(new Rank, ['guild_id' => $guild->getId()]);
        foreach($rankGenerator as $rank) {
            if ($rank) {
                $ranks[] = $rank;
            }
        }

        if ( ! count($ranks)) {
            return [];
        }

        usort($ranks, function($a, $b) { return $a->getPosition() > $b->getPosition(); });

        return $ranks;
    }

    /**
     * Check if a character is a guildleader
     *
     * @param Character        $character
     * @param GuildEntity|null $guild
     *
     * @return bool
     * @throws \Game\Module\Entity\Exception
     */
    public function isGuildLeader(Character $character, ?GuildEntity $guild = null) : bool
    {
        // if we're not checking against a particular guild we only need to count
        if (is_null($guild)) {
            return ($this->entityManager->count(new Staff(), ['character_id' => $character->getId(), 'position' => Staff::GUILDLEADER]) > 0);
        }

        return $this->checkIsStaff($guild, $character, Staff::GUILDLEADER);
    }

    /**
     * Check if a character is a recruiter
     *
     * @param Character        $character
     * @param GuildEntity|null $guild
     *
     * @return bool
     * @throws \Game\Module\Entity\Exception
     */
    public function isRecruiter(Character $character, ?GuildEntity $guild = null) : bool
    {
        // if we're not checking against a particular guild we only need to count
        if (is_null($guild)) {
            return ($this->entityManager->count(new Staff(), ['character_id' => $character->getId(), 'position' => Staff::RECRUITER]) > 0);
        }

        return $this->checkIsStaff($guild, $character, Staff::RECRUITER);
    }

    public function removeRecruiter(Character $character, GuildEntity $guild) : bool
    {
        return $this->removeStaffMember($guild, $character, Staff::RECRUITER);
    }

    public function addRecruiter(Character $character, GuildEntity $guild) : bool
    {
        return $this->addStaffMember($guild, $character, Staff::RECRUITER);
    }

    public function removeGuildLeader(Character $character, GuildEntity $guild) : bool
    {
        return $this->removeStaffMember($guild, $character, Staff::GUILDLEADER);
    }

    public function addGuildLeader(Character $character, GuildEntity $guild) : bool
    {
        return $this->addStaffMember($guild, $character, Staff::GUILDLEADER);
    }

    /**
     * Check if a character is staff of a guild
     *
     * @param GuildEntity $guild
     * @param Character   $character
     * @param int         $position
     *
     * @return bool
     * @throws \Game\Module\Entity\Exception
     */
    protected function checkIsStaff(GuildEntity $guild, Character $character, int $position) : bool
    {
        /** @var GuildGroup[] $groups */
        $groups = $this->getGroupsByGuild($guild);
        foreach($groups as $group) {
            if ($group) {
                if ($this->entityManager->count(
                        new Staff(),
                        [
                            'group_id' => $group->getId(),
                            'character_id' => $character->getId(),
                            'position' => $position
                        ]
                    ) > 0) { return true; }
            }
        }

        return false;
    }

    protected function removeStaffMember(GuildEntity $guild, Character $character, int $position) : bool
    {
        /** @var GuildGroup[] $groups */
        $groups = $this->getGroupsByGuild($guild);
        foreach($groups as $group) {
            if ($group) {
                $staffGenerator = $this->entityManager->find(
                        new Staff(),
                        [
                            'group_id' => $group->getId(),
                            'character_id' => $character->getId(),
                            'position' => $position
                        ]
                    );

                foreach($staffGenerator as $staff) {
                    if ($staff) {
                        return $this->entityManager->delete($staff);
                    }
                }
            }
        }

        return false;
    }

    protected function addStaffMember(GuildEntity $guild, Character $character, int $position) : bool
    {
        /** @var GuildGroup[] $groups */
        $groups = $this->getGroupsByGuild($guild);
        foreach($groups as $group) {
            if ($group) {

                $staff = new Staff();
                $staff->setCharacterId($character->getId());
                $staff->setGroupId($group->getId());
                $staff->setPosition($position);

                return $this->entityManager->save($staff);
            }
        }

        return false;
    }


    /**
     * Return a list of guilds the character is allowed to administer
     *
     * @param Character $character
     *
     * @return GuildEntity[]
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function getCharacterStaffPositions(Character $character)
    {
        $guilds = [];

        /** @var Staff[] $positions */
        $positions = $this->entityManager->find(new Staff(), ['character_id' => $character->getId()]);
        foreach($positions as $position) {
            if ($position) {

                /** @var GuildGroup $group */
                $group = $this->entityManager->load(new GuildGroup(), $position->getGroupId());
                if ($group) {

                    $groupGuilds = $this->getGuildsByGroup($group);
                    foreach ($groupGuilds as $groupGuild) {
                        if ($groupGuild) {
                            $guilds[$groupGuild->getId()] = $groupGuild;
                        }
                    }
                }
            }
        }

        return $guilds;
    }
}
