<?php

namespace Modules\Guilds;

use Game\Helper\Output;
use Modules\Colors\Helper\Color;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Room as RoomHelper;

class Module extends \Game\Module\Module
{
    /**
     * @throws \Game\Exception
     */
    public function init()
    {
        $characterHelper = new CharacterHelper(new RoomHelper($this->getEntityManager()));

        $guildHelper  = new Helper\Guild($this->getEntityManager());
        $outputHelper = new Output();
        $colorHelper  = new Color();

        $this->getInput()->registerCommands([
            'glist' => new Command\Glist($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper, $guildHelper, $colorHelper),
            'gcom'  => new Command\Gcom($this->getEntityManager(), $this->getInput(), $this->getOutput(), $guildHelper, $characterHelper, $outputHelper)
        ]);
    }
}
