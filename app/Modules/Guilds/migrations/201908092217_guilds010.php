<?php

use Phinx\Migration\AbstractMigration;

class Guilds010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('guild_groups');
        $table
            ->addColumn('name', 'string')
            ->addColumn('label', 'string')
            ->addTimestamps()
            ->addIndex('name', ['unique' => true])
            ->save();


        $table = $this->table('guilds');
        $table
            ->addColumn('group_id', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('label', 'string')
            ->addTimestamps()
            ->addIndex('name', ['unique' => true])
            ->addForeignKey('group_id', 'guild_groups', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->save();

        $table = $this->table('guild_ranks');
        $table
            ->addColumn('guild_id', 'integer')
            ->addColumn('position', 'integer')
            ->addColumn('rank', 'integer')
            ->addTimestamps()
            ->addForeignKey('guild_id', 'guilds', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->save();

        $table = $this->table('guild_staff');
        $table
            ->addColumn('guild_id', 'integer')
            ->addColumn('character_id', 'integer')
            ->addColumn('position', 'integer')
            ->addIndex('guild_id')
            ->addIndex('character_id')
            ->addIndex(['guild_id', 'character_id'], ['unique' => true])
            ->addForeignKey('guild_id', 'guilds', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->addForeignKey('character_id', 'characters', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->save();
    }
}
