<?php

use Phinx\Migration\AbstractMigration;

class Guilds020 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('guild_relations');
        $table
            ->addColumn('group_id', 'integer')
            ->addColumn('guild_id', 'integer')
            ->addTimestamps()
            ->addIndex('group_id')
            ->addIndex('guild_id')
            ->addIndex(['group_id', 'guild_id'], ['unique' => true])
            ->addForeignKey('group_id', 'guild_groups', 'id', ['delete' => 'CASCADE'])
            ->addForeignKey('guild_id', 'guilds', 'id', ['delete' => 'CASCADE'])
            ->save();

        $table = $this->table('guilds');
        $table
            ->dropForeignKey('group_id')
            ->save();

        $table = $this->table('guilds');
        $table
            ->removeColumn('group_id')
            ->save();

        $table = $this->table('guild_staff');
        $table
            ->renameColumn('guild_id', 'group_id')
            ->save();
    }
}
