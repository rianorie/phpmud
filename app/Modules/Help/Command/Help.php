<?php

namespace Modules\Help\Command;

use Game\Connection\Interfaces\Connection;
use Game\Helper\Output as OutputHelper;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use Modules\Help\Entity\Help as HelpEntity;

class Help extends \Game\Module\Command
{
    /**
     * @var OutputHelper
     */
    protected $outputHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $outputHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->outputHelper = $outputHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var HelpEntity[] $records */
        $records = $this->getEntityManager()->find(new HelpEntity(), ['keywords' => '%' . $input . '%']);

        $output = sprintf("Help files for '%s'\n", $input);

        foreach($records as $record) {
            if ($record) {
                $output .= $record->getTopic()."\n";
                $output .= $record->getKeywords()."\n";
                $output .= $this->outputHelper->format($record->getDescription())."\n\n";
            }
        }

        $this->getOutput()->write($connection, $output);
        return;
    }
}
