<?php

namespace Modules\Help\Entity;

use Game\Module\Entity\Entity;

/**
 * Help entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class Help extends Entity
{
    /**
     * @var string
     */
    public $topic;

    /**
     * @var string
     */
    public $description;

    /**
     * @var array
     */
    public $keywords;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'help';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return [
            'id',
            'topic',
            'description'
        ];
    }

    /**
     * Return the help topic
     *
     * @return string
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Return the help description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Return the help keywords
     *
     * @return array
     */
    public function getKeywords()
    {
        return $this->keywords;
    }
}
