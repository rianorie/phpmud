<?php

namespace Modules\Help;

use Game\Helper\Output as OutputHelper;

class Module extends \Game\Module\Module
{
    /**
     * Set up the help module
     *
     * @throws \Game\Exception
     */
    public function init()
    {
        $outputHelper = new OutputHelper();

        $this->getInput()->registerCommands([
            'help' => new Command\Help($this->getEntityManager(), $this->getInput(), $this->getOutput(), $outputHelper)
        ]);
    }
}
