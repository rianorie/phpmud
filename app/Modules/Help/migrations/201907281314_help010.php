<?php

use Phinx\Migration\AbstractMigration;

class Help010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('help');
		$table->addColumn('topic', 'string')
			  ->addColumn('description', 'text')
              ->addColumn('keywords', 'text')
			  ->addIndex('topic', ['unique' => true])
		      ->save();
	}
}
