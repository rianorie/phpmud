<?php

namespace Modules\Intro;

class Module extends \Game\Module\Module
{
    public function init()
    {
        $stateHelper = new \Modules\World\Helper\Character\State();

        $this->getEvent()->observe(
            'game.after_connection',
            new Observer\ShowWelcome($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper),
            -1
        );
    }
}
