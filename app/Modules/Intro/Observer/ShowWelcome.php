<?php

namespace Modules\Intro\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Helper\Character\State as StateHelper;
use stdClass;

class ShowWelcome extends Observer
{
    /**
     * @var StateHelper
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->stateHelper = $stateHelper;
    }

    public function fire(stdClass $data)
    {
        if ($this->stateHelper->getState($data->connection) == $this->stateHelper::INACTIVE) {

            $this->getOutput()->write($data->connection, "{r Prophecies of the Pattern - A WoT MUD", true, true);
            $this->getOutput()->write($data->connection, "{w Administration by Bellas", true, true);
            $this->getOutput()->write($data->connection, "{w Code by Aldron", true, true);
            $this->getOutput()->write($data->connection, "{w Maintained by the staff", true, true);
            $this->getOutput()->write($data->connection, "{w Immortal Emeritus: Pacis, Ryghen, Nuri", true, true);

            $this->getOutput()->write($data->connection, "\n{x        PHPMUD 0.1 based on DikuMUD", true, true);

            $this->getOutput()->write($data->connection, "\r\n", true, true);
        }
    }
}
