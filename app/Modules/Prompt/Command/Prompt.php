<?php

namespace Modules\Prompt\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;

class Prompt extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        if (empty($input)) {
            $this->getInput()->handle($connection, 'toggle prompt');
            return;
        }

        $connection->getData('character')->setMetadata('prompt', $input);
        $this->getEntityManager()->update($connection->getData('character'), 'metadata');
        $this->getOutput()->write($connection, 'Prompt saved!');
    }
}
