<?php

namespace Modules\Prompt;

use Game\Exception;
use Modules\Toggle\Utility as ToggleUtility;
use Modules\World\Helper\Character\State as StateHelper;

class Module extends \Game\Module\Module
{
    /**
     * Set up the communication module
     *
     * @throws Exception
     */
    public function init()
    {
        $stateHelper = new StateHelper();

        ToggleUtility::registerToggle('Prompt', 'General', 'Prompt', true);
        ToggleUtility::registerToggle('Compact', 'General', 'Compact output', false);

        $this->getEvent()->observe(
            'output.before_write',
            new Observer\OutputPrompt($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper)
        );

        $this->getEvent()->observe(
            'world.after_character_created',
            new Observer\SetDefaultPrompt($this->getEntityManager(), $this->getInput(), $this->getOutput())
        );

        $this->getInput()->registerCommand(
            'prompt',
            new Command\Prompt($this->getEntityManager(), $this->getInput(), $this->getOutput())
        );
    }
}
