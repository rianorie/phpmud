<?php

namespace Modules\Prompt\Observer;

use Exception;
use Game\Input;
use Game\Module\Entity\Entity;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Helper\Character\State as StateHelper;
use stdClass;

use Modules\Toggle\Utility as ToggleUtility;
use Modules\World\Character\State;

class OutputPrompt extends Observer
{
    protected $replacements = [
        '%h' => 'hitpoints',
        '%H' => 'max_hitpoints',
        '%m' => 'stamina',
        '%M' => 'max_stamina'
    ];

    /**
     * @var StateHelper
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
    }

    /**
     * @param stdClass   $data
     *
     * @throws Exception
     */
    public function fire(stdClass $data)
    {
        /** @var Character $char */
        $char = $data->connection->getData('character');

        // if the character is active
        if ($this->stateHelper->getState($data->connection) == $this->stateHelper::ACTIVE) {

            // if compact output is disabled, add another blank line
            if ( ! ToggleUtility::getToggle($char, 'compact')) {
                $data->output .= "\n";
            }

            // if prompt is enabled
            if (ToggleUtility::getToggle($char, 'prompt')) {
                $data->output .= "\n".$this->getPrompt($char)."{x\r\n";
            }

        } elseif ($this->stateHelper->getState($data->connection) == $this->stateHelper::AFK) {

            // if compact output is disabled, add another blank line
            if ( ! ToggleUtility::getToggle($char, 'compact')) {
                $data->output .= "\n";
            }

            $data->output .= "\n{c[{wAFK{c]\r\n";
        }
    }

    /**
     * Generate a prompt with replacements
     *
     * @param Entity $character
     *
     * @return string
     */
    protected function getPrompt(Entity $character) : string
    {
        /** @var Character $character */

        $prompt = str_replace('%%', '_#_', $character->getMetadata('prompt'));

        while(($pos = strpos($prompt, '%')) !== false) {
            $mod    = substr($prompt, $pos, 2);
            $prompt = str_replace($mod, $character->{$this->replacements[$mod]}, $prompt);
        }

        return str_replace('_#_', '%', $prompt);
    }
}
