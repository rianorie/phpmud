<?php

namespace Modules\Prompt\Observer;

use Game\Module\Entity\Exception;
use Game\Module\Observer;
use stdClass;

class SetDefaultPrompt extends Observer
{
    public function fire(stdClass $data)
    {
        if ($data->connection->getData('character')) {
            $data->connection->getData('character')->setMetadata('prompt', '[%hhp %mst] ');

            try {

                $this->getEntityManager()->update($data->connection->getData('character'), 'metadata');

            } catch (Exception $e) {
                #TODO determine what to do here
            }
        }
    }
}
