<?php

namespace Modules\Socials\Command;

use Game\Connection\Interfaces\Connection;
use Game\Helper\Output as OutputHelper;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;

class Social extends Command
{
    protected $socials = [];

    /**
     * @var OutputHelper
     */
    protected $outputHelper;

    public function __construct(EntityManager $entityManager, Input $input, \Game\Output $output, $socials, $outputHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->socials = $socials;
        $this->outputHelper = $outputHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $this->getOutput()->write($connection, $this->outputHelper->getColumnized($this->socials, 5, true));
    }
}
