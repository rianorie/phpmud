<?php

namespace Modules\Socials\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Entity\Room;
use Modules\World\Table\Position;

class VirtualSocialCommand extends Command
{
    /**
     * @var \Modules\Socials\Entity\Social
     */
    protected $social;

    /**
     * @var \Modules\World\Helper\Character
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $social, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->social = $social;

        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var Character $character */
        $character = $connection->getData('character');

        if ($character->getPosition() == Position::SLEEP) {
            $this->getOutput()->write($connection, 'You must wake up before doing that.');
            return;
        }

        /** @var Room $room */
        $room = $character->getData('room');

        /** @var Character[] $others */
        $others = $this->characterHelper->getOnlineRoomCharacters($room, $character);

        // no target given
        if (empty($input)) {
            // to self
            $this->getOutput()->write($connection, $this->getFinalString($character, $this->social->getSolo()));

            // to the room
            foreach ($others as $other) {
                if ($other) {
                    $this->getOutput()->write(
                        $other->getConnection(),
                        $this->getFinalString($character, $this->social->getSoloRoom())
                    );
                }
            }

            return;
        }

        // first lets check if the character is the intended target
        if (strtolower($input) == 'self' || stripos($character->getName(), $input) !== false) {

            // to self
            $this->getOutput()->write($connection, $this->getFinalString($character, $this->social->getToSelf()));

            // to the room
            foreach ($others as $other) {
                if ($other) {
                    $this->getOutput()->write(
                        $other->getConnection(),
                        $this->getFinalString($character, $this->social->getToSelfRoom())
                    );
                }
            }

            return;
        }

        // then lets see if it's a target that's present

        $target = false;
        foreach($others as $other) {
            if ($other && stripos($other->getName(), $input) !== false) {
                $target = $other;
            }
        }

        // if the target isn't present..
        if ( ! $target) {

            if (empty($this->social->getNoTarget())) {
                $this->getOutput()->write($connection, 'They aren\'t here.');
            } else {
                $this->getOutput()->write($connection, $this->getFinalString($character, $this->social->getNoTarget()));
            }
            return;
        }

        // finally, output the targeted social

        $this->getOutput()->write($connection, $this->getFinalString($character, $this->social->getToVictimSelf(), $target));
        $this->getOutput()->write($target->getConnection(), $this->getFinalString($character, $this->social->getToVictim(), $target));

        foreach($others as $other) {
            if ($other && $other != $target) {
                $this->getOutput()->write(
                    $other->getConnection(),
                    $this->getFinalString($character, $this->social->getToVictimRoom(), $target)
                );
            }
        }
    }

    protected function getFinalString(Character $char, string $string, Character $target = null)
    {
        $replacements = [
            '$n' => $char->getName(),
            '$m' => ($char->getGender() == 1 ? 'him' : 'her'),
            '$s' => ($char->getGender() == 1 ? 'his' : 'her'),
            '$e' => ($char->getGender() == 1 ? 'he' : 'she'),
            ];

        if ($target) {
            $replacements['$N'] = $target->getName();
            $replacements['$M'] = ($target->getGender() == 1 ? 'him' : 'her');
            $replacements['$S'] = ($target->getGender() == 1 ? 'his' : 'her');
            $replacements['$E'] = ($target->getGender() == 1 ? 'he' : 'she');
        }

        return '{w'.str_replace(array_keys($replacements), array_values($replacements), $string).'{x';
    }
}
