<?php

namespace Modules\Socials\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * Character entity
 *
 * @package PHPMUD\Socials
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class Social extends Entity
{
    use Timestamp;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $solo;

    public $solo_room;

    /**
     * @var string
     */
    public $toself;

    public $toself_room;

    /**
     * @var string
     */
    public $tovictim;

    public $tovictim_room;

    public $tovictim_self;

    /**
     * @var string
     */
    public $notarget;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'socials';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return [
            'id',
            'name',
            'solo',
            'solo_room',
            'toself',
            'toself_room',
            'tovictim',
            'tovictim_room',
            'tovictim_self',
            'notarget',
            'created_at',
            'updated_at'
        ];
    }


    /**
     * Return the social name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getSolo()
    {
        return $this->solo;
    }

    public function getSoloRoom()
    {
        return $this->solo_room;
    }

    public function getToSelf()
    {
        return $this->toself;
    }

    public function getToSelfRoom()
    {
        return $this->toself_room;
    }

    public function getToVictim()
    {
        return $this->tovictim;
    }

    public function getToVictimRoom()
    {
        return $this->tovictim_room;
    }

    public function getToVictimSelf()
    {
        return $this->tovictim_self;
    }

    public function getNoTarget()
    {
        return $this->notarget;
    }
}
