<?php

namespace Modules\Socials;

use Game\Helper\Output as OutputHelper;
use Modules\Socials\Entity\Social;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Room as RoomHelper;

class Module extends \Game\Module\Module
{
    /**
     * Set up the socials module
     *
     * @throws \Game\Exception
     */
    public function init()
    {
        $characterHelper = new CharacterHelper(new RoomHelper($this->getEntityManager()));
        $outputHelper = new OutputHelper();

        $social = new Social();

        $socials = [];

        // generate a callback list of the available socials
        /** @var Social $item */
        foreach ($this->getEntityManager()->find($social) as $item) {
            if ($item != false) {
                $socials[$item->getName()] = new Command\VirtualSocialCommand($this->getEntityManager(), $this->getInput(), $this->getOutput(), $item, $characterHelper);
            }
        }

        // Check if "command not found" happens because we're trying a social
        $this->getEvent()->observe(
            'input.not_found',
            new Observer\HandleSocial($this->getEntityManager(), $this->getInput(), $this->getOutput(), $socials)
        );

        // have a command that lists the available socials
        $this->getInput()->registerCommands([
            'social' => new Command\Social($this->getEntityManager(), $this->getInput(), $this->getOutput(), array_keys($socials), $outputHelper),
        ], $this->getInput()::PRIORITY_LOW);
    }
}
