<?php

namespace Modules\Socials\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\Socials\Entity\Social;
use stdClass;

class HandleSocial extends Observer
{
    /**
     * @var Social[]
     */
    protected $socials;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $socials)
    {
        parent::__construct($entityManager, $input, $output);

        $this->socials = $socials;
    }

    public function fire(stdClass $data)
    {
        // clean off anything that shouldn't be at the start of the line and then separate the input
        $arguments = explode(' ', rtrim($data->input));

        if (isset($arguments[0])) {

            // clean the input. ltrim potential whitespaces, then ltrim the first argument then trim potential whitespaces again
            $cleaned = trim(ltrim(ltrim($data->input), $arguments[0]));

            // loop through the commands and
            foreach($this->socials as $command => $callback) {
                if (stripos($command, $arguments[0]) !== false) {

                    // fire the command off since we're allowed access
                    call_user_func_array([$callback, 'execute'], [$data->connection, $cleaned, $command, $data->input]);

                    // stop processing further events
                    return \Game\Event::STOP;
                }
            }
        }
    }
}
