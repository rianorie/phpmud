<?php

use Phinx\Migration\AbstractMigration;

class Social010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('socials');
		$table->addColumn('name', 'string')
			  ->addColumn('solo', 'string')
			  ->addColumn('toself', 'string')
			  ->addColumn('tovictim', 'string')
			  ->addColumn('toroom', 'string')
			  ->addColumn('created_on', 'datetime')
			  ->addIndex('name', ['unique' => true])
		      ->save();
	}
}
