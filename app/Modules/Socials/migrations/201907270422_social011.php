<?php

use Phinx\Migration\AbstractMigration;

class Social011 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('socials');
    	$table
            ->addColumn('solo_room', 'string', ['after' => 'solo'])
            ->addColumn('tovictim_self', 'string', ['after' => 'tovictim'])
            ->addColumn('tovictim_room', 'string', ['after' => 'tovictim'])
            ->renameColumn('toroom', 'notarget')
            ->addColumn('toself_room', 'string', ['after' => 'toself'])
            ->save();
	}
}

/*
solo        {wYou lift up your shirt and wait for someone to blow into your navel...
solo_room   {w$n lifts up $s shirt and waits for someone to blow into $s navel...

tovictim_self {wYou lift $N's shirt and blow loudly into $S navel!
tovictim_room {w$n lifts $N's shirt and blows loudly into $S navel!
tovictim      {wYeek!  $n lifts your shirt and blows loudly into your navel!

notarget   {wYou missed.

toself   {wCareful, people are staring...
toself_room {w$n looks around furtively, and then bends over and blows loudly into $s own navel!

 */
