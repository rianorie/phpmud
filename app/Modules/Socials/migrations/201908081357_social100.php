<?php

use Phinx\Migration\AbstractMigration;

class Social100 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('socials');
    	$table
            ->removeColumn('created_on')
            ->save();

    	$table
            ->addTimestamps()
            ->save();
	}
}
