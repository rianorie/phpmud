<?php

namespace Modules\Staff\Command;

use Game\Connection\Interfaces\Connection;
use Game\Event;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Entity\Location;
use Modules\World\Entity\Room;

class GotoCmd extends Command
{
    protected $event;

    /**
     * @var \Modules\World\Helper\Character
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, Event $event, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->event = $event;
        $this->characterHelper = $characterHelper;
    }


    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var Character $character */
        $character = $connection->getData('character');
        $room = $character->getData('room');

        if (empty($input)) {
            $this->getOutput()->write(
                $connection,
                "You can use goto to jump to another location in the game.\n".
                "Syntax: goto [id]            This will take you to a room id.\n".
                "Syntax: goto room [id]\n".
                "Syntax: goto char [name]"
            );
        }

        list($type, $target) = explode(' ', strtolower($input));

        try {

            if ( ! in_array($type, ['room', 'char']) && ! is_numeric($type)) {
                $this->getOutput()->write($connection, 'Only goto room or char works right now.');
                return;
            }

            if ($type == 'room' || is_numeric($type)) {

                /** @var Room $newRoom */
                $newRoom = $this->getEntityManager()->load(new Room, (is_numeric($type) ? $type : $target));
                if ( ! $newRoom) {
                    $this->getOutput()->write($connection, "That's not a valid location.");
                    return;
                }

                $others = $this->characterHelper->getOnlineRoomCharacters($room, $character);
                foreach($others as $other) {
                    $this->getOutput()->write($other->getConnection(), ['%s leaves in a swirling mist.', $character->getName()]);
                }

                $this->getOutput()->write($connection, 'You leave in a swirling mist.');

                /** @var Location $location */
                $location = $character->getData('location');
                $location->setRoomId($type);

                $this->getEntityManager()->update($location, 'room_id');

                $character->setData('room', $newRoom);

                $this->getInput()->handle($connection, 'look');

                $others = $this->characterHelper->getOnlineRoomCharacters($newRoom, $character);
                foreach($others as $other) {
                    $this->getOutput()->write($other->getConnection(), ['%s arrives in a swirling mist.', $character->getName()]);
                }

                return;

            } elseif ($type == 'char') {

                if (empty($target)) {
                    $this->getOutput()->write($connection, 'Goto who?');
                    return;
                }

                if (stripos($character->getName(), $target) !== false) {
                    $this->getOutput()->write($connection, "Poof! You're here.");
                    return;
                }

                $gotoChar = false;

                $others = $this->characterHelper->getCharacters();
                foreach($others as $other) {
                    if (stripos($other->getName(), $target) !== false) {
                        $gotoChar = $other;
                        break;
                    }
                }

                if ( ! $gotoChar) {
                    $this->getOutput()->write($connection, "Can't find anyone by that name.");
                    return;
                }

                $others = $this->characterHelper->getOnlineRoomCharacters($room, $character);
                foreach($others as $other) {
                    $this->getOutput()->write($other->getConnection(), ['%s leaves in a swirling mist.', $character->getName()]);
                }

                $this->getOutput()->write($connection, 'You leave in a swirling mist.');

                /** @var Location $location */
                $location = $character->getData('location');
                $location->setRoomId($gotoChar->getData('room')->getId());

                $this->getEntityManager()->update($location, 'room_id');

                $character->setData('room', $gotoChar->getData('room'));

                $this->getInput()->handle($connection, 'look');

                $others = $this->characterHelper->getOnlineRoomCharacters($gotoChar->getData('room'), $character);
                foreach($others as $other) {
                    $this->getOutput()->write($other->getConnection(), ['%s arrives in a swirling mist.', $character->getName()]);
                }

                return;
            }

            $this->getOutput()->write($connection, 'Goto what?');
            return;

        } catch (\Exception $exception) {

            $this->getOutput()->write($connection, ["Goto failed.\nException %s\n", $exception->getMessage()]);
        }
    }
}
