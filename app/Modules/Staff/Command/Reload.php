<?php

namespace Modules\Staff\Command;

use Game\Connection\Interfaces\Connection;
use Game\Event;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Area;
use Modules\World\Entity\Character;
use Modules\World\Entity\Room;

class Reload extends Command
{
    protected $event;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, Event $event)
    {
        parent::__construct($entityManager, $input, $output);
        $this->event = $event;
    }


    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var Character $character */
        $character = $connection->getData('character');

        /** @var Room $room */
        $room = $character->getData('room');

        try {

            /** @var Area $area */
            $area = $this->getEntityManager()->load(new Area(), $room->getAreaId());

            $this->getOutput()->write($connection, ['Reloading %s!', $area->getName()]);

            $this->event->dispatch('world.area.entering', ['area' => $area, 'connection' => $connection]);

        } catch (\Exception $exception) {

            $this->getOutput()->write($connection, ["Reloading failed.\nException %s\n", $exception->getMessage()]);
        }
    }
}
