<?php

namespace Modules\Staff\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;

class Restart extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        foreach($connection->getManager()->getAll() as $conn) {
            $this->getOutput()->write($conn, "{W***\n\n{wRestarting game, hold on.\n\n{W***{x");
        }

        exit();
    }
}
