<?php

namespace Modules\Staff\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use Modules\World\Helper\Character\State as StateHelper;

class Sockets extends Command
{
    /**
     * @var StateHelper
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $output = sprintf("%4s %-10s %-20s %-10s %-15s\n", 'ID', 'State', 'Connected', 'Name', 'Host');
        $output .= "----------------------------------------------------------------\n";

        foreach($connection->getManager()->getAll() as $con) {
            if ($con) {
                $char = $con->getData('character');

                $state = '';
                switch($this->stateHelper->getState($con)) {
                    case $this->stateHelper::ACTIVE:
                        $state = 'Active';
                        break;
                    case $this->stateHelper::INACTIVE:
                        $state = 'Inactive';
                        break;
                    case $this->stateHelper::NOTE:
                        $state = 'Note';
                        break;
                    case $this->stateHelper::AFK:
                        $state = 'AFK';
                        break;
                }

                $output .= sprintf("%4d %-10s %-20s %-10s %-15s\n", $con->getId(), $state, date('Y-m-d H:i:s', $con->getData('connected')), ($char ? $char->getName() : ''), $con->getData('address'));
            }
        }

        $this->getOutput()->write($connection, $output);
    }
}
