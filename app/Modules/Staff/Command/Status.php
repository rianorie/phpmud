<?php

namespace Modules\Staff\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Manager;
use Game\Output;

class Status extends Command
{
    /**
     * @var Manager
     */
    protected $moduleManager;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, Manager $moduleManager)
    {
        parent::__construct($entityManager, $input, $output);
        $this->moduleManager = $moduleManager;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $output  = "Memory usage\n-------------------------\n";
        $output .= sprintf("{xMemory: %gkb (%gkb)\nPeak:   %gkb (%gkb)\n",
            round(floatval(memory_get_usage()/1024), 2),
            round(memory_get_usage(true)/1024, 2),
            round(memory_get_peak_usage()/1024, 2),
            round(memory_get_peak_usage(true)/1024, 2));

        $output .= "\nLoaded modules\n-------------------------\n";

        foreach($this->moduleManager->getModules(true) as $module) {
            $output .= sprintf("%s (v%s)", $module->getData('name'), $module->getData('version'))."\n";
        }

        $this->getOutput()->write($connection, $output);
    }
}
