<?php

namespace Modules\Staff\Command;

use Game\Connection\Interfaces\Connection;
use Game\Helper\Output as OutputHelper;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Manager;
use Game\Output;
use Modules\World\Entity\Character;

class Wizhelp extends Command
{
    protected $commands;

    /**
     * @var OutputHelper
     */
    protected $outputHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $outputHelper, $commands)
    {
        parent::__construct($entityManager, $input, $output);

        $this->commands = $commands;
        $this->outputHelper = $outputHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var Character $char */
        $char = $connection->getData('character');

        $output = 'Sorry, no staff related commands available.';

        $availableCommands = [];

        foreach($this->commands as $flag => $commandList) {
            if ($char->hasFlag($flag)) {
                $availableCommands = array_merge($availableCommands, array_keys($commandList));
            }
        }

        if (count($availableCommands)) {
            sort($availableCommands);

            $output = $this->outputHelper->getColumnized($availableCommands, 6, true);
        }


        $this->getOutput()->write($connection, $output);
    }
}
