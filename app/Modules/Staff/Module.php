<?php

namespace Modules\Staff;

use Game\Exception;
use Game\Helper\Output as OutputHelper;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Room as RoomHelper;

class Module extends \Game\Module\Module
{
    protected $restorePath;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * Set up the staff related features
     *
     * @throws Exception
     */
    public function init()
    {
        $this->restorePath = HOMEDIR.'/var/character.json';

        $this->characterHelper = new CharacterHelper(new RoomHelper($this->getEntityManager()));
        $stateHelper = new CharacterHelper\State();
        $outputHelper = new OutputHelper();

        // structure like 'flag' => ['command' => callback, etc]
        $commands = [
            'staff' => [
                'status'  => new Command\Status($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getModuleManager()),
            ],
            'immortal' => [
                'sockets' => new Command\Sockets($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper),
                'restart' => new Command\Restart($this->getEntityManager(), $this->getInput(), $this->getOutput()),
                'reload' => new Command\Reload($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getEvent()),
                'goto' => new Command\GotoCmd($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getEvent(), $this->characterHelper)
            ]
        ];

        // register a command to get a list of the available staff commands
        $this->getInput()->registerCommand(
            'staffcommands',
            new Command\Wizhelp($this->getEntityManager(), $this->getInput(), $this->getOutput(), $outputHelper, $commands),
            $this->getInput()::PRIORITY_LOW
        );

        // check if the input a staff command
        $this->getEvent()->observe(
            'input.not_found',
            new Observer\HandleStaffCommands($this->getEntityManager(), $this->getInput(), $this->getOutput(), $commands)
        );

        // set up the restore-after-crash-or-copyover system
        $this->getEvent()->observe(
            'game.after_connection',
            new Observer\RestoreCharacters(
                $this->getEntityManager(), $this->getInput(), $this->getOutput(),
                $this->getEvent(), $this->restorePath, $this->characterHelper,
                $stateHelper
            ),
            -10
        );
    }

    /**
     * Store the character data on a persistent storage location
     *
     * @throws \Exception
     */
    public function __destruct()
    {
        $data = [];
        foreach ($this->characterHelper->getCharacters() as $char) {
            $data[] = ['external' => $char->getConnection()->getData('socket.external'), 'character' => $char->getId()];
        }

        file_put_contents($this->restorePath, json_encode($data));
    }
}
