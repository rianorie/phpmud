<?php

namespace Modules\Staff\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\Character;
use stdClass;

class HandleStaffCommands extends Observer
{
    /**
     * @var array
     */
    protected $commandList;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $commands)
    {
        parent::__construct($entityManager, $input, $output);

        $this->commandList = $commands;
    }

    public function fire(stdClass $data)
    {
        /** @var Character $char */
        $char = $data->connection->getData('character');

        if ( ! $char) { return 0; }

        $commandList = [];

        foreach($this->commandList as $flag => $commands) {
            if ($char->hasFlag($flag)) {
                $commandList = array_merge($commandList, $commands);
            }
        }

        if (count($commandList)) {

            // clean off anything that shouldn't be at the start of the line and then separate the input
            $arguments = explode(' ', rtrim($data->input));

            if (isset($arguments[0])) {

                // clean the input. ltrim potential whitespaces, then ltrim the first argument then trim potential whitespaces again
                $cleaned = trim(ltrim(ltrim($data->input), $arguments[0]));

                // loop through the commands and
                foreach($commandList as $command => $callback) {
                    if (stripos($command, $arguments[0]) !== false) {

                        // fire the command off since we're allowed access
                        call_user_func_array([$callback, 'execute'], [$data->connection, $cleaned, $command, $data->input]);

                        // clear the input to the regular parser
                        $data->input = null;
                        $data->arguments = null;
                        return \Game\Event::STOP;
                    }
                }
            }
        }
    }
}
