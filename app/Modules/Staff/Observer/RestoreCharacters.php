<?php

namespace Modules\Staff\Observer;

use Game\Event;
use Game\Input;
use Game\Module\Entity\Exception as EntityException;
use Game\Module\Entity\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\Character as CharacterEntity;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Observer\CharacterCreation;
use stdClass;

class RestoreCharacters extends Observer
{
    /**
     * @var \Game\Event
     */
    protected $event;

    protected $restorePath;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * @var CharacterHelper\State
     */
    protected $stateHelper;

    public function __construct(
        EntityManager $entityManager,
        Input $input,
        Output $output,
        Event $event,
        string $restorePath,
        $characterHelper,
        $stateHelper
    )
    {
        parent::__construct($entityManager, $input, $output);
        $this->event = $event;
        $this->restorePath = $restorePath;
        $this->characterHelper = $characterHelper;
        $this->stateHelper = $stateHelper;
    }

    public function fire(stdClass $data)
    {
        if (file_exists($this->restorePath)) {

            $restore = file_get_contents($this->restorePath);
            $restore = json_decode($restore, true);
            if ($restore) {

                foreach($restore as $pos => $row) {

                    if ($row['external'] == $data->connection->getData('socket.external')) {

                        $character = new CharacterEntity($this->getEntityManager());
                        $character->setConnection($data->connection);
                        try {
                            $character = $this->getEntityManager()->load($character, $row['character']);
                            $data->connection->setData('character', $character);

                            // clear the creation stuff
                            $this->stateHelper->setState($data->connection, $this->stateHelper::ACTIVE);
                            $data->connection->setData('creation', CharacterCreation::STATE_DONE);
                            $data->connection->writeLock(false);

                            $this->characterHelper->addCharacter($data->connection->getData('character'));

                            $this->event->dispatch('character.after_init', ['character' => $character]);

                            unset($restore[$pos]);

                        } catch (EntityException $e) {

                            #TODO what to do here..

                        } catch (\Exception $e) {

                            #TODO or here
                        }
                    }
                }

                // if there's records left
                if (count($restore) > 0) {
                    file_put_contents($this->restorePath, json_encode($restore));

                } else {
                    unlink($this->restorePath);

//                    $characters = Utilities::getCharacters();
//                    foreach($characters as $character) {
//                        $this->getOutput()->write($character->getConnection(), "AFTER RESTART\n");
//                    }
                }
            }

            return \Game\Event::STOP;
        }
    }
}
