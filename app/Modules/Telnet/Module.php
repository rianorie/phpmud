<?php

namespace Modules\Telnet;

class Module extends \Game\Module\Module
{
    public function init()
    {
        $negotiateObserver = new Observer\Negotiate($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getLogger());

        $this->getEvent()->observe('input.before_process', $negotiateObserver, -1000);
    }
}
