<?php

namespace Modules\Telnet\Observer;

use Game\Event;
use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use JetBrains\PhpStorm\Pure;
use Modules\Telnet\Utilities;
use Psr\Log\LoggerInterface;
use stdClass;

class Negotiate extends Observer
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    #[Pure] public function __construct(EntityManager $entityManager, Input $input, Output $output, LoggerInterface $logger)
    {
        parent::__construct($entityManager, $input, $output);
        $this->logger = $logger;
    }

    public function fire(stdClass $data) : int
    {
        if (ord(substr($data->input, 0, 1)) == Utilities::$command['IAC']) {

            $iac = implode('.', array_map('ord', str_split($data->input)));

            $this->logger->debug('Captured IAC response: '.$iac);

            switch($iac) {
                // IAC DO ECHO
                case '255.253.1':
                    $this->logger->debug('Sending 255 252 1 (IAC WONT ECHO)');
                    $data->connection->write(chr(255) .' ' . chr(252).' '.chr(1));
                    break;

                // IAC DONT ECHO
// disabled because it writes them to the output
//                case '255.254.1':
//                    $this->logger->debug('Sending 255 251 1 (IAC WILL ECHO)');
//                    $data->connection->write(chr(255) .' ' . chr(251).' '.chr(1));
//                    break;
            }

            $data->input = '';
            $data->arguments = null;

            return Event::STOP;
        }

        return 0;
    }
}
