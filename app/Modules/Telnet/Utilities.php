<?php

namespace Modules\Telnet;

use Game\Connection\Interfaces\Connection;

class Utilities
{
    public static $command = [
        'IS'   => 0,
        'SEND' => 1,
        'INFO' => 2,
        'VAR'  => 0,
        'SE'   => 240,
        'NOP'  => 241,
        'DM'   => 242,
        'BRK'  => 243,
        'IP'   => 244,
        'AO'   => 245,
        'AYT'  => 246,
        'EC'   => 247,
        'EL'   => 248,
        'GA'   => 249,
        'SB'   => 250,
        'WILL' => 251,
        'WONT' => 252,
        'DO'   => 253,
        'DONT' => 254,
        'IAC'  => 255
    ];

    public static $option = [
        'BINARY'    => 0,
        'ECHO'      => 1,
        'RECONNECT' => 2,
        'SGA'       => 3,
        'STATUS'    => 5,
        'TIMING'    => 6,
        'WIDTH'     => 8,
        'HEIGHT'    => 9,
        'TYPE'      => 24,
        'NAWS'      => 31,
        'SPEED'     => 32,
        'FLOW'      => 33,
        'LINE'      => 34,
        'ENV'       => 36
    ];

    public static function disableVisibleOutput(Connection $connection)
    {
//        $connection->write(chr(TelnetUtility::$command['IAC']).chr(TelnetUtility::$command['WILL']).chr(TelnetUtility::$option['ECHO'])."\0", true);

        // IAC WILL ECHO -- Server says "I'll handle output"
        $input = [
            self::$command['IAC'],
            self::$command['WILL'],
            self::$option['ECHO']
        ];

        $connection->write(implode('', array_map('chr', $input))."\0", true);
    }

    public static function enableVisibleOutput(Connection $connection)
    {
        // IAC WILL ECHO -- Server says "I wont handle output"
        $input = [
            self::$command['IAC'],
            self::$command['WONT'],
            self::$option['ECHO'],

        ];

        $connection->write(implode('', array_map('chr', $input))."\0", true);
    }
}
