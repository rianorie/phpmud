<?php

namespace Modules\Toggle\Command;

use Game\Connection\Interfaces\Connection;
use Game\Helper\Output as OutputHelper;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use JetBrains\PhpStorm\Pure;
use Modules\Toggle\Utility as ToggleUtility;

class Toggle extends Command
{
    /**
     * @var OutputHelper
     */
    protected OutputHelper $outputHelper;

    #[Pure] public function __construct(EntityManager $entityManager, Input $input, \Game\Output $output, $outputHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->outputHelper = $outputHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $character = $connection->getData('character');

        $toggles = ToggleUtility::getCharacterToggles($character);

        if (trim($input) == '') {
            $outputToggles = [];

            foreach ($toggles as $toggle) {
                $outputToggles[$toggle['group']][] = sprintf(' %-10s %3s', $toggle['code'], ($toggle['state'] ? '{GON{x' : '{ROFF{x'));
            }

            $output = "-*- Communications and Configuration Status -*-";

            foreach ($outputToggles as $group => $groupToggles) {
                $output .= "\n" . $group . "\n";
                $output .= $this->outputHelper->getColumnized($groupToggles, 4, true);
                $output .= "\n";
            }

            $this->getOutput()->write($connection, $output);
            return;
        }

        foreach ($toggles as $code => $toggle) {
            if (strlen($input) > 1 && stripos($code, $input) === 0) {
                $input = $code;
                break;
            }
        }

        if ( ! isset($toggles[$input])) {
            $this->getOutput()->write($connection, ['You must specify an option to toggle. Type toggle alone to see a list.']);
            return;
        }

        if ($toggles[$input]['state'] == 1) {
            $this->getOutput()->write($connection, ['%s output disabled.', $toggles[$input]['label']]);
            ToggleUtility::setToggle($character, $input, false);

        } else if ($toggles[$input]['state'] == 0) {
            $this->getOutput()->write($connection, ['%s output enabled.', $toggles[$input]['label']]);
            ToggleUtility::setToggle($character, $input, true);
        }
    }
}
