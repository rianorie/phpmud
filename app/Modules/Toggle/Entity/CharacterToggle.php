<?php

namespace Modules\Toggle\Entity;

use Game\Module\Entity\Entity;

class CharacterToggle extends Entity
{
    public $character_id;

    public $code;

    public $state;

    public function getTable() : string
    {
        return 'character_toggles';
    }

    public function getColumns() : array
    {
        return [
            'character_id',
            'code',
            'state'
        ];
    }

    public function getCharacterId()
    {
        return $this->character_id;
    }

    public function setCharacterId(int $id)
    {
        $this->character_id = $id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode(string $code)
    {
        $this->code = $code;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setState(bool $state)
    {
        $this->state = (int) $state;
    }
}
