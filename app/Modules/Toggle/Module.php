<?php

namespace Modules\Toggle;

use Game\Exception;
use Game\Helper\Output as OutputHelper;

class Module extends \Game\Module\Module
{
    /**
     * Set up the toggle module
     * @throws Exception
     */
	public function init()
	{
	    $outputHelper = new OutputHelper();

        $this->getInput()->registerCommand(
            'toggle',
            new Command\Toggle($this->getEntityManager(), $this->getInput(), $this->getOutput(), $outputHelper)
        );
	}
}
