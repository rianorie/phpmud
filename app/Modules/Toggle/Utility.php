<?php

namespace Modules\Toggle;

use Game\Module\Entity\Exception;
use Modules\Toggle\Entity\CharacterToggle;
use Modules\World\Entity\Character;

class Utility
{
    /**
     * Register a settable toggle in the system
     *
     * @param string $code
     * @param string $group
     * @param string $label
     * @param bool   $state
     *
     * @throws \Game\Exception
     */
    public static function registerToggle(string $code, string $group, string $label, bool $state)
    {
        $toggles = [];

        try {
            if (\Game\Registry::exists('toggles')) {
                $toggles = \Game\Registry::get('toggles');
            }
        } catch (\Exception $exception) {
            // never gonna happen, exists prevents it
        }

        if (isset($toggles[strtolower($code)])) {
            throw new \Game\Exception('A toggle with that code already exists.');
        }

        $toggles[strtolower($code)] = [
            'code' => $code,
            'group' => $group,
            'label' => $label,
            'state' => $state
        ];

        \Game\Registry::set('toggles', $toggles);
    }

    /**
     * Retrieve a list of the toggles a character has
     *
     * @param Character $character
     *
     * @return array
     */
    public static function getCharacterToggles(Character $character)
    {
        $storedToggles = $character->getData('toggles');
        if ( ! is_null($storedToggles)) {
            return $storedToggles;
        }

        $entityManager = $character->getEntityManager();

        $toggleEntity = new CharacterToggle($entityManager);

        $toggles = [];

        try {

            if (\Game\Registry::exists('toggles')) {
                $toggles = \Game\Registry::get('toggles');
            }

        } catch (\Exception $exception) {
            // never gonna happen, exists check prevents the get exception
        }

        /** @var CharacterToggle[] $characterToggleCollection */
        $characterToggleCollection = $entityManager->find($toggleEntity, ['character_id' => $character->getId()]);
        foreach($characterToggleCollection as $characterToggle) {
            if ($characterToggle) {
                if (isset($toggles[$characterToggle->getCode()])) {
                    $toggles[$characterToggle->getCode()]['state'] = $characterToggle->getState();
                }
            }
        }

        $character->setData('toggles', $toggles);

        return $toggles;
    }

    /**
     * Get a specific toggle state for a character
     *
     * @param Character $character
     * @param string    $code
     *
     * @return boolean
     */
    public static function getToggle(Character $character, string $code)
    {
        $toggles = self::getCharacterToggles($character);
        return $toggles[strtolower($code)]['state'];
    }

    /**
     * Set a toggle for a character
     *
     * @param Character $character
     * @param string    $code
     * @param bool      $state
     *
     * @return boolean
     */
    public static function setToggle(Character $character, string $code, bool $state)
    {
        $code = strtolower($code);

        $entityManager = $character->getEntityManager();

        $toggleEntity = new CharacterToggle($entityManager);

        $return = false;

        $characterToggles = $entityManager->count($toggleEntity, ['character_id' => $character->getId(), 'code' => $code]);
        if ($characterToggles > 0) {

            /** @var CharacterToggle[] $characterToggleCollection */
            $characterToggleCollection = $entityManager->find($toggleEntity, ['character_id' => $character->getId(), 'code' => $code]);
            foreach ($characterToggleCollection as $characterToggle) {
                if ($characterToggle) {
                    $characterToggle->setState($state);

                    try {
                        $return = $entityManager->save($characterToggle);

                    } catch (Exception $exception) {
                        #TODO log the exception
                        return false;
                    }
                }
            }
        } else {

            $toggleEntity->setCharacterId($character->getId());
            $toggleEntity->setCode($code);
            $toggleEntity->setState($state);

            try {
                $return = $entityManager->save($toggleEntity);
            } catch (Exception $exception) {
                #TODO log the exception
                return false;
            }
        }

        $storedToggles = $character->getData('toggles');
        $storedToggles[$code]['state'] = $state;
        $character->setData('toggles', $storedToggles);

        return $return;
    }
}
