<?php

use Phinx\Migration\AbstractMigration;

class Toggle010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('character_toggles');
        $table
            ->addColumn('character_id', 'integer')
            ->addColumn('code', 'string')
            ->addColumn('state', 'boolean')
            ->addTimestamps()
            ->addIndex('character_id')
            ->addIndex('code')
            ->save();
    }
}
