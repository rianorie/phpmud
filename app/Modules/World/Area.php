<?php

namespace Modules\World;

use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Helper\Room as RoomHelper;

class Area extends \Game\Module\Module
{
    /**
     * Set up the Area module
     * @throws \Game\Exception
     */
    public function init()
    {
        $linkHelper      = new Helper\Link($this->getEntityManager());
        $itemHelper      = new ItemHelper($this->getEntityManager());
        $mobileHelper    = new Helper\Mobile($this->getEntityManager());
        $characterHelper = new Helper\Character(new RoomHelper($this->getEntityManager()));
        $stateHelper     = new Helper\Character\State();

        $this->getEvent()->observe(
            'world.area.entering',
            new Observer\AreaReloadResets($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $mobileHelper)
        );

        $this->getEvent()->observe(
            'game.pulse.slow_tick',
            new Observer\AreaSectorMessages($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper, $stateHelper)
        );

        $this->getEvent()->observe(
            'game.pulse.slow_tick',
            new Observer\MobileUpdate($this->getEntityManager(), $this->getInput(), $this->getOutput(), $mobileHelper)
        );

        $this->getEvent()->observe(
            'look.room',
            new Observer\LookRoom($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper, $linkHelper)
        );

        $this->getEvent()->observe(
            'character.after_init',
            new Observer\CharacterToRoom($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getConfig())
        );

        $commandMove = new Command\Move($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getEvent(), $characterHelper, $linkHelper);

        $this->getInput()->registerCommands([
            'north'     => $commandMove,
            'south'     => $commandMove,
            'east'      => $commandMove,
            'west'      => $commandMove,
            'up'        => $commandMove,
            'down'      => $commandMove,
            'northeast' => $commandMove,
            'northwest' => $commandMove,
            'southeast' => $commandMove,
            'southwest' => $commandMove
        ], $this->getInput()::PRIORITY_HIGH);

        $this->getInput()->registerCommands([
            'ne'    => $commandMove,
            'nw'    => $commandMove,
            'se'    => $commandMove,
            'sw'    => $commandMove,
            'exits' => new Command\Exits($this->getEntityManager(), $this->getInput(), $this->getOutput(), $linkHelper),
            'open'  => new Command\Open($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper, $linkHelper),
            'close' => new Command\Close($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper, $linkHelper)
        ], $this->getInput()::PRIORITY_NORMAL);
    }
}
