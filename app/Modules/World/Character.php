<?php

namespace Modules\World;

use Game\Helper\Output as OutputHelper;
use Modules\Colors\Helper\Color as ColorHelper;
use Modules\Toggle\Utility as ToggleUtility;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Helper\Room as RoomHelper;
use Modules\World\Table\Gender as GenderTable;
use Modules\World\Table\Race as RaceTable;

class Character extends \Game\Module\Module
{
    /**
     * Set up the character module
     *
     * @throws \Game\Exception
     */
    public function init()
    {
        ToggleUtility::registerToggle('logins', 'General', 'Logins', true);

        $mobileHelper    = new Helper\Mobile($this->getEntityManager());
        $characterHelper = new CharacterHelper(new RoomHelper($this->getEntityManager()));
        $stateHelper     = new CharacterHelper\State();
        $passwordHelper  = new CharacterHelper\Password();
        $outputHelper    = new OutputHelper();
        $raceTable       = new RaceTable();
        $genderTable     = new GenderTable();
        $itemHelper      = new ItemHelper($this->getEntityManager());

        // initiate the creation/login system
        $creationObserver = new Observer\CharacterCreation(
            $this->getEntityManager(),
            $this->getInput(),
            $this->getOutput(),
            $this->getEvent(),
            $characterHelper,
            $stateHelper,
            $passwordHelper,
            $outputHelper,
            $raceTable,
            $genderTable
        );
        $this->getEvent()->observe('game.after_connection', $creationObserver);
        $this->getEvent()->observe('input.before_process', $creationObserver);

        // update the idle time
        $this->getEvent()->observe(
            'input.before_process',
            new Observer\CharacterUpdateLastActive($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper)
        );

        // prevent commands from being executed if we're AFK
        $this->getEvent()->observe(
            'input.before_process',
            new Observer\CharacterPreventAfkInput($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper)
        );

        // prevent (some) commands from being executed if we're sleeping
        $this->getEvent()->observe(
            'input.before_command',
            new Observer\CharacterPreventInputSleeping($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper)
        );

        // if someone is idle too long, put them in AFK
        $this->getEvent()->observe(
            'game.pulse.tick',
            new Observer\CharacterSetAfk($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper, $characterHelper)
        );

        // if someone is idle too long, put them in AFK
        $this->getEvent()->observe(
            'game.pulse.tick',
            new Observer\CharacterHealTick($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getEvent(), $stateHelper, $characterHelper),
            10
        );

        // if someone looks at a character
        $this->getEvent()->observe(
            'look.character',
            new Observer\LookCharacter($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper)
        );

        // if someone looks at a mobile/npc
        $this->getEvent()->observe(
            'look.mobile',
            new Observer\LookMobile($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper, $mobileHelper)
        );

        $positionCommand = new Command\Position($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper);

        $this->getInput()->registerCommands([
            'quit'        => new Command\Quit($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getEvent(), $characterHelper, $stateHelper),
            'password'    => new Command\Password($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            'delete'      => new Command\Delete($this->getEntityManager(), $this->getInput(), $this->getOutput(), $this->getEvent()),
            'who'         => new Command\Who($this->getEntityManager(), $this->getInput(), $this->getOutput(), $characterHelper, $raceTable, $stateHelper),
            'afk'         => new Command\Afk($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper),
            'score'       => new Command\Score($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            'rpscore'     => new Command\Rpscore($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            'description' => new Command\Description($this->getEntityManager(), $this->getInput(), $this->getOutput(), new ColorHelper()),
            'sit'         => $positionCommand,
            'rest'        => $positionCommand,
            'sleep'       => $positionCommand,
            'stand'       => $positionCommand
        ]);
    }
}
