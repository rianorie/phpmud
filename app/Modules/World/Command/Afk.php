<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Output;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Modules\World\Helper\Character\State as StateHelper;

class Afk extends Command
{
    /**
     * @var StateHelper
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        // if we're currently AFK, switch back to not-being-afk
        if ($this->stateHelper->getState($connection) == StateHelper::AFK) {
            $this->stateHelper->setState($connection, StateHelper::ACTIVE);
            $this->getOutput()->write($connection, 'You return to the keyboard.');
            return;
        }

        $this->stateHelper->setState($connection, StateHelper::AFK);
        $this->getOutput()->write($connection, 'You step away from the keyboard. AFK mode implemented.');
    }
}
