<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Link;
use Modules\World\Entity\Room;
use Modules\World\Entity\RoomItem;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Helper\Link as LinkHelper;

class Close extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * @var LinkHelper
     */
    protected $linkHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper, $linkHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper      = $itemHelper;
        $this->characterHelper = $characterHelper;
        $this->linkHelper      = $linkHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        /** @var Room $room */
        $room = $character->getData('room');

        /** @var Link $link */
        $link = $this->linkHelper->getRoomLinkByInput($room, $input);
        if ($link) {

            if ( ! in_array('door', $link->getFlags())) {
                $this->getOutput()->write($connection, ["You can't do that."]);
                return;
            }

            if ( ! $this->linkHelper->isOpen($link)) {
                $this->getOutput()->write($connection, ["It's already closed."]);
                return;
            }

            $this->linkHelper->setOpen($link, false);

            $this->getOutput()->write($connection, ["You close the %s.", (empty($link->getKeyword()) ? 'door' : $link->getKeyword())]);

            $others = $this->characterHelper->getOnlineRoomCharacters($room, $character);
            foreach($others as $other) {
                $this->getOutput()->write(
                    $other->getConnection(),
                    [
                        '%s closes the %s to the %s.',
                        $character->getName(),
                        (empty($link->getKeyword()) ? 'door' : $link->getKeyword()),
                        $this->linkHelper->getDirectionNameForId($link->getDirection())
                    ]
                );
            }

            return;
        }


        // first try to find the item on the character
        $matchedItem = $this->itemHelper->getFirstCharacterItemByInput($character, $input);

        // if that fails, see if it's in the room
        if ( ! $matchedItem) {
            $matchedItem = $this->itemHelper->getFirstRoomItemByInput($room, $input);
        }

        // if we found it, use it
        if ($matchedItem) {

            if ( ! $this->itemHelper->canOpen($matchedItem)) {
                $this->getOutput()->write($connection, ["You can't close a %s.", $matchedItem->getShortDescription()]);
                return;
            }

            if ( ! $this->itemHelper->isOpen($matchedItem)) {
                $this->getOutput()->write($connection, ["%s is already closed.", $matchedItem->getShortDescription()]);
                return;
            }

            $this->itemHelper->setOpen($matchedItem, false);
            $this->getOutput()->write($connection, ['You close %s{x.', $matchedItem->getShortDescription()]);


            $others = $this->characterHelper->getOnlineRoomCharacters($room, $character);
            foreach($others as $other) {
                $this->getOutput()->write(
                    $other->getConnection(),
                    ['%s closes %s{x.', $character->getName(), $matchedItem->getShortDescription()]
                );
            }

            try {
                $this->getEntityManager()->update($matchedItem, 'metadata');

            } catch (Exception $e) {
                #TODO ..
            }

            return;
        }


        $this->getOutput()->write($connection, ["I see no %s here.", $input]);
    }
}
