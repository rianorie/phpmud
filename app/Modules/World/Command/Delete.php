<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Event;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;

class Delete extends Command
{
    /**
     * @var Event
     */
    protected $event;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, Event $event)
    {
        parent::__construct($entityManager, $input, $output);
        $this->event = $event;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {

        // if the raw input doesn't match the "delete" command exactly
        if (trim($raw) != $command) {

            // and the status is null, the received input was incomplete
            if ( is_null($connection->getData('delete'))) {
                $this->getOutput()->write($connection, 'If you want to DELETE, you have to spell it out.');

                // if the status is set, they've already at least once done it right
                // so we're removing the status now
            } else {
                $connection->setData('delete', null);
                $this->getOutput()->write($connection, 'Delete status removed.');
            }

            return;
        }

        // if delete was set, we're here for the second time around and this character needs to be deleted
        if ($connection->getData('delete')) {

            $this->event->dispatch('character.before_final_delete', ['connection' => $connection]);

            try {

                if ($this->getEntityManager()->delete($connection->getData('character'))) {

                    $this->getOutput()->write($connection, "We're sorry to see you go. Goodbye.");
                    $this->event->dispatch('character.after_final_delete', ['connection' => $connection]);

                    $this->getInput()->handle($connection, 'quit');

                } else {
                    $this->getOutput()->write($connection, 'Something went wrong while trying to delete your character.');
                }

            } catch (Exception $exception) {
                #TODO what to do here.?
                $this->getOutput()->write($connection, 'Something went wrong while trying to delete your character.');
            }

            return;

            // delete wasn't set yet, this is the first time the command has been executed.
            // warn the player
        } else {
            $connection->setData('delete', true);
            $this->getOutput()->write($connection, "Type delete again to confirm this command.\n"
                                                   ."WARNING: this command is irreversible.\n"
                                                   ."Typing delete with an argument will undo the delete status.");

            $this->event->dispatch('character.after_first_delete', ['connection' => $connection]);
        }
    }
}
