<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\Colors\Helper\Color;
use Modules\World\Entity\Character;

class Description extends Command
{
    /** @var Color */
    protected $colorHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $colorHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->colorHelper = $colorHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $subcommands = [
            'walkin',
            'walkout',
            'roomdesc',
            'description'
        ];

        /** @var Character $character */
        $character = $connection->getData('character');

        // no input? view our own description!
        if (empty($input)) {
            $desc = $character->getDescription() ?? 'There is nothing particular to see about '.$character->getName();
            $this->getOutput()->write($connection, ["Your description is:\n%s", $desc]);
            return;
        }

        // determine the input
        $sub  = substr($input, 0, strpos($input, ' '));
        $text = substr($input, strpos($input, ' ')+1);

        // if we can't find a proper command
        if (empty($sub) || in_array($sub, ['w', 'wa', 'wal', 'walk'])) {
            $this->getOutput()->write(
                $connection,
                [
                    "You need to define a field to describe. Name is mandatory in the\n".
                    "description. The other fields are prefixed by the name automatically.\n\n".
                    "If the text of a description starts with a + the line is appended.\n".
                    "If the text of a description is a - the last line is removed.\n\n".
                    "Valid options are: %s\n\n".
                    "Syntax: description [field] [text]\n".
                    "Syntax: description [field] clear\n".
                    "Syntax: description description + [text]\n".
                    "Syntax: description description -\n",
                    implode(' ', $subcommands)
                ]
            );

            return;
        }

        // setting the description
        if (stripos('description', $sub) !== false) {

            // by default we're assuming the entire string is the new description
            $desc = $text;

            // reset the description back to nothing
            if ($text == 'clear') {
                $desc = null;

            // remove the last line from the description
            } elseif ($text == '-') {

                $desc = $character->getDescription();
                $desc = substr($desc, 0, strrpos($desc, "\n"));

            // if we're adding this line
            } elseif (strpos($text, '+') === 0) {
                $desc  = $character->getDescription() . "\n" . substr($text, 2);
            }

            // but make sure the name is still in there, unless it was cleared
            if ( ! is_null($desc) && strpos($desc, $character->getName()) === false) {
                $this->getOutput()->write($connection, 'Your description must have your name in it.');
                return;
            }

            $character->setDescription($desc);
            if ($this->getEntityManager()->update($character, 'description')) {
                $this->getOutput()->write($connection, 'Description updated!');
            }

        } else {

            // find the exact field we're setting
            foreach($subcommands as $sc) {
                if (stripos($sc, $sub) !== false) {
                    $sub = $sc;
                    break;
                }
            }

            $clean = $this->colorHelper->strip($text);
            $desc  = $text;

            if ($text == 'clear') {
                $desc = null;

            } elseif (strlen($clean) > 60) {
                $this->getOutput()->write($connection, ['%s shouldn\'t be more than 60 characters long, this was %d.', $sub, strlen($text)]);
                return;

            } elseif (strpos($clean, $character->getName()) === false) {
                $this->getOutput()->write($connection, ['%s must have your name in it.', $sub]);
                return;

            } elseif ($sub == 'roomdesc' && strpos($clean, $character->getName()) !== 0) {
                $this->getOutput()->write($connection, ['roomdesc must start with your name. Color prefixes are allowed.']);
                return;
            }

            $character->{'set'.ucfirst($sub)}($desc);
            if ($this->getEntityManager()->update($character, $sub)) {
                $this->getOutput()->write($connection, ['%s updated!', ucfirst($sub)]);
            }
        }
    }
}
