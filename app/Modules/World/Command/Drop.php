<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\Room;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;

class Drop extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper = $itemHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        /** @var Room $room */
        $room = $character->getData('room');

        $characterItems = [];

        if (strtolower($input) != 'all') {
            $characterItem = $this->itemHelper->getFirstCharacterItemByInput($character, $input);
            if ( ! $characterItem) {
                $this->getOutput()->write($connection, "You do not have that item.");
                return;
            }

            if ( ! $characterItem->isInInventory()) {
                $this->getOutput()->write($connection, ["You're wearing that."]);
                return;
            }

            $characterItems[] = $characterItem;

        } else {
            $characterItems = $this->getEntityManager()->find(new CharacterItem(), ['inventory' => 1, 'parent_id' => null, 'character_id' => $character->getId()]);
        }

        $outputSelf = $outputRoom = '';

        foreach($characterItems as $characterItem) {
            if ($characterItem) {
                $roomItem = $this->itemHelper->characterItemToRoomItem($characterItem, $room);
                if ($roomItem) {
                    $outputSelf .= sprintf("You release and drop %s.\n", $roomItem->getShortDescription());
                    $outputRoom .= sprintf("%s drops %s.\n", $character->getName(), $roomItem->getShortDescription());
                }
            }
        }

        $this->getOutput()->write($connection, $outputSelf);

        $roomCharacters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
        foreach($roomCharacters as $roomCharacter) {
            $this->getOutput()->write($roomCharacter->getConnection(), $outputRoom);
        }
    }
}
