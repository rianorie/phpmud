<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;
use Modules\World\Entity\CharacterItem;
use Modules\World\Table\CharacterWearslot;

class Equipment extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        /** @var CharacterItem[] $characterItemCollection */
        $characterItemCollection = $this->getEntityManager()->find(new CharacterItem(), ['character_id' => $character->getId(), 'inventory' => 0]);

        $characterItems = [];

        foreach ($characterItemCollection as $characterItem) {
            if ($characterItem) {
                $characterItems[$characterItem->getCharacterSlot()] = $characterItem;
            }
        }

        $output = "You are using:\n";

        $slots = (new CharacterWearslot())->all();

        if (count($characterItems) == 0) {

            foreach($slots as $slot => $value) {
                $output .= sprintf("{c<{w%s{c>\n", trim($value->label));
            }

            $output .= "You're naked!\n";

        } else {
            foreach($slots as $data) {

                if (isset($characterItems[$data->slot])) {
                    $output .= sprintf("%-26s %s\n", sprintf("{c<{w%s{c>", trim($data->label)), $characterItems[$data->slot]->getShortDescription());

                } else {
                    $output .= sprintf("{c<{w%s{c>\n", trim($data->label));
                }
            }
        }

        $this->getOutput()->write($connection, $output);
    }
}
