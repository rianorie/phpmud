<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Entity\Link;
use Modules\World\Entity\Room;
use Modules\World\Helper\Link as LinkHelper;

class Exits extends Command
{
    /**
     * @var LinkHelper
     */
    protected $linkHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $linkHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->linkHelper = $linkHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var Character $char */
        $char = $connection->getData('character');

        /** @var \Modules\World\Entity\Room $room */
        $room = $char->getData('room');

        // grab the links for this room
        $links = [];

        /** @var Link $link */
        foreach ($this->getEntityManager()->find(new Link(), ['room_id' => $room->getId()]) as $link) {
            if ($link) {

                if ($input == 'auto') {
                    if (in_array('door', $link->getFlags()) && ! $this->linkHelper->isOpen($link)) {
                        $links[] = '{c({w'. $this->linkHelper->getDirectionNameForId($link->getDirection()) .'{c){w';

                    } else {
                        $links[] = $this->linkHelper->getDirectionNameForId($link->getDirection());
                    }

                } else {

                    /** @var Room $target */
                    $target = $this->getEntityManager()->load(new Room($this->getEntityManager()), $link->getTargetId());
                    $links[] = sprintf(
                        "{w%-6s - %s",
                        $this->linkHelper->getDirectionNameForId($link->getDirection()),
                        $target->getTitle()
                    );
                }
            }
        }

        if ($input == 'auto') {
            $this->getOutput()->write($connection, ['{c[{wObvious Exits: %s{c]{x', implode(' ', $links)]);
            return;
        }

        $this->getOutput()->write($connection, ["{cObvious exits:\n%s", implode("\n", $links)]);
    }
}
