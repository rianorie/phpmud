<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\Item;
use Modules\World\Entity\Room;
use Modules\World\Entity\RoomItem;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;

class Get extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper = $itemHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        /** @var Room $room */
        $room      = $character->getData('room');

        // trying to get something from another object
        if (stripos($input, 'from ') !== false || substr_count($input, ' ')) {

            $target = str_replace('from ', '', $input);
            list($input, $target) = explode(' ', $target);

            return $this->getFromItem($connection, $character, $room, $input, $target);
        }


        /** @var RoomItem[] $roomItems */
        $roomItems = [];

        if (strtolower($input) !== 'all') {

            $roomItem = $this->itemHelper->getFirstRoomItemByInput($room, $input);
            if ( ! $roomItem) {
                $this->getOutput()->write($connection, ["I see no %s here.", $input]);
                return;
            }

            $roomItems[] = $roomItem;

        } else {
            $roomItems = $this->getEntityManager()->find(new RoomItem($this->getEntityManager()), ['room_id' => $room->getId()]);
        }

        $outputSelf = $outputRoom = '';

        foreach($roomItems as $roomItem) {
            if ($roomItem) {
                $characterItem = $this->itemHelper->roomItemToCharacterItem($roomItem, $character);
                if ($characterItem) {
                    $outputSelf .= sprintf("You get %s.\n", $characterItem->getShortDescription());
                    $outputRoom .= sprintf("%s gets %s.\n", $character->getName(), $characterItem->getShortDescription());
                }
            }
        }

        $this->getOutput()->write($connection, $outputSelf);

        $roomCharacters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
        foreach($roomCharacters as $roomCharacter) {
            $this->getOutput()->write($roomCharacter->getConnection(), $outputRoom);
        }
    }

    protected function getFromItem(Connection $connection, Character $character, Room $room, string $input, string $target)
    {
        $outputSelf = $outputRoom = '';

        $roomItem    = true;
        $matchedItem = $this->itemHelper->getFirstRoomItemByInput($room, $target);
        if ( ! $matchedItem) {

            $roomItem    = false;
            $matchedItem = $this->itemHelper->getFirstCharacterItemByInput($character, $target);
            if ( ! $matchedItem) {

                $this->getOutput()->write($connection, ["I see no %s here.", $target]);
                return;
            }
        }

        if ($input == 'all') {
            /** @var Item[] $childItems */
            $childItems = $this->getEntityManager()->find($matchedItem, ['parent_id' => $matchedItem->getId()]);

        } else {
            $childItems = $this->itemHelper->getChildItemsByInput($matchedItem, $input);
            if ( ! $childItems) {
                $this->getOutput()->write($connection, ["I see nothing like that in the %s{x.", $matchedItem->getShortDescription()]);
                return;
            }
        }

        foreach ($childItems as $childItem) {
            if ($childItem) {

                if ($roomItem) {
                    /** @var RoomItem $childItem */
                    $this->itemHelper->roomItemToCharacterItem($childItem, $character);

                } else {
                    /** @var CharacterItem $childItem */
                    $childItem->setParentId(null);
                    $childItem->setInInventory(true);

                    $this->getEntityManager()->update($childItem, ['parent_id', 'inventory']);
                }

                $outputSelf .= sprintf("{xYou get %s {xfrom %s{x.\n", $childItem->getShortDescription(), $matchedItem->getShortDescription());
                $outputRoom .= sprintf("{x%s gets %s {xfrom %s{x.\n", $character->getName(), $childItem->getShortDescription(), $matchedItem->getShortDescription());
            }
        }

        $this->getOutput()->write($connection, $outputSelf);

        $others = $this->characterHelper->getOnlineRoomCharacters($room, $character);
        foreach($others as $other) {
            $this->getOutput()->write($other->getConnection(), $outputRoom);
        }
    }
}
