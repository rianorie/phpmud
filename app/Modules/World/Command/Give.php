<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Room;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;

class Give extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper      = $itemHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        $arguments = explode(' ', $input);
        if (count($arguments) < 2) {
            $this->getOutput()->write($connection, 'Give what to whom?');
            return;
        }

        $keyword = $arguments[0];
        $target  = $arguments[1];

        /** @var Room $room */
        $room = $character->getData('room');

        $characterItem = $this->itemHelper->getFirstCharacterItemByInput($character, $keyword);
        if ( ! $characterItem) {
            $this->getOutput()->write($connection, "You do not have that item.");
            return;
        }

        if ( ! $characterItem->isInInventory()) {
            $this->getOutput()->write($connection, ["You're wearing that."]);
            return;
        }

        $roomCharacters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
        foreach ($roomCharacters as $roomCharacter) {
            if ($roomCharacter && stripos($roomCharacter->getName(), $target) === 0) {

                $characterItem->setCharacterId($roomCharacter->getId());
                if ($this->getEntityManager()->save($characterItem)) {
                    $this->getOutput()->write($connection, ['You give %s to %s.', $characterItem->getShortDescription(), $roomCharacter->getName()]);
                    $this->getOutput()->write($roomCharacter->getConnection(), ['%s gives you %s', $character->getName(), $characterItem->getShortDescription()]);
                    return;
                }
            }
        }

        $this->getOutput()->write($connection, "They aren't here.");
    }
}
