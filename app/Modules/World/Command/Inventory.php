<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;
use Modules\World\Entity\CharacterItem;

class Inventory extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        /** @var CharacterItem[] $characterItemCollection */
        $characterItemCollection = $this->getEntityManager()->find(
            new CharacterItem(),
            ['character_id' => $character->getId(), 'inventory' => 1, 'parent_id' => null]
        );

        $characterItems = [];

        foreach ($characterItemCollection as $characterItem) {
            if ($characterItem) {
                $characterItems[$characterItem->keywords] = [
                    'item' => $characterItem,
                    'count' => (isset($characterItems[$characterItem->keywords]) ? $characterItems[$characterItem->keywords]['count'] + 1 : 1)
                ];
            }
        }

        $output = "You are carrying:\n";

        if (count($characterItems) == 0) {
            $output .= "     Nothing.\n";
        }

        foreach ($characterItems as $characterItem) {
            $output .= sprintf(
                "{x%4s %s\n",
                ($characterItem['count'] < 2 ? '' : sprintf('{c({w%2d{c)', $characterItem['count'])),
                $characterItem['item']->getShortDescription()
            );
        }

        $this->getOutput()->write($connection, $output);
    }
}
