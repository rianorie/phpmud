<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Event;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Room;
use Modules\World\Entity\RoomMobile;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Helper\Mobile as MobileHelper;

class Look extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * @var MobileHelper
     */
    protected $mobileHelper;

    protected $event;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, Event $event, $itemHelper, $characterHelper, $mobileHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->characterHelper = $characterHelper;
        $this->itemHelper = $itemHelper;
        $this->mobileHelper = $mobileHelper;
        $this->event = $event;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        // no argument? we're looking at the room
        if (trim($input) == '') {
            $this->event->dispatch('look.room', ['connection' => $connection]);

        } else {

            /** @var \Modules\World\Entity\Character $character */
            $character = $connection->getData('character');

            // if we're looking at ourselves, stop here
            if ($input == 'self' || stripos($character->getName(), $input) !== false) {
                $this->event->dispatch('look.character', ['connection' => $connection, 'character' => $character]);
                return;
            }

            /** @var Room $room */
            $room = $character->getData('room');

            $inside = false;

            // if we're not trying to look inside something
            if (stripos($input, 'in ') === false) {


                // check if we're looking at another character
                $characters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
                foreach($characters as $other) {
                    if (stripos($other->getName(), $input) !== false) {
                        $this->event->dispatch('look.character', ['connection' => $connection, 'character' => $other]);
                        return;
                    }
                }

                $roomMobile = $this->mobileHelper->getRoomMobileByInput($room, $input);
                if ($roomMobile) {
                    $this->event->dispatch('look.mobile', ['connection' => $connection, 'mobile' => $roomMobile]);
                    return;
                }

            } else {
                $inside = true;
                $input = ltrim($input, 'in ');
            }

            // next is looking at an item in the room
            $roomItem = $this->itemHelper->getFirstRoomItemByInput($room, $input);
            if ($roomItem) {
                $this->event->dispatch('look.roomitem', ['connection' => $connection, 'item' => $roomItem, 'inside' => $inside]);
                return;
            }

            // then an item on ourselves
            $characterItem = $this->itemHelper->getFirstCharacterItemByInput($character, $input);
            if ($characterItem) {
                $this->event->dispatch('look.characteritem', ['connection' => $connection, 'item' => $characterItem, 'inside' => $inside]);
                return;
            }

            // otherwise we're just failing
            $this->getOutput()->write($connection, "You do not see that here.");
        }
    }
}
