<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Area;
use Modules\World\Entity\Character;
use Modules\World\Entity\Link;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Link as LinkHelper;

class Move extends Command
{
    /**
     * @var \Game\Event
     */
    protected $event;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * @var LinkHelper
     */
    protected $linkHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $event, $characterHelper, $linkHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->event = $event;
        $this->characterHelper = $characterHelper;
        $this->linkHelper = $linkHelper;
    }

    /**
     * @param Connection $connection
     * @param string     $input
     * @param string     $command
     * @param string     $raw
     *
     */
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        try {
            /** @var Character $char */
            $char = $connection->getData('character');

            /** @var \Modules\World\Entity\Room $room */
            $room        = $char->getData('room');
            $currentRoom = clone $room;

            // if we're using an alias, switch to using the full direction
            $aliases = ['ne' => 'northeast', 'nw' => 'northwest', 'se' => 'southeast', 'sw' => 'southwest'];
            if (in_array($command, array_keys($aliases))) {
                $command = $aliases[$command];
            }

            // grab the link we're trying to reach
            /** @var Link $link */
            $link = false;
            $linkCollection = $this->getEntityManager()->find(new Link(), ['room_id' => $room->getId(), 'direction' => $this->linkHelper->getDirectionIdForName($command)]);
            foreach($linkCollection as $linkEntity) {
                if ($linkEntity) {
                    $link = $linkEntity;
                    break;
                }
            }

            // if the link didn't load, we're trying to reach a link that doesn't exist
            if ( ! $link) {
                $this->getOutput()->write($connection, ['Alas, you cannot go that way.']);
                return;
            }

            if (in_array('door', $link->getFlags()) && ! $this->linkHelper->isOpen($link)) {
                $this->getOutput()->write($connection, ['The %s %s is closed.', (empty($link->getKeyword()) ? 'door' : $link->getKeyword()), $command]);
                return;
            }

            // current area
            $currentArea = $this->getEntityManager()->load(new Area(), $room->getAreaId());

            // grab the online characters in the room we're departing
            $online = $this->characterHelper->getOnlineRoomCharacters($room, $char);

            // determine the walk out text
            if ($char->getWalkout()) {
                $walkout = sprintf($char->getWalkout().' to the %s.{x', $command);
            } else {
                $walkout = sprintf('{x%s leaves %s.', $char->getName(), $command);
            }

            // and show that we're leaving.
            foreach($online as $other) {
                if ($other) {
                    $this->getOutput()->write($other->getConnection(), $walkout);
                }
            }

            // update the room data in the character
            $char->setData('room', $this->getEntityManager()->load($room, $link->getTargetId()));

            $area = $this->getEntityManager()->load(new Area(), $room->getAreaId());

            // fire entry and leave events for the area
            if ($char->getData('room')->getAreaId() != $currentRoom->getAreaId()) {
                $this->event->dispatch('world.area.leaving', ['area' => $currentArea, 'connection' => $connection]);
                $this->event->dispatch('world.area.entering', ['area' => $area, 'connection' => $connection]);
            }

            // update the location in the database
            $char->getData('location')->setRoomId($link->getTargetId());
            $this->getEntityManager()->update($char->getData('location'), ['room_id']);



            if ($char->getWalkin()) {
                $walkin = sprintf('%s from the %s.{x', $char->getWalkin(), $command);
            } else {
                $walkin = sprintf("{x%s arrives from the %s.", $char->getName(), $command);
            }

            // grab the online characters in the room we're departing
            $online = $this->characterHelper->getOnlineRoomCharacters($char->getData('room'), $char);
            foreach($online as $other) {
                if ($other) {
                    $this->getOutput()->write($other->getConnection(), $walkin);
                }
            }

            // and execute a look command using the input handler
            // so events get fired
            $this->getInput()->handle($connection, 'look');

        } catch (Exception $e) {
            #TODO What to do here..?
        }
    }
}
