<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;
use Modules\World\Character\State;

class Password extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        if (empty($input)) {
            $this->getOutput()->write($connection, 'Syntax: password [old] [new]');
            return;
        }

        $passwords[] = substr($input, 0, strpos($input, ' '));
        $passwords[] = substr($input, strpos($input, ' ')+1);

        if ( ! isset($passwords[1]) || strlen($passwords[1]) < 8) {
            $this->getOutput()->write($connection, 'The new password should be at least 8 characters long.');
            return;
        }

        if ( ! $connection->getData('character')->checkPassword($passwords[0])) {
            $this->getOutput()->write($connection, 'The first password should match your current one.');
            return;
        }

        // finally, try and save the new password
        $connection->getData('character')->setPassword($passwords[1]);
        if ($this->getEntityManager()->save($connection->getData('character'))) {

            $this->getOutput()->write($connection, 'Ok.');
        } else {

            $this->getOutput()->write($connection, 'Something went wrong while trying to update your password.');
        }
    }
}
