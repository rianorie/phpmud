<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Room;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Table\Itemtype;
use Modules\World\Table\Position as CharacterPosition;

class Position extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper      = $itemHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        /** @var Room $room */
        $room = $character->getData('room');

        // grab the target, "sit on bench" should work as well as "sit bench"
        $target = ltrim(strtolower($input), 'on ');

        if ($command == 'stand' && empty($target)) {

            if ($character->getPosition() == CharacterPosition::NORMAL) {
                $this->getOutput()->write($connection, "You're already standing.");
                return;
            }

            $character->unsetData('position_desc');
            $character->unsetData('position_item');
            $character->setPosition(CharacterPosition::NORMAL);

            $outputSelf = 'You stand up.';
            $outputRoom = ['%s stands up.', $character->getName()];

        } elseif (empty($target)) {

            $character->unsetData('position_desc');
            $character->unsetData('position_item');

            switch($command) {

                case 'sit':
                    $outputSelf = ["You sit down."];
                    $outputRoom = ['%s sits down.', $character->getName()];
                    $character->setPosition(CharacterPosition::SIT);
                    break;

                case 'rest':
                    $outputSelf = ["You rest."];
                    $outputRoom = ['%s rests.', $character->getName()];
                    $character->setPosition(CharacterPosition::REST);
                    break;

                case 'sleep':
                    $outputSelf = ["You go to sleep."];
                    $outputRoom = ['%s goes to sleep.', $character->getName()];
                    $character->setPosition(CharacterPosition::SLEEP);
                    break;

                case 'stand':
                    $outputSelf = ["You stand up."];
                    $outputRoom = ['%s stands up.', $character->getName()];
                    $character->setPosition(CharacterPosition::NORMAL);
                    break;
            }

        } else {

            $matchedItem = $this->itemHelper->getFirstCharacterItemByInput($character, $target);
            if ( ! $matchedItem) {

                $matchedItem = $this->itemHelper->getFirstRoomItemByInput($room, $target);
                if ( ! $matchedItem) {
                    $this->getOutput()->write($connection, "You don't see that here.");
                    return;
                }
            }

            if ($matchedItem->getType() != Itemtype::FURNITURE) {
                $this->getOutput()->write($connection, ["You can't %s on that.", $command]);
                return;
            }

            $action = false;

            $flags = explode(', ', $matchedItem->getV2());
            foreach($flags as $flag) {
                if (stripos($flag, $command) !== false) {
                    $action = $flag;
                }
            }

            if ( ! $action) {
                $this->getOutput()->write($connection, ["You can't %s on that.", $command]);
                return;
            }

            $parts = explode('_', $action);

            $outputSelf = $outputRoom = '';

            $character->setData('position_desc', $parts[1].' '.$matchedItem->getShortDescription());
            $character->setData('position_item', $matchedItem);

            switch($command) {

                case 'sit':
                    $outputSelf = ["You sit %s %s.", $parts[1], $matchedItem->getShortDescription()];
                    $outputRoom = ['%s sits %s %s.', $character->getName(), $parts[1], $matchedItem->getShortDescription()];
                    $character->setPosition(CharacterPosition::SIT);
                    break;

                case 'rest':
                    $outputSelf = ["You sit % %s and rest.", $parts[1], $matchedItem->getShortDescription()];
                    $outputRoom = ['%s sits %s %s and rests.', $character->getName(), $parts[1], $matchedItem->getShortDescription()];
                    $character->setPosition(CharacterPosition::REST);
                    break;

                case 'sleep':
                    $outputSelf = ["You go to sleep %s %s.", $parts[1], $matchedItem->getShortDescription()];
                    $outputRoom = ['%s goes to sleep %s %s.', $character->getName(), $parts[1], $matchedItem->getShortDescription()];
                    $character->setPosition(CharacterPosition::SLEEP);
                    break;

                case 'stand':
                    $outputSelf = ["You stand %s %s.", $parts[1], $matchedItem->getShortDescription()];
                    $outputRoom = ['%s stands %s %s.', $character->getName(), $parts[1], $matchedItem->getShortDescription()];
                    $character->setPosition(CharacterPosition::STAND);
                    break;
            }
        }

        try {
            $this->getEntityManager()->update($character, 'position');

            $this->getOutput()->write($connection, $outputSelf);

            $roomCharacters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
            foreach ($roomCharacters as $roomCharacter) {
                $this->getOutput()->write($roomCharacter->getConnection(), $outputRoom);
            }

        } catch (Exception $e) {
            #TODO oh noes
        }
    }
}
