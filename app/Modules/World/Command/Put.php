<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\Room;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;

class Put extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper = $itemHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        /** @var Room $room */
        $room = $character->getData('room');

        $characterItems = [];

        @list($input, $target) = explode(' ', str_replace(' in ', ' ', $input));

        // make sure we're getting all the input
        if (is_null($input) || is_null($target)) {
            $this->getOutput()->write($connection, 'Put what in what?');
            return;
        }

        $objInRoom = false;

        // try and find the container, first on the character, if that fails, in the room
        $containerItem = $this->itemHelper->getFirstCharacterItemByInput($character, $target);
        if ( ! $containerItem) {

            $objInRoom = true;
            $containerItem = $this->itemHelper->getFirstRoomItemByInput($room, $target);
            if ( ! $containerItem) {
                $this->getOutput()->write($connection, ["I see no %s here.", $target]);
                return;
            }
        }

        // make sure it's a container
        if ( ! $containerItem->isContainer()) {
            $this->getOutput()->write($connection, "That's not an appropriate container.");
            return;
        }

        // make sure the container is open
        if ( ! $this->itemHelper->isOpen($containerItem)) {
            $this->getOutput()->write($connection, ['%s is closed.', $containerItem->getShortDescription()]);
            return;
        }

        $this->getOutput()->write($connection, ['Input "%s"', $input]);

        // try and find the designated object
        if (strtolower($input) != 'all') {
            $characterItem = $this->itemHelper->getFirstCharacterItemByInput($character, $input);
            if ( ! $characterItem) {
                $this->getOutput()->write($connection, "You do not have that item.");
                return;
            }

            if ( ! $characterItem->isInInventory()) {
                $this->getOutput()->write($connection, ["You're wearing that."]);
                return;
            }

            $characterItems[] = $characterItem;

        } else {
            $characterItems = $this->getEntityManager()->find(new CharacterItem(), ['inventory' => 1, 'parent_id' => null, 'character_id' => $character->getId()]);
        }

        $outputSelf = $outputRoom = '';

        foreach($characterItems as $characterItem) {
            if ($characterItem) {

                if ($objInRoom) {
                    $this->itemHelper->characterItemToRoomItem($characterItem, $room, $containerItem);

                } else {
                    // skip the item if it's the container itself'
                    if ($characterItem->getId() == $containerItem->getId()) {
                        if (count($characterItems) == 1) {
                            $this->getOutput()->write($connection, "You can't fold it into itself.");
                            return;
                        }
                        continue;
                    }

                    $characterItem->setParentId($containerItem->getId());
                    $this->getEntityManager()->update($characterItem, 'parent_id');
                }

                $outputSelf .= sprintf("You put %s in %s.\n", $characterItem->getShortDescription(), $containerItem->getShortDescription());
                $outputRoom .= sprintf("%s puts %s in %s.\n", $character->getName(), $characterItem->getShortDescription(), $containerItem->getShortDescription());
            }
        }

        $this->getOutput()->write($connection, $outputSelf);

        $roomCharacters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
        foreach($roomCharacters as $roomCharacter) {
            $this->getOutput()->write($roomCharacter->getConnection(), $outputRoom);
        }
    }
}
