<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Event;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Character\State;
use Modules\World\Character\Utilities;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Character\State as StateHelper;

class Quit extends Command
{
    /**
     * @var Event
     */
    protected $event;

    /**
     * @var StateHelper
     */
    protected $stateHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, Event $event, $characterHelper, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->event = $event;
        $this->stateHelper = $stateHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        if (trim($raw) != $command) {
            $this->getOutput()->write($connection, 'If you want to QUIT, you have to spell it out.');
            return;
        }

        // throw out an event for observers that need to do stuff when the character leaves
        $this->event->dispatch('character.before_quit', ['connection' => $connection]);

        $this->stateHelper->setState($connection, $this->stateHelper::INACTIVE);

        // Output the quit messages
        foreach($connection->getManager()->getAllExcept($connection) as $other) {
            if ($other) {
                $this->getOutput()->write($other, ['%s returns home after a hard day of adventuring.', $connection->getData('character')->getName()]);
            }
        }

        $this->getOutput()->write($connection, 'You return home after a hard day of adventuring.', true, true);

        // remove the character from the list
        $this->characterHelper->removeCharacter($connection->getData('character'));

//        // clean up the elements, if we don't, the memory doesn't get freed up
//        if ( ! is_null($connection->getData())) {
//            foreach($connection->getData() as $key => $item) {
//                $connection->setData($key, null);
//            }
//        }

        // remove the connection from the manager
        $connection->getManager()->remove($connection);

        // close the connection
        $connection->close();
    }
}
