<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\Room;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Table\CharacterWearslot;
use Modules\World\Table\Gender;
use Modules\World\Table\Wearslot;

class Remove extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper = $itemHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

//        $arguments = explode(' ', $input);
        if (empty($input)) {
            $this->getOutput()->write($connection, 'Remove what?');
            return;
        }

        $keyword = $input;

        /** @var Room $room */
        $room = $character->getData('room');

        $characterItemToRemove = [];

        if ($keyword != 'all') {

            $characterItem = $this->itemHelper->getFirstCharacterItemByInput($character, $keyword, false, true);
            if ( ! $characterItem) {
                $this->getOutput()->write($connection, "You do not have that item.");
                return;
            }

            $characterItemToRemove[] = $characterItem;

        } else {
            // get the number of items we're wearing with this wear slot
            $characterInventoryItems = $this->entityManager->find(new CharacterItem(),
                ['character_id' => $character->getId(), 'inventory' => 0]
            );

            // flatten the yield result
            foreach($characterInventoryItems as $characterInventoryItem) {
                if ($characterInventoryItem) {
                    $characterItemToRemove[] = $characterInventoryItem;
                }
            }
        }

        $outputSelf = $outputRoom = "";

        /** @var CharacterItem $item */
        foreach ($characterItemToRemove as $item) {
            $item->setInInventory(true);
            $this->getEntityManager()->update($item, 'inventory');
            $outputSelf .= sprintf("You stop using %s{x.\n", $item->getShortDescription());
            $outputRoom .= sprintf("%s stops using %s{x.\n", $character->getName(), $item->getShortDescription());
        }

        $this->getOutput()->write($connection, $outputSelf);

        /** @var Character[] $roomCharacters */
        $roomCharacters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
        foreach($roomCharacters as $roomCharacter) {
            $this->getOutput()->write($roomCharacter->getConnection(), $outputRoom);
        }
    }
}
