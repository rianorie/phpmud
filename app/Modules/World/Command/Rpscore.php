<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;
use Modules\World\Entity\Character;

class Rpscore extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var Character $char */
        $char = $connection->getData('character');
        $output = sprintf(
'--------------------------------------------------------------
Walkin: %s{x
Walkout: %s{x
--------------------------------------------------------------
Room Desc: %s{x
--------------------------------------------------------------
Battle Cry: 
--------------------------------------------------------------
Title:   
--------------------------------------------------------------
Description:
%s{x

--------------------------------------------------------------

See also: Score',
            ($char->getWalkin() ?? 'Game default'),
            ($char->getWalkout() ?? 'Game default'),
            ($char->getRoomdesc() ?? 'Game default'),
            ($char->getDescription() ?? 'Game default')
        );

        $this->getOutput()->write($connection, $output);
    }
}
