<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;

class Score extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        /** @var \Modules\World\Entity\Character $character */
        $character = $connection->getData('character');

        $output = sprintf(
' You are %s.
 LvL: {C%4d{x   ({C%4d{x  ) QP: {C%6d{x QT: {C%d{x  
 You have remorted  %d times.
{W.{B----------------------------------------------------------------{W.
{B){w COMBAT STATS                                                   {B(
{B|{c STR: {C%3d{c({C%3d{c)   INT: {C%3d{c({C%3d{c)        HP: {C%7d{c({C%7d{c)      {B|
{B|{c DEX: {C%3d{c({C%3d{c)   WIS: {C%3d{c({C%3d{c)        ST: {C%7d{c({C%7d{c)      {B|
{B|{c CON: {C%3d{c({C%3d{c)   INS: {C%3d{c({C%3d{c)                                  {B|
{B|{c REF: {C%3d{c({C%3d{c)   CHA: {C%3d{c({C%3d{c)                                  {B|
{B|                                                                |
{B|{c Air:     {C%3d    {cFire:  {C%3d  {cHR: {C%5d                          {B|
{B|{c Earth:   {C%3d    {cWater: {C%3d  {cDR: {C%5d  {cTrains: {C%6d          {B|
{B|{c Spirit:  {C%3d                                                   {B|
{B|{c You are a channeler. Average OP score is {C%3d{c ({C%3d{c).            {B|
{B|                                                                |
{B|{w ARMOR STATS                                                    {B|
{B|{c Pierce: {C%6d  {cBash: {C%6d  {cSlash: {C%6d  {cWeaves: {C%6d    {B|
{B|                                                                |
{B|{w INFORMATIONAL STATS                                            {B|
{B|{c CP:{C %8d{c SP:{C %8d{c SM:{C %8d{c GM:{C %8d            {B|
{B|                                                                |
{B|{cYou are carrying {C%4d{c items, of a possible ({C%4d{c)               {B|
{B|{cYour total weight is {C%7d{c stones.                            {B|
{B|                                                                |
{B|{cWimpy set to {C%5d{c HP  Scroll Length is {C%3d{c lines.              {B|
{B|                                                                |
{B|{c You are somewhat thirsty.                                      {B|
{B|{c You are somewhat hungry.                                       {B|
{B|{c You are standing.                                              {B|
{B)                                                                (
{W.{B------------------------ {cExplored [{W    0 {x of {W    0  {w({W00.00%%%%{w){c]{B---{W.
{xSee also: RPScore
',

$character->getName(),
        0, // level
            0, // xp per level
            0, // qp
            0, // quest timer
            0, // remort
            $character->getStrength(),
            $character->getStrength(),
            $character->getIntelligence(),
            $character->getIntelligence(),
            $character->getHitpoints(),
            $character->getMaxHitpoints(),
            $character->getDexterity(),
            $character->getDexterity(),
            $character->getWisdom(),
            $character->getWisdom(),
            $character->getStamina(),
            $character->getMaxStamina(),
            $character->getConstitution(),
            $character->getConstitution(),
            $character->getInstinct(),
            $character->getInstinct(),
            $character->getReflex(),
            $character->getReflex(),
            $character->getCharisma(),
            $character->getCharisma(),
            0, // air sphere
            0, // fire sphere
            0, // hr
            0, // earth sphere
            0, // water sphere
            0, // dr
            0, // trains
            0, // spirit sphere
            0, // affected OP
            0, // base OP
            0, // armor pierce
            0, // armor bash
            0, // armor slash
            0, // armor weaves
            0, // copper penny
            0, // silver penny
            0, // silver mark
            0, // gold mark
            0, // current carrying
            0, // max carry
            0, // weight
            0, // wimpy
            0 // scroll length
        );
        $this->getOutput()->write($connection, $output);
    }
}
