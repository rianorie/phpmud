<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Module\Command;
use Game\Registry;

class Time extends Command
{
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        try {

            $storedHour = 0;
            if (Registry::exists('game.time')) {
                $storedHour = Registry::get('game.time');
            }

            $output = sprintf(
                "It is %d o'clock %s",
            ($storedHour % 12 == 0) ? 12 : $storedHour %12,
            $storedHour >= 12 ? "{win the evening{c" : "{wbefore noon{c"
            );

            $this->getOutput()->write($connection, $output);

        } catch (\Exception $exception) { }


    }
}
