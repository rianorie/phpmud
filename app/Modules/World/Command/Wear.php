<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Entity\Character;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\Room;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Table\CharacterWearslot;
use Modules\World\Table\Gender;
use Modules\World\Table\Wearslot;

class Wear extends Command
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->itemHelper      = $itemHelper;
        $this->characterHelper = $characterHelper;
    }

    public function execute(Connection $connection, string $input, string $command, string $raw)
    {

        try {
            /** @var \Modules\World\Entity\Character $character */
            $character = $connection->getData('character');

            if (empty($input)) {
                $this->getOutput()->write($connection, 'Wear, wield, or hold what?');
                return;
            }

            $keyword = $input;

            /** @var Room $room */
            $room = $character->getData('room');

            $characterItemToWear = [];

            if ($keyword != 'all') {

                $characterItem = $this->itemHelper->getFirstCharacterItemByInput($character, $keyword, true);
                if ( ! $characterItem) {
                    $this->getOutput()->write($connection, "You do not have that item.");
                    return;
                }

                // check if it can actually be worn at all
                if ($characterItem->getWearSlot() == Wearslot::TAKE) {
                    $this->getOutput()->write($connection, "You can't wear, wield, or hold that.");
                    return;
                }

                $characterItemToWear[] = $characterItem;

            } else {
                // get the number of items we're wearing with this wear slot
                $characterInventoryItems = $this->entityManager->find(new CharacterItem(),
                    ['character_id' => $character->getId(), 'parent_id' => null, 'inventory' => 1]
                );

                // flatten the yield result
                foreach ($characterInventoryItems as $characterInventoryItem) {
                    if ($characterInventoryItem) {
                        $characterItemToWear[] = $characterInventoryItem;
                    }
                }
            }

            $characterWearSlots = (new CharacterWearslot())->all();

            $outputSelf = $outputRoom = "";

            /** @var CharacterItem $item */
            foreach ($characterItemToWear as $item) {

                // get the number of items we're wearing with this wear slot
                $characterWornItemsCount = $this->entityManager->count(new CharacterItem(),
                    ['character_id' => $character->getId(), 'inventory' => 0, 'wear_slot' => $item->getWearSlot()]
                );

                // determine the number of slots
                $slots = array_values(array_filter($characterWearSlots, function($w) use ($item) {
                    return $w->wearslot == $item->getWearSlot();
                }));

                // if we're wearing as many items as there are slots, remove the first one
                if (count($slots) == $characterWornItemsCount && $keyword != 'all') {

                    // find the first one
                    $characterWornItems = $this->entityManager->find(new CharacterItem(),
                        ['character_id' => $character->getId(), 'inventory' => 0, 'wear_slot' => $item->getWearSlot()],
                        1
                    );

                    // and set it to be in the inventory
                    /** @var CharacterItem $characterWornItem */
                    foreach ($characterWornItems as $characterWornItem) {
                        if ($characterWornItem) {
                            $characterWornItem->setInInventory(true);
                            $characterWornItem->setCharacterSlot(null);

                            $this->getEntityManager()->update($characterWornItem, ['inventory', 'character_slot']);

                            $outputSelf .= sprintf("You stop using %s{x.\n", $characterWornItem->getShortDescription());
                            $outputRoom .= sprintf("%s stops using %s{x.\n", $character->getName(), $characterWornItem->getShortDescription());
                            $characterWornItemsCount--;
                        }
                    }
                }

                if (count($slots) > $characterWornItemsCount) {

                    // if the item is to hold, a shield, or a weapon, check for conflicts, because only two can be worn at any time
                    if (in_array($item->getWearSlot(), [Wearslot::HOLD, Wearslot::WEAR_SHIELD, Wearslot::WIELD])) {

                        $slotCount = $this->entityManager->count(new CharacterItem(),
                            [
                                'character_id' => $character->getId(), 'inventory' => 0,
                                'wear_slot'    => [Wearslot::HOLD, Wearslot::WEAR_SHIELD, Wearslot::WIELD]
                            ],
                            2
                        );

                        if ($slotCount > 1) {
                            if ($keyword != 'all') {
                                $outputSelf .= "Your hands are full.\n";
                            }
                            continue;
                        }
                    }

                    // we default to first available slot
                    $slot = $slots[0];

                    // if there's more than one slot, we have to figure out which of two is filled
                    if (count($slots) > 1) {
                        $slotCount = $this->entityManager->count(new CharacterItem(),
                            [
                                'character_id' => $character->getId(), 'inventory' => 0,
                                'wear_slot'    => $item->getWearSlot(), 'character_slot' => $slot->slot
                            ],
                            1
                        );

                        // we found an item in the first available slot, so it has to be second slot
                        // that's available. Otherwise we can stick to the default
                        if ($slotCount > 0) {
                            $slot = $slots[1];
                        }
                    }

                    $type = 'wear';
                    if ($item->getWearSlot() == Wearslot::WIELD) {
                        $type = 'wield';
                    } else if ($item->getWearSlot() == Wearslot::WEAR_FLOAT) {
                        $type = 'release';
                    } else if ($item->getWearSlot() == Wearslot::HOLD) {
                        $type = 'hold';
                    }

                    // then update the other item
                    $item->setInInventory(false);
                    $item->setCharacterSlot($slot->slot);
                    $this->getEntityManager()->update($item, ['inventory', 'character_slot']);
                    $outputSelf .= sprintf(
                        "You %s %s{x%s.\n",
                        $type,
                        $item->getShortDescription(),
                        (! empty($slot->wear_label) ? ' ' : '') . sprintf($slot->wear_label, 'your')
                    );

                    $outputRoom .= sprintf(
                        "%s %s %s{x%s.\n",
                        $character->getName(),
                        $type,
                        $item->getShortDescription(),
                        (! empty($slot->wear_label) ? ' ' : '') .
                        sprintf($slot->wear_label, ($character->getGender() == Gender::MALE ? 'his' : 'her'))
                    );
                }
            }

            $this->getOutput()->write($connection, $outputSelf);

            /** @var Character[] $roomCharacters */
            $roomCharacters = $this->characterHelper->getOnlineRoomCharacters($room, $character);
            foreach ($roomCharacters as $roomCharacter) {
                $this->getOutput()->write($roomCharacter->getConnection(), $outputRoom);
            }
        } catch (Exception $exception) {

            #TODO something went wrong..
            $this->getOutput()->write($connection, 'Something went wrong.');

        }
    }
}
