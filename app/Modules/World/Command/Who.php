<?php

namespace Modules\World\Command;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Command;
use Game\Module\Entity\Manager as EntityManager;
use Game\Output;
use Modules\World\Character\State;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Table\Race as RaceTable;

class Who extends Command
{
    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * @var RaceTable
     */
    protected $raceTable;

    /**
     * @var CharacterHelper\State
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper, $raceTable, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->characterHelper = $characterHelper;
        $this->raceTable = $raceTable;
        $this->stateHelper = $stateHelper;
    }

    /**
     * @param Connection $connection
     * @param string     $input
     * @param string     $command
     * @param string     $raw
     *
     * @throws \Exception
     */
    public function execute(Connection $connection, string $input, string $command, string $raw)
    {
        $characters = $this->characterHelper->getCharacters();

        $output = sprintf(
            "                  {wWho is currently playing:\n\n".
            "          {gLargest number of players today: X; Ever: X.\n".
            "       	        {gYou can see %d character out of %d.\n",
            count($characters),
            count($characters)
        );

        foreach($characters as $character) {
            $output .= sprintf(
                " {w%11s   {WX   {w%s\n{w              ----- Idle Time:  {W%d %s{x\n",
                $this->raceTable->get($character->getRace())->label,
                $this->characterHelper->getPublicInfo($character),
                round((time() - $character->getData('last_update')) / 60),
                ($this->stateHelper->getState($character->getConnection()) == $this->stateHelper::AFK ? '{c[{wAFK{c]' : '')
            );
        }

        $this->getOutput()->write($connection, $output);
    }
}
