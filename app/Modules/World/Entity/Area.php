<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * Area entity for the World module
 *
 * @package PHPMUD\World
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class Area extends Entity
{
    use Timestamp;

	/**
	 * @var string
	 */
	public $name;

    /**
     * @var string
     */
	public $last_reload;

	/**
	 * {@inheritdoc}
	 *
	 * @return string
	 */
	public function getTable() : string
	{
		return 'areas';
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return array
	 */
	public function getColumns() : array
	{
		return ['id', 'name', 'last_reload', 'created_at', 'updated_at'];
	}

	/**
	 * Retrieve the name of the area
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

    /**
     * Last reload timestamp
     *
     * @return string
     */
	public function getLastReload()
    {
        return $this->last_reload;
    }
}
