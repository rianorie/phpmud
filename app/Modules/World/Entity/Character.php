<?php

namespace Modules\World\Entity;

use Game\Connection\Interfaces\Connection;
use JetBrains\PhpStorm\Pure;

/**
 * Character entity
 *
 * @package PHPMUD\Character
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class Character extends Skeleton
{
    /**
     * @var Connection
     */
    private Connection $connection;

    /**
     * @var string
     */
    public string $password;

    /**
     * @var string
     */
    public ?string $email;

    /**
     * @var string
     */
    public ?string $metadata;

    /**
     * @var string
     */
    public ?string $lastname;

    /**
     * @var string
     */
    public ?string $title;

    /**
     * @var string
     */
    public ?string $flags;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'characters';
    }

    /**
     * Load the columns for the entity
     *
     * @return array
     */
    public function getColumns() : array
    {
        return array_merge(parent::getColumns(), ['password', 'email', 'metadata', 'lastname', 'flags', 'title']);
    }

    /**
     * Retrieve the connection associated with the character
     *
     * @return Connection
     */
    public function getConnection() : Connection
    {
        return $this->connection;
    }

    /**
     * Set the connection for the character
     *
     * @param Connection $connection
     */
    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Return the character password
     *
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }

    /**
     * Set a password for the character
     *
     * @param string $password
     */
    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    /**
     * Load the email from the character
     *
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * Set the email in the character
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @param string $key
     *
     * @return mixed|null
     */
    public function getMetadata(string $key) : mixed
    {
        $metadata = @json_decode($this->metadata, true);
        return ($metadata[$key] ?? null);
    }

    /**
     * @param string $key
     * @param mixed  $data
     */
    public function setMetadata(string $key, $data) : void
    {
        if ( ! is_scalar($data)) {
            throw new \InvalidArgumentException('Data can only be a scalar.');
        }

        $metadata = @json_decode($this->metadata, true);
        if ( ! $metadata) {
            $metadata = [];
        }

        $metadata[$key] = $data;

        $this->metadata = json_encode($metadata);
    }

    public function unsetMetadata(string $key) : void
    {
        $metadata = @json_decode($this->metadata, true);
        if ( ! $metadata) {
            $metadata = [];
        }

        unset($metadata[$key]);

        $this->metadata = json_encode($metadata);
    }

    /**
     * @return string|null
     */
    public function getLastname() : ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     */
    public function setLastname(?string $lastname) : void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string|null
     */
    public function getTitle() : ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title) : void
    {
        $this->title = $title;
    }

    /**
     * Retrieve all set flags for the character
     *
     * @return array
     */
    #[Pure] public function getFlags() : array
    {
        return explode(',', $this->flags);
    }

    /**
     * Set a collection of flags for the character
     *
     * @param array $flags
     */
    public function setFlags(array $flags) : void
    {
        $this->flags = implode(',', $flags);
    }

    /**
     * Add a flag to the character
     *
     * @param string $flag
     */
    public function addFlag(string $flag) : void
    {
        $flags = $this->getFlags();
        $flags[] = $flag;
        $this->setFlags(array_unique($flags));
    }

    /**
     * Remove a single flag from the character
     *
     * @param string $flag
     */
    public function removeFlag(string $flag) : void
    {
        $flags = $this->getFlags();
        $this->setFlags(array_filter($flags, function($f) use ($flag) { return $f != $flag; }));
    }

    /**
     * Check if a flag is set for a character
     *
     * @param string $flag
     *
     * @return bool
     */
    #[Pure] public function hasFlag(string $flag) : bool
    {
        return in_array($flag, $this->getFlags());
    }
}
