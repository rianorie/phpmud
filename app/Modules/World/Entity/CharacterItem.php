<?php

namespace Modules\World\Entity;

/**
 * CharacterItem entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class CharacterItem extends Item
{
    /**
     * @var int
     */
    public $character_id;

    /**
     * @var int
     */
    public $inventory;

    /**
     * @var int
     */
    public $character_slot;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'character_items';
    }

    public function getColumns() : array
    {
        $columns = parent::getColumns();

        $columns[] = 'character_id';
        $columns[] = 'inventory';
        $columns[] = 'character_slot';

        // remove item specific column(s)
        $columns = array_diff($columns, ['area_id']);

        return $columns;
    }

    /**
     * Set the character ID for an object
     *
     * @param $id
     */
    public function setCharacterId($id)
    {
        $this->character_id = $id;
    }

    /**
     * Set if the item is in the inventory or not
     *
     * @param bool $inInventory
     */
    public function setInInventory(bool $inInventory)
    {
        $this->inventory = (int)$inInventory;
    }

    /**
     * Is the item in the characters inventory
     *
     * @return bool
     */
    public function isInInventory()
    {
        return (bool)$this->inventory;
    }

    /**
     * @return int|null
     */
    public function getCharacterSlot() : int
    {
        return $this->character_slot;
    }

    /**
     * @param int|null $character_slot
     */
    public function setCharacterSlot(?int $character_slot) : void
    {
        $this->character_slot = $character_slot;
    }
}
