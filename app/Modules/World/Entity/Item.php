<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * Item entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class Item extends Entity
{
    use Timestamp;

    /**
     * @var int
     */
    public $area_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $keywords;

    /**
     * @var string
     */
    public $short_desc;

    /**
     * @var string
     */
    public $long_desc;

    /**
     * @var int
     */
    public $type;

    /**
     * @var string
     */
    public $extra_desc;

    /**
     * @var int
     */
    public $weight;

    /**
     * @var boolean
     */
    public $container;

    /**
     * @var int
     */
    public $parent_id;

    /**
     * @var int
     */
    public $wear_slot;

    /**
     * @var string
     */
    public $flags;

    /**
     * @var string
     */
    public $metadata;

    public $v0;

    public $v1;

    public $v2;

    public $v3;

    public $v4;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return [
            'area_id',
            'name',
            'keywords',
            'short_desc',
            'long_desc',
            'type',
            'extra_desc',
            'weight',
            'container',
            'parent_id',
            'wear_slot',
            'v0',
            'v1',
            'v2',
            'v3',
            'v4',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Return the item name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return the help keywords
     *
     * @return array
     */
    public function getKeywords()
    {
        return explode(' ', $this->keywords);
    }

    /**
     * Return the short item description
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->short_desc;
    }

    /**
     * Return the long item description
     *
     * @return string
     */
    public function getLongDescription()
    {
        return $this->long_desc;
    }

    /**
     * Return the extra item description
     *
     * @return array
     */
    public function getExtraDescriptions()
    {
        return ( ! empty($this->extra_desc) ? json_decode($this->extra_desc, true) : []);
    }

    /**
     * Retrieve the item type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Retrieve the item weight
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Is the item a container
     *
     * @return bool
     */
    public function isContainer()
    {
        return (bool)$this->container;
    }

    /**
     * Retrieve the parent item
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    public function setParentId(?int $id) : void
    {
        $this->parent_id = $id;
    }

    /**
     * Retrieve the wear slot
     *
     * @return int
     */
    public function getWearSlot()
    {
        return $this->wear_slot;
    }

    /**
     * Retrieve the items area id
     *
     * @return int
     */
    public function getAreaId()
    {
        return $this->area_id;
    }

    /**
     * @return mixed
     */
    public function getV0()
    {
        return $this->v0;
    }

    /**
     * @param mixed $v0
     */
    public function setV0($v0) : void
    {
        $this->v0 = $v0;
    }

    /**
     * @return mixed
     */
    public function getV1()
    {
        return $this->v1;
    }

    /**
     * @param mixed $v1
     */
    public function setV1($v1) : void
    {
        $this->v1 = $v1;
    }

    /**
     * @return mixed
     */
    public function getV2()
    {
        return $this->v2;
    }

    /**
     * @param mixed $v2
     */
    public function setV2($v2) : void
    {
        $this->v2 = $v2;
    }

    /**
     * @return mixed
     */
    public function getV3()
    {
        return $this->v3;
    }

    /**
     * @param mixed $v3
     */
    public function setV3($v3) : void
    {
        $this->v3 = $v3;
    }

    /**
     * @return mixed
     */
    public function getV4()
    {
        return $this->v4;
    }

    /**
     * @param mixed $v4
     */
    public function setV4($v4) : void
    {
        $this->v4 = $v4;
    }

    /**
     * @param string $key
     *
     * @return mixed|null
     */
    public function getMetadata(string $key)
    {
        $metadata = @json_decode($this->metadata, true);
        return ($metadata[$key] ?? null);
    }

    /**
     * @param string $key
     * @param mixed  $data
     */
    public function setMetadata(string $key, $data) : void
    {
        if ( ! is_scalar($data)) {
            throw new \InvalidArgumentException('Data can only be a scalar.');
        }

        $metadata = @json_decode($this->metadata, true);
        if ( ! $metadata) {
            $metadata = [];
        }

        $metadata[$key] = $data;

        $this->metadata = json_encode($metadata);
    }

    public function unsetMetadata(string $key) : void
    {
        $metadata = @json_decode($this->metadata, true);
        if ( ! $metadata) {
            $metadata = [];
        }

        unset($metadata[$key]);

        $this->metadata = json_encode($metadata);
    }

    /**
     * @return array
     */
    public function getFlags() : array
    {
        return explode(', ', $this->flags);
    }

    /**
     * @param array $flags
     */
    public function setFlags(array $flags) : void
    {
        $this->flags = implode(', ', $flags);
    }

    /**
     *
     * @param string $flag
     */
    public function addFlag(string $flag) : void
    {
        $flags = $this->getFlags();
        $flags[] = $flag;

        $this->setFlags(array_unique($flags));
    }

    /**
     * @param string $flag
     */
    public function removeFlag(string $flag) : void
    {
        $this->setFlags( array_filter($this->getFlags(), function($f) use ($flag) { return $f != $flag; }) );
    }
}
