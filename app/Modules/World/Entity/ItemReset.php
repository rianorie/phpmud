<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * Reset entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class ItemReset extends Entity
{
    use Timestamp;

    /**
     * @var int
     */
    public $area_id;

    /**
     * @var int
     */
    public $room_id;

    /**
     * @var int
     */
    public $item_id;

    /**
     * @var int
     */
    public $parent_id;

    /**
     * @var int
     */
    public $count;

    /**
     * @var int
     */
    public $chance;

    /**
     * @var integer
     */
    public $mobile_reset_id;

    /**
     * @var integer
     */
    public $mobile_wear_loc;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'item_resets';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return [
            'id',
            'area_id',
            'room_id',
            'item_id',
            'parent_id',
            'count',
            'chance',
            'created_at',
            'updated_at',
            'mobile_reset_id',
            'mobile_wear_loc'
        ];
    }

    /**
     * Return the area id
     *
     * @return int
     */
    public function getAreaId()
    {
        return $this->area_id;
    }

    /**
     * Return the room id
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * Return the item id
     *
     * @return int
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * Return the parent id
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Return the reset count
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Return the reset chance percentage
     *
     * @return int
     */
    public function getChance()
    {
        return $this->chance;
    }

    /**
     * @return int
     */
    public function getMobileResetId() : ?int
    {
        return $this->mobile_reset_id;
    }

    /**
     * @param int $mobile_reset_id
     */
    public function setMobileResetId(?int $mobile_reset_id) : void
    {
        $this->mobile_reset_id = $mobile_reset_id;
    }

    /**
     * @return int
     */
    public function getMobileWearLoc() : ?int
    {
        return $this->mobile_wear_loc;
    }

    /**
     * @param int $mobile_wear_loc
     */
    public function setMobileWearLoc(?int $mobile_wear_loc) : void
    {
        $this->mobile_wear_loc = $mobile_wear_loc;
    }
}
