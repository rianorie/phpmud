<?php

namespace Modules\World\Entity;
use Game\Module\Entity\Entity;

/**
 * Link entity
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-28
 * @version 1.0
 */
class Link extends Entity
{
	const DIRECTION_NORTH = 0;

	const DIRECTION_EAST = 1;

	const DIRECTION_SOUTH = 2;

	const DIRECTION_WEST = 3;

	const DIRECTION_UP = 4;

    const DIRECTION_DOWN = 5;

    const DIRECTION_NORTHEAST = 6;

    const DIRECTION_SOUTHEAST = 7;

    const DIRECTION_SOUTHWEST = 8;

	const DIRECTION_NORTHWEST = 9;

    /**
     * @var int
     */
	public $direction;

    /**
     * @var int
     */
	public $target_id;

    /**
     * @var int
     */
	public $room_id;

    /**
     * @var string
     */
	public $keyword;

    /**
     * @var string
     */
	public $description;

    /**
     * @var int
     */
	public $key_id;

    /**
     * @var string
     */
	public $flags;

	/**
	 * {@inheritdoc}
	 *
	 * @return string
	 */
	public function getTable() : string
	{
		return 'links';
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return array
	 */
	public function getColumns() : array
	{
		return ['id', 'room_id', 'direction', 'target_id'];
	}

	/**
	 * Return the direction this link goes
	 *
	 * @return int
	 */
	public function getDirection()
	{
		return $this->direction;
	}

	/**
	 * Return the room this link links to
	 *
	 * @return int
	 */
	public function getTargetId()
	{
		return $this->target_id;
	}

	/**
	 * Return the room id this link belongs to
	 *
	 * @return int
	 */
	public function getRoomId()
	{
		return $this->room_id;
	}

    /**
     * @return array
     */
    public function getFlags() : array
    {
        return explode(', ', $this->flags);
    }

    /**
     * @param array $flags
     */
    public function setFlags(array $flags) : void
    {
        $this->flags = implode(', ', $flags);
    }

    /**
     * @return string
     */
    public function getKeyword() : string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword) : void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getKeyId() : int
    {
        return $this->key_id;
    }

    /**
     * @param int $key_id
     */
    public function setKeyId(int $key_id) : void
    {
        $this->key_id = $key_id;
    }

}
