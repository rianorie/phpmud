<?php

namespace Modules\World\Entity;

use \Game\Module\Entity\Entity;

/**
 * Location entity for the World module
 *
 * @package PHPMUD\World
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class Location extends Entity
{
	/**
	 * @var int
	 */
	public $room_id;

	/**
	 * @var int
	 */
	public $character_id;

	/**
	 * {@inheritdoc}
	 *
	 * @return string
	 */
	public function getTable() : string
	{
		return 'locations';
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return array
	 */
	public function getColumns() : array
	{
		return ['id', 'room_id', 'character_id'];
	}


	/**
	 * Retrieve the name of the area
	 *
	 * @return string
	 */
	public function getRoomId()
	{
		return $this->room_id;
	}

	/**
	 * Update the room id for this location
	 *
	 * @param int $id
	 *
	 * @return boolean
	 */
	public function setRoomId($id)
	{
		$this->room_id = $id;
		return true;
	}

	/**
	 * Retrieve the creation date for the area
	 *
	 * @return string
	 */
	public function getCharacterId()
	{
		return $this->character_id;
	}

	/**
	 * Set the character id for this location
	 *
	 * @param int $id
	 *
	 * @return boolean
	 */
	public function setCharacterId($id)
	{
		$this->character_id = $id;
		return true;
	}
}
