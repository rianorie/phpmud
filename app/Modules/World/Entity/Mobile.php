<?php

namespace Modules\World\Entity;

use \Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * Mobile entity for the World module
 *
 * @package PHPMUD\World
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class Mobile extends Entity
{
    use Timestamp;

    /**
     * @var integer
     */
    public $area_id;

    /**
     * @var integer
     */
    public $original_vnum;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $die_message;

    /**
     * @var string
     */
    public $kill_message;

    /**
     * @var string
     */
    public $short_description;

    /**
     * @var string
     */
    public $long_description;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $race;

    /**
     * @var string
     */
    public $flag_action;

    /**
     * @var string
     */
    public $flag_affected;

    /**
     * @var integer
     */
    public $group;

    /**
     * @var integer
     */
    public $level;

    /**
     * @var integer
     */
    public $hitroll;

    /**
     * @var integer
     */
    public $sex;


    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'mobiles';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return [
            'id',
            'area_id',
            'original_vnum',
            'name',
            'die_message',
            'kill_message',
            'short_description',
            'long_description',
            'description',
            'race',
            'flag_action',
            'flag_affected',
            'group',
            'level',
            'hitroll',
            'hit_dice',
            'hit_dice_type',
            'hit_dice_bonus',
            'stamina_dice',
            'stamina_dice_type',
            'stamina_dice_bonus',
            'damage_dice',
            'damage_dice_type',
            'damage_dice_bonus',
            'damage_type',
            'ac_pierce',
            'ac_bash',
            'ac_slash',
            'ac_exotic',
            'flag_offensive',
            'flag_immune',
            'flag_resistance',
            'flag_vulnerable',
            'position_start',
            'position_default',
            'sex',
            'wealth',
            'flag_form',
            'flag_parts',
            'size',
            'material',
            'created_at',
            'updated_at'
        ];
    }


    /**
     * Retrieve the name of the area
     *
     * @return string
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * Update the room id for this location
     *
     * @param int $id
     *
     * @return boolean
     */
    public function setRoomId($id)
    {
        $this->room_id = $id;
        return true;
    }

    /**
     * Retrieve the creation date for the area
     *
     * @return string
     */
    public function getCharacterId()
    {
        return $this->character_id;
    }

    /**
     * Set the character id for this location
     *
     * @param int $id
     *
     * @return boolean
     */
    public function setCharacterId($id)
    {
        $this->character_id = $id;
        return true;
    }

    /**
     * @return int
     */
    public function getAreaId() : int
    {
        return $this->area_id;
    }

    /**
     * @param int $area_id
     */
    public function setAreaId(int $area_id) : void
    {
        $this->area_id = $area_id;
    }

    /**
     * @return int
     */
    public function getOriginalVnum() : int
    {
        return $this->original_vnum;
    }

    /**
     * @param int $original_vnum
     */
    public function setOriginalVnum(int $original_vnum) : void
    {
        $this->original_vnum = $original_vnum;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDieMessage() : string
    {
        return $this->die_message;
    }

    /**
     * @param string $die_message
     */
    public function setDieMessage(string $die_message) : void
    {
        $this->die_message = $die_message;
    }

    /**
     * @return string
     */
    public function getKillMessage() : string
    {
        return $this->kill_message;
    }

    /**
     * @param string $kill_message
     */
    public function setKillMessage(string $kill_message) : void
    {
        $this->kill_message = $kill_message;
    }

    /**
     * @return string
     */
    public function getShortDescription() : string
    {
        return $this->short_description;
    }

    /**
     * @param string $short_description
     */
    public function setShortDescription(string $short_description) : void
    {
        $this->short_description = $short_description;
    }

    /**
     * @return string
     */
    public function getLongDescription() : string
    {
        return $this->long_description;
    }

    /**
     * @param string $long_description
     */
    public function setLongDescription(string $long_description) : void
    {
        $this->long_description = $long_description;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getRace() : string
    {
        return $this->race;
    }

    /**
     * @param string $race
     */
    public function setRace(string $race) : void
    {
        $this->race = $race;
    }

    /**
     * @return string
     */
    public function getFlagAction() : string
    {
        return $this->flag_action;
    }

    /**
     * @param string $flag_action
     */
    public function setFlagAction(string $flag_action) : void
    {
        $this->flag_action = $flag_action;
    }

    /**
     * @return string
     */
    public function getFlagAffected() : string
    {
        return $this->flag_affected;
    }

    /**
     * @param string $flag_affected
     */
    public function setFlagAffected(string $flag_affected) : void
    {
        $this->flag_affected = $flag_affected;
    }

    /**
     * @return int
     */
    public function getGroup() : int
    {
        return $this->group;
    }

    /**
     * @param int $group
     */
    public function setGroup(int $group) : void
    {
        $this->group = $group;
    }

    /**
     * @return int
     */
    public function getLevel() : int
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level) : void
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getHitroll() : int
    {
        return $this->hitroll;
    }

    /**
     * @param int $hitroll
     */
    public function setHitroll(int $hitroll) : void
    {
        $this->hitroll = $hitroll;
    }

    /**
     * @return int
     */
    public function getSex() : int
    {
        return $this->sex;
    }

    /**
     * @param int $sex
     */
    public function setSex(int $sex) : void
    {
        $this->sex = $sex;
    }
}
