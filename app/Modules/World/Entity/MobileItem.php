<?php

namespace Modules\World\Entity;

/**
 * CharacterItem entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class MobileItem extends Item
{
    /**
     * @var int
     */
    public $room_mobile_id;

    /**
     * @var int
     */
    public $inventory;

    /**
     * @var int
     */
    public $character_slot;

    /**
     * @var integer
     */
    public $reset_id;

    /**
     * @var integer
     */
    public $item_id;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'room_mobile_items';
    }

    public function getColumns() : array
    {
        $columns = parent::getColumns();

        $columns[] = 'room_mobile_id';
        $columns[] = 'item_id';
        $columns[] = 'reset_id';
        $columns[] = 'inventory';
        $columns[] = 'character_slot';

        // remove item specific column(s)
        $columns = array_diff($columns, ['area_id']);

        return $columns;
    }

    /**
     * Set the mobile ID for an object
     *
     * @param $id
     */
    public function setRoomMobileId($id)
    {
        $this->room_mobile_id = $id;
    }

    public function getRoomMobileId()
    {
        return $this->room_mobile_id;
    }

    /**
     * Set if the item is in the inventory or not
     *
     * @param bool $inInventory
     */
    public function setInInventory(bool $inInventory)
    {
        $this->inventory = (int)$inInventory;
    }

    /**
     * Is the item in the characters inventory
     *
     * @return bool
     */
    public function isInInventory()
    {
        return (bool)$this->inventory;
    }

    /**
     * @return int|null
     */
    public function getCharacterSlot() : int
    {
        return $this->character_slot;
    }

    /**
     * @param int|null $character_slot
     */
    public function setCharacterSlot(?int $character_slot) : void
    {
        $this->character_slot = $character_slot;
    }

    /**
     * @return mixed
     */
    public function getResetId()
    {
        return $this->reset_id;
    }

    /**
     * @param mixed $reset_id
     */
    public function setResetId(?int $reset_id) : void
    {
        $this->reset_id = $reset_id;
    }

    /**
     * @return int
     */
    public function getItemId() : int
    {
        return $this->item_id;
    }

    /**
     * @param int $item_id
     */
    public function setItemId(int $item_id) : void
    {
        $this->item_id = $item_id;
    }
}
