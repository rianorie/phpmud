<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * Reset entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class MobileReset extends Entity
{
    use Timestamp;

    /**
     * @var int
     */
    public $area_id;

    /**
     * @var int
     */
    public $room_id;

    /**
     * @var int
     */
    public $mobile_id;

    /**
     * @var integer
     */
    public $min;

    /**
     * @var integer
     */
    public $max;

    /**
     * @var int
     */
    public $chance;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'mobile_resets';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return [
            'id',
            'area_id',
            'room_id',
            'mobile_id',
            'min',
            'max',
            'chance',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Return the area id
     *
     * @return int
     */
    public function getAreaId()
    {
        return $this->area_id;
    }

    /**
     * Return the room id
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * Return the reset chance percentage
     *
     * @return int
     */
    public function getChance()
    {
        return $this->chance;
    }

    /**
     * @return int
     */
    public function getMobileId() : int
    {
        return $this->mobile_id;
    }

    /**
     * @param int $mobile_id
     */
    public function setMobileId(int $mobile_id) : void
    {
        $this->mobile_id = $mobile_id;
    }

    /**
     * @return int
     */
    public function getMin() : int
    {
        return $this->min;
    }

    /**
     * @param int $min
     */
    public function setMin(int $min) : void
    {
        $this->min = $min;
    }

    /**
     * @return int
     */
    public function getMax() : int
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax(int $max) : void
    {
        $this->max = $max;
    }
}
