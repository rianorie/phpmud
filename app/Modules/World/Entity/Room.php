<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Entity\Interfaces\Manager;
use Game\Module\Traits\Timestamp;

/**
 * Entity definition for rooms
 *
 * @package PHPMUD\World
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class Room extends Entity
{
    use Timestamp;

    /**
     * @var int
     */
    public $area_id;

    /**
	 * @var string
	 */
	public $title;

    /**
	 * @var string
	 */
	public $description;

    /**
     * @var string
     */
	public $flags;

    /**
     * @var integer
     */
	public $sector_id;

    /**
     * @var integer
     */
	public $original_vnum;

    /**
     * @var integer
     */
	public $heal_hitpoints;

    /**
     * @var integer
     */
	public $heal_stamina;

    /**
     * @var string
     */
	public $extra_descs;

    /**
     * @var integer
     */
	public $grade;

	/**
	 * {@inheritdoc}
	 *
	 * @return string
	 */
	public function getTable() : string
	{
		return 'rooms';
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return array
	 */
	public function getColumns() : array
	{
		return [
            'id',
            'area_id',
            'title',
            'description',
            'created_at',
            'updated_at',
            'flags',
            'sector_id',
            'original_vnum',
            'heal_hitpoints',
            'heal_stamina',
            'extra_descs',
            'grade'
        ];
	}


	/**
	 * Return the room title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Return the room description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Return the room area id
	 *
	 * @return int
	 */
	public function getAreaId()
	{
		return $this->area_id;
	}

    /**
     * @return array
     */
    public function getFlags() : array
    {
        return explode(', ', $this->flags);
    }

    /**
     * @param array $flags
     */
    public function setFlags(array $flags) : void
    {
        $this->flags = implode(', ', $flags);
    }

    /**
     * @return int
     */
    public function getSectorId() : int
    {
        return $this->sector_id;
    }

    /**
     * @param int $sector_id
     */
    public function setSectorId(int $sector_id) : void
    {
        $this->sector_id = $sector_id;
    }

    /**
     * @return int|null
     */
    public function getOriginalVnum() : ?int
    {
        return $this->original_vnum;
    }

    /**
     * @param int|null $original_vnum
     */
    public function setOriginalVnum(?int $original_vnum) : void
    {
        $this->original_vnum = $original_vnum;
    }

    /**
     * @return int
     */
    public function getHealHitpoints() : int
    {
        return $this->heal_hitpoints;
    }

    /**
     * @param int $heal_hitpoints
     */
    public function setHealHitpoints(int $heal_hitpoints) : void
    {
        $this->heal_hitpoints = $heal_hitpoints;
    }

    /**
     * @return int
     */
    public function getHealStamina() : int
    {
        return $this->heal_stamina;
    }

    /**
     * @param int $heal_stamina
     */
    public function setHealStamina(int $heal_stamina) : void
    {
        $this->heal_stamina = $heal_stamina;
    }

    /**
     * @return array
     */
    public function getExtraDescs() : array
    {
        return json_decode($this->extra_descs, true);
    }

    /**
     * @param array $extra_descs
     */
    public function setExtraDescs(array $extra_descs) : void
    {
        $this->extra_descs = json_encode($extra_descs);
    }

    /**
     * @return int
     */
    public function getGrade() : int
    {
        return $this->grade;
    }

    /**
     * @param int $grade
     */
    public function setGrade(int $grade) : void
    {
        $this->grade = $grade;
    }
}
