<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * RoomItem entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class RoomItem extends Item
{
    /**
     * @var int
     */
    public $area_id;

    /**
     * @var int
     */
    public $room_id;

    /**
     * @var int
     */
    public $item_id;

    /**
     * @var int
     */
    public $reset_id;

    /**
     * @var int
     */
    public $parent_id;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'room_items';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return array_merge(parent::getColumns(), [
            'room_id',
            'item_id',
            'reset_id'
        ]);
    }

    /**
     * Return the area id
     *
     * @return int
     */
    public function getAreaId()
    {
        return $this->area_id;
    }

    /**
     * Set the area id
     *
     * @param int $id
     */
    public function setAreaId($id)
    {
        $this->area_id = $id;
    }

    /**
     * Return the room id
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * Set the room id
     *
     * @param int $id
     */
    public function setRoomId($id)
    {
        $this->room_id = $id;
    }

    /**
     * Return the item id
     *
     * @return int
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * Return the reset id
     *
     * @return int
     */
    public function getResetId()
    {
        return $this->reset_id;
    }

    /**
     * Return the parent id
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }
}
