<?php

namespace Modules\World\Entity;

/**
 * RoomMobile entity
 *
 * @package PHPMUD\Help
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2019-07-28
 * @version 1.0
 */
class RoomMobile extends Mobile
{
    /**
     * @var int
     */
    public $room_id;

    /**
     * @var int
     */
    public $mobile_id;

    /**
     * @var int
     */
    public $reset_id;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'room_mobiles';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return array_merge(parent::getColumns(), [
            'room_id',
            'mobile_id',
            'reset_id'
        ]);
    }

    /**
     * Return the room id
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * Set the room id
     *
     * @param int $id
     */
    public function setRoomId($id)
    {
        $this->room_id = $id;
    }

    /**
     * Return the item id
     *
     * @return int
     */
    public function getMobileId()
    {
        return $this->mobile_id;
    }

    /**
     * @param int $mobile_id
     */
    public function setMobileId(int $mobile_id) : void
    {
        $this->mobile_id = $mobile_id;
    }

    /**
     * Return the reset id
     *
     * @return int
     */
    public function getResetId()
    {
        return $this->reset_id;
    }

    /**
     * @param int $reset_id
     */
    public function setResetId(int $reset_id) : void
    {
        $this->reset_id = $reset_id;
    }
}
