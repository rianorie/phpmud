<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Timestamp;

/**
 * SectorMessage entity for the World module
 *
 * @package PHPMUD\World
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class SectorMessage extends Entity
{
    use Timestamp;

    /**
     * @var int
     */
    public $sector_id;

    /**
     * @var int
     */
    public $hour_from;

    /**
     * @var int
     */
    public $hour_to;

    /**
     * @var string
     */
    public $message;

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getTable() : string
    {
        return 'area_sector_messages';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return ['id', 'sector_id', 'hour_from', 'hour_to', 'message'];
    }

    /**
     * @return int
     */
    public function getSectorId() : int
    {
        return $this->sector_id;
    }

    /**
     * @param int $sector_id
     */
    public function setSectorId(int $sector_id) : void
    {
        $this->sector_id = $sector_id;
    }

    /**
     * @return int
     */
    public function getHourFrom() : int
    {
        return $this->hour_from;
    }

    /**
     * @param int $hour_from
     */
    public function setHourFrom(int $hour_from) : void
    {
        $this->hour_from = $hour_from;
    }

    /**
     * @return int
     */
    public function getHourTo() : int
    {
        return $this->hour_to;
    }

    /**
     * @param int $hour_to
     */
    public function setHourTo(int $hour_to) : void
    {
        $this->hour_to = $hour_to;
    }

    /**
     * @return string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message) : void
    {
        $this->message = $message;
    }

}
