<?php

namespace Modules\World\Entity;

use Game\Module\Entity\Entity;
use Game\Module\Traits\Temporary;
use Game\Module\Traits\Timestamp;

/**
 * Skeleton entity - Servers as the basis for the character entity
 *
 * @package PHPMUD\Character
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-27
 * @version 1.0
 */
class Skeleton extends Entity
{
    use Timestamp;

    use Temporary;

    /**
     * @var string
     */
    public $name;

    public $gender;

    public $race;

    public $channeler;

    /**
     * @var int
     */
    public $hitpoints;

    /**
     * @var int
     */
    public $max_hitpoints;

    /**
     * @var int
     */
    public $stamina;

    /**
     * @var int
     */
    public $max_stamina;

    /**
     * @var int
     */
    public $strength;

    /**
     * @var int
     */
    public $dexterity;

    /**
     * @var int
     */
    public $constitution;

    /**
     * @var int
     */
    public $reflex;

    /**
     * @var int
     */
    public $intelligence;
    /**
     * @var int
     */
    public $wisdom;

    /**
     * @var int
     */
    public $instinct;

    /**
     * @var int
     */
    public $charisma;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $roomdesc;

    /**
     * @var string
     */
    public $walkin;

    /**
     * @var string
     */
    public $walkout;

    /**
     * @var integer
     */
    public $position;

    public function getTable() : string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getColumns() : array
    {
        return [
            'id',
            'name',
            'gender',
            'race',
            'channeler',
            'created_at',
            'updated_at',
            'hitpoints',
            'max_hitpoints',
            'stamina',
            'max_stamina',
            'strength',
            'dexterity',
            'constitution',
            'reflex',
            'intelligence',
            'wisdom',
            'instinct',
            'charisma'
        ];
    }

    /**
     * Return the character name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the character name
     *
     * @param string $name A name for the character
     *
     * @return boolean
     */
    public function setName($name)
    {
        if (preg_match('/^[a-zA-Z]+$/', $name)) {
            $this->name = $name;
            return true;
        }

        return false;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender(int $gender)
    {
        $this->gender = $gender;
    }

    public function getRace()
    {
        return $this->race;
    }

    public function setRace(string $race)
    {
        $this->race = $race;
    }

    public function getChanneler()
    {
        return $this->channeler;
    }

    public function setChanneler(bool $channeler)
    {
        $this->channeler = (int) $channeler;
    }

    /**
     * @return int
     */
    public function getHitpoints() : int
    {
        return $this->hitpoints;
    }

    /**
     * @param int $hitpoints
     */
    public function setHitpoints(int $hitpoints) : void
    {
        $this->hitpoints = $hitpoints;
    }

    /**
     * @return int
     */
    public function getMaxHitpoints() : int
    {
        return $this->max_hitpoints;
    }

    /**
     * @param int $max_hitpoints
     */
    public function setMaxHitpoints(int $max_hitpoints) : void
    {
        $this->max_hitpoints = $max_hitpoints;
    }

    /**
     * @return int
     */
    public function getStamina() : int
    {
        return $this->stamina;
    }

    /**
     * @param int $stamina
     */
    public function setStamina(int $stamina) : void
    {
        $this->stamina = $stamina;
    }

    /**
     * @return int
     */
    public function getMaxStamina() : int
    {
        return $this->max_stamina;
    }

    /**
     * @param int $max_stamina
     */
    public function setMaxStamina(int $max_stamina) : void
    {
        $this->max_stamina = $max_stamina;
    }

    /**
     * @return int
     */
    public function getStrength() : int
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     */
    public function setStrength(int $strength) : void
    {
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getConstitution() : int
    {
        return $this->constitution;
    }

    /**
     * @param int $constitution
     */
    public function setConstitution(int $constitution) : void
    {
        $this->constitution = $constitution;
    }

    /**
     * @return int
     */
    public function getReflex() : int
    {
        return $this->reflex;
    }

    /**
     * @param int $reflex
     */
    public function setReflex(int $reflex) : void
    {
        $this->reflex = $reflex;
    }

    /**
     * @return int
     */
    public function getIntelligence() : int
    {
        return $this->intelligence;
    }

    /**
     * @param int $intelligence
     */
    public function setIntelligence(int $intelligence) : void
    {
        $this->intelligence = $intelligence;
    }

    /**
     * @return int
     */
    public function getWisdom() : int
    {
        return $this->wisdom;
    }

    /**
     * @param int $wisdom
     */
    public function setWisdom(int $wisdom) : void
    {
        $this->wisdom = $wisdom;
    }

    /**
     * @return int
     */
    public function getInstinct() : int
    {
        return $this->instinct;
    }

    /**
     * @param int $instinct
     */
    public function setInstinct(int $instinct) : void
    {
        $this->instinct = $instinct;
    }

    /**
     * @return int
     */
    public function getCharisma() : int
    {
        return $this->charisma;
    }

    /**
     * @param int $charisma
     */
    public function setCharisma(int $charisma) : void
    {
        $this->charisma = $charisma;
    }

    /**
     * @return int
     */
    public function getDexterity() : int
    {
        return $this->dexterity;
    }

    /**
     * @param int $dexterity
     */
    public function setDexterity(int $dexterity) : void
    {
        $this->dexterity = $dexterity;
    }

    /**
     * @return string|null
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getRoomdesc() : ?string
    {
        return $this->roomdesc;
    }

    /**
     * @param string|null $roomdesc
     */
    public function setRoomdesc(?string $roomdesc) : void
    {
        $this->roomdesc = $roomdesc;
    }

    /**
     * @return string|null
     */
    public function getWalkin() : ?string
    {
        return $this->walkin;
    }

    /**
     * @param string|null $walkin
     */
    public function setWalkin(?string $walkin) : void
    {
        $this->walkin = $walkin;
    }

    /**
     * @return string|null
     */
    public function getWalkout() : ?string
    {
        return $this->walkout;
    }

    /**
     * @param string|null $walkout
     */
    public function setWalkout(?string $walkout) : void
    {
        $this->walkout = $walkout;
    }

    /**
     * @return int
     */
    public function getPosition() : int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position) : void
    {
        $this->position = $position;
    }
}
