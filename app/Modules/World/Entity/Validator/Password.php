<?php

namespace Modules\World\Entity\Validator;

use Game\Module\Entity\Interfaces\Validator;

class Password implements Validator
{
    public function validate($input) : bool
    {
        return strlen($input) >= 8;
    }
}
