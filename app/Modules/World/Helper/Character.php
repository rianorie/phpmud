<?php

namespace Modules\World\Helper;

use Game\Module\Entity\Interfaces\Entity as EntityInterface;
use Game\Registry;
use JetBrains\PhpStorm\Pure;
use Modules\World\Entity\Character as CharacterEntity;
use Modules\World\Entity\Room;
use Modules\World\Helper\Room as RoomHelper;
use Modules\World\Table\Position;

/**
 * Character Helper
 *
 * @package PHPMUD\World
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-08-02
 * @version 1.0
 */
class Character
{
    /**
     * @var RoomHelper
     */
    protected \Modules\World\Helper\Room $roomHelper;

    public function __construct(RoomHelper $roomHelper)
    {
        $this->roomHelper = $roomHelper;
    }

    /**
     * Add a character to the list
     *
     * @param CharacterEntity $character
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function addCharacter(CharacterEntity $character) : bool
    {
        $list = $map = [];

        try {

            if (Registry::exists('character.list')) {
                $list = Registry::get('character.list');
            }

            if (Registry::exists('character.map')) {
                $map = Registry::get('character.map');
            }

            $list[$character->getId()]  = $character;
            $map[$character->getName()] = $character->getId();

            Registry::set('character.list', $list);
            Registry::set('character.map', $map);

            return true;

        } catch (\Exception $exception) {

            return false;
        }
    }

    /**
     * Remove a character from the list
     *
     * @param CharacterEntity $character
     *
     * @return bool
     */
    public function removeCharacter(CharacterEntity $character) : bool
    {
        try {
            $list = $map = [];

            if (Registry::exists('character.list')) {
                $list = Registry::get('character.list');
            }

            if (Registry::exists('character.map')) {
                $map = Registry::get('character.map');
            }

            unset($list[$character->getId()], $map[$character->getName()]);

            Registry::set('character.list', $list);
            Registry::set('character.map', $map);

            return true;

        } catch (\Exception $exception) {

            #TODO Add logging?
            return false;
        }
    }

    /**
     * Retrieve the list of characters
     *
     * @return CharacterEntity[]
     */
    public function getCharacters() : array
    {
        try {
            $list = [];

            if (Registry::exists('character.list')) {
                $list = Registry::get('character.list');
            }

            return $list;

        } catch (\Exception $exception) {

            return [];
        }
    }

    /**
     * Retrieve a specific character by its ID
     *
     * @param int $id The id to retrieve the character for
     *
     * @return CharacterEntity|bool
     */
    public function getCharacterById(int $id) : bool|CharacterEntity
    {
        $list = self::getCharacters();

        if ($list && isset($list[$id])) {
            return $list[$id];
        }

        return false;
    }

    /**
     * Retrieve a specific character by its name
     *
     * @param string $name The name of the character
     *
     * @return bool|CharacterEntity
     */
    public function getCharacterByName(string $name) : bool|CharacterEntity
    {
        try {
            $map = false;

            if (Registry::exists('character.map')) {
                $map = Registry::get('character.map');
            }

            if ($map && isset($map[$name])) {
                return self::getCharacterById($map[$name]);
            }

            return false;

        } catch (\Exception $exception) {

            #TODO log exceptions?
            return false;
        }
    }

    /**
     * Get the currently online room characters
     *
     * @param Room                 $room
     * @param EntityInterface|null $except
     * @param bool                 $noSleeping
     *
     * @return array|bool|\Generator
     */
    public function getOnlineRoomCharacters(Room $room, EntityInterface $except = null, $noSleeping = true) : array|bool|\Generator
    {
        // grab the character ids for this room
//        $ids = $this->roomHelper->getCharacterIds($room);

        // grab the online ids
        $online = $this->getCharacters();

        foreach($online as $character) {
            if ($character &&
                ($character->getData('room') && $character->getData('room')->getId() != $room->getId()) &&
                (is_null($except) || $character->getId() != $except->getId())) {

                if ($noSleeping && $character->getPosition() == Position::SLEEP) {
                    continue;
                }

                yield $character;
            }
        }

//        // if there's actually other players in the room, output their names. If an
//        // exception is defined, ignore that particular one
//        if (count($ids) > 0) {
//            foreach ($ids as $id) {
//                var_dump($id, isset($online[$id]), is_null($except) || $id !== $except->getId());
//                if (isset($online[$id]) && (is_null($except) || $id !== $except->getId())) {
//
//                    if ($exceptSleeping && $online[$id]->getPosition() == Position::SLEEP) {
//                        continue;
//                    }
//
//                    yield $online[$id];
//                }
//            }
//        }

        return false;
    }

    /**
     * Retrieve a who string based on the character information
     *
     * @param CharacterEntity $character
     *
     * @return string
     */
    #[Pure] public function getPublicInfo(CharacterEntity $character) : string
    {
        return sprintf(
            "%s %s. %s",
            $character->getName(),
            $character->getLastname(),
            $character->getTitle()
        );
    }
}
