<?php

namespace Modules\World\Helper\Character;

use Modules\World\Entity\Character as CharacterEntity;

class Password
{
    /**
     * Validate a password against the given password
     *
     * @param CharacterEntity $character The character which to check
     * @param string          $password  The password to check
     *
     * @return bool
     */
    public function checkPassword(CharacterEntity $character, $password)
    {
        // old passwords are short, new passwords are much longer
        if (strlen($character->getPassword()) < 50) {
            return ($character->getPassword() == crypt($password, $character->getName()));

        } else {
            return password_verify($password, $character->getPassword());
        }
    }

    /**
     * Check to make sure the password uses the new algorithm
     *
     * @param CharacterEntity $character
     *
     * @return bool
     */
    public function isPasswordNew(CharacterEntity $character)
    {
        return (strlen($character->getPassword()) > 50);
    }

    /**
     * Generate a password hash based on the given password
     *
     * @param string $password
     *
     * @return string
     */
    public function generatePasswordHash(string $password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}
