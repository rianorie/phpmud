<?php

namespace Modules\World\Helper\Character;

use Game\Connection\Interfaces\Connection;

class State
{
    const INACTIVE = 0;

    const ACTIVE   = 1;

    const NOTE     = 2;

    const AFK      = 3;

    /**
     * Update the current connection state
     *
     * @param Connection $connection
     * @param int        $state
     */
    public function setState(Connection $connection, int $state)
    {
        $connection->setData('state', $state);
    }

    /**
     * Retrieve the current connection state
     *
     * @param Connection $connection
     *
     * @return int
     */
    public function getState(Connection $connection) : int
    {
        return ($connection->getData('state') ?? $this::INACTIVE);
    }
}
