<?php

namespace Modules\World\Helper;

use Game\Module\Entity\Interfaces\Manager;
use Modules\World\Entity\Character;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\Room;
use Modules\World\Entity\RoomItem;
use Modules\World\Entity\Item as ItemEntity;
use Modules\World\Table\Container;

class Item
{
    /**
     * @var Manager
     */
    protected $entityManager;

    public function __construct(Manager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Find the given room item by input
     *
     * @param Room $room
     * @param string $input
     *
     * @return RoomItem[]|false
     */
    public function getRoomItemsByInput(Room $room, string $input)
    {
        $pos = 1;
        $keyword = $input;

        if (preg_match('/(\d+)|all\.(.+)/', $input)) {
            list($pos, $keyword) = explode('.', $input);
        }

        $loopPosition = 1;

        $output = [];

        /** @var RoomItem[] $roomItemsCollection */
        $roomItemsCollection = $this->entityManager->find(new RoomItem($this->entityManager), ['room_id' => $room->getId()]);
        foreach ($roomItemsCollection as $roomItem) {
            if ($roomItem) {

                foreach ($roomItem->getKeywords() as $itemKeyword) {
                    if (stripos($itemKeyword, $keyword) === 0) {
                        if ($pos == $loopPosition) {
                            $output[] = $roomItem;
                        }

                        $loopPosition++;
                    }
                }
            }
        }

        return (count($output) ? $output : false);
    }

    /**
     * Retrieve the first matched room item
     *
     * @param Room   $room
     * @param string $input
     *
     * @return bool|RoomItem
     */
    public function getFirstRoomItemByInput(Room $room, string $input)
    {
        $items = $this->getRoomItemsByInput($room, $input);
        if ($items) {
            return $items[0];
        }

        return false;
    }

    /**
     * Find the given child item by input
     *
     * @param ItemEntity $item
     * @param string $input
     *
     * @return ItemEntity[]|false
     */
    public function getChildItemsByInput(ItemEntity $item, string $input)
    {
        $pos = 1;
        $keyword = $input;

        if (preg_match('/(\d+)|all\.(.+)/', $input)) {
            list($pos, $keyword) = explode('.', $input);
        }

        $loopPosition = 1;

        $output = [];

        /** @var ItemEntity[] $itemsCollection */
        $itemsCollection = $this->entityManager->find($item, ['parent_id' => $item->getId()]);
        foreach ($itemsCollection as $childItem) {
            if ($childItem) {

                foreach ($childItem->getKeywords() as $itemKeyword) {
                    if (stripos($itemKeyword, $keyword) === 0) {
                        if ($pos == 'all' || $pos == $loopPosition) {
                            $output[] = $childItem;
                        }

                        $loopPosition++;
                    }
                }
            }
        }

        return (count($output) == 0 ? false : $output);
    }

    /**
     * Find the given character item by input
     *
     * @param Character $character
     * @param string    $input
     * @param bool      $onlyInventory
     * @param bool      $onlyWorn
     *
     * @return bool|CharacterItem[]
     */
    public function getCharacterItemsByInput(Character $character, string $input, bool $onlyInventory = false, bool $onlyWorn = false)
    {
        $pos = 1;
        $keyword = $input;

        if (preg_match('/(\d+)|all\.(.+)/', $input)) {
            list($pos, $keyword) = explode('.', $input);
        }

        $loopPosition = 1;

        $filters = [
            'character_id' => $character->getId(),
            'parent_id' => null
        ];

        if ($onlyInventory) {
            $filters['inventory'] = 1;

        } elseif ($onlyWorn) {
            $filters['inventory'] = 0;
        }

        /** @var CharacterItem[] $characterItemsCollection */
        $characterItemsCollection = $this->entityManager->find(
            new CharacterItem($this->entityManager),
            $filters
        );

        $output = [];

        foreach ($characterItemsCollection as $characterItem) {
            if ($characterItem) {

                foreach ($characterItem->getKeywords() as $itemKeyword) {
                    if (stripos($itemKeyword, $keyword) === 0) {
                        if ($pos == 'all' || $pos == $loopPosition) {
                            $output[] = $characterItem;
                        }

                        $loopPosition++;
                    }
                }
            }
        }

        return (count($output) ? $output : false);
    }

    /**
     * Retrieve the first character item by input
     *
     * @param Character $character
     * @param string    $input
     * @param bool      $onlyInventory
     * @param bool      $onlyWorn
     *
     * @return bool|CharacterItem
     */
    public function getFirstCharacterItemByInput(Character $character, string $input, bool $onlyInventory = false, bool $onlyWorn = false)
    {
        $item = $this->getCharacterItemsByInput($character, $input, $onlyInventory, $onlyWorn);
        if ($item) {
            return $item[0];
        }

        return false;
    }

    /**
     * Move an item from the room to a character
     *
     * @param RoomItem           $roomItem
     * @param Character          $character
     * @param CharacterItem|null $parentObject
     *
     * @return CharacterItem|false
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function roomItemToCharacterItem(RoomItem $roomItem, Character $character, ?CharacterItem $parentObject = null)
    {
        $characterItem = new CharacterItem($this->entityManager);
        $this->importItem($roomItem, $characterItem);
        $characterItem->setCharacterId($character->getId());
        $characterItem->setInInventory(true);
        $characterItem->setParentId(null);

        if ( ! is_null($parentObject)) {
            $characterItem->setParentId($parentObject->getId());
        }

        // save it
        if ($this->entityManager->save($characterItem)) {

            if ($roomItem->isContainer()) {

                /** @var RoomItem[] $childItems */
                $childItems = $this->entityManager->find(new RoomItem, ['parent_id' => $roomItem->getId()]);
                foreach($childItems as $childItem) {
                    if ($childItem) {
                        $this->roomItemToCharacterItem($childItem, $character, $characterItem);
                    }
                }
            }

            // remove the item in the room
            $this->entityManager->delete($roomItem);

            // return it for usage
            return $characterItem;
        }

        return false;
    }

    /**
     * Move an item from a character to a room
     *
     * @param CharacterItem $characterItem
     * @param Room          $room
     *
     * @param RoomItem|null $parentObject
     *
     * @return bool|RoomItem
     * @throws \Game\Module\Entity\Exception
     */
    public function characterItemToRoomItem(CharacterItem $characterItem, Room $room, ?RoomItem $parentObject = null)
    {
        $roomItem = new RoomItem($this->entityManager);
        $this->importItem($characterItem, $roomItem);
        $roomItem->setAreaId($room->getAreaId());
        $roomItem->setRoomId($room->getId());

        if (! is_null($parentObject)) {
            $roomItem->setParentId($parentObject->getId());
        }

        // save it
        if ($this->entityManager->save($roomItem)) {

            if ($characterItem->isContainer()) {

                /** @var CharacterItem[] $childItems */
                $childItems = $this->entityManager->find(new CharacterItem(), ['parent_id' => $characterItem->getId()]);
                foreach($childItems as $childItem) {
                    if ($childItem) {
                        $this->characterItemToRoomItem($childItem, $room, $roomItem);
                    }
                }
            }

            // remove the item in the room
            $this->entityManager->delete($characterItem);

            // return it for usage
            return $roomItem;
        }

        return false;
    }

    /**
     * Move an item from a character to a room
     *
     * @param CharacterItem $characterItem
     * @param Character     $character
     *
     * @return bool|CharacterItem
     * @throws \Game\Module\Entity\Exception
     */
    public function characterItemToCharacter(CharacterItem $characterItem, Character $character)
    {
        $characterItem->setCharacterId($character->getId());

        // save it
        if ($this->entityManager->update($characterItem, 'character_id')) {

            if ($characterItem->isContainer()) {

                /** @var CharacterItem[] $childItems */
                $childItems = $this->entityManager->find(new CharacterItem(), ['parent_id' => $characterItem->getId()]);
                foreach($childItems as $childItem) {
                    if ($childItem) {
                        $childItem->setCharacterId($character->getId());
                        $this->entityManager->update($childItem, 'character_id');
                    }
                }
            }

            // return it for usage
            return $characterItem;
        }

        return false;
    }

    /**
     * Import information from one object into another
     *
     * @param ItemEntity $fromItem
     * @param ItemEntity $toItem
     *
     * @return ItemEntity
     */
    public function importItem(ItemEntity $fromItem, ItemEntity $toItem)
    {
        foreach($fromItem->getColumns() as $column) {
            if (in_array($column, $toItem->getColumns())) {
                $toItem->{$column} = $fromItem->{$column};
            }
        }

        return $toItem;
    }

    /**
     * @param ItemEntity $item
     *
     * @return bool
     */
    public function canOpen(ItemEntity $item) : bool
    {
        return $item->isContainer() && in_array(Container::CLOSEABLE, explode(', ', $item->v1));
    }

    /**
     * @param ItemEntity $item
     *
     * @return bool
     */
    public function canLock(ItemEntity $item) : bool
    {
        return $item->isContainer() && in_array(Container::LOCKABLE, explode(', ', $item->v1));
    }

    /**
     * @param ItemEntity $item
     *
     * @return bool
     */
    public function isOpen(ItemEntity $item) : bool
    {
        return $item->isContainer()  &&
               ( ! is_null($item->getMetadata('open')) ?
                   $item->getMetadata('open') :
                   ! in_array(Container::CLOSED, explode(', ', $item->v1))
               );
    }

    /**
     * @param ItemEntity $item
     *
     * @return bool
     */
    public function isLocked(ItemEntity $item) : bool
    {
        return $item->isContainer()  &&
               ( ! is_null($item->getMetadata('locked')) ?
                   $item->getMetadata('locked') :
                   in_array(Container::LOCKED, explode(', ', $item->v1)));
    }

    /**
     * @param ItemEntity $item
     * @param bool       $state
     */
    public function setOpen(ItemEntity $item, bool $state) : void
    {
        $item->setMetadata('open', $state);
    }

    /**
     * @param ItemEntity $item
     * @param bool       $state
     */
    public function setLocked(ItemEntity $item, bool $state) : void
    {
        $item->setMetadata('locked', $state);
    }

    protected function recursiveSetCharacterItemToRoom(CharacterItem $characterItem, RoomItem $roomItem)
    {
        if ($characterItem->isContainer()) {

            /** @var CharacterItem[] $childItems */
            $childItems = $this->entityManager->find(new CharacterItem(), ['parent_id' => $characterItem->getId()]);
            foreach($childItems as $childItem) {
                if ($childItem) {
                    $childRoomItem = new RoomItem();
                    $this->importItem($childItem, $childRoomItem);
                    $childRoomItem->setParentId($roomItem->getId());

                    $this->entityManager->save($childRoomItem);

                    $this->recursiveSetCharacterItemToRoom($childItem, $childRoomItem);

                    $this->entityManager->delete($childItem);
                }
            }
        }
    }
}
