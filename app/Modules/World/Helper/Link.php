<?php

namespace Modules\World\Helper;

use Game\Module\Entity\Interfaces\Manager;
use Game\Registry;
use Modules\World\Entity\Link as LinkEntity;
use Modules\World\Entity\Room as RoomEntity;

class Link
{
    /**
     * @var Manager
     */
    protected $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Retrieve the id that goes with a direction name
     *
     * @param string $name The link direction name
     *
     * @return bool|int
     */
    public function getDirectionIdForName($name)
    {
        switch (trim(strtolower($name))) {
            case 'north':
                return LinkEntity::DIRECTION_NORTH;
            case 'east':
                return LinkEntity::DIRECTION_EAST;
            case 'south':
                return LinkEntity::DIRECTION_SOUTH;
            case 'west':
                return LinkEntity::DIRECTION_WEST;
            case 'up':
                return LinkEntity::DIRECTION_UP;
            case 'down':
                return LinkEntity::DIRECTION_DOWN;
            case 'southeast':
                return LinkEntity::DIRECTION_SOUTHEAST;
            case 'southwest':
                return LinkEntity::DIRECTION_SOUTHWEST;
            case 'northeast':
                return LinkEntity::DIRECTION_NORTHEAST;
            case 'northwest':
                return LinkEntity::DIRECTION_NORTHWEST;
        }

        return false;
    }

    /**
     * Get the reverse direction for a given a direction
     *
     * @param int|string $direction The direction to retrieve the reverse for
     *
     * @return bool|int
     */
    public function getReverseDirection($direction)
    {
        if ( ! is_numeric($direction)) {
            $direction = $this->getDirectionIdForName($direction);
        }

        switch ($direction) {
            case LinkEntity::DIRECTION_NORTH:
                return LinkEntity::DIRECTION_SOUTH;
            case LinkEntity::DIRECTION_EAST:
                return LinkEntity::DIRECTION_WEST;
            case LinkEntity::DIRECTION_SOUTH:
                return LinkEntity::DIRECTION_NORTH;
            case LinkEntity::DIRECTION_WEST:
                return LinkEntity::DIRECTION_EAST;
            case LinkEntity::DIRECTION_UP:
                return LinkEntity::DIRECTION_DOWN;
            case LinkEntity::DIRECTION_DOWN:
                return LinkEntity::DIRECTION_UP;
            case LinkEntity::DIRECTION_SOUTHEAST:
                return LinkEntity::DIRECTION_NORTHWEST;
            case LinkEntity::DIRECTION_SOUTHWEST:
                return LinkEntity::DIRECTION_NORTHEAST;
            case LinkEntity::DIRECTION_NORTHEAST:
                return LinkEntity::DIRECTION_SOUTHWEST;
            case LinkEntity::DIRECTION_NORTHWEST:
                return LinkEntity::DIRECTION_SOUTHEAST;
        }

        return false;
    }

    /**
     * Get the direction name for a direction number
     *
     * @param int $direction The direction to retrieve the name for
     *
     * @return string
     */
    public function getDirectionNameForId($direction)
    {
        switch ($direction) {
            case LinkEntity::DIRECTION_NORTH:
                return 'north';
            case LinkEntity::DIRECTION_EAST:
                return 'east';
            case LinkEntity::DIRECTION_SOUTH:
                return 'south';
            case LinkEntity::DIRECTION_WEST:
                return 'west';
            case LinkEntity::DIRECTION_UP:
                return 'up';
            case LinkEntity::DIRECTION_DOWN:
                return 'down';
            case LinkEntity::DIRECTION_SOUTHEAST:
                return 'southeast';
            case LinkEntity::DIRECTION_SOUTHWEST:
                return 'southwest';
            case LinkEntity::DIRECTION_NORTHEAST:
                return 'northeast';
            case LinkEntity::DIRECTION_NORTHWEST:
                return 'northwest';
        }

        return 'invalid';
    }

    public function getDirectionIdForInput($input)
    {
        $map = [
            'north' => LinkEntity::DIRECTION_NORTH,
            'east' => LinkEntity::DIRECTION_EAST,
            'south' => LinkEntity::DIRECTION_SOUTH,
            'west' => LinkEntity::DIRECTION_WEST,
            'up' => LinkEntity::DIRECTION_UP,
            'down' => LinkEntity::DIRECTION_DOWN,
            'southeast' => LinkEntity::DIRECTION_SOUTHEAST,
            'se' => LinkEntity::DIRECTION_SOUTHEAST,
            'southwest' => LinkEntity::DIRECTION_SOUTHWEST,
            'sw' => LinkEntity::DIRECTION_SOUTHWEST,
            'northeast' => LinkEntity::DIRECTION_NORTHEAST,
            'ne' => LinkEntity::DIRECTION_NORTHEAST,
            'northwest' => LinkEntity::DIRECTION_NORTHWEST,
            'nw' => LinkEntity::DIRECTION_NORTHWEST
        ];

        foreach($map as $key => $value) {
            if (stripos($key, $input) !== false) {
                return $value;
            }
        }

        return false;
    }

    public function getRoomLinks(RoomEntity $room)
    {
        return $this->entityManager->find(new \Modules\World\Entity\Link(), ['room_id' => $room->getId()]);
    }

    public function getRoomLinkByInput(RoomEntity $room, string $input)
    {
        $id = $this->getDirectionIdForInput($input);

        $key = 'direction';
        if ( ! $id) {
            $key = 'keyword';
            $input .= '%';
        } else {
            $input = $id;
        }

        $rooms = $this->entityManager->find(new \Modules\World\Entity\Link(), ['room_id' => $room->getId(), $key => $input]);
        foreach($rooms as $room) {
            if ($room) {
                return $room;
            }
        }

        return false;
    }

    /**
     * @param LinkEntity $link
     *
     * @return bool
     */
    public function isOpen(LinkEntity $link) : bool
    {
        $doors = [];

        try {

            if (Registry::exists('link.doors')) {
                $doors = Registry::get('link.doors');
            }

            // if it's set, it could be both
            if (isset($doors[$link->getId()])) {
                return $doors[$link->getId()];
            }

            // if it's not set it'll always be closed
            return false;

        } catch (\Exception $exception) {

            return false;
        }
    }

    /**
     * @param LinkEntity $link
     *
     * @return bool
     */
    public function isLocked(LinkEntity $link) : bool
    {
        $locks = [];

        try {

            if (Registry::exists('link.locks')) {
                $locks = Registry::get('link.locks');
            }

            // if it's set, it could be both
            if (isset($locks[$link->getId()])) {
                return $locks[$link->getId()];
            }

            // if it's not set it'll always be locked
            return true;

        } catch (\Exception $exception) {

            return false;
        }
    }

    /**
     * @param LinkEntity $link
     * @param bool       $state
     */
    public function setOpen(LinkEntity $link, bool $state) : void
    {
        $doors = [];

        try {

            if (Registry::exists('link.doors')) {
                $doors = Registry::get('link.doors');
            }

            $doors[$link->getId()] = $state;

            $reverseGenerator = $this->entityManager->find(
                new LinkEntity(),
                [
                    'room_id' => $link->getTargetId(),
                    'direction' => $this->getReverseDirection($link->getDirection())
                ]
            );

            foreach($reverseGenerator as $reverse) {
                if ($reverse) {
                    $doors[$reverse->getId()] = $state;
                }
            }

            Registry::set('link.doors', $doors);

        } catch (\Exception $exception) {

        }
    }

}
