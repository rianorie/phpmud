<?php

namespace Modules\World\Helper;

use Game\Module\Entity\Interfaces\Manager;
use Modules\World\Entity\Item as ItemEntity;
use Modules\World\Entity\Mobile as MobileEntity;
use Modules\World\Entity\MobileItem;
use Modules\World\Entity\Room as RoomEntity;
use Modules\World\Entity\RoomMobile;
use Modules\World\Table\CharacterWearslot;

class Mobile
{
    /**
     * @var Manager
     */
    protected $entityManager;

    public function __construct(Manager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Import information from one mobile into another
     *
     * @param MobileEntity $fromMobile
     * @param MobileEntity $toMobile
     *
     * @return MobileEntity
     */
    public function importMobile(MobileEntity $fromMobile, MobileEntity $toMobile)
    {
        foreach($fromMobile->getColumns() as $column) {
            if ($column != 'id' && in_array($column, $fromMobile->getColumns())) {
                $toMobile->{$column} = $fromMobile->{$column};
            }
        }

        return $toMobile;
    }

    public function getRoomMobileByInput(RoomEntity $room, string $input)
    {
        $pos = 1;
        $keyword = $input;

        if (preg_match('/(\d+)\.(.+)/', $input)) {
            list($pos, $keyword) = explode('.', $input);
        }

        $loopPosition = 1;

        /** @var RoomMobile[] $mobileItemsCollection */
        $mobileItemsCollection = $this->entityManager->find(new RoomMobile(), ['room_id' => $room->getId()]);
        foreach ($mobileItemsCollection as $roomMobile) {
            if ($roomMobile) {

                foreach (explode(' ', $roomMobile->getName()) as $mobKeywords) {
                    if (stripos($mobKeywords, $keyword) === 0) {
                        if ($pos == $loopPosition) {
                            return $roomMobile;
                        }

                        $loopPosition++;
                    }
                }
            }
        }

        return false;
    }

    public function getFreeWearSlot(MobileEntity $mobile, ItemEntity $item)
    {
        $set = [];

        $characterSlots = (new CharacterWearslot())->all();
        foreach($characterSlots as $characterSlot) {
            if ($characterSlot->wearslot == $item->getWearSlot() &&  ! in_array($characterSlot->slot, $set)) {

                $count = $this->entityManager->count(
                    new MobileItem(), [
                        'room_mobile_id' => $mobile->getId(),
                        'inventory' => 0,
                        'character_slot' => $characterSlot->slot
                    ]
                );

                if ($count == 0) {
                    return $characterSlot;
                }

                $set[] = $characterSlot->slot;
            }
        }

        return false;
    }
}
