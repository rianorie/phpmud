<?php

namespace Modules\World\Helper;

use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Modules\World\Entity\Location as LocationEntity;
use Modules\World\Entity\Room as RoomEntity;

class Room
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Retrieve an array of character ids that are in the room
     *
     * @param RoomEntity $room
     *
     * @return array
     */
    public function getCharacterIds(RoomEntity $room)
    {
        $characters = [];
        foreach($this->entityManager->find(new LocationEntity(), ['room_id' => $room->getId()]) as $location) {

            /** @var LocationEntity $location */
            if ($location) {
                $characters[] = $location->getCharacterId();
            }
        }

        return $characters;
    }
}
