<?php

namespace Modules\World;

use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Helper\Room as RoomHelper;

class Item extends \Game\Module\Module
{
    /**
     * Set up the item module
     *
     * @throws \Game\Exception
     */
    public function init()
    {
        $itemHelper = new ItemHelper($this->getEntityManager());
        $characterHelper = new Helper\Character(new RoomHelper($this->getEntityManager()));

        $this->getEvent()->observe(
            'look.roomitem',
            new Observer\LookItem($this->getEntityManager(), $this->getInput(), $this->getOutput())
        );

        $this->getEvent()->observe(
            'look.characteritem',
            new Observer\LookItem($this->getEntityManager(), $this->getInput(), $this->getOutput())
        );

        $this->getInput()->registerCommands([
            'get'       => new Command\Get($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper),
            'put'       => new Command\Put($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper),
            'give'      => new Command\Give($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper),
            'drop'      => new Command\Drop($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper),
            'wear'      => new Command\Wear($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper),
            'remove'    => new Command\Remove($this->getEntityManager(), $this->getInput(), $this->getOutput(), $itemHelper, $characterHelper),
            'inventory' => new Command\Inventory($this->getEntityManager(), $this->getInput(), $this->getOutput()),
            'equipment' => new Command\Equipment($this->getEntityManager(), $this->getInput(), $this->getOutput()),
        ]);
    }
}
