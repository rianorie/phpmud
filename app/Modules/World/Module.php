<?php

namespace Modules\World;

use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Helper\Room as RoomHelper;

class Module extends \Game\Module\Module
{
    /**
     * Set up the World module
     *
     * @throws \Game\Exception
     */
    public function init()
    {
        $modules = [
            \Modules\World\Character::class,
            \Modules\World\Area::class,
            \Modules\World\Item::class
        ];

        foreach ($modules as $module) {
            $m = new $module(
                [],
                $this->getConfig(),
                $this->getEvent(),
                $this->getInput(),
                $this->getOutput(),
                $this->getEntityManager(),
                $this->getModuleManager(),
                $this->getLogger()
            );

            $m->init();
        }

        $itemHelper      = new ItemHelper($this->getEntityManager());
        $characterHelper = new Helper\Character(new RoomHelper($this->getEntityManager()));
        $mobileHelper    = new Helper\Mobile($this->getEntityManager());
        $stateHelper     = new Helper\Character\State();

        // Tick away at the time
        $this->getEvent()->observe(
            'game.pulse.tick',
            new Observer\WorldTime($this->getEntityManager(), $this->getInput(), $this->getOutput(), $stateHelper)
        );

        // shared commands
        $this->getInput()->registerCommands([
            'look' => new Command\Look($this->getEntityManager(), $this->getInput(), $this->getOutput(),
                                       $this->getEvent(), $itemHelper, $characterHelper, $mobileHelper),
            'time' => new Command\Time($this->getEntityManager(), $this->getInput(), $this->getOutput())
        ]);
    }
}
