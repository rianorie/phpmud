<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Exception;
use Game\Module\Entity\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\Item as ItemEntity;
use Modules\World\Entity\ItemReset;
use Modules\World\Entity\Mobile;
use Modules\World\Entity\MobileItem;
use Modules\World\Entity\MobileReset;
use Modules\World\Entity\RoomItem;
use Modules\World\Entity\RoomMobile;
use Modules\World\Helper\Item as ItemHelper;
use Modules\World\Helper\Mobile as MobileHelper;
use stdClass;

class AreaReloadResets extends Observer
{
    /**
     * @var ItemHelper
     */
    protected $itemHelper;

    /**
     * @var MobileHelper
     */
    protected $mobileHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $itemHelper, $mobileHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->itemHelper   = $itemHelper;
        $this->mobileHelper = $mobileHelper;
    }

    public function fire(stdClass $data)
    {
        $this->resetMobs($data);
        $this->resetRoomObjects($data);
    }

    public function resetRoomObjects(stdClass $data)
    {

        $itemResetEntity = new ItemReset($this->getEntityManager());

        $parentMap = [];

        /** @var ItemReset[] $resets */
        $resets = $this->getEntityManager()->find($itemResetEntity, ['area_id' => $data->area->getId(), 'mobile_reset_id' => null]);
        foreach ($resets as $reset) {
            if ($reset) {

                // find the existing reset items
                $currentItems = $this->getEntityManager()->count(
                    (new RoomItem($this->getEntityManager())),
                    ['reset_id' => $reset->getId()]
                );

                // if it's less than the reset count, try and add new ones
                if ($currentItems < $reset->getCount()) {

                    // try the reset for every missing item
                    $execute = $reset->getCount() - $currentItems;

                    // grab the item template
                    try {

                        /** @var ItemEntity $item */
                        $item = $this->getEntityManager()->load(new ItemEntity($this->getEntityManager()), $reset->getItemId());

                        for ($i = 0; $i < $execute; $i++) {
                            $dice = rand(0, 100);

                            // roll the dice
                            if ($dice <= $reset->getChance()) {


                                $roomItem = new RoomItem($this->getEntityManager());

                                $this->itemHelper->importItem($item, $roomItem);

                                // try and load up the parent ID
                                $parentId = (isset($parentMap[$reset->getParentId()]) ? $parentMap[$reset->getParentId()] : null);
                                if (is_null($parentId) && $reset->getParentId()) {
                                    $parentItem = $this->getEntityManager()->load(new RoomItem, $reset->getParentId(), 'reset_id');
                                    if ($parentItem) {
                                        $parentId = $parentItem->getId();
                                    }
                                }

                                $roomItem->area_id   = $reset->getAreaId();
                                $roomItem->room_id   = $reset->getRoomId();
                                $roomItem->item_id   = $reset->getItemId();
                                $roomItem->reset_id  = $reset->getId();
                                $roomItem->parent_id = $parentId;

                                $this->getEntityManager()->save($roomItem);

                                $parentMap[$reset->getId()] = $roomItem->getId();
                            }
                        }

                    } catch (Exception $e) {
                        #TODO what to do here..
                    }
                }
            }
        }
    }

    public function resetMobs(stdClass $data)
    {
        $mobileResetEntity = new MobileReset;

        /** @var MobileReset[] $mobileResets */
        $mobileResets = $this->getEntityManager()->find($mobileResetEntity, ['area_id' => $data->area->getId()]);
        foreach ($mobileResets as $reset) {
            if ($reset) {

//                $this->getOutput()->write($data->connection, ["Reset ID %d for mob %d\n", $reset->getId(), $reset->getMobileId()]);

                // find the existing reset items
                $currentMobiles = $this->getEntityManager()->count(
                    new RoomMobile(),
                    ['reset_id' => $reset->getId()]
                );

                // if it's less than the reset count, try and add new ones
                if ($currentMobiles < $reset->getMax()) {

//                    $this->getOutput()->write($data->connection, ["Found %d of %d max.", $currentMobiles, $reset->getMax()]);

                    // try the reset for every missing item
                    $execute = $reset->getMax() - $currentMobiles;

                    // grab the minimum number to fire off
                    $min = ($currentMobiles <= $reset->getMin() ? $reset->getMin() - $currentMobiles : 0);

//                    $this->getOutput()->write($data->connection, ["Min %d - %d = %d.", $reset->getMin(), $currentMobiles, $min]);

                    // grab the item template
                    try {

                        /** @var Mobile $mobile */
                        $mobile = $this->getEntityManager()->load(new Mobile(), $reset->getMobileId());

                        for ($i = 0; $i < $execute; $i++) {

                            if ($min > 0) {
                                $dice = 0; // percentages are inverted!
                                $min--;
                            } else {
                                $dice = rand(0, 100);
                            }


                            // roll the dice
                            if ($dice <= $reset->getChance()) {

//                                $this->getOutput()->write($data->connection, ["Min %d, dice %d, chance %d", $min, $dice, $reset->getChance()]);

                                $roomMobile = new RoomMobile();

                                $this->mobileHelper->importMobile($mobile, $roomMobile);

                                $roomMobile->area_id   = $reset->getAreaId();
                                $roomMobile->room_id   = $reset->getRoomId();
                                $roomMobile->mobile_id = $reset->getMobileId();
                                $roomMobile->reset_id  = $reset->getId();

                                if ($this->getEntityManager()->save($roomMobile)) {

                                    // mobile was reset, now lets see if it's carrying anything

                                    $itemResetEntity = new ItemReset();

                                    /** @var ItemReset[] $itemResets */
                                    $itemResets = $this->getEntityManager()->find($itemResetEntity, ['area_id' => $data->area->getId(), 'mobile_reset_id' => $reset->getId()]);
                                    foreach ($itemResets as $itemReset) {
                                        if ($itemReset) {

                                            try {

                                                /** @var ItemEntity $item */
                                                $item = $this->getEntityManager()->load(new ItemEntity($this->getEntityManager()), $itemReset->getItemId());

                                                // for now we'll always have a successful dice roll
//                                                $dice = rand(0, 100);

                                                // roll the dice
//                                                if ($dice <= $reset->getChance()) {

                                                $mobileItem = new MobileItem($this->getEntityManager());

                                                $this->itemHelper->importItem($item, $mobileItem);

                                                $mobileItem->setRoomMobileId($roomMobile->getId());
                                                $mobileItem->item_id   = $itemReset->getItemId();
                                                $mobileItem->reset_id  = $itemReset->getId();
                                                $mobileItem->parent_id = $itemReset->getParentId();

                                                if ($itemReset->getMobileWearLoc() > 0) {
                                                    $mobileItem->setInInventory(true);

                                                    $wearslot = $this->mobileHelper->getFreeWearSlot($roomMobile, $mobileItem);
                                                    if ($wearslot != false) {
                                                        $mobileItem->setInInventory(false);
                                                        $mobileItem->setCharacterSlot($wearslot->slot);
                                                    }
                                                }

                                                $this->getEntityManager()->save($mobileItem);
//                                                }

                                            } catch (Exception $e) {
                                                #TODO what to do here..
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    } catch (Exception $e) {
                        #TODO what to do here..

                        $this->getOutput()->write($data->connection, ["Failed!\nException: %s", $e->getMessage()]);
                    }
                }
            }
        }
    }
}
