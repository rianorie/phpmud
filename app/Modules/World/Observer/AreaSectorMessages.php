<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Game\Registry;
use Modules\World\Entity\SectorMessage;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Character\State;
use Modules\World\Table\Position;
use stdClass;

class AreaSectorMessages extends Observer
{
    /**
     * @var \Modules\World\Helper\Character\State
     */
    protected $stateHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper     = $stateHelper;
        $this->characterHelper = $characterHelper;
    }

    public function fire(stdClass $data)
    {
        try {

            $storedHour = 0;
            if (Registry::exists('game.time')) {
                $storedHour = Registry::get('game.time');
            }

            $messages = [];

            foreach ($this->characterHelper->getCharacters() as $character) {
                $sectorId = $character->getData('room')->getSectorId();

                if ( ! isset($messages[$sectorId])) {

                    /** @var SectorMessage[] $messageCollection */
                    $messageCollection = $this->getEntityManager()->find(
                        new SectorMessage(),
                        [
                            'sector_id' => $sectorId,
                            'hour_from' => ['<=' => $storedHour],
                            'hour_to'   => ['>=' => $storedHour]
                        ]
                    );

                    foreach ($messageCollection as $messageRow) {
                        if ($messageRow) {
                            $messages[$sectorId][] = $messageRow->getMessage();
                        }
                    }

                    if (isset($messages[$sectorId])) {

                        if (rand(1, 100) <= 33) { // only fire 33% of the time
                            $messages[$sectorId] = sprintf("{w%s.{x", $messages[$sectorId][array_rand($messages[$sectorId])]);

                        } else {
                            $messages[$sectorId] = false;
                        }
                    }
                }
            }

            foreach ($this->characterHelper->getCharacters() as $character) {
                if ($character->getPosition() !== Position::SLEEP &&
                    in_array($this->stateHelper->getState($character->getConnection()), [State::ACTIVE, State::AFK])) {

                    $sectorId = $character->getData('room')->getSectorId();
                    if (isset($messages[$sectorId]) && $messages[$sectorId] != false) {
                        $this->getOutput()->write($character->getConnection(), $messages[$sectorId]);
                    }
                }
            }

        } catch (\Exception $exception) {
        }

    }
}
