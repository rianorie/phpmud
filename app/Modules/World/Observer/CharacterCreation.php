<?php

namespace Modules\World\Observer;

use Game\Connection\Exception as ConnectionException;
use Game\Module\Entity\Exception as EntityException;
use Game\Module\Observer;
use Game\Connection\Interfaces\Connection;

use Game\Module\Entity\Interfaces\Manager;
use Game\Event;
use Game\Input;
use Game\Output;

use Game\Helper\Output as OutputHelper;
use JetBrains\PhpStorm\Pure;
use Modules\World\Entity\Validator\Password as PasswordValidator;
use Modules\World\Helper\Character as CharacterHelper;

use Modules\Toggle\Utility as ToggleUtility;
use Modules\Telnet\Utilities as TelnetUtility;

use Modules\World\Entity\Character;
use Modules\World\Table\Gender as GenderTable;
use Modules\World\Table\Race as RaceTable;
use stdClass;

/**
 * Creation handler, this class handles all the creation and login related elements
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-31
 * @version 1.0
 */
class CharacterCreation extends Observer
{
    /**
     * First state where the connection is asked for their character name
     */
    const STATE_LOGIN = 1;

    /**
     * Password request state, either log an existing character in or continue on with creation
     */
    const STATE_PASSWORD = 2;

    /**
     * Confirm the earlier given password
     */
    const STATE_CONFIRM = 3;

    /**
     * Enter an email to associate with the character
     */
    const STATE_EMAIL = 4;

    const STATE_GENDER = 5;

    const STATE_RACE = 6;

    const STATE_CHANNELER = 7;

    /**
     * Final creation state where the character is initialized and the connection's write locked removed
     */
    const STATE_FINAL = 20;

    /**
     * Claim state where an actively logged in character will get claimed for the current connection
     */
    const STATE_CLAIM = 50;

    /**
     * State that's set after creation or login has been completed
     */
    const STATE_DONE = 100;

    /**
     * @var Event
     */
    private Event $event;

    /**
     * @var CharacterHelper
     */
    protected CharacterHelper $characterHelper;

    /**
     * @var CharacterHelper\State
     */
    protected CharacterHelper\State $stateHelper;

    /**
     * @var CharacterHelper\Password
     */
    protected CharacterHelper\Password $passwordHelper;

    /**
     * @var OutputHelper
     */
    protected OutputHelper $outputHelper;

    /**
     * @var RaceTable
     */
    protected RaceTable $raceTable;

    /**
     * @var GenderTable
     */
    protected GenderTable $genderTable;

    #[Pure] public function __construct(
        Manager $entityManager,
        Input $input,
        Output $output,
        Event $event,
        $characterHelper,
        $stateHelper,
        $passwordHelper,
        $outputHelper,
        $raceTable,
        $genderTable
    )
    {
        parent::__construct($entityManager, $input, $output);

        $this->event = $event;
        $this->characterHelper = $characterHelper;
        $this->outputHelper = $outputHelper;
        $this->stateHelper = $stateHelper;
        $this->passwordHelper = $passwordHelper;
        $this->raceTable = $raceTable;
        $this->genderTable = $genderTable;
    }

    /**
     * Handle the output based on the position in the creation process
     *
     * @param stdClass $data
     */
    public function fire(stdClass $data)
    {
        /** @var Connection $connection */
        $connection = $data->connection;

        /** @var string $input */
        $input = ($data->input ?? '');

        // this observer is only relevant when we're in the creation process
        if ($connection->getData('creation') == $this::STATE_DONE) {
            return;
        }

        // if nothing's happened for creation yet, set it up now
        if ($connection->getData('creation') == null) {

            // lock writing to the connection off, this will prevent
            // any output being sent to the client unless it's forceful
            $connection->writeLock(true);

            // set the creation state in the connection
            $connection->setData('creation', $this::STATE_LOGIN);
        }

        // now simply step through the different states
        switch ($connection->getData('creation')) {
            case self::STATE_LOGIN:
                $this->login($connection, $input);
                break;
            case self::STATE_PASSWORD:
                $this->password($connection, $input);
                break;
            case self::STATE_CONFIRM:
                $this->confirm($connection, $input);
                break;
            case self::STATE_EMAIL:
                $this->email($connection, $input);
                break;
            case self::STATE_GENDER:
                $this->gender($connection, $input);
                break;
            case self::STATE_RACE:
                $this->race($connection, $input);
                break;
            case self::STATE_CHANNELER:
                $this->channeler($connection, $input);
                break;
            case self::STATE_FINAL:
                $this->create($connection);
                break;
            case self::STATE_CLAIM:
                $this->claim($connection, $input);
                break;
        }

        // clearing this stops the input handling because there's no arguments to process
        $data->arguments = [];
    }

    /**
     * Handle the first step - after receiving a name
     *
     * @param Connection $connection
     * @param string     $input
     */
    public function login(Connection $connection, string &$input)
    {
        // An empty login string brings us to the login message
        if (empty(trim($input))) {
            $this->write($connection, ["A voice whispers, 'Who stands before the gates?' "]);
            return;
        }

        // since they're in the login state, the entered info is their name.
        $name = ucfirst(strtolower(trim($input)));

        // Load up a character entity
        $character = new Character($this->getEntityManager());

        // try and set the name, effectively validating it
        if ($character->setName($name)) {

            // load up the entity from the database
            foreach ($this->getEntityManager()->find($character, ['name' => $name], 1) as $item) {
                if ($item != false) {
                    /** @var Character $item */
                    $character = $item;
                }
            }

            // generate a character based off of the skeleton
            $character->setConnection($connection);

            // store the skeleton in the connection
            $connection->setData('character', $character);

            // if the id isn't set, we're creating a new character.
            if (is_null($character->getId())) {
                $this->write($connection, ["The voice says, 'Welcome %s, you are a new character.'", $name]);
            }

            // create placeholders
            $input = $arguments = null;

            // move us down to the password
            $connection->setData('creation', $this::STATE_PASSWORD);
            $this->password($connection, $input);

            // setName failed, so the name is invalid.
        } else {
            $this->write($connection, ["\nI'm sorry %s, that's not a valid name.\nPick another? ", $name]);
        }
    }

    /**
     * Handle the second step - after receiving a password
     *
     * @param Connection $connection
     * @param            $input
     *
     */
    public function password(Connection $connection, &$input)
    {
        $password = trim($input);

        /** @var Character $character */
        $character = $connection->getData('character');

        // first check if the given password is empty, or not. If it is, show the initial message
        if (empty($password)) {

            TelnetUtility::disableVisibleOutput($connection);

            // if it's a new character
            if ($character->getId() == null) {
                $this->write($connection, ["It continues, 'Give me a password for %s?' ", $character->getName()]);

            // or if it's an existing character
            } else {
                $this->write($connection, ["The voice intones, 'Whisper to me your password.' "]);
            }

            return;
        }

        $passwordValidator = new PasswordValidator();

        // if we're an existing character it's not a creation process so much as a login process
        // so if the character has an ID
        if ( ! is_null($character->getId())) {

            // and the password validated
            if ($this->passwordHelper->checkPassword($character, $password)) {

                TelnetUtility::enableVisibleOutput($connection);

                if ($this->characterHelper->getCharacterById($character->getId()) !== false) {

                    $connection->setData('creation', $this::STATE_CLAIM);
                    $connection->setData('password', $password);
                    $input = null;
                    $this->claim($connection, $input);
                    return;
                }



                // initiate a proper character and be done with it
                $this->initCharacter($connection);

                // since we're all for better security and passwords, inform our players that they need to
                // reset their password.
                if ( ! $this->passwordHelper->isPasswordNew($character)) {

                    // try and validate the password
                    if ($passwordValidator->validate($password)) {

                        // the password validates, so set it in the character
                        $character->setPassword($this->passwordHelper->generatePasswordHash($password));

                        // try and update the character
                        try {

                            if ( ! $this->getEntityManager()->update($character, 'password')) {

                                $this->write($connection, [
                                    "\nThe password you're using is encrypted using the old " .
                                    "algorithm. We strongly advice you to update it.\n"
                                ]);

                                $this->input->handle($connection, 'password');
                            }

                        } catch (EntityException $exception) {

                            #TODO log exceptions
                            try {
                                $this->write($connection, ['Sorry, something went wrong!']);

                                $connection->getManager()->remove($connection);
                                $connection->close();

                            } catch (ConnectionException $connectionException) {

                                #TODO figure out what to do here..
                            }
                        }


                    } else {

                        $this->write($connection, ["\nThe password you're using is encrypted using the old".
                                                   " algorithm. We strongly advice you to update it.\n"]);

                        $this->input->handle($connection, 'password');
                    }
                }

                return;

            // but if they enter the wrong password, close the connection. Because we don't
            // want to give people room for brute forcing a password
            } else {

                try {
                    $this->write($connection, ['Wrong password!']);
                    $connection->getManager()->remove($connection);
                    $connection->close();

                } catch (ConnectionException $e) {
                    #TODO figure out what to do here..
                }

                return;
            }
        }

        // try and validate the password
        if ( ! $passwordValidator->validate($password)) {
            $this->write($connection, ["The voice growls, 'The password must be at least eight characters long.'"]);
            $input = null;
            $this->password($connection, $input);
            return;
        }

        // Set the password
        $character->setPassword($this->passwordHelper->generatePasswordHash($password));

        // but if we're not an existing character, move us on to the next phase.
        $connection->setData('creation', $this::STATE_CONFIRM);
        $input = $arguments = null;
        $this->confirm($connection, $input);
    }

    /**
     * Handle the third step - after receiving a confirmation for the password
     *
     * @param Connection $connection
     * @param            $input
     */
    public function confirm(Connection $connection, &$input)
    {
        $password = trim($input);

        // first check if the given password is empty, or not. If it is, show the initial message
        if (empty($password)) {
            $this->write($connection, ["The voice says, 'Please retype the password as a confirmation' "]);
            return;
        }

        // if the confirmation matches the earlier entered password, continue on
        if ($this->passwordHelper->checkPassword($connection->getData('character'), $password)) {

            TelnetUtility::enableVisibleOutput($connection);

            // move on to the next step
            $connection->setData('creation', $this::STATE_EMAIL);
            $input = null;
            $this->email($connection, $input);
            return;
        }

        // confirmation failed, push back to the password phase
        $this->write($connection, ["The voice intones, 'The passwords do not match.'"]);
        $connection->setData('creation', $this::STATE_PASSWORD);
        $input = null;
        $this->password($connection, $input);
        return;
    }

    /**
     * Handle the forth step - after receiving an email address
     *
     * @param Connection $connection
     * @param string     $input
     *
     */
    public function email(Connection $connection, &$input)
    {
        $input = strtolower(trim($input));

        // no input? output the initial step
        if (empty($input)) {
            $this->write($connection, ["The voice says, 'Please give us an email address for your character'"]);
            $this->write($connection, ["{W** {DOptionally enter 'no'. Be aware we cannot recover lost characters without it.'{x "]);
            return;
        }

        // check the given email for validity
        if ($input != 'no' && ! filter_var($input, FILTER_VALIDATE_EMAIL)) {
            $this->write($connection, ["The voice intones, 'That does not look like a valid email. Please try again.' "]);
            return;
        }

        // move on to the next step
        $character = $connection->getData('character');

        if ($input != 'no') {
            $character->setEmail($input);
        }

        $connection->setData('creation', self::STATE_GENDER);
        $input = null;
        $this->gender($connection, $input);
        return;
    }

    /**
     * Claim an already logged in character
     *
     * @param Connection $connection
     * @param            $input
     */
    public function claim(Connection $connection, &$input)
    {
        $input = strtolower(trim($input));

        // No input given
        if (empty($input)) {
            $this->write($connection, ["That character is already playing.\nDo you wish to connect anyway (Y/N)?"]);
            return;
        }

        // the wrong input is given
        if ( ! in_array($input, ['y', 'n'])) {
            $this->write($connection, ["The voice urges, 'Please type Y or N'"]);
            return;
        }

        // unload the character
        $character = $this->characterHelper->getCharacterById(
            $connection->getData('character')->getId()
        );

        $this->characterHelper->removeCharacter($character);

        // if we're to take over the connection
        if ($input == 'y') {
            try {

                // disconnect the other socket
                $other = $character->getConnection();
                $other->getManager()->remove($other);
                $other->close();

            } catch (ConnectionException $e) {
                #TODO what to do here..?
            }

            $connection->setData('creation', self::STATE_PASSWORD);
            $password = $connection->getData('password');
            $connection->setData('password', null);

            $input = $password;

            $this->password($connection, $input);
            return;
        }

        // disconnect the socket
        try {
            $connection->getManager()->remove($connection);
            $connection->close();

        } catch (ConnectionException $e) {
            #TODO what to do here..?
        }
    }

    /**
     * @param Connection $connection
     * @param            $input
     */
    public function gender(Connection $connection, &$input)
    {
        $input = substr(strtolower(trim($input)), 0, 1);

        // no input given
        if (empty($input)) {
            $this->write($connection, ["The voice says, 'Are you male or female? Please enter (M)ale or (F)emale.'"]);
            return;

        }

        // set the gender
        if (in_array($input, ['m', 'f'])) {

            $connection->getData('character')->setGender($this->genderTable::MALE);

            if ($input == 'f') {
                $connection->getData('character')->setGender($this->genderTable::FEMALE);
            }

            $connection->setData('creation', self::STATE_RACE);
            $input = null;
            $this->race($connection, $input);
            return;
        }

        // no valid option given, try again
        $this->write($connection, ["The voice intones, 'That is not a valid option, please try again. (M)ale or (F)emale.' "]);
        return;
    }

    /**
     * @param Connection $connection
     * @param            $input
     */
    public function race(Connection $connection, &$input)
    {
        $input = ucfirst(strtolower(trim($input)));

        $races = array_keys(array_filter($this->raceTable->all(), function($race) {
            return $race->enabled;
        }));

        // no input given
        if (empty($input)) {
            $this->write($connection, ["The voice says, 'What race are you?'"]);

            $output = $this->outputHelper->getColumnized($races, 4, true);

            $this->write($connection, ["{W** {DAvailable races{x"]);
            $this->write($connection, [$output]);
            return;
        }

        // if valid input is given, set the race
        if (in_array($input, $races)) {

            $connection->getData('character')->setRace($input);

            $connection->setData('creation', self::STATE_CHANNELER);
            $input = null;
            $this->channeler($connection, $input);
            return;
        }

        // no valid input is given
        $this->write($connection, ["The voice intones, 'That is not a valid option, please try again.' "]);
        return;
    }

    /**
     * @param Connection $connection
     * @param            $input
     */
    public function channeler(Connection $connection, &$input)
    {
        $input = substr(strtolower(trim($input)), 0, 1);

        // no input given
        if (empty($input)) {
            $this->write($connection, ["The voice says, 'Are you a channeler? Please enter (Y)es or (N)o.'"]);
            return;

        }

        // correct input given
        if (in_array($input, ['y', 'n'])) {

            $connection->getData('character')->setChanneler(true);

            if ($input == 'n') {
                $connection->getData('character')->setChanneler(false);
            }

            $connection->setData('creation', self::STATE_FINAL);
            $input = null;
            $this->create($connection);
            return;
        }

        // no valid input was given
        $this->write($connection, ["The voice intones, 'That is not a valid option, please try again. (Y)es or (N)o.' "]);
        return;
    }

    /**
     * Take the final steps to create the character
     *
     * @param Connection $connection
     *
     */
    protected function create(Connection $connection)
    {
        /** @var Character $character */
        $character = $connection->getData('character');

        // Set some base stats
        $character->setConstitution(rand(14, 22));
        $character->setHitpoints(50);
        $character->setMaxHitpoints(250);
        $character->setStamina(50);
        $character->setMaxStamina(250);

        // fire an event before creation
        $this->event->dispatch('world.before_character_created', ['connection' => $connection]);

        try {
            // something went wrong while trying to save, stop here.
            if ( ! $this->getEntityManager()->save($connection->getData('character'))) {

                try {

                    $this->write($connection, ['Sorry, something went wrong while trying to create your character!']);
                    $connection->getManager()->remove($connection);
                    $connection->close();

                } catch (ConnectionException $e) {
                    #TODO What to do here?
                }

                return;
            }

            // saving worked perfectly, so log us in now

            // load the new character from the database, this makes sure the entity is recognized
            // as an existing entity in case someone needs to update it
            $connection->setData(
                'character',
                $this->getEntityManager()->load(
                    $connection->getData('character'),
                    $connection->getData('character')->getName(),
                    'name'
                )
            );

            // fire an event after creation to hook into
            $this->event->dispatch('world.after_character_created', ['connection' => $connection]);

            // set up an actual proper character for this connection
            $this->initCharacter($connection);

            // throw a welcome message at the new player
            $this->write($connection, ['Welcome, %s!', $connection->getData('character')->getName()]);

        } catch (EntityException $exception) {

            try {

                $this->write($connection, ['Sorry, something went wrong while trying to create your character!']);
                $connection->getManager()->remove($connection);
                $connection->close();

            } catch (ConnectionException $e) {
                #TODO What to do here?
            }

            return;
        }
    }

    /**
     * Upgrade the skeleton to a an character entity and clean up connection variables
     *
     * @param Connection $connection
     *
     */
    protected function initCharacter(Connection $connection)
    {
        try {
            $connection->write("\r\n\r\n", true);

            // clear the creation stuff
            $this->stateHelper->setState($connection, $this->stateHelper::ACTIVE);
            $connection->setData('creation', $this::STATE_DONE);
            $connection->writeLock(false);

            // load the new character from the database, this makes sure the entity is recognized as an existing entity
            $connection->setData(
                'character',
                $this->getEntityManager()->load(
                    $connection->getData('character'),
                    $connection->getData('character')->getName(),
                    'name'
                )
            );

            // Register the character in the global space
            $this->characterHelper->addCharacter($connection->getData('character'));

            // dispatch the after character initialization
            $this->event->dispatch('character.after_init', ['character' => $connection->getData('character')]);

            // Output the login messages
            foreach ($connection->getManager()->getAllExcept($connection) as $other) {
                if ($other && $other->getData('character')) {
                    if (ToggleUtility::getToggle($other->getData('character'), 'logins')) {
                        $this->output->write($other, ['%s joins in, looking for some adventure.', $connection->getData('character')->getName()]);
                    }
                }
            }

        // if it fails, stop
        } catch (\Exception | EntityException $e) {
            try {

                $this->write($connection, ['Sorry, something went wrong while trying to load your character!']);
                $connection->getManager()->remove($connection);
                $connection->close();

            } catch (ConnectionException $e) {
                #TODO What to do here?
            }

            return;
        }
    }

    /**
     * Write forced output
     *
     * @param Connection $connection
     * @param array      $message
     *
     * @return false|void
     */
    protected function write(Connection $connection, array $message) : bool
    {
        return $this->output->write($connection, $message, true, true);
    }
}
