<?php

namespace Modules\World\Observer;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\Room as RoomEntity;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Table\Position;
use stdClass;

class CharacterHealTick extends Observer
{
    /**
     * @var \Modules\World\Helper\Character\State
     */
    protected $stateHelper;

    /** @var \Game\Event */
    protected $event;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $event, $stateHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->event = $event;
        $this->stateHelper = $stateHelper;
        $this->characterHelper = $characterHelper;
    }

    public function fire(stdClass $data)
    {
        $online = $this->characterHelper->getCharacters();
        foreach ($online as $character) {

            if ($this->stateHelper->getState($character->getConnection()) == CharacterHelper\State::ACTIVE) {

                /** @var RoomEntity $room */
                $room = $character->getData('room');
                if ($room) {

                    $healHP = $healST = 0;

                    if ($character->getHitpoints() < $character->getMaxHitpoints()) {

                        // max healing is 20% of your total unless you've got less
                        // than 10000 hp, then we modify for an increased rate
                        $healHP = $character->getMaxHitpoints() / 5;

                        if ($character->getMaxHitpoints() < 10000) {
                            $healHP = $character->getMaxHitpoints() / (5 - (2.5 * (1-($character->getMaxHitpoints()/10000))));
                        }

                        // modified by constitution
                        $healHP *= ($character->getConstitution() / 200);

                        // max heal item is "200" which we'll read as 1.2
                        if ( ! is_null($character->getData('position_item'))) {
                            $healHP *= (($character->getData('position_item')->getV3() + 1000) / 1000);
                        }

                        // max heal room is "200" which we'll also read as 1.2
                        $healHP *= (($room->getHealHitpoints() + 1000) / 1000);

                        switch ($character->getPosition()) {
                            // under normal circumstances healing sucks
                            default :
                                $healHP /= 4;
                                break;

                            // resting heals ever slightly less than sleeping
                            case Position::REST:
                                $healHP /= 1.25;
                                break;

                            case Position::SLEEP:
                                // nothing here, gain as defined is fine
                                break;
                        }

                        // best healing is 20% of your max
                        // if constitution is less than 100%, that is deducted

                        // 125095 / 5 = 25019 - best possible
                        // con is 108 of 200, that's 54%
                        // 25019 * 0.54 = 13510.26

                        // below 10k, adjust the percentage of modifier based on the amount of hp
                        // but never more than 1
                        // 10000 / 9000 = 0.1
                        // 10000 / 5000 = 0.5
                        // 10000 / 1000 = 1
                        // 10000 / 150 = 6.6

                        // 1000 / (5 - (2.5*1)) = 400
                        // 5000 / (5 - (2.5*0.5)) = 1333
                        // 9000 / (5 - (2.5*0.1)) = 1894

                        // 1000 / 5 = 400
                        // con is 16 of 200 that's 8%
                        // 400 * 0.08 = 32
                        // 32   * 1.2 = 38    plus room bonus
                        // 38   * 1.2 = 45   plus furniture bonus


                        // 5500 / 5 = 1100
                        // con is 55 of 200 that's 27%
                        // 1100 * 0.27 = 297
                        // 297 * 1.2 = 356    plus room bonus
                        // 342 * 1.2 = 427    plus furniture bonus

                        // 5500 / 5 = 1100
                        // con is 100 of 200 that's 50%
                        // 1100 * 0.5 = 550
                        // 550 * 1.2 = 660    plus room bonus
                        // 342 * 1.2 = 792    plus furniture bonus


                        // 7500 / 5 = 1500
                        // con is 39 of 200 that's 19%
                        // 1500 * 0.19 = 285
                        // 285 * 1.2 = 342    plus room bonus
                        // 342 * 1.2 = 410    plus furniture bonus

                        // 7500 / 5 = 1500
                        // con is 100 of 200 that's 50%
                        // 1500 * 0.5 = 750
                        // 285 * 1.2 = 900     plus room bonus
                        // 342 * 1.2 = 1080    plus furniture bonus
                    }

                    if ($character->getStamina() < $character->getMaxStamina()) {

                        // max healing is 20% of your total unless you've got less
                        // than 10000 st, then we modify for an increased rate
                        $healST = $character->getMaxStamina() / 5;

                        if ($character->getMaxStamina() < 10000) {
                            $healST = $character->getMaxStamina() / (5 - (2.7 * (1-($character->getMaxStamina()/10000))));
                        }

                        // modified by constitution
                        $healST = $healST * ($character->getConstitution() / 200);

                        // max heal item is "200" which we'll read as 1.2
                        if ( ! is_null($character->getData('position_item'))) {
                            $healST *= (($character->getData('position_item')->getV4() + 1000) / 1000);
                        }

                        // max heal room is "200" which we'll also read as 1.2
                        $healST *= (($room->getHealStamina() + 1000) / 1000);

                        switch ($character->getPosition()) {
                            // under normal circumstances healing sucks
                            default :
                                $healST /= 4;
                                break;

                            // resting heals ever slightly less than sleeping
                            case Position::REST:
                                $healST /= 1.25;
                                break;

                            case Position::SLEEP:
                                // nothing here, gain as defined is fine
                                break;
                        }
                    }

                    $this->event->dispatch('character.heal', ['character' => $character, 'hitpoints' => &$healHP, 'stamina' => &$healST]);

                    $diffHP = $character->getMaxHitpoints() - $character->getHitpoints();
                    $diffST = $character->getMaxStamina() - $character->getStamina();

                    // heal either the determined heal rate or the difference if that's less
                    $character->setHitpoints( $character->getHitpoints() + min($healHP * (rand(80, 110)/100), $diffHP) );
                    $character->setStamina( $character->getStamina() + min($healST * (rand(80, 110)/100), $diffST) );

                    $this->getEntityManager()->update($character, ['stamina', 'hitpoints']);
                }
            }
        }
    }
}
