<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use stdClass;

class CharacterPreventAfkInput extends Observer
{
    /**
     * @var \Modules\World\Helper\Character\State
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
    }

    public function fire(stdClass $data)
    {
        if ($this->stateHelper->getState($data->connection) == $this->stateHelper::AFK && strtolower($data->input) != 'afk') {
            $this->getOutput()->write($data->connection, "How are you planning to do that while AFK?");
            $data->input = $data->arguments = null;
        }
    }
}
