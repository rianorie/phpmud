<?php

/*

        if ($char->getPosition() == Position::SLEEP) {
            $this->getOutput()->write($connection, 'You need to wake up before you can do that.');
            return;
        }
 */


namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use stdClass;
use Modules\World\Table\Position;

class CharacterPreventInputSleeping extends Observer
{
    /**
     * @var \Modules\World\Helper\Character\State
     */
    protected $stateHelper;

    protected $commands = [
        'say', 'osay', 'look', 'north', 'south', 'east', 'west', 'up', 'down', 'northeast', 'ne', 'northwest', 'nw',
        'southeast', 'se', 'southwest', 'sw', 'get', 'put', 'drop', 'wear', 'open', 'close', 'inventory', 'equipment',
        'exits', 'remove'
    ];

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
    }

    public function fire(stdClass $data)
    {
        if (   $this->stateHelper->getState($data->connection) == $this->stateHelper::ACTIVE
            && $data->connection->getData('character')
            && $data->connection->getData('character')->getPosition() == Position::SLEEP
            && in_array($data->command, $this->commands)) {
            $this->getOutput()->write($data->connection, "You must wake up before you can do that.");
            $data->input = $data->command = $data->cleaned = null;
        }
    }
}
