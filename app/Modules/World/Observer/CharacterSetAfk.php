<?php

namespace Modules\World\Observer;

use Game\Connection\Interfaces\Connection;
use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use stdClass;

class CharacterSetAfk extends Observer
{
    /**
     * @var \Modules\World\Helper\Character\State
     */
    protected $stateHelper;

    /**
     * @var \Modules\World\Helper\Character
     */
    protected $characterHelper;

    protected $limit = 15;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
        $this->characterHelper = $characterHelper;
    }

    public function fire(stdClass $data)
    {
        /** @var Connection $connection */
        foreach ($data->connections as $connection) {
            if (   $this->stateHelper->getState($connection) === $this->stateHelper::ACTIVE
                && round((time() - $connection->getData('character')->getData('last_update')) / 60) >= $this->limit) {

                $this->getOutput()->write($connection, "{W***{c *Knock Knock!* Anybody home? Guess not, so away from keyboard we go!");

                $character = $connection->getData('character');

                $others = $this->characterHelper->getOnlineRoomCharacters($character->getData('room'), $character);
                foreach($others as $other) {
                    $this->getOutput()->write($other->getConnection(), ["%s steps away from the keyboard.", $character->getName()]);
                }

                $idle = $connection->getData('character')->getData('last_update');
                $this->getInput()->handle($connection, 'afk');
                $connection->getData('character')->setData('last_update', $idle);
            }
        }
    }
}
