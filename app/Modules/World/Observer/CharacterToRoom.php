<?php

namespace Modules\World\Observer;

use Game\Config;
use Game\Input;
use Game\Module\Entity\Exception as EntityException;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\Location;
use Modules\World\Entity\Room;
use stdClass;

class CharacterToRoom extends Observer
{
    /**
     * @var \Game\Config
     */
    protected $config;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, Config $config)
    {
        parent::__construct($entityManager, $input, $output);
        $this->config = $config;
    }

    public function fire(stdClass $data)
    {
        try {
            // try and load up the last location of the character
            /** @var \Modules\World\Entity\Location $location */
            $location = $this->getEntityManager()->load(new Location(), $data->character->getId(), 'character_id');

            // but if no location exists, create one now
            if (is_null($location->getRoomId())) {
                $location->setRoomId($this->config->get('world.startroom'));
                $location->setCharacterId($data->character->getId());

                $this->getEntityManager()->save($location);
                $location = $this->getEntityManager()->load($location, $data->character->getId(), 'character_id');
            }

            // Set up the area, room and links structure
            $room = new Room($this->getEntityManager());

            // hydrate the room object, either using the room_id stored in the location or room 1
            $room = $this->getEntityManager()->load($room, $location->getRoomId());

            // store the room and location in the character
            $data->character->setData('room', $room);
            $data->character->setData('location', $location);

            // let the character do a look
            $this->getInput()->handle($data->character->getConnection(), 'look');

        } catch (EntityException $e) {
            #TODO what to do here..
        }
    }
}
