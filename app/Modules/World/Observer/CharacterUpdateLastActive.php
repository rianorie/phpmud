<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use stdClass;

class CharacterUpdateLastActive extends Observer
{
    /**
     * @var \Modules\World\Helper\Character\State
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
    }

    public function fire(stdClass $data)
    {
        if ($this->stateHelper->getState($data->connection) != $this->stateHelper::INACTIVE) {
            $data->connection->getData('character')->setData('last_update', time());
        }
    }
}
