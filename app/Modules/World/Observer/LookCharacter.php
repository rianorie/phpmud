<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\Link;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Link as LinkHelper;
use Modules\World\Table\CharacterWearslot;
use stdClass;

use Modules\World\Entity\Character;
use Modules\World\Entity\RoomItem;

class LookCharacter extends Observer
{
    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->characterHelper = $characterHelper;
    }

    /**
     * @param stdClass $data
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function fire(stdClass $data)
    {
        /** @var Character $character */
        $character = $data->connection->getData('character');

        /** @var Character $target */
        $target = $data->character;

        $this->getOutput()->write($target->getConnection(), ['%s looks at you.', $character->getName()]);

        /** @var CharacterItem[] $characterItemCollection */
        $characterItemCollection = $this->getEntityManager()->find(new CharacterItem(), ['character_id' => $target->getId(), 'inventory' => 0]);

        $characterItems = [];

        foreach ($characterItemCollection as $characterItem) {
            if ($characterItem) {
                $characterItems[$characterItem->getCharacterSlot()] = $characterItem;
            }
        }

        $output  = $this->characterHelper->getPublicInfo($target)."\n";
        $output .= ($target->getDescription() ?? 'There is nothing particular to see about '.$target->getName())."\n\n";

        if (count($characterItems) > 0) {

            $output .= sprintf("{w%s is using:\n", $target->getName());
            $output .= ".::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.{c\n";

            $slots = (new CharacterWearslot())->all();
            foreach($slots as $slot => $slotdata) {

                if (isset($characterItems[$slotdata->slot])) {
                    $output .= sprintf("%-26s %s\n", sprintf("{c<{w%s{c>", trim($slotdata->label)), $characterItems[$slotdata->slot]->getShortDescription());
                }
            }
        }

        $this->getOutput()->write($data->connection, $output);
    }
}
