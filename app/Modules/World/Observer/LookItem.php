<?php

namespace Modules\World\Observer;

use Game\Module\Observer;
use Modules\World\Entity\Item;
use stdClass;

class LookItem extends Observer
{
    public function fire(stdClass $data)
    {
        if ( ! isset($data->inside) || ! $data->inside) {
            $this->getOutput()->write($data->connection, $data->item->getLongDescription());
            return;
        }


        $childItems = [];
        $total = 0;

        /** @var Item[] $childItemCollection */
        $childItemCollection = $this->getEntityManager()->find($data->item, ['parent_id' => $data->item->getId()]);
        foreach($childItemCollection as $childItem) {
            if ($childItem) {
                $childItems[$childItem->keywords] = [
                    'item' => $childItem,
                    'count' => (isset($childItems[$childItem->keywords]) ? $childItems[$childItem->keywords]['count'] + 1 : 1)
                ];

                $total++;
            }
        }

        $output = sprintf("%s {xholds:\n", $data->item->getShortDescription());

        if ($total == 0) {
            $output .= "     Nothing.\n";
        } else {

            $output .= sprintf("You count %d item%s in the container.\n", $total, ($total > 0 ? 's' : ''));

            foreach ($childItems as $childItem) {
                $output .= sprintf(
                    "{x%4s %s\n",
                    ($childItem['count'] < 2 ? '' : sprintf('{c({w%2d{c)', $childItem['count'])),
                    $childItem['item']->getShortDescription()
                );
            }
        }

        $this->getOutput()->write($data->connection, $output);
    }
}
