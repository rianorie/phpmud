<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\CharacterItem;
use Modules\World\Entity\MobileItem;
use Modules\World\Entity\RoomMobile;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Mobile as MobileHelper;
use Modules\World\Table\CharacterWearslot;
use Modules\World\Table\Gender;
use stdClass;

use Modules\World\Entity\Character;

class LookMobile extends Observer
{
    /**
     * @var MobileHelper
     */
    protected $mobileHelper;

    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper, $mobileHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->mobileHelper = $mobileHelper;
        $this->characterHelper = $characterHelper;
    }

    /**
     * @param stdClass $data
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function fire(stdClass $data)
    {
        /** @var Character $character */
        $character = $data->connection->getData('character');

        /** @var RoomMobile $target */
        $target = $data->mobile;

        /** @var CharacterItem[] $characterItemCollection */
        $characterItemCollection = $this->getEntityManager()->find(new MobileItem(), ['room_mobile_id' => $target->getId(), 'inventory' => 0]);

        $characterItems = [];

        foreach ($characterItemCollection as $characterItem) {
            if ($characterItem) {
                $characterItems[$characterItem->getCharacterSlot()] = $characterItem;
            }
        }

        $output = ( ! empty($target->getDescription()) ? $target->getDescription() : ('You see nothing special about '.($target->getSex() == Gender::MALE ? 'him' : 'her').".\n"));

        if (count($characterItems) > 0) {

            $output .= sprintf("{w%s is using:\n", $target->getShortDescription());
            $output .= ".::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.{c\n";

            $slots = (new CharacterWearslot())->all();
            foreach($slots as $slot => $slotdata) {

                if (isset($characterItems[$slotdata->slot])) {
                    $output .= sprintf("%-26s %s\n", sprintf("{c<{w%s{c>", trim($slotdata->label)), $characterItems[$slotdata->slot]->getShortDescription());
                }
            }
        }

        $this->getOutput()->write($data->connection, $output);

        $online = $this->characterHelper->getOnlineRoomCharacters($character->getData('room'), $character);
        foreach($online as $other) {
            $this->getOutput()->write($other->getConnection(), ['%s looks at %s.', $character->getName(), $target->getShortDescription()]);
        }
    }
}
