<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Modules\World\Entity\Link;
use Modules\World\Entity\RoomMobile;
use Modules\World\Helper\Character as CharacterHelper;
use Modules\World\Helper\Link as LinkHelper;
use Modules\World\Table\Position;
use stdClass;

use Modules\World\Entity\Character;
use Modules\World\Entity\RoomItem;

class LookRoom extends Observer
{
    /**
     * @var CharacterHelper
     */
    protected $characterHelper;

    /**
     * @var LinkHelper
     */
    protected $linkHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $characterHelper, $linkHelper)
    {
        parent::__construct($entityManager, $input, $output);

        $this->characterHelper = $characterHelper;
        $this->linkHelper = $linkHelper;
    }

    /**
     * @param stdClass $data
     *
     * @throws \Game\Module\Entity\Exception
     */
    public function fire(stdClass $data)
    {
        if ( ! $data->connection || ! $data->connection->getData('character')) {
            return;
        }

        /** @var Character $char */
        $char = $data->connection->getData('character');

        /** @var \Modules\World\Entity\Room $room */
        $room = $char->getData('room');

        // grab the links for this room
        $links = [];

        /** @var Link $link */
        foreach ($this->getEntityManager()->find(new Link(), ['room_id' => $room->getId()]) as $link) {
            if ($link) {
                if (in_array('door', $link->getFlags()) && ! $this->linkHelper->isOpen($link)) {

                    $links[] = '{c({w'. $this->linkHelper->getDirectionNameForId($link->getDirection()) .'{c){w';

                } else {

                    $links[] = $this->linkHelper->getDirectionNameForId($link->getDirection());
                }
            }
        }

        // convert the array to a string
        $links = (count($links) > 0 ? implode(' ', $links) : 'none');

        // Start generating the room output
        $output = sprintf('%s {c[{w%d{c]', $room->getTitle(), $room->getId()) . "\n";
        $output .= '  {w'.$room->getDescription() . "\n\n";
        $output .= sprintf('{x[{wObvious Exits: %s{x]', $links) . "\n";

        /** @var RoomItem[] $roomItemsCollection */
        $roomItemsCollection = $this->getEntityManager()->find(
            new RoomItem($this->getEntityManager()),
            [
                'room_id' => $room->getId(),
                'parent_id' => null
            ]
        );

        $roomItems = [];

        foreach ($roomItemsCollection as $roomItem) {
            if ($roomItem) {
                $roomItems[$roomItem->keywords] = [
                    'item'  => $roomItem,
                    'count' => (isset($roomItems[$roomItem->keywords]) ? $roomItems[$roomItem->keywords]['count'] + 1 : 1)
                ];
            }
        }

        foreach ($roomItems as $roomItem) {
            $output .= sprintf(
                "{x%4s %s\n",
                ($roomItem['count'] < 2 ? '' : sprintf('{c({w%2d{c)', $roomItem['count'])),
                $roomItem['item']->getLongDescription()
            );
        }

        /** @var RoomMobile[] $mobiles */
        $mobiles = $this->getEntityManager()->find(new RoomMobile(), ['room_id' => $room->getId()]);
        foreach($mobiles as $mobile) {
            if ($mobile) {
                $output .= $mobile->getLongDescription()."\n";
            }
        }

        // output the characters in the room
        $online = $this->characterHelper->getOnlineRoomCharacters($room, $char, false);
        foreach($online as $other) {
            if ($other) {

                $posItemDesc = $other->getData('position_desc');
                if ( ! empty($posItemDesc)) {
                    $posItemDesc = ' '.$posItemDesc;
                }

                $posItemDesc .= '.';

                switch($other->getPosition()) {
                    case Position::NORMAL:
                        $rdesc = ($other->getRoomdesc() ?? '{x'.$other->getName().' is here.');
                        if (strpos($rdesc, $other->getName()) === false) {
                            $rdesc = $other->getName().' '.$rdesc;
                        }
                        break;

                    case Position::SIT:
                        $rdesc = $other->getName().' is sitting'.$posItemDesc;
                        break;

                    case Position::REST:
                        $rdesc = $other->getName().' is resting'.$posItemDesc;
                        break;

                    case Position::SLEEP:
                        $rdesc = $other->getName().' is sleeping'.$posItemDesc;
                        break;

                    case Position::STAND:
                        $rdesc = $other->getName().' is standing'.$posItemDesc;
                        break;
                }

                $output .= $rdesc."\n";
            }
        }

        $output = rtrim($output);

        $this->getOutput()->write($data->connection, $output);
    }
}
