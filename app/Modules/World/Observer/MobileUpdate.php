<?php

namespace Modules\World\Observer;

use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;

use Modules\World\Entity\RoomMobile;
use stdClass;

class MobileUpdate extends Observer
{
    /**
     * @var \Modules\World\Helper\Mobile
     */
    protected $mobileHelper;


    public function __construct(EntityManager $entityManager, Input $input, Output $output, $mobileHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->mobileHelper = $mobileHelper;
    }

    public function fire(stdClass $data)
    {
        try {

            $mobiles = $this->getEntityManager()->find(new RoomMobile(), ['flag_action' => ['not like' => '%sentinel%']]);
            foreach($mobiles as $mobile) {
                if ($mobile) {



                }
            }

        } catch (\Exception $exception) {
        }

    }
}
