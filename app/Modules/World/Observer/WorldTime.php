<?php

namespace Modules\World\Observer;

use Game\Config;
use Game\Input;
use Game\Module\Entity\Interfaces\Manager as EntityManager;
use Game\Module\Observer;
use Game\Output;
use Game\Registry;
use Modules\World\Helper\Character\State;
use stdClass;

class WorldTime extends Observer
{
    /**
     * @var State
     */
    protected $stateHelper;

    public function __construct(EntityManager $entityManager, Input $input, Output $output, $stateHelper)
    {
        parent::__construct($entityManager, $input, $output);
        $this->stateHelper = $stateHelper;
    }

    public function fire(stdClass $data)
    {
        try {
            $storedHour = 0;
            if (Registry::exists('game.time')) {
                $storedHour = Registry::get('game.time');
            }

            $currentHour = (((time() - strtotime("today")) / 1200) % 24);
            if ($currentHour != $storedHour) {

                Registry::set('game.time', $currentHour);

                switch($currentHour) {
                    case 5:
                        $output = "\n{wRejoice, you've survived to see a new day begin.{x\n";
                        break;
                    case 6:
                        $output = "{wThe sun rises like a ball of fire in the east.\n";
                        break;
                    case 19:
                        $output = "{wThe sun slowly disappears in the west.{x\n";
                        break;
                }

                if (isset($output)) {
                    foreach($data->connections as $con) {
                        if (in_array($this->stateHelper->getState($con), [State::ACTIVE, State::AFK])) {
                            $this->getOutput()->write($con, $output);
                        }
                    }
                }
            }

        } catch (\Exception $exception) { }
    }
}

/**
 * 20min = 1hr
 * 1 day = 3 days
 *
 *
 * 24 hrs in a day
 * 35 days in a month
 * 13 months
 * 455 days in a year
 * at 3:1 that means 151.6 real days per year
 *
 * 373
 */
