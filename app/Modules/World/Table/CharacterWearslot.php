<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class CharacterWearslot implements Table
{
    const SLOT_LIGHT = 1;
    const SLOT_EYES = 2;
    const SLOT_NOSE = 3;
    const SLOT_NECK1 = 4;
    const SLOT_NECK2 = 5;
    const SLOT_HEAD = 6;
    const SLOT_LEFT_EAR = 7;
    const SLOT_RIGHT_EAR = 8;
    const SLOT_HANDS = 9;
    const SLOT_LEFT_WRIST = 10;
    const SLOT_RIGHT_WRIST = 11;
    const SLOT_LEFT_RING = 12;
    const SLOT_RIGHT_RING = 13;
    const SLOT_ARMS = 14;
    const SLOT_LEFT_FOREARM = 15;
    const SLOT_RIGHT_FOREARM = 16;
    const SLOT_BODY = 17;
    const SLOT_ABOUT = 18;
    const SLOT_WAIST = 19;
    const SLOT_LEGS = 20;
    const SLOT_FEET = 21;
    const SLOT_LEFT_CALF = 22;
    const SLOT_RIGHT_CALF = 23;
    const SLOT_LEFT_THIGH = 24;
    const SLOT_RIGHT_THIGH = 25;
    const SLOT_LEFT_ANKLE = 26;
    const SLOT_RIGHT_ANKLE = 27;
    const SLOT_SHOULDERS = 28;
    const SLOT_BACK = 29;
    const SLOT_SHIELD = 30;
    const SLOT_HOLD = 31;
    const SLOT_PRIMARY = 32;
    const SLOT_SECONDARY = 33;
    const SLOT_FLOAT = 34;
    const SLOT_MIND = 35;
    const SLOT_FACE = 36;
    const SLOT_ANGREAL = 37;

    public $list = [
        self::SLOT_LIGHT         => ['slot' => self::SLOT_LIGHT, 'label' => 'as a light', 'wearslot' => Wearslot::WEAR_LIGHT, 'wear_label' => 'as %s light', 'enabled' => true],
        self::SLOT_EYES          => ['slot' => self::SLOT_EYES, 'label' => 'over the eyes', 'wearslot' => Wearslot::WEAR_EYES, 'wear_label' => 'over %s eyes', 'enabled' => true],
        self::SLOT_NOSE          => ['slot' => self::SLOT_NOSE, 'label' => 'in the nose', 'wearslot' => Wearslot::WEAR_NOSE, 'wear_label' => 'in %s nose', 'enabled' => true],
        self::SLOT_NECK1         => ['slot' => self::SLOT_NECK1, 'label' => 'around the neck', 'wearslot' => Wearslot::WEAR_NECK, 'wear_label' => 'around %s neck', 'enabled' => true],
        self::SLOT_NECK2         => ['slot' => self::SLOT_NECK2, 'label' => 'around the neck ', 'wearslot' => Wearslot::WEAR_NECK, 'wear_label' => 'around %s neck', 'enabled' => true], // space is a hackish solution but who came up with two slots with one name >:(
        self::SLOT_HEAD          => ['slot' => self::SLOT_HEAD, 'label' => 'on the head', 'wearslot' => Wearslot::WEAR_HEAD, 'wear_label' => 'on %s head', 'enabled' => true],
        self::SLOT_LEFT_EAR      => ['slot' => self::SLOT_LEFT_EAR, 'label' => 'in left ear', 'wearslot' => Wearslot::WEAR_EAR, 'wear_label' => 'in %s left ear', 'enabled' => true],
        self::SLOT_RIGHT_EAR     => ['slot' => self::SLOT_RIGHT_EAR, 'label' => 'in right ear', 'wearslot' => Wearslot::WEAR_EAR, 'wear_label' => 'in %s right ear', 'enabled' => true],
        self::SLOT_HANDS         => ['slot' => self::SLOT_HANDS, 'label' => 'on the hands', 'wearslot' => Wearslot::WEAR_HANDS, 'wear_label' => 'on %s hands', 'enabled' => true],
        self::SLOT_LEFT_WRIST    => ['slot' => self::SLOT_LEFT_WRIST, 'label' => 'on left wrist', 'wearslot' => Wearslot::WEAR_WRIST, 'wear_label' => 'around %s left wrist', 'enabled' => true],
        self::SLOT_RIGHT_WRIST   => ['slot' => self::SLOT_RIGHT_WRIST, 'label' => 'on right wrist', 'wearslot' => Wearslot::WEAR_WRIST, 'wear_label' => 'around %s right wrist', 'enabled' => true],
        self::SLOT_LEFT_RING     => ['slot' => self::SLOT_LEFT_RING, 'label' => 'left ring finger', 'wearslot' => Wearslot::WEAR_FINGER, 'wear_label' => 'on %s left finger', 'enabled' => true],
        self::SLOT_RIGHT_RING    => ['slot' => self::SLOT_RIGHT_RING, 'label' => 'right ring finger', 'wearslot' => Wearslot::WEAR_FINGER, 'wear_label' => 'on %s right finger', 'enabled' => true],
        self::SLOT_ARMS          => ['slot' => self::SLOT_ARMS, 'label' => 'on the arms', 'wearslot' => Wearslot::WEAR_ARMS, 'wear_label' => 'on %s your arms', 'enabled' => true],
        self::SLOT_LEFT_FOREARM  => ['slot' => self::SLOT_LEFT_FOREARM, 'label' => 'on left forearm', 'wearslot' => Wearslot::WEAR_FOREARM, 'wear_label' => 'around %s left forearm', 'enabled' => true],
        self::SLOT_RIGHT_FOREARM => ['slot' => self::SLOT_RIGHT_FOREARM, 'label' => 'on right forearm', 'wearslot' => Wearslot::WEAR_FOREARM, 'wear_label' => 'around %s right forearm', 'enabled' => true],
        self::SLOT_BODY          => ['slot' => self::SLOT_BODY, 'label' => 'on the body', 'wearslot' => Wearslot::WEAR_BODY, 'wear_label' => 'on %s body', 'enabled' => true],
        self::SLOT_ABOUT         => ['slot' => self::SLOT_ABOUT, 'label' => 'about the body', 'wearslot' => Wearslot::WEAR_ABOUT, 'wear_label' => 'about %s body', 'enabled' => true],
        self::SLOT_WAIST         => ['slot' => self::SLOT_WAIST, 'label' => 'around the waist', 'wearslot' => Wearslot::WEAR_WAIST, 'wear_label' => 'around %s waist', 'enabled' => true],
        self::SLOT_LEGS          => ['slot' => self::SLOT_LEGS, 'label' => 'on the legs', 'wearslot' => Wearslot::WEAR_LEGS, 'wear_label' => 'on %s legs', 'enabled' => true],
        self::SLOT_FEET          => ['slot' => self::SLOT_FEET, 'label' => 'on the feet', 'wearslot' => Wearslot::WEAR_FEET, 'wear_label' => 'on %s feet', 'enabled' => true],
        self::SLOT_LEFT_CALF     => ['slot' => self::SLOT_LEFT_CALF, 'label' => 'on left calf', 'wearslot' => Wearslot::WEAR_CALF, 'wear_label' => 'around %s left calf', 'enabled' => true],
        self::SLOT_RIGHT_CALF    => ['slot' => self::SLOT_RIGHT_CALF, 'label' => 'on right calf', 'wearslot' => Wearslot::WEAR_CALF, 'wear_label' => 'around %s right calf', 'enabled' => true],
        self::SLOT_LEFT_THIGH    => ['slot' => self::SLOT_LEFT_THIGH, 'label' => 'on left thigh', 'wearslot' => Wearslot::WEAR_THIGH, 'wear_label' => 'around %s left thigh', 'enabled' => true],
        self::SLOT_RIGHT_THIGH   => ['slot' => self::SLOT_RIGHT_THIGH, 'label' => 'on right thigh', 'wearslot' => Wearslot::WEAR_THIGH, 'wear_label' => 'around %s right thigh', 'enabled' => true],
        self::SLOT_LEFT_ANKLE    => ['slot' => self::SLOT_LEFT_ANKLE, 'label' => 'on left ankle', 'wearslot' => Wearslot::WEAR_ANKLE, 'wear_label' => 'on %s left ankle', 'enabled' => true],
        self::SLOT_RIGHT_ANKLE   => ['slot' => self::SLOT_RIGHT_ANKLE, 'label' => 'on right ankle', 'wearslot' => Wearslot::WEAR_ANKLE, 'wear_label' => 'on %s right ankle', 'enabled' => true],
        self::SLOT_SHOULDERS     => ['slot' => self::SLOT_SHOULDERS, 'label' => 'on the shoulders', 'wearslot' => Wearslot::WEAR_SHOULDERS, 'wear_label' => 'on %s shoulders', 'enabled' => true],
        self::SLOT_BACK          => ['slot' => self::SLOT_BACK, 'label' => 'on the back', 'wearslot' => Wearslot::WEAR_BACK, 'wear_label' => 'on %s back', 'enabled' => true],
        self::SLOT_SHIELD        => ['slot' => self::SLOT_SHIELD, 'label' => 'as a shield', 'wearslot' => Wearslot::WEAR_SHIELD, 'wear_label' => 'in %s hand', 'enabled' => true],
        self::SLOT_HOLD          => ['slot' => self::SLOT_HOLD, 'label' => 'held in hands', 'wearslot' => Wearslot::HOLD, 'wear_label' => 'in %s hands', 'enabled' => true],
        self::SLOT_PRIMARY       => ['slot' => self::SLOT_PRIMARY, 'label' => 'primary weapon', 'wearslot' => Wearslot::WIELD, 'wear_label' => '', 'enabled' => true],
        self::SLOT_SECONDARY     => ['slot' => self::SLOT_SECONDARY, 'label' => 'secondary weapon', 'wearslot' => Wearslot::WIELD, 'wear_label' => '', 'enabled' => true],
        self::SLOT_FLOAT         => ['slot' => self::SLOT_FLOAT, 'label' => 'floating nearby', 'wearslot' => Wearslot::WEAR_FLOAT, 'wear_label' => 'and it floats next to %s', 'enabled' => true],
        self::SLOT_MIND          => ['slot' => self::SLOT_MIND, 'label' => 'near the mind', 'wearslot' => Wearslot::WEAR_NEAR_MIND, 'wear_label' => 'near %s mind', 'enabled' => true],
        self::SLOT_FACE          => ['slot' => self::SLOT_FACE, 'label' => 'over the face', 'wearslot' => Wearslot::WEAR_FACE, 'wear_label' => 'over %s face', 'enabled' => true],
        self::SLOT_ANGREAL       => ['slot' => self::SLOT_ANGREAL, 'label' => 'as an angreal', 'wearslot' => Wearslot::WEAR_ANGREAL, 'wear_label' => 'around %s left forearm', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
