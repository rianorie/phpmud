<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Container implements Table
{
    const CLOSEABLE = 'closeable';
    const PICKPROOF = 'pickproof';
    const CLOSED = 'closed';
    const LOCKED = 'locked';
    const PUT_ON = 'put_on';
    const LOCKABLE = 'lockable';


    public $list = [
        Container::CLOSEABLE => ['label' => 'closeable', 'enabled' => true],
        Container::CLOSED    => ['label' => 'closed', 'enabled' => true],
        Container::LOCKABLE  => ['label' => 'lockable', 'enabled' => true],
        Container::LOCKED    => ['label' => 'locked', 'enabled' => true],
        Container::PICKPROOF => ['label' => 'pickproof', 'enabled' => true],
        Container::PUT_ON    => ['label' => 'put_on', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
