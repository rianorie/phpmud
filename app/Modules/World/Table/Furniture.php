<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Furniture implements Table
{
    const STAND_AT = 'stand_at';
    const STAND_ON = 'stand_on';
    const STAND_IN = 'stand_in';
    const SIT_AT = 'sit_at';
    const SIT_ON = 'sit_on';
    const SIT_IN = 'sit_in';
    const REST_AT = 'rest_at';
    const REST_ON = 'rest_on';
    const REST_IN = 'rest_in';
    const SLEEP_AT = 'sleep_at';
    const SLEEP_ON = 'sleep_on';
    const SLEEP_IN = 'spee_in';
    const PUT_AT = 'put_at';
    const PUT_ON = 'put_on';
    const PUT_IN = 'put_in';
    const PUT_INSIDE = 'put_inside';
    const SIT_UNDER = 'sit_under';
    const REST_UNDER = 'rest_under';
    const SLEEP_UNDER = 'sleep_under';
    const STAND_UNDER = 'stand_under';

    public $list = [
        Furniture::STAND_AT    => ['label' => 'stand_at', 'enabled' => true],
        Furniture::STAND_ON    => ['label' => 'stand_on', 'enabled' => true],
        Furniture::STAND_IN    => ['label' => 'stand_in', 'enabled' => true],
        Furniture::SIT_AT      => ['label' => 'sit_at', 'enabled' => true],
        Furniture::SIT_ON      => ['label' => 'sit_on', 'enabled' => true],
        Furniture::SIT_IN      => ['label' => 'sit_in', 'enabled' => true],
        Furniture::REST_AT     => ['label' => 'rest_at', 'enabled' => true],
        Furniture::REST_ON     => ['label' => 'rest_on', 'enabled' => true],
        Furniture::REST_IN     => ['label' => 'rest_in', 'enabled' => true],
        Furniture::SLEEP_AT    => ['label' => 'sleep_at', 'enabled' => true],
        Furniture::SLEEP_ON    => ['label' => 'sleep_on', 'enabled' => true],
        Furniture::SLEEP_IN    => ['label' => 'sleep_in', 'enabled' => true],
        Furniture::PUT_AT      => ['label' => 'put_at', 'enabled' => true],
        Furniture::PUT_ON      => ['label' => 'put_on', 'enabled' => true],
        Furniture::PUT_IN      => ['label' => 'put_in', 'enabled' => true],
        Furniture::PUT_INSIDE  => ['label' => 'put_inside', 'enabled' => true],
        Furniture::SIT_UNDER   => ['label' => 'sit_under', 'enabled' => true],
        Furniture::REST_UNDER  => ['label' => 'rest_under', 'enabled' => true],
        Furniture::SLEEP_UNDER => ['label' => 'sleep_under', 'enabled' => true],
        Furniture::STAND_UNDER => ['label' => 'stand_under', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
