<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Gender implements Table
{
    const MALE = 1;

    const FEMALE = 2;

    public $list = [
        Gender::MALE    => ['label' => 'Male', 'enabled' => true],
        Gender::FEMALE  => ['label' => 'Female', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) { return (object) $g; }, $this->list);
    }
}
