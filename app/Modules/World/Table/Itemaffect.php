<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Itemaffect implements Table
{
    const NONE = 0;
    const STR = 1;
    const DEX = 2;
    const INT = 3;
    const WIS = 4;
    const CON = 5;
    const SEX = 6;
//    const CLASS = 7;
    const LEVEL = 8;
    const AGE = 9;
    const HEIGHT = 10;
    const WEIGHT = 11;
    const STAMINA = 12;
    const HIT = 13;
    const GOLD = 15;
    const EXP = 16;
    const AC = 17;
    const HITROLL = 18;
    const DAMROLL = 19;
    const SAVES = 20;
    const SAVING_ROD = 21;
    const SAVING_PETRI = 22;
    const SAVING_BREATH = 23;
    const SAVING_SPELL = 24;
    const SPELL_AFFECT = 25;
    const REF = 26;
    const INS = 27;
    const CHA = 28;


    public $list = [
        Itemaffect::NONE          => ['label' => 'NONE', 'enabled' => true],
        Itemaffect::STR           => ['label' => 'STR', 'enabled' => true],
        Itemaffect::DEX           => ['label' => 'DEX', 'enabled' => true],
        Itemaffect::INT           => ['label' => 'INT', 'enabled' => true],
        Itemaffect::WIS           => ['label' => 'WIS', 'enabled' => true],
        Itemaffect::CON           => ['label' => 'CON', 'enabled' => true],
        Itemaffect::SEX           => ['label' => 'SEX', 'enabled' => true],
        Itemaffect::LEVEL         => ['label' => 'LEVEL', 'enabled' => true],
        Itemaffect::AGE           => ['label' => 'AGE', 'enabled' => true],
        Itemaffect::HEIGHT        => ['label' => 'HEIGHT', 'enabled' => true],
        Itemaffect::WEIGHT        => ['label' => 'WEIGHT', 'enabled' => true],
        Itemaffect::STAMINA       => ['label' => 'STAMINA', 'enabled' => true],
        Itemaffect::HIT           => ['label' => 'HIT', 'enabled' => true],
        Itemaffect::GOLD          => ['label' => 'GOLD', 'enabled' => true],
        Itemaffect::EXP           => ['label' => 'EXP', 'enabled' => true],
        Itemaffect::AC            => ['label' => 'AC', 'enabled' => true],
        Itemaffect::HITROLL       => ['label' => 'HITROLL', 'enabled' => true],
        Itemaffect::DAMROLL       => ['label' => 'DAMROLL', 'enabled' => true],
        Itemaffect::SAVES         => ['label' => 'SAVES', 'enabled' => true],
        Itemaffect::SAVING_ROD    => ['label' => 'SAVING_ROD', 'enabled' => true],
        Itemaffect::SAVING_PETRI  => ['label' => 'SAVING_PETRI', 'enabled' => true],
        Itemaffect::SAVING_BREATH => ['label' => 'SAVING_BREATH', 'enabled' => true],
        Itemaffect::SAVING_SPELL  => ['label' => 'SAVING_SPELL', 'enabled' => true],
        Itemaffect::SPELL_AFFECT  => ['label' => 'SPELL_AFFECT', 'enabled' => true],
        Itemaffect::REF           => ['label' => 'REF', 'enabled' => true],
        Itemaffect::INS           => ['label' => 'INS', 'enabled' => true],
        Itemaffect::CHA           => ['label' => 'CHA', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
