<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Itemtype implements Table
{
    const LIGHT = 0; // light
    const SCROLL = 1; // scroll
    const WAND = 2; // wand
    const STAFF = 3; // staff
    const WEAPON = 4; // weapon
    const TREASURE = 5; // treasure
    const ARMOR = 6; // armor
    const POTION = 7; // potion
    const CLOTHING = 8; // clothing
    const FURNITURE = 9; // furniture
    const TRASH = 10; // trash
    const CONTAINER = 11; // container
    const DRINK_CON = 12; // drink
    const KEY = 13; // key
    const FOOD = 14; // food
    const MONEY = 15; // money
    const BOAT = 16; // boat
    const CORPSE_NPC = 17; // npc_corpse
    const CORPSE_PC = 18; // pc_corpse
    const FOUNTAIN = 19; // fountain
    const PILL = 20; // pill
    const PROTECT = 21; // protect
    const MAP = 22; // map
    const PORTAL = 23; // portal
    const WARP_STONE = 24; // warp_stone
    const ROOM_KEY = 25; // room_key
    const GEM = 26; // gem
    const JEWELRY = 27; // jewelry
    const JUKEBOX = 28; // jukebox
    const QUIVER = 29; // quiver
    const ARROW = 30; // arrow
    const PIPE_TABAC = 31; // pipe
    const CHEW_TABAC = 32; // chew
    const ANGREAL_MALE = 33; // maleangreal
    const ANGREAL_FEMALE = 34; // femaleangreal
    const CLOTH_BOLT = 35; // cloth_bolt
    const TREE_PC = 36; // treepc
    const NOTEPAPER = 37; // notepaper
    const TRIGGER = 38; // trigger
    const TREASURECHEST = 39; // treasurechest
    const LOCKPICK = 40; // lockpick
    const MIRROR = 41; // mirror
    const RAZOR = 42; // razor
    const BRUSH = 43; // brush
    const FIREOIL = 44; // fireoil
    const INK = 45; // ink
    const QUILL = 46; // quill
    const JOURNAL = 47; // journal
    const WIND_PIPES = 48; // pipes
    const WIND_FLUTE = 49; // flute
    const STRINGED_BITTERN = 50; // bittern
    const PERCUSSION_DRUMS = 51; // drums
    const SHAPE_KNIFE = 52; // carver_knife
    const SHAPE_SHAPER = 53; // wood_shaper
    const SHAPE_STICK = 54; // wood_stick
    const SHAPE_LIMB = 55; // wood_limb
    const SHAPE_BRANCH = 56; // wood_branch
    const SHAPE_BLOCK = 57; // wood_block
    const SHAPE_KEYBLANK = 58; // keyblank
    const SMITH_ORE = 59; // ore
    const SMITH_HAMMER = 60; // hammer
    const SMITH_ANVIL = 61; // anvil
    const SMITH_BARREL = 62; // barrel
    const SMITH_FURNACE = 63; // furnace
    const SMITH_GRINDER = 64; // grinder
    const COMM_DEV = 65; // commdev
    const WINDOW = 66; // window
    const KEYRING = 67; // keyring
    const SHAPE_MUSHROOM = 68; // mushroom
    const SHAPE_CLEMATIS = 69; // clematis
    const SHAPE_HAWTHORN = 70; // hawthorn
    const SHAPE_BONESET = 71; // boneset
    const SHAPE_OBSIDIAN = 72; // obsidian
    const SHAPE_SNAKESKIN = 73; // snakeskin
    const TOME = 74; // tome
    const CLOCK = 75; // clock
    const LAMPPOST = 76; // lamppost
    const ROPE = 77; // rope
    const PITON = 78; // piton
    const SHOP = 79; // shop


    public $list = [
        Itemtype::LIGHT            => ['label' => 'light', 'enabled' => true],
        Itemtype::SCROLL           => ['label' => 'scroll', 'enabled' => true],
        Itemtype::WAND             => ['label' => 'wand', 'enabled' => true],
        Itemtype::STAFF            => ['label' => 'staff', 'enabled' => true],
        Itemtype::WEAPON           => ['label' => 'weapon', 'enabled' => true],
        Itemtype::TREASURE         => ['label' => 'treasure', 'enabled' => true],
        Itemtype::ARMOR            => ['label' => 'armor', 'enabled' => true],
        Itemtype::POTION           => ['label' => 'potion', 'enabled' => true],
        Itemtype::CLOTHING         => ['label' => 'clothing', 'enabled' => true],
        Itemtype::FURNITURE        => ['label' => 'furniture', 'enabled' => true],
        Itemtype::TRASH            => ['label' => 'trash', 'enabled' => true],
        Itemtype::CONTAINER        => ['label' => 'container', 'enabled' => true],
        Itemtype::DRINK_CON        => ['label' => 'drink', 'enabled' => true],
        Itemtype::KEY              => ['label' => 'key', 'enabled' => true],
        Itemtype::FOOD             => ['label' => 'food', 'enabled' => true],
        Itemtype::MONEY            => ['label' => 'money', 'enabled' => true],
        Itemtype::BOAT             => ['label' => 'boat', 'enabled' => true],
        Itemtype::CORPSE_NPC       => ['label' => 'npc_corpse', 'enabled' => true],
        Itemtype::CORPSE_PC        => ['label' => 'pc_corpse', 'enabled' => true],
        Itemtype::FOUNTAIN         => ['label' => 'fountain', 'enabled' => true],
        Itemtype::PILL             => ['label' => 'pill', 'enabled' => true],
        Itemtype::PROTECT          => ['label' => 'protect', 'enabled' => true],
        Itemtype::MAP              => ['label' => 'map', 'enabled' => true],
        Itemtype::PORTAL           => ['label' => 'portal', 'enabled' => true],
        Itemtype::WARP_STONE       => ['label' => 'warp_stone', 'enabled' => true],
        Itemtype::ROOM_KEY         => ['label' => 'room_key', 'enabled' => true],
        Itemtype::GEM              => ['label' => 'gem', 'enabled' => true],
        Itemtype::JEWELRY          => ['label' => 'jewelry', 'enabled' => true],
        Itemtype::JUKEBOX          => ['label' => 'jukebox', 'enabled' => true],
        Itemtype::QUIVER           => ['label' => 'quiver', 'enabled' => true],
        Itemtype::ARROW            => ['label' => 'arrow', 'enabled' => true],
        Itemtype::PIPE_TABAC       => ['label' => 'pipe', 'enabled' => true],
        Itemtype::CHEW_TABAC       => ['label' => 'chew', 'enabled' => true],
        Itemtype::ANGREAL_MALE     => ['label' => 'maleangreal', 'enabled' => true],
        Itemtype::ANGREAL_FEMALE   => ['label' => 'femaleangreal', 'enabled' => true],
        Itemtype::CLOTH_BOLT       => ['label' => 'cloth_bolt', 'enabled' => true],
        Itemtype::TREE_PC          => ['label' => 'treepc', 'enabled' => true],
        Itemtype::NOTEPAPER        => ['label' => 'notepaper', 'enabled' => true],
        Itemtype::TRIGGER          => ['label' => 'trigger', 'enabled' => true],
        Itemtype::TREASURECHEST    => ['label' => 'treasurechest', 'enabled' => true],
        Itemtype::LOCKPICK         => ['label' => 'lockpick', 'enabled' => true],
        Itemtype::MIRROR           => ['label' => 'mirror', 'enabled' => true],
        Itemtype::RAZOR            => ['label' => 'razor', 'enabled' => true],
        Itemtype::BRUSH            => ['label' => 'brush', 'enabled' => true],
        Itemtype::FIREOIL          => ['label' => 'fireoil', 'enabled' => true],
        Itemtype::INK              => ['label' => 'ink	', 'enabled' => true],
        Itemtype::QUILL            => ['label' => 'quill	', 'enabled' => true],
        Itemtype::JOURNAL          => ['label' => 'journal	', 'enabled' => true],
        Itemtype::WIND_PIPES       => ['label' => 'pipes', 'enabled' => true],
        Itemtype::WIND_FLUTE       => ['label' => 'flute', 'enabled' => true],
        Itemtype::STRINGED_BITTERN => ['label' => 'bittern', 'enabled' => true],
        Itemtype::PERCUSSION_DRUMS => ['label' => 'drums', 'enabled' => true],
        Itemtype::SHAPE_KNIFE      => ['label' => 'carver_knife', 'enabled' => true],
        Itemtype::SHAPE_SHAPER     => ['label' => 'wood_shaper', 'enabled' => true],
        Itemtype::SHAPE_STICK      => ['label' => 'wood_stick', 'enabled' => true],
        Itemtype::SHAPE_LIMB       => ['label' => 'wood_limb', 'enabled' => true],
        Itemtype::SHAPE_BRANCH     => ['label' => 'wood_branch', 'enabled' => true],
        Itemtype::SHAPE_BLOCK      => ['label' => 'wood_block', 'enabled' => true],
        Itemtype::SHAPE_KEYBLANK   => ['label' => 'keyblank', 'enabled' => true],
        Itemtype::SMITH_ORE        => ['label' => 'ore', 'enabled' => true],
        Itemtype::SMITH_HAMMER     => ['label' => 'hammer', 'enabled' => true],
        Itemtype::SMITH_ANVIL      => ['label' => 'anvil', 'enabled' => true],
        Itemtype::SMITH_BARREL     => ['label' => 'barrel', 'enabled' => true],
        Itemtype::SMITH_FURNACE    => ['label' => 'furnace', 'enabled' => true],
        Itemtype::SMITH_GRINDER    => ['label' => 'grinder', 'enabled' => true],
        Itemtype::COMM_DEV         => ['label' => 'commdev', 'enabled' => true],
        Itemtype::WINDOW           => ['label' => 'window', 'enabled' => true],
        Itemtype::KEYRING          => ['label' => 'keyring', 'enabled' => true],
        Itemtype::SHAPE_MUSHROOM   => ['label' => 'mushroom', 'enabled' => true],
        Itemtype::SHAPE_CLEMATIS   => ['label' => 'clematis', 'enabled' => true],
        Itemtype::SHAPE_HAWTHORN   => ['label' => 'hawthorn', 'enabled' => true],
        Itemtype::SHAPE_BONESET    => ['label' => 'boneset', 'enabled' => true],
        Itemtype::SHAPE_OBSIDIAN   => ['label' => 'obsidian', 'enabled' => true],
        Itemtype::SHAPE_SNAKESKIN  => ['label' => 'snakeskin', 'enabled' => true],
        Itemtype::TOME             => ['label' => 'tome', 'enabled' => true],
        Itemtype::CLOCK            => ['label' => 'clock', 'enabled' => true],
        Itemtype::LAMPPOST         => ['label' => 'lamppost', 'enabled' => true],
        Itemtype::ROPE             => ['label' => 'rope', 'enabled' => true],
        Itemtype::PITON            => ['label' => 'piton', 'enabled' => true],
        Itemtype::SHOP             => ['label' => 'shop', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
