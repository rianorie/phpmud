<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Link implements Table
{
    const ISDOOR = 0;
    const CLOSED = 2;
    const LOCKED = 3;
    const CLIMB = 4;
    const PICKPROOF = 5;
    const NOPASS = 6;
    const EASY = 7;
    const HARD = 8;
    const INFURIATING = 9;
    const NOCLOSE = 10;
    const NOLOCK = 11;
    const PAINWARD = 12;
    const NOENTERWARD = 13;
    const ALARMWARD = 14;

    public $list = [
        Link::ISDOOR      => ['label' => 'door', 'enabled' => true],
        Link::CLOSED      => ['label' => 'closed', 'enabled' => true],
        Link::LOCKED      => ['label' => 'locked', 'enabled' => true],
        Link::CLIMB       => ['label' => 'climb', 'enabled' => true],
        Link::PICKPROOF   => ['label' => 'pickproof', 'enabled' => true],
        Link::NOPASS      => ['label' => 'nopass', 'enabled' => true],
        Link::EASY        => ['label' => 'easy', 'enabled' => true],
        Link::HARD        => ['label' => 'hard', 'enabled' => true],
        Link::INFURIATING => ['label' => 'infuriating', 'enabled' => true],
        Link::NOCLOSE     => ['label' => 'noclose', 'enabled' => true],
        Link::NOLOCK      => ['label' => 'nolock', 'enabled' => true],
        Link::PAINWARD    => ['label' => 'painward', 'enabled' => true],
        Link::NOENTERWARD => ['label' => 'noenterward', 'enabled' => true],
        Link::ALARMWARD   => ['label' => 'alarmward', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
