<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Position implements Table
{
    const NORMAL = 1;

    const SIT = 2;

    const REST = 3;

    const SLEEP = 4;

    const STAND = 5;

    public $list = [
        Position::NORMAL    => ['label' => 'standing', 'enabled' => true],
        Position::SIT  => ['label' => 'sitting', 'enabled' => true],
        Position::REST  => ['label' => 'resting', 'enabled' => true],
        Position::SLEEP  => ['label' => 'sleeping', 'enabled' => true],
        Position::STAND  => ['label' => 'standing on', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) { return (object) $g; }, $this->list);
    }
}
