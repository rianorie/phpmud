<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Race implements Table
{
    public $list = [
        'Andor'        => ['label' => 'Andoran', 'enabled' => true, 'recall' => 3001],
        'Amador'       => ['label' => 'Amadician', 'enabled' => true, 'recall' => 28151],
        'Illian'       => ['label' => 'Illianer', 'enabled' => true, 'recall' => 27929],
        'Shienar'      => ['label' => 'Shienaran', 'enabled' => true, 'recall' => 29006],
        'Domani'       => ['label' => 'Domani', 'enabled' => true, 'recall' => 19024],
        'Aiel'         => ['label' => 'Aiel', 'enabled' => true, 'recall' => 8797],
        'Tairen'       => ['label' => 'Tairen', 'enabled' => true, 'recall' => 27850],
        'Cairhien'     => ['label' => 'Cairhienin', 'enabled' => true, 'recall' => 5306],
        'Tuathaan'     => ['label' => 'Tuatha\'an', 'enabled' => true, 'recall' => 28800],
        'Seanchan'     => ['label' => 'Seanchan', 'enabled' => true, 'recall' => 31700],
        'Altara'       => ['label' => 'Altaran', 'enabled' => true, 'recall' => 8036],
        'Arafel'       => ['label' => 'Arafellin', 'enabled' => true, 'recall' => 26700],
        'Tarabon'      => ['label' => 'Taraboner', 'enabled' => true, 'recall' => 20132],
        'Kandor'       => ['label' => 'Kandori', 'enabled' => true, 'recall' => 6800],
        'Mayene'       => ['label' => 'Mayener', 'enabled' => true, 'recall' => 28051],
        'Tworiver'     => ['label' => 'Two Rivers', 'enabled' => true, 'recall' => 27700],
        'Malkier'      => ['label' => 'Malkieri', 'enabled' => true, 'recall' => 29006],
        'Nym'          => ['label' => 'Nym', 'enabled' => false, 'recall' => 3001],
        'Ghealdan'     => ['label' => 'Ghealdan', 'enabled' => true, 'recall' => 27951],
        'Murandy'      => ['label' => 'Murandian', 'enabled' => true, 'recall' => 1511],
        'Saldaea'      => ['label' => 'Saldaean', 'enabled' => true, 'recall' => 26900],
        'Tarvalon'     => ['label' => 'Tar Valon', 'enabled' => true, 'recall' => 7255],
        'Seafolk'      => ['label' => 'Sea Folk', 'enabled' => true, 'recall' => 21000],
        'Falman'       => ['label' => 'Falman', 'enabled' => true, 'recall' => 21251],
        'Maddinger'    => ['label' => 'Maddinger', 'enabled' => true, 'recall' => 13943],
        'Borderlander' => ['label' => 'Borderlander', 'enabled' => true, 'recall' => 3001],
        'Midlander'    => ['label' => 'Midlander', 'enabled' => true, 'recall' => 3001],
        'Southlander'  => ['label' => 'Southlander', 'enabled' => true, 'recall' => 3001],
        'Westlander'   => ['label' => 'Westlander', 'enabled' => true, 'recall' => 3001],
        'Ogier'        => ['label' => 'Ogier', 'enabled' => true, 'recall' => 1924],
        'Seangier'     => ['label' => 'Ogier', 'enabled' => true, 'recall' => 31799],
        'Trolloc'      => ['label' => 'Trolloc', 'enabled' => true, 'recall' => 3001],
        'Draghkar'     => ['label' => 'Draghkar', 'enabled' => false, 'recall' => 3001],
        'Myrddraal'    => ['label' => 'Myrddraal', 'enabled' => false, 'recall' => 3001],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of races
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($r) { return (object) $r; }, $this->list);
    }
}

/*
{ "Andor", "Andoran", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 3001, FALSE },
{ "Amador", "Amadician", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 28151, FALSE },
{ "Illian", "Illianer", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 27929, FALSE },
{ "Shienar", "Shienaran", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 29006, FALSE },
{ "Domani", "Domani", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 19024, FALSE },
{ "Aiel", "Aiel", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 8797, TRUE },
{ "Tairen", "Tairen", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 27850, FALSE },
{ "Cairhien", "Cairhienin", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 5306, FALSE },
{ "Tuathaan", "Tuatha'an", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 28800, TRUE },
{ "Seanchan", "Seanchan", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 31700, TRUE },
{ "Altara", "Altaran", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 8036, FALSE },
{ "Arafel", "Arafellin", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 60, 50, 50, 50, 50 }, SIZE_MEDIUM, 26700, FALSE },
{ "Tarabon", "Taraboner", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 60, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 20132, FALSE },
{ "Kandor", "Kandori", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 55, 55, 55, 55, 50, 50, 50 }, SIZE_MEDIUM, 6800, FALSE },
{ "Mayene", "Mayener", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 65, 55, 55, 55, 50, 50, 50, 50 }, SIZE_MEDIUM, 28051, FALSE },
{ "Tworiver", "Two Rivers", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 55, 55, 55, 55, 55, 50, 50, 50 }, SIZE_MEDIUM, 27700, FALSE },
{ "Malkier", "Malkieri", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 60, 50, 55, 50, 60, 50, 50, 50 }, SIZE_MEDIUM, 29006, FALSE },
{ "Nym", "Nym", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 75, 50, 50, 50 }, SIZE_HUGE, 3001, TRUE },
{ "Ghealdan", "Ghealdan", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 27951, FALSE },
{ "Murandy", "Murandian", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 1511, FALSE },
{ "Saldaea", "Saldaean", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 26900, FALSE },
{ "Tarvalon", "Tar Valon", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 7255, FALSE },
{ "Seafolk", "Sea Folk", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 13, 10, 13, 10, 10 }, { 50, 50, 50, 60, 50, 60, 50, 50 }, SIZE_MEDIUM, 21000, TRUE },
{ "Falman", "Falman", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 21251, FALSE },
{ "Maddinger", "Maddinger", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 13943, FALSE },
{ "Borderlander", "Borderlander", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 3001, FALSE },
{ "Midlander", "Midlander", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 3001, FALSE },
{ "Southlander", "Southlander", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 3001, FALSE },
{ "Westlander", "Westlander", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_MEDIUM, 3001, FALSE },
{ "Ogier", "Ogier", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_LARGE, 1924, TRUE },
{ "Seangier", "Ogier", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 50, 50, 50, 50, 50 }, SIZE_LARGE, 31799, TRUE },
{ "Trolloc", "Trolloc", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 60, 40, 40, 40, 60, 50, 50, 50 }, SIZE_LARGE, 3001, TRUE },
{ "Draghkar", "Draghkar", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 75, 50, 50, 50, 50 }, SIZE_MEDIUM, 3001, TRUE },
{ "Myrddraal", "Myrddraal", 0, { 100, 100, 100, 100 }, { "" }, { 10, 10, 10, 10, 10, 10, 10, 10 }, { 50, 50, 50, 75, 50, 50, 50, 50 }, SIZE_MEDIUM, 3001, TRUE},

 */
