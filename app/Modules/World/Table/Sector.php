<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Sector implements Table
{
    const INSIDE = 0;
    const CITY = 1;
    const FIELD = 2;
    const FOREST = 3;
    const HILLS = 4;
    const MOUNTAIN = 5;
    const WATER_SWIM = 6;
    const WATER_NOSWIM = 7;
    const UNUSED = 8;
    const AIR = 9;
    const DESERT = 10;
    const TUNDRA = 11;
    const BLIGHT = 12;
    const COASTAL = 13;
    const ROAD = 14;
    const MARSH = 15;
    const WASTE = 16;
    const LAKE_FRESH = 17;
    const LAKE_SALT = 18;
    const RIVER_FRESH = 19;
    const RIVER_SALT = 20;
    const THEWAYS = 21;
    const OCEAN = 22;
    const UNDERGROUND = 23;
    const UNDERWATER = 24;
    const FOOTHILLS = 25;
    const JUNGLE = 26;
    const FARMLAND = 27;
    const STEPPE = 28;
    const PLAIN = 29;
    const MEADOW = 30;
    const HOLD = 31;

    public $list = [
        self::INSIDE       => ['sector' => self::INSIDE, 'label' => 'inside', 'enabled' => true],
        self::CITY         => ['sector' => self::CITY, 'label' => 'city', 'enabled' => true],
        self::FIELD        => ['sector' => self::FIELD, 'label' => 'field', 'enabled' => true],
        self::FOREST       => ['sector' => self::FOREST, 'label' => 'forest', 'enabled' => true],
        self::HILLS        => ['sector' => self::HILLS, 'label' => 'hills', 'enabled' => true],
        self::MOUNTAIN     => ['sector' => self::MOUNTAIN, 'label' => 'mountain', 'enabled' => true],
        self::WATER_SWIM   => ['sector' => self::WATER_SWIM, 'label' => 'swimable water', 'enabled' => true],
        self::WATER_NOSWIM => ['sector' => self::WATER_NOSWIM, 'label' => 'unswimable water', 'enabled' => true],
        self::UNUSED       => ['sector' => self::UNUSED, 'label' => 'UNUSED', 'enabled' => true],
        self::AIR          => ['sector' => self::AIR, 'label' => 'air', 'enabled' => true],
        self::DESERT       => ['sector' => self::DESERT, 'label' => 'desert', 'enabled' => true],
        self::TUNDRA       => ['sector' => self::TUNDRA, 'label' => 'tundra', 'enabled' => true],
        self::BLIGHT       => ['sector' => self::BLIGHT, 'label' => 'blight', 'enabled' => true],
        self::COASTAL      => ['sector' => self::COASTAL, 'label' => 'coastal', 'enabled' => true],
        self::ROAD         => ['sector' => self::ROAD, 'label' => 'road', 'enabled' => true],
        self::MARSH        => ['sector' => self::MARSH, 'label' => 'marsh', 'enabled' => true],
        self::WASTE        => ['sector' => self::WASTE, 'label' => 'waste', 'enabled' => true],
        self::LAKE_FRESH   => ['sector' => self::LAKE_FRESH, 'label' => 'freshwater lake', 'enabled' => true],
        self::LAKE_SALT    => ['sector' => self::LAKE_SALT, 'label' => 'saltwater lake', 'enabled' => true],
        self::RIVER_FRESH  => ['sector' => self::RIVER_FRESH, 'label' => 'freshwater river', 'enabled' => true],
        self::RIVER_SALT   => ['sector' => self::RIVER_SALT, 'label' => 'saltwater river', 'enabled' => true],
        self::THEWAYS      => ['sector' => self::THEWAYS, 'label' => 'the ways', 'enabled' => true],
        self::OCEAN        => ['sector' => self::OCEAN, 'label' => 'ocean', 'enabled' => true],
        self::UNDERGROUND  => ['sector' => self::UNDERGROUND, 'label' => 'underground', 'enabled' => true],
        self::UNDERWATER   => ['sector' => self::UNDERWATER, 'label' => 'underwater', 'enabled' => true],
        self::FOOTHILLS    => ['sector' => self::FOOTHILLS, 'label' => 'foothills', 'enabled' => true],
        self::JUNGLE       => ['sector' => self::JUNGLE, 'label' => 'jungle', 'enabled' => true],
        self::FARMLAND     => ['sector' => self::FARMLAND, 'label' => 'farmland', 'enabled' => true],
        self::STEPPE       => ['sector' => self::STEPPE, 'label' => 'steppe', 'enabled' => true],
        self::PLAIN        => ['sector' => self::PLAIN, 'label' => 'plain', 'enabled' => true],
        self::MEADOW       => ['sector' => self::MEADOW, 'label' => 'meadow', 'enabled' => true],
        self::HOLD         => ['sector' => self::HOLD, 'label' => 'hold', 'enabled' => true]
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
