<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Weapontype implements Table
{
    const EXOTIC = 0;
    const SWORD = 1;
    const DAGGER = 2;
    const SPEAR = 3;
    const MACE = 4;
    const AXE = 5;
    const FLAIL = 6;
    const WHIP = 7;
    const POLEARM = 8;
    const SHORTBOW = 9;
    const STAFF = 10;
    const LONGBOW = 11;
    const RECURVEDBOW = 12;
    const COMPOSITEBOW = 13;
    const CROSSBOW = 14;
    const NET = 15;
    const SCYTHESWORD = 16;
    const TRIDENT = 17;
    const HAMMER = 18;
    const SLING = 19;
    const SWORDBREAKER = 20;
    const SAP = 21;
    const SHORTSWORD = 22;
    const LANCE = 23;
    const ASHANDAREI = 24;
    const LIGHTCROSSBOW = 25;

    public $list = [
        Weapontype::EXOTIC        => ['label' => 'exotic', 'enabled' => true],
        Weapontype::SWORD         => ['label' => 'sword', 'enabled' => true],
        Weapontype::DAGGER        => ['label' => 'dagger', 'enabled' => true],
        Weapontype::SPEAR         => ['label' => 'spear', 'enabled' => true],
        Weapontype::MACE          => ['label' => 'mace', 'enabled' => true],
        Weapontype::AXE           => ['label' => 'axe', 'enabled' => true],
        Weapontype::FLAIL         => ['label' => 'flail', 'enabled' => true],
        Weapontype::WHIP          => ['label' => 'whip', 'enabled' => true],
        Weapontype::POLEARM       => ['label' => 'polearm', 'enabled' => true],
        Weapontype::SHORTBOW      => ['label' => 'shortbow', 'enabled' => true],
        Weapontype::STAFF         => ['label' => 'staff', 'enabled' => true],
        Weapontype::LONGBOW       => ['label' => 'longbow', 'enabled' => true],
        Weapontype::RECURVEDBOW   => ['label' => 'recurvedbow', 'enabled' => true],
        Weapontype::COMPOSITEBOW  => ['label' => 'compositebow', 'enabled' => true],
        Weapontype::CROSSBOW      => ['label' => 'crossbow', 'enabled' => true],
        Weapontype::NET           => ['label' => 'net', 'enabled' => true],
        Weapontype::SCYTHESWORD   => ['label' => 'scythesword', 'enabled' => true],
        Weapontype::TRIDENT       => ['label' => 'trident', 'enabled' => true],
        Weapontype::HAMMER        => ['label' => 'hammer', 'enabled' => true],
        Weapontype::SLING         => ['label' => 'sling', 'enabled' => true],
        Weapontype::SWORDBREAKER  => ['label' => 'swordbreaker', 'enabled' => true],
        Weapontype::SAP           => ['label' => 'sap', 'enabled' => true],
        Weapontype::SHORTSWORD    => ['label' => 'shortsword', 'enabled' => true],
        Weapontype::LANCE         => ['label' => 'lance', 'enabled' => true],
        Weapontype::ASHANDAREI    => ['label' => 'ashandarei', 'enabled' => true],
        Weapontype::LIGHTCROSSBOW => ['label' => 'lightcrossbow', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
