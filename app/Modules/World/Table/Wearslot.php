<?php

namespace Modules\World\Table;

use Game\Module\Entity\Interfaces\Table;

class Wearslot implements Table
{
    const TAKE = 1;
    const WEAR_FINGER = 2;
    const WEAR_NECK = 3;
    const WEAR_BODY = 4;
    const WEAR_HEAD = 5;
    const WEAR_LEGS = 6;
    const WEAR_FEET = 7;
    const WEAR_HANDS = 8;
    const WEAR_ARMS = 9;
    const WEAR_SHIELD = 10;
    const WEAR_ABOUT = 11;
    const WEAR_WAIST = 12;
    const WEAR_WRIST = 13;
    const WIELD = 14;
    const HOLD = 15;
    const WEAR_FLOAT = 16;
    const WEAR_NOSE = 17;
    const WEAR_SHOULDERS = 18;
    const WEAR_BACK = 19;
    const WEAR_EYES = 20;
    const WEAR_CALF = 21;
    const WEAR_THIGH = 22;
    const WEAR_FOREARM = 23;
    const WEAR_EAR = 24;
    const WEAR_ANKLE = 25;
    const WEAR_NEAR_MIND = 26;
    const WEAR_FACE = 27;
    const WEAR_ANGREAL = 28;
    const WEAR_LIGHT = 29;


    public $list = [
        Wearslot::WEAR_FINGER    => ['label' => 'finger', 'enabled' => true],
        Wearslot::WEAR_NECK      => ['label' => 'neck', 'enabled' => true],
        Wearslot::WEAR_BODY      => ['label' => 'body', 'enabled' => true],
        Wearslot::WEAR_HEAD      => ['label' => 'head', 'enabled' => true],
        Wearslot::WEAR_LEGS      => ['label' => 'legs', 'enabled' => true],
        Wearslot::WEAR_FEET      => ['label' => 'feet', 'enabled' => true],
        Wearslot::WEAR_HANDS     => ['label' => 'hands', 'enabled' => true],
        Wearslot::WEAR_ARMS      => ['label' => 'arms', 'enabled' => true],
        Wearslot::WEAR_SHIELD    => ['label' => 'shield', 'enabled' => true],
        Wearslot::WEAR_ABOUT     => ['label' => 'about', 'enabled' => true],
        Wearslot::WEAR_WAIST     => ['label' => 'waist', 'enabled' => true],
        Wearslot::WEAR_WRIST     => ['label' => 'wrist', 'enabled' => true],
        Wearslot::WIELD          => ['label' => 'wield', 'enabled' => true],
        Wearslot::HOLD           => ['label' => 'hold', 'enabled' => true],
        Wearslot::WEAR_FLOAT     => ['label' => 'float', 'enabled' => true],
        Wearslot::WEAR_NOSE      => ['label' => 'nose', 'enabled' => true],
        Wearslot::WEAR_SHOULDERS => ['label' => 'shoulders', 'enabled' => true],
        Wearslot::WEAR_BACK      => ['label' => 'back', 'enabled' => true],
        Wearslot::WEAR_EYES      => ['label' => 'eyes', 'enabled' => true],
        Wearslot::WEAR_CALF      => ['label' => 'calf', 'enabled' => true],
        Wearslot::WEAR_THIGH     => ['label' => 'thigh', 'enabled' => true],
        Wearslot::WEAR_FOREARM   => ['label' => 'forearm', 'enabled' => true],
        Wearslot::WEAR_EAR       => ['label' => 'ear', 'enabled' => true],
        Wearslot::WEAR_ANKLE     => ['label' => 'ankle', 'enabled' => true],
        Wearslot::WEAR_NEAR_MIND => ['label' => 'mind', 'enabled' => true],
        Wearslot::WEAR_FACE      => ['label' => 'face', 'enabled' => true],
        Wearslot::WEAR_ANGREAL   => ['label' => 'angreal', 'enabled' => true],
        Wearslot::WEAR_LIGHT     => ['label' => 'light', 'enabled' => true],
    ];

    /**
     * Return the value for the given definition
     *
     * @param string $definition
     *
     * @return object
     */
    public function get($definition)
    {
        if ( ! isset($this->list[$definition])) {
            throw new \UnexpectedValueException('The given key does not have a value.');
        }

        return (object) $this->list[$definition];
    }

    /**
     * Return the full list of genders
     *
     * @return array
     */
    public function all()
    {
        return array_map(function($g) {
            return (object) $g;
        }, $this->list);
    }
}
