<?php

use Phinx\Migration\AbstractMigration;

class World010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		// create the table
		$table = $this->table('areas');
		$table->addColumn('name', 'string')
		      ->addColumn('created_on', 'datetime')
		      ->addIndex('name', ['unique' => true])
		      ->create();

		$table = $this->table('rooms');
		$table->addColumn('area_id', 'integer')
		      ->addColumn('title', 'string')
		      ->addColumn('description', 'text')
		      ->addColumn('created_on', 'datetime')
		      ->create();

		$table = $this->table('exits');
		$table->addColumn('room_id', 'integer')
		      ->addColumn('direction', 'integer')
		      ->addIndex('room_id')
		      ->addForeignKey('room_id', $this->table('rooms'), 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
		      ->create();

	}
}
