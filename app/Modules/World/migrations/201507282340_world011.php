<?php

use Phinx\Migration\AbstractMigration;

class World011 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('exits');
		$table->addColumn('target_id', 'integer')
		      ->addForeignKey('target_id', $this->table('rooms'), 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
		      ->save();

	}
}
