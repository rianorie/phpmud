<?php

use Phinx\Migration\AbstractMigration;

class World013 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('locations');
		$table->addColumn('character_id', 'integer')
			  ->addColumn('room_id', 'integer')
		      ->addIndex('character_id', ['unique' => true])
		      ->addIndex('room_id')
		      ->addForeignKey('room_id', $this->table('rooms'), 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
			  ->addForeignKey('character_id', $this->table('characters'), 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
		      ->create();
	}
}
