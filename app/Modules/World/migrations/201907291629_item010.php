<?php

use Phinx\Migration\AbstractMigration;

class Item010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('items');
		$table
            ->addColumn('area_id', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('keywords', 'string')
            ->addColumn('short_desc', 'string')
            ->addColumn('long_desc', 'string')
            ->addColumn('type', 'integer')
            ->addColumn('extra_desc', 'text', ['null' => true, 'default' => null])
            ->addColumn('weight', 'integer', ['default' => 1])
            ->addColumn('container', 'boolean', ['default' => false])
            ->addColumn('wear_slot', 'integer', ['null' => true, 'default' => null])
            ->addTimestamps()
            ->addForeignKey(['area_id'], 'areas', 'id', ['delete'=> 'CASCADE'])
            ->save();

		$table = $this->table('item_resets');
		$table
            ->addColumn('area_id', 'integer')
            ->addColumn('room_id', 'integer')
            ->addColumn('item_id', 'integer')
            ->addColumn('parent_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('count', 'integer', ['default' => 1])
            ->addColumn('chance', 'integer', ['default' => 100])
            ->addTimestamps()
            ->addForeignKey(['parent_id'], 'item_resets', 'id', ['delete'=> 'CASCADE'])
            ->addForeignKey(['area_id'], 'areas', 'id', ['delete'=> 'CASCADE'])
            ->addForeignKey(['room_id'], 'rooms', 'id', ['delete'=> 'CASCADE'])
            ->addForeignKey(['item_id'], 'items', 'id', ['delete'=> 'CASCADE'])
            ->save();

		$table = $this->table('room_items');
		$table
            ->addColumn('area_id', 'integer')
            ->addColumn('room_id', 'integer')
            ->addColumn('item_id', 'integer')
            ->addColumn('reset_id', 'integer')
            ->addColumn('parent_id', 'integer', ['null' => true, 'default' => null])
            ->addTimestamps()
            ->addForeignKey(['area_id'], 'areas', 'id', ['delete'=> 'CASCADE'])
            ->addForeignKey(['room_id'], 'rooms', 'id', ['delete'=> 'CASCADE'])
            ->addForeignKey(['item_id'], 'items', 'id', ['delete'=> 'CASCADE'])
            ->addForeignKey(['reset_id'], 'item_resets', 'id')
            ->save();

		$table = $this->table('character_items');
		$table
            ->addColumn('character_id', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('keywords', 'string')
            ->addColumn('short_desc', 'string')
            ->addColumn('long_desc', 'string')
            ->addColumn('type', 'integer')
            ->addColumn('extra_desc', 'text', ['null' => true, 'default' => null])
            ->addColumn('weight', 'integer', ['default' => 1])
            ->addColumn('container', 'boolean', ['default' => false])
            ->addColumn('parent_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('wear_slot', 'integer', ['null' => true, 'default' => null])
            ->addColumn('inventory', 'integer', ['null' => true, 'default' => 1])
            ->addTimestamps()
            ->save();
	}
}
