<?php

use Phinx\Migration\AbstractMigration;

class World014 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('areas');
		$table
            ->addColumn('last_reload', 'timestamp')
            ->removeColumn('created_on')
            ->addTimestamps()
            ->save();
	}
}
