<?php

use Phinx\Migration\AbstractMigration;

class Item020 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('room_items');
		$table
            ->addColumn('name', 'string')
            ->addColumn('keywords', 'string')
            ->addColumn('short_desc', 'string')
            ->addColumn('long_desc', 'string')
            ->addColumn('type', 'integer')
            ->addColumn('extra_desc', 'text', ['null' => true, 'default' => null])
            ->addColumn('weight', 'integer', ['default' => 1])
            ->addColumn('container', 'boolean', ['default' => false])
            ->addColumn('wear_slot', 'integer', ['null' => true, 'default' => null])
            ->save();
	}
}
