<?php

use Phinx\Migration\AbstractMigration;

class Item021 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('room_items');
		$table
            ->changeColumn('item_id', 'integer', ['null' => true])
            ->changeColumn('reset_id', 'integer', ['null' => true])
            ->save();
	}
}
