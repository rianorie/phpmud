<?php

use Phinx\Migration\AbstractMigration;

class Character020 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('characters');
		$table
            ->removeColumn('created_on')
            ->save();

		$table
            ->addColumn('email', 'string', ['null' => true])
            ->addColumn('gender', 'integer', ['length' => 1, 'null' => false, 'default' => 1])
            ->addColumn('race', 'string', ['null' => false])
            ->addColumn('channeler', 'boolean', ['null' => false, 'default' => 0])
            ->addTimestamps()
            ->save();
	}
}
