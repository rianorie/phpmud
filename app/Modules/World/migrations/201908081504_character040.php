<?php

use Phinx\Migration\AbstractMigration;

class Character040 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('characters');

		$table
            ->addColumn('hitpoints', 'integer', ['default' => 1])
            ->addColumn('max_hitpoints', 'integer', ['default' => 1])
            ->addColumn('stamina', 'integer', ['default' => 1])
            ->addColumn('max_stamina', 'integer', ['default' => 1])
            ->addColumn('strength', 'integer', ['default' => 1])
            ->addColumn('dexterity', 'integer', ['default' => 1])
            ->addColumn('constitution', 'integer', ['default' => 1])
            ->addColumn('reflex', 'integer', ['default' => 1])
            ->addColumn('intelligence', 'integer', ['default' => 1])
            ->addColumn('wisdom', 'integer', ['default' => 1])
            ->addColumn('instinct', 'integer', ['default' => 1])
            ->addColumn('charisma', 'integer', ['default' => 1])
            ->save();
	}
}
