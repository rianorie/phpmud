<?php

use Phinx\Migration\AbstractMigration;

class Character041 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('characters');

		$table
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('roomdesc', 'text', ['null' => true])
            ->addColumn('walkin', 'text', ['null' => true])
            ->addColumn('walkout', 'text', ['null' => true])
            ->save();
	}
}
