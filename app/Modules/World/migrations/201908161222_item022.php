<?php

use Phinx\Migration\AbstractMigration;

class Item022 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('character_items');
		$table
            ->addColumn('character_slot', 'integer', ['default' => 0])
            ->save();
	}
}
