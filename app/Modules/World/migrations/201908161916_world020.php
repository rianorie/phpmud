<?php

use Phinx\Migration\AbstractMigration;

class World020 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('rooms');
		$table
            ->removeColumn('created_on')
            ->addTimestamps()
            ->save();

		$table = $this->table('area_sectors');
		$table
            ->addColumn('name', 'string')
            ->addColumn('messages', 'text', ['null' => true])
            ->addTimestamps()
            ->addIndex('name', ['unique' => true])
            ->save();

        $table = $this->table('rooms');
        $table
            ->addColumn('flags', 'string', ['null' => true])
            ->addColumn('sector_id', 'integer', ['default' => 0])
            ->addColumn('original_vnum', 'integer', ['null' => true])
            ->addColumn('heal_hitpoints', 'integer', ['default' => 100])
            ->addColumn('heal_stamina', 'integer', ['default' => 100])
            ->addColumn('extra_descs', 'text', ['null' => true])
            ->addColumn('grade', 'integer', ['default' => 0])
            ->save();
	}
}
