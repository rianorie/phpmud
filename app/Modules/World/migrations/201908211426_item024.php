<?php

use Phinx\Migration\AbstractMigration;

class Item024 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
        $table = $this->table('items');
        $table
            ->addColumn('condition', 'integer', ['default' => 100])
            ->save();

        $table = $this->table('room_items');
        $table
            ->addColumn('current_condition', 'integer', ['default' => 100])
            ->addColumn('condition', 'integer', ['default' => 100])
            ->save();

		$table = $this->table('character_items');
		$table
            ->addColumn('current_condition', 'integer', ['default' => 100])
            ->addColumn('condition', 'integer', ['default' => 100])
            ->save();
	}
}
