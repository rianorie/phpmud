<?php

use Phinx\Migration\AbstractMigration;

class Item025 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
        $table = $this->table('items');
        $table
            ->addColumn('original_vnum', 'integer', ['null' => true])
            ->save();
	}
}
