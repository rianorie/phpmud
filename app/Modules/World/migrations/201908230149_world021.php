<?php

use Phinx\Migration\AbstractMigration;

class World021 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
        $table = $this->table('links');
        $table
            ->addColumn('keyword', 'string', ['null' => true])
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('type', 'integer', ['default' => 0])
            ->addColumn('key_id', 'integer', ['null' => true])
            ->save();
	}
}
