<?php

use Phinx\Migration\AbstractMigration;

class Mobile010 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
        $table = $this->table('mobiles');
        $table
            ->addColumn('area_id', 'integer')
            ->addColumn('original_vnum', 'integer', ['null' => true])
            ->addColumn('name', 'string')
            ->addColumn('die_message', 'text', ['null' => true])
            ->addColumn('kill_message', 'text', ['null' => true])
            ->addColumn('short_description', 'string')
            ->addColumn('long_description', 'string')
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('race', 'string')
            ->addColumn('flag_action', 'text', ['null' => true])
            ->addColumn('flag_affected', 'text', ['null' => true])
            ->addColumn('group', 'integer', ['null' => true])
            ->addColumn('level', 'integer', ['default' => 1])
            ->addColumn('hitroll', 'integer', ['default' => 1])

            ->addColumn('hit_dice', 'integer', ['default' => 1])
            ->addColumn('hit_dice_type', 'integer', ['default' => 1])
            ->addColumn('hit_dice_bonus', 'integer', ['default' => 0])

            ->addColumn('stamina_dice', 'integer', ['default' => 1])
            ->addColumn('stamina_dice_type', 'integer', ['default' => 1])
            ->addColumn('stamina_dice_bonus', 'integer', ['default' => 0])

            ->addColumn('damage_dice', 'integer', ['default' => 1])
            ->addColumn('damage_dice_type', 'integer', ['default' => 1])
            ->addColumn('damage_dice_bonus', 'integer', ['default' => 0])

            ->addColumn('damage_type', 'string', ['null' => false])

            ->addColumn('ac_pierce', 'integer', ['default' => 0])
            ->addColumn('ac_bash', 'integer', ['default' => 0])
            ->addColumn('ac_slash', 'integer', ['default' => 0])
            ->addColumn('ac_exotic', 'integer', ['default' => 0])

            ->addColumn('flag_offensive', 'text', ['null' => true])
            ->addColumn('flag_immune', 'text', ['null' => true])
            ->addColumn('flag_resistance', 'text', ['null' => true])
            ->addColumn('flag_vulnerable', 'text', ['null' => true])

            ->addColumn('position_start', 'string', ['null' => false])
            ->addColumn('position_default', 'string', ['null' => false])

            ->addColumn('sex', 'string', ['null' => false])
            ->addColumn('wealth', 'integer', ['default' => 1])

            ->addColumn('flag_form', 'text', ['null' => true])
            ->addColumn('flag_parts', 'text', ['null' => true])

            ->addColumn('size', 'string', ['default' => 'medium'])
            ->addColumn('material', 'string', ['null' => true])

            ->addTimestamps()
            ->save();

        $table = $this->table('mobile_resets');
        $table
            ->addColumn('area_id', 'integer')
            ->addColumn('room_id', 'integer')
            ->addColumn('mobile_id', 'integer')
            ->addColumn('min', 'integer', ['default' => 1])
            ->addColumn('max', 'integer', ['default' => 1])
            ->addColumn('chance', 'integer', ['default' => 50])
            ->addForeignKey('area_id', 'areas', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->addForeignKey('room_id', 'rooms', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->addForeignKey('mobile_id', 'mobiles', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->addTimestamps()
            ->save();

        $table = $this->table('item_resets');
        $table
            ->addColumn('mobile_reset_id', 'integer', ['null' => true])
            ->addColumn('mobile_wear_loc', 'integer', ['null' => true])
            ->addForeignKey('mobile_reset_id', 'mobile_resets', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->save();
	}
}
