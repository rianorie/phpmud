<?php

use Phinx\Migration\AbstractMigration;

class Item030 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
		$table = $this->table('room_mobile_items');
		$table
            ->addColumn('item_id', 'integer')
            ->addColumn('reset_id', 'integer')
            ->addColumn('room_mobile_id', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('keywords', 'string')
            ->addColumn('short_desc', 'string')
            ->addColumn('long_desc', 'string')
            ->addColumn('type', 'integer')
            ->addColumn('extra_desc', 'text', ['null' => true, 'default' => null])
            ->addColumn('weight', 'integer', ['default' => 1])
            ->addColumn('container', 'boolean', ['default' => false])
            ->addColumn('parent_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('wear_slot', 'integer', ['null' => true, 'default' => null])
            ->addColumn('inventory', 'integer', ['null' => true, 'default' => 1])
            ->addColumn('character_slot', 'integer', ['default' => 0])
            ->addColumn('flags', 'string', ['null' => true])
            ->addColumn('metadata', 'text', ['null' => true])
            ->addColumn('v0', 'string', ['null' => true])
            ->addColumn('v1', 'string', ['null' => true])
            ->addColumn('v2', 'string', ['null' => true])
            ->addColumn('v3', 'string', ['null' => true])
            ->addColumn('v4', 'string', ['null' => true])
            ->addColumn('current_condition', 'integer', ['default' => 100])
            ->addColumn('condition', 'integer', ['default' => 100])
            ->addTimestamps()
            ->addForeignKey('room_mobile_id', 'room_mobiles', 'id', ['delete' => 'cascade', 'update' => 'no_action'])
            ->save();
	}
}
