<?php

use Phinx\Migration\AbstractMigration;

class World023 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
	public function change()
	{
        $table = $this->table('area_sectors');
        $table
            ->removeColumn('messages')
            ->save();

        $table = $this->table('area_sector_messages');
        $table
            ->addColumn('sector_id', 'integer', ['null' => false])
            ->addColumn('hour_from', 'integer', ['default' => 0])
            ->addColumn('hour_to', 'integer', ['default' => 24])
            ->addColumn('message', 'string', ['null' => false])
            ->addTimestamps()
            ->save();
	}
}
