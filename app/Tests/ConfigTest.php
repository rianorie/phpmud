<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Tests;

/**
 * Test the configuration library
 *
 * @package Tests
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-30
 */
class ConfigTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var string
	 */
	private $file;

	/**
	 * Set up the test case, creating a configuration file that we'll use later on
	 */
	public function setUp()
	{
		$this->file = sys_get_temp_dir().DIRECTORY_SEPARATOR.uniqid();
		$config = ['foo' => ['bar' => 'flub']];
		file_put_contents($this->file, json_encode($config));
	}

	/**
	 * Test the get functionality of the config library
	 */
	public function testGet()
	{
		$config = new \Game\Config($this->file);

		$this->assertEquals(null, $config->get('something.non.existing'));
		$this->assertEquals('flub', $config->get('foo.bar'));
	}

	/**
	 * Remove the temporary file we created in setUp()
	 */
	public function tearDown()
	{
		unlink($this->file);
	}
}
