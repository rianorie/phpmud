<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Tests\Connection;

/**
 * Unittest for the Connection class
 *
 * This unit test is not complete, but testing sockets is a pain. socket_close() for example closes the socket but then
 * leaves the resource intact, so there's no solid way to assert anything from that. There's no way to check the blocking
 * status on a socket either.
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-29
 * @version 1.0
 */
class ConnectionTest extends \PHPUnit_Framework_TestCase
{
	private $manager;

	public function setUp()
	{
		$this->manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
	}

	/**
	 * @expectedException \Game\Connection\Exception
	 */
	public function testConstructWithBadSocket()
	{
		new \Game\Connection\Connection(false, $this->manager);
	}

	public function testConstructWithCorrectSocket()
	{
		$connection = new \Game\Connection\Connection(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET), $this->manager);
		$this->assertInstanceOf('\Game\Connection\Connection', $connection);
	}

	public function testGetManager()
	{
		$connection = new \Game\Connection\Connection(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET), $this->manager);
		$this->assertInstanceOf('\Game\Connection\Manager', $connection->getManager());
	}

	public function testSetGetId()
	{
		$connection = new \Game\Connection\Connection(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET), $this->manager);
		$connection->setId(555);

		$this->assertEquals(555, $connection->getId());
	}

	public function testGetSocket()
	{
		$socket = socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET);
		$connection = new \Game\Connection\Connection($socket, $this->manager);

		$this->assertEquals($socket, $connection->getSocket());
	}

	public function testRead()
	{
		$sockets = [];
		socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $sockets);

		$connection = new \Game\Connection\Connection($sockets[1], $this->manager);

		$message = uniqid()."\n";

		socket_write($sockets[0], $message, strlen($message));

		$this->assertEquals($message, $connection->read());

		socket_close($sockets[0]);
		socket_close($sockets[1]);
	}

	public function testWrite()
	{
		$sockets = [];
		socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $sockets);

		$connection = new \Game\Connection\Connection($sockets[1], $this->manager);

		$message = uniqid()."\n";

		$connection->write($message);

		$this->assertEquals($message, socket_read($sockets[0], strlen($message)));

		socket_close($sockets[0]);
		socket_close($sockets[1]);
	}
}