<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Tests\Connection;

/**
 * Unit tests for the Connection Manager
 *
 * @package PHPMUD
 * @author  Rian Orie <rian.orie@gmail.com>
 * @created 2015-07-29
 * @version 1.0
 */
class ManagerTest extends \PHPUnit_Framework_TestCase
{
	public function testConstruct()
	{
		$manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$this->assertInstanceOf('\Game\Connection\Manager', $manager, 'Testing if the created manager is an instance of the right class.');
	}

	public function testCreate()
	{
		$manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));

		$this->assertInstanceOf('\Game\Connection\Connection', $connection, 'Testing if the created connection is an instance of the right class.');
	}

	public function testAddSuccess()
	{
		$manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));

		$manager->add($connection);
		foreach($manager->getAll() as $set) {
			$this->assertTrue(($set == $connection), 'Testing that the created connection and the one set in the manager are identical.');
		}
	}

	/**
	 * @expectedException \Game\Connection\Exception
	 */
	public function testAddFail()
	{
		$manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));

		$manager->add($connection);
		$manager->add($connection);
	}

	public function testRemoveSuccess()
	{
		$manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));

		$manager->add($connection);
		$this->assertEquals(1, count($manager->getAll()), 'Verify the manager has one connection registered.');

		$manager->remove($connection);
		$this->assertEquals(0, count($manager->getAll()), 'Verify there\'s no connections set in the manager after removal.');
	}

	/**
	 * @expectedException \Game\Connection\Exception
	 */
	public function testRemoveFail()
	{
		$manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));

		$manager->remove($connection);
	}

	public function testGetAllExcept()
	{
		$manager = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection1 = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));
		$connection2 = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));
		$connection3 = $manager->create(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET));

		$manager->add($connection1);
		$manager->add($connection2);
		$manager->add($connection3);

		foreach($manager->getAllExcept($connection1) as $set) {
			$this->assertTrue(($set != $connection1), 'Test to make sure the returned connection is not the one we don\'t want.');
		}
	}
}
