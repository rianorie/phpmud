<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Tests;

/**
 * Test the events library
 *
 * @package Tests
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-30
 */
class EventTest extends \PHPUnit_Framework_TestCase
{
	public function testObserverRegistration()
	{
		$event = new \Game\Event();
		$id = $event->observe('test.dispatch', function() {});

		$this->assertTrue(( ! is_null($id)), 'returned id should not be null');
	}

	public function testDispatch()
	{
		$x = 1;

		$event = new \Game\Event();
		$event->observe('test.dispatch', function(&$a) { $a++; });
		$event->dispatch('test.dispatch', ['a' => &$x]);
		$this->assertEquals(2, $x, 'Testing the result of the event observer that should have increment the value by one.');

		$event->observe('test.dispatch', function(&$a) { $a = $a*3; }, 0);
		$event->dispatch('test.dispatch', ['a' => &$x]);
		$this->assertEquals(7, $x, 'Testing to make sure the execution order is honored properly. 2*3 = 6+1 = 7. Otherwise it would be 2+1 = 3*3 = 9');

		$x = 1;
		$event->observe('test.dispatch', function() { return \Game\Event::STOP; }, 10);
		$event->dispatch('test.dispatch', ['a' => &$x]);
		$this->assertEquals(3, $x, 'Testing to make sure the execution is stopped when the stop state is returned.');
	}

	public function testObserverRemoval()
	{
		$x = 1;

		$event = new \Game\Event();
		$id = $event->observe('test.dispatch', function(&$a) { $a++; });
		$event->removeObserver('test.dispatch', $id);
		$event->dispatch('test.dispatch', ['a' => &$x]);

		$this->assertEquals(1, $x, 'Nothing should have happened since the observer was removed.');
	}
}
