<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Tests;

/**
 * Test the input handler
 *
 * @package Tests
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-07-30
 */
class InputTest extends \PHPUnit_Framework_TestCase
{
	public function testRegisterCommand() {
		$input = new \Game\Input(new \Game\Event());
		$input->registerCommand('test', function () {});
		$this->assertTrue(in_array('test', $input->getCommands()));
	}

	/**
	 * @expectedException \Game\Exception
	 */
	public function testRegisterBadCommand() {
		$input = new \Game\Input(new \Game\Event());
		$input->registerCommand('TEST', function () {});
	}

	/**
	 * @expectedException \Game\Exception
	 */
	public function testRegisterBadCommand2() {
		$input = new \Game\Input(new \Game\Event());
		$input->registerCommand('te st', function () {});
	}

	/**
	 * @expectedException \Game\Exception
	 */
	public function testRegisterBadCommand3() {
		$input = new \Game\Input(new \Game\Event());
		$input->registerCommand('te_st', function () {});
	}

	public function testHandle()
	{
		$input = new \Game\Input(new \Game\Event());
		$input->registerCommand('test', function ($connection, $input, $command) {
			$connection->setData('testhandle', true);
		});

		$manager    = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection = new \Game\Connection\Connection(socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET), $manager);

		$connection->setData('testhandle', false);

		// test with full command
		$input->handle($connection, 'test');
		$this->assertTrue($connection->getData('testhandle'));

		$connection->setData('testhandle', false);

		// test with a shortcut
		$input->handle($connection, 't');
		$this->assertTrue($connection->getData('testhandle'));

		$connection->setData('testhandle', false);

		// test with extra words
		$input->handle($connection, 'test bla foo bar');
		$this->assertTrue($connection->getData('testhandle'));

		$connection->setData('testhandle', false);

		// test with extra words and the shortcut
		$input->handle($connection, 't bla foo bar');
		$this->assertTrue($connection->getData('testhandle'));

		$connection->setData('testhandle', false);

		$input->handle($connection, 'fail');
		$this->assertTrue(($connection->getData('testhandle') == false));
	}

	public function testGetCommands()
	{
		$input = new \Game\Input(new \Game\Event());
		$input->registerCommand('foo', function () {});
		$input->registerCommand('bar', function () {});
		$input->registerCommand('blob', function () {});

		$this->assertEquals(['bar', 'blob', 'foo'], $input->getCommands());
	}
}
