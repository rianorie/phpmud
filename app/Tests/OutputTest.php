<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Tests;

class OutputTest extends \PHPUnit_Framework_TestCase
{
	public function testWrite()
	{
		$output = new \Game\Output(new \Game\Event());

		$sockets = [];
		socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $sockets);

		$manager    = new \Game\Connection\Manager(new \Game\Config('/dev/null'), new \Game\Event());
		$connection = new \Game\Connection\Connection($sockets[1], $manager);

		$message = 'Test test test';

		$output->write($connection, [$message]);
		$this->assertEquals($message."\n\n", socket_read($sockets[0], strlen($message)+2));

		$output->write($connection, [$message], false);
		$this->assertEquals($message."\n", socket_read($sockets[0], strlen($message)+1));
	}
}
