<?php

require_once(__DIR__.'/../vendor/autoload.php');

spl_autoload_register(function ($class) {
	$path = '/'.str_replace('\\', '/', $class).'.php';
	require_once(__DIR__.$path);
});
