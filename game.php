#!/usr/bin/php
<?php

error_reporting(E_ALL);
ini_set('error_log', './var/log/error.log');

require_once(__DIR__.'/app/autoload.php');

// needed for signal handling
declare(ticks = 1);

if ( ! isset($_SERVER['argv']) || count($_SERVER['argv']) < 2) {
	die('syntax: php game.php /path/to/store/socket.sock');
}
if ( ! is_writable(dirname($_SERVER['argv'][1]))) {
	die('socket path not writable');
}

define('HOMEDIR', __DIR__.'/');

// Construct the dependencies
$config  = new Game\Config(__DIR__.'/app/config/config.json');
$event   = new Game\Event();
$input   = new Game\Input($event);
$output  = new Game\Output($event);
$db      = new \PDO('mysql:dbname='.$config->get('database.name').';host='.$config->get('database.host'),
                    $config->get('database.user'), $config->get('database.pass'));

// set up the logger
$logger    = new \Monolog\Logger('game');
$formatter = new \Monolog\Formatter\LineFormatter(null, null, false, true);

$handler = new \Monolog\Handler\StreamHandler('php://output');
$handler->setFormatter($formatter);
$logger->pushHandler($handler);

$handler = new \Monolog\Handler\StreamHandler(__DIR__.'/var/log/game.log');
$handler->setFormatter($formatter);
$logger->pushHandler($handler);

unset($handler, $formatter);

// set up the managers
$connectionManager = new Game\Connection\Manager($config, $event, $logger);
$entityManger      = new Game\Module\Entity\Manager(new Game\Module\Entity\Adapter\Pdo($db));
$moduleManager     = new Game\Module\Manager($config, $input, $output, $event, $entityManger, $logger);

\Game\Registry::init();

// and feed it all to the server process
$game = new Game\Server($config, $event, $input, $connectionManager, $moduleManager, $logger);
$game->setSocketPath($_SERVER['argv'][1]);
$game->run();
