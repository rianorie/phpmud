<?php

/*
#3000
old peddler~
(null)~
(null)~
an old peddler~
A peddler cries out his wares to all who pass by.
~
The peddler is an older man with a shiny bald head covered by a green
cap.  He wanders about the Oval Plaza attempting to push his goods onto
anyone who passes by.
~
Andor~
AB 0 -100 0
23 23 23d23+46 1d23+1 1d23+1 pound
5 5 5 5
FIK 0 0 AHMV
stand stand male 50
AFHMV ABCDEFGHIJK medium unknown
M GREET 3000 100~

#3001
merchant~
(null)~
(null)~
a merchant~
A merchant stands at the edge of the plaza, loudly pawning his wares.
~
The merchant is a shrewd man with a hawk-bill nose and a wrinkled face.
He spends his time convincing people to buy goods they have no possible
use for.
~
Andor~
AB 0 0 0
25 14 25d25+50 3d8+0 2d12+1 pound
7 7 7 7
0 0 0 AHMV
stand stand male 58
AFHMV ABCDEFGHIJK medium unknown
M GREET 3001 100~

#3006
dangerous man~
(null)~
(null)~
A dangerous looking man~
A dangerous looking man stays to the shadows.
~
This man has a hungry look in his eyes.  He stays to the shadows,
creeping along in search of who knows what.
~
Andor~
A 0 -100 0
5 1 5d6+6 1d8+0 1d1+1 punch
9 9 9 9
0 0 0 AHMV
stand stand male 20
AFHMV ABCDEFGHIJK medium unknown
 */


/*
VNUM            #3001
NAME            {RC{raemlyn {RN{rew {RC{rity - {YT{yhe {YO{yval {YP{ylaza{x~
DESC            {wAll roads lead to Caemlyn, it is oftimes said, and that old adage is
                proven true here where the Road of Lions, Caemlyn Road, and the multitude of
                byways and side streets that crisscross the city converge together to form
                possibly the largest marketplace in the known world.  The Oval Plaza is a
                madhouse of activity, with folk from all walks of life straining against
                each other to make headway through the throng of bodies that crowd the
                streets.  At every corner there is a merchant panning off whatever can be
                sold to whoever is willing to buy.  Queen's Guard in their deep red cloaks
                and brightly polished breastplates make a vain attempt to control the mob,
                but to no avail.  Towards the south the gleaming spires of the Inner City
                rise up like fingers reaching for the sky, the colorful banners of all the
                noble houses flying from their peaks.  To the north, east, and west roads
                lead out of the plaza and deeper into the city.  {x
                ~
0=nothing,flags,sector        0 8389632 1
EXIT DIRECTION                D0
EXIT DESC                     ~
EXIT KEYWORD                  ~
EXIT LOCKS, Key, Leads to     0 0 3002
D1
~
~
0 0 3003
D2
~
~
0 0 3005
D3
~
~
0 0 3004
D4
~
~
0 0 3700
D5
~
~
0 0 1050
D7
~
~
0 0 3281
D8
~
~
0 0 3280
D9
~
~
0 0 3279
M 200 H 200
G 0
S


#3002
{RC{raemlyn {RN{rew {RC{rity - {YT{yhe {YO{yval {YP{ylaza{x~
{wThis is the northern most edge of the Oval Plaza.  A mass of bodies
crowds the plaza, people intent on making their way to their destination no
matter who or what has to be pushed aside.  Folk come from all about to see
the grand city of Caemlyn and wonder at it's gleaming spires, high walls,
and roads paved with gray and white streaked stone.  The roads lead around
and over rolling hills towards the center of the city to the south, while to
the north, the Road of Lions leads towards the Tar Valon gate.
~
0 0 1
D0
~
~
0 0 3029
D2
~
~
0 0 3001
D3
~
~
0 0 3279
G 0
S

#3006
{BCaemlyn New City - The Inner City gates{x~
{wThe Origan Gate leading into the famed Inner City of Caemlyn is a massive
arch, purest white is color and carved with lions charging, rearing, and
rampant.  Ogier built, it is like much of the rest of Inner City:
magnificient to behold and without equal.  Great gilded doors of solid oak
banded with black iron bars entrance to the Inner City, and the guards who
stand at the gate prohibit entrance to those without a reason to pass.
~
0 0 1
D0
~
~
0 0 3005
D2
~
gate~
1 0 3155
G 0
S


 */

define('A', 1);
define('B', 2);
define('C', 4);
define('D', 8);
define('E', 16);
define('F', 32);
define('G', 64);
define('H', 128);
define('I', 256);
define('J', 512);
define('K', 1024);
define('L', 2048);
define('M', 4096);
define('N', 8192);
define('O', 16384);
define('P', 32768);
define('Q', 65536);
define('R', 131072);
define('S', 262144);
define('T', 524288);
define('U', 1048576);
define('V', 2097152);
define('W', 4194304);
define('X', 8388608);
define('Y', 16777216);
define('Z', 33554432);
define('aa', 67108864);
define('bb', 134217728);
define('cc', 268435456);
define('dd', 536870912);
define('ee', 1073741824);
define('ff', 2147483648);


function get_flags($bits) {

    $flags = [
        'dark'         => A,
        'jail'         => B,
        'no_mobs'       => C,
        'indoors'      => D,
        'stedding'     => E,
        'casino'       => F,
        'camp'         => G,
        'private'      => J,
        'safe'         => K,
        'solitary'     => L,
        'pet_shop'     => M,
        'no_recall'    => N,
        'imp_only'     => O,
        'gods_only'    => P,
        'heroes_only'  => Q,
        'newbie_only'  => R,
        'law'          => S,
        'nowhere'      => T,
        'bank'         => U,
        'pawnshop'     => V,
        'pk'           => W,
        'nogate'       => X,
        'climb'        => Y,
        'home'         => Z,
        'msp'          => aa,
        'mount_shop'   => bb,
        'locker'       => cc,
        'quest'        => dd
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) { return $bits & $f; }));
}

$area = file_get_contents('caemnew.txt');

$start = strpos($area, '#ROOMS')+7;
$end   = strpos($area, '#SPECIALS');
$rooms = substr($area, $start, ($end-$start));

$lines = explode("\n", $rooms);
//print_r($lines);

$found = [];
$current = [];

$pos = 0;
foreach($lines as $line) {
    if (strpos($line, '#') === 0 && $line != '#0') {

        $current['vnum'] = substr($line, 1);

        next($lines);

        $current['title'] = rtrim(next($lines), '~');

        $current['description'] = '';
        while(($line = next($lines)) !== '~' && $line !== false) {
            $current['description'] .= $line."\n";
        }

        $specs = explode(' ', next($lines));

        $current['flags'] = get_flags($specs[1]);
        $current['sector'] = $specs[2];


        while(($line = next($lines)) !== false) {
            switch(substr($line, 0, 1)) {


                case 'S';break;
                case 'N': // mine
                    break;

                case 'M': // heal room: "M 200 H 200"
                    $parts = explode(' ', $line);
                    $current['stamina'] = $parts[1];
                    $current['hitpoints'] = $parts[3];
                    break;
                case 'C': // guilded
                    $current['metadata'] = json_encode(['guild' => substr($line, 2, -1)]);
                    break;
                case 'D': // exits
                    prev($lines);
                    while(strpos(($line = next($lines)), 'D') === 0 && $line !== false) {
                        $exit = [
                            'direction' => substr($line, 1),
                            'description' => ''
                        ];

                        while(strpos(($line = next($lines)), '~') !== 0 && $line !== false) {
                            $exit['description'] .= $line."\n";
                        }

                        $exit['keyword'] = rtrim(next($lines), '~');

                        $specs = explode(' ', next($lines));
                        $exit['type'] = $specs[0];
                        $exit['key'] = $specs[1];
                        $exit['target'] = $specs[2];

                        $current['exits'][] = $exit;
                    }

                    prev($lines);
                    break;
                case 'E': // extra descs
                    $extra = ['keyword' => substr(next($lines), 0, -1), 'description' => ''];
                    while(strpos(($line = next($lines)), '~') !== 0 && $line !== false) {
                        $extra['description'] .= $line."\n";
                    }
                    $current['extras'][] = $extra;
                    break;

                case 'G': // Grade "G 0"
                    $parts = explode(' ', $line);
                    $current['grade'] = $parts[1];
                    break;
                case 'O': // owner
                    break;
                case 'R': // roomprog
                    break;
                case 'U': // Roa shit
                    break;

                // if we find a hash, next room is being started, back up one line and send it back to the foreach
                case '#';
                    prev($lines);
                    break 2;
            }
        }

        $found[] = $current;
        $current = [];
    }
    $pos++;
}

//print_r(array_slice($lines, 5931));
//print_r($found);
print_r(array_filter($found, function($f) { return in_array($f['vnum'], ['3108', '3100', '3191']); }));


/*
Array
        (
            [vnum] =>
            [title] =>
            [description] =>
            [stamina] => -100
            [hitpoints] => 100
            [grade] => 0
            [flags] => Array
                (
                    [0] => no_mobs
                    [1] => safe
                )

            [sector] => 0
            [exits] => Array
                (
                    [0] => Array
                        (
                            [direction] => 2
                            [description] =>
                            [keyword] =>
                            [type] => 0
                            [key] => 0
                            [target] => 3069
                        )

                    [1] => Array
                        (
                            [direction] => 3
                            [description] =>
                            [keyword] =>
                            [type] => 0
                            [key] => 0
                            [target] => 3071
                        )

                )
            [extras] => Array
                (
                    [0] => Array
                        (
                            [keyword] => sign
                            [description] => Marek is a restringer.  He will take an item and set the short, long, or
name of an item to what you wish it to be - for a price.

restring (item) short this is the new short description
restring (item) long This is the new long description.
restring (item) name this is the new name

You will NOT abuse this feature of the MUD. Items found with offensive descriptions
will be removed from you and NOT replaced.

                        )

                )

        )
 */
