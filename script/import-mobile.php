<?php

/*
#3000
old peddler~
(null)~
(null)~
an old peddler~
A peddler cries out his wares to all who pass by.
~
The peddler is an older man with a shiny bald head covered by a green
cap.  He wanders about the Oval Plaza attempting to push his goods onto
anyone who passes by.
~
Andor~
AB 0 -100 0
23 23 23d23+46 1d23+1 1d23+1 pound
5 5 5 5
FIK 0 0 AHMV
stand stand male 50
AFHMV ABCDEFGHIJK medium unknown
M GREET 3000 100~

#3001
merchant~
(null)~
(null)~
a merchant~
A merchant stands at the edge of the plaza, loudly pawning his wares.
~
The merchant is a shrewd man with a hawk-bill nose and a wrinkled face.
He spends his time convincing people to buy goods they have no possible
use for.
~
Andor~
AB 0 0 0
25 14 25d25+50 3d8+0 2d12+1 pound
7 7 7 7
0 0 0 AHMV
stand stand male 58
AFHMV ABCDEFGHIJK medium unknown
M GREET 3001 100~

#3006
dangerous man~
(null)~
(null)~
A dangerous looking man~
A dangerous looking man stays to the shadows.
~
This man has a hungry look in his eyes.  He stays to the shadows,
creeping along in search of who knows what.
~
Andor~
A 0 -100 0
5 1 5d6+6 1d8+0 1d1+1 punch
9 9 9 9
0 0 0 AHMV
stand stand male 20
AFHMV ABCDEFGHIJK medium unknown
 */


define('A', 1);
define('B', 2);
define('C', 4);
define('D', 8);
define('E', 16);
define('F', 32);
define('G', 64);
define('H', 128);
define('I', 256);
define('J', 512);
define('K', 1024);
define('L', 2048);
define('M', 4096);
define('N', 8192);
define('O', 16384);
define('P', 32768);
define('Q', 65536);
define('R', 131072);
define('S', 262144);
define('T', 524288);
define('U', 1048576);
define('V', 2097152);
define('W', 4194304);
define('X', 8388608);
define('Y', 16777216);
define('Z', 33554432);
define('aa', 67108864);
define('bb', 134217728);
define('cc', 268435456);
define('dd', 536870912);
define('ee', 1073741824);
define('ff', 2147483648);

function calculate_flag($bits)
{

    $val = 0;
    foreach (str_split($bits) as $char) {
        $val += @constant($char);
    }

    return $val;
}

function form_bit_name($form_flags)
{
    $buffer = [];

    if ($form_flags & B) $buffer[] = "poison";
    else if ($form_flags & A) $buffer[] = "edible";
    if ($form_flags & C) $buffer[] = "magical";
    if ($form_flags & D) $buffer[] = "instant_rot";
    if ($form_flags & E) $buffer[] = "other";
    if ($form_flags & G) $buffer[] = "animal";
    if ($form_flags & H) $buffer[] = "sentient";
    if ($form_flags & I) $buffer[] = "undead";
    if ($form_flags & J) $buffer[] = "construct";
    if ($form_flags & K) $buffer[] = "mist";
    if ($form_flags & L) $buffer[] = "intangible";
    if ($form_flags & M) $buffer[] = "biped";
    if ($form_flags & N) $buffer[] = "centaur";
    if ($form_flags & O) $buffer[] = "insect";
    if ($form_flags & P) $buffer[] = "spider";
    if ($form_flags & Q) $buffer[] = "crustacean";
    if ($form_flags & R) $buffer[] = "worm";
    if ($form_flags & S) $buffer[] = "blob";
    if ($form_flags & V) $buffer[] = "mammal";
    if ($form_flags & W) $buffer[] = "bird";
    if ($form_flags & X) $buffer[] = "reptile";
    if ($form_flags & Y) $buffer[] = "snake";
    if ($form_flags & Z) $buffer[] = "dragon";
    if ($form_flags & aa) $buffer[] = "amphibian";
    if ($form_flags & bb) $buffer[] = "fish";
    if ($form_flags & cc) $buffer[] = "cold_blooded";

    return $buffer;
}

function part_bit_name($part_flags)
{
    $buffer = [];

    if ($part_flags & A) $buffer[] = "head";
    if ($part_flags & B) $buffer[] = "arms";
    if ($part_flags & C) $buffer[] = "legs";
    if ($part_flags & D) $buffer[] = "heart";
    if ($part_flags & E) $buffer[] = "brains";
    if ($part_flags & F) $buffer[] = "guts";
    if ($part_flags & G) $buffer[] = "hands";
    if ($part_flags & H) $buffer[] = "feet";
    if ($part_flags & I) $buffer[] = "fingers";
    if ($part_flags & J) $buffer[] = "ears";
    if ($part_flags & K) $buffer[] = "eyes";
    if ($part_flags & L) $buffer[] = "long_tongue";
    if ($part_flags & M) $buffer[] = "eyestalks";
    if ($part_flags & N) $buffer[] = "tentacles";
    if ($part_flags & O) $buffer[] = "fins";
    if ($part_flags & P) $buffer[] = "wings";
    if ($part_flags & Q) $buffer[] = "tail";

    if ($part_flags & U) $buffer[] = "claws";
    if ($part_flags & V) $buffer[] = "fangs";
    if ($part_flags & W) $buffer[] = "horns";
    if ($part_flags & X) $buffer[] = "scales";
    if ($part_flags & Y) $buffer[] = "tusks";

    return $buffer;
}

function off_flags($off_flags)
{
    $buffer = [];

    if ($off_flags & A) $buffer[] = "area_attack";
    if ($off_flags & B) $buffer[] = "backstab";
    if ($off_flags & C) $buffer[] = "bash";
    if ($off_flags & D) $buffer[] = "berserk";
    if ($off_flags & E) $buffer[] = "disarm";
    if ($off_flags & F) $buffer[] = "dodge";
    if ($off_flags & G) $buffer[] = "fade";
    if ($off_flags & H) $buffer[] = "fast";
    if ($off_flags & I) $buffer[] = "kick";
    if ($off_flags & J) $buffer[] = "dirtkick";
    if ($off_flags & K) $buffer[] = "parry";
    if ($off_flags & L) $buffer[] = "rescue";
    if ($off_flags & M) $buffer[] = "tail";
    if ($off_flags & N) $buffer[] = "trip";
    if ($off_flags & O) $buffer[] = "crush";
    if ($off_flags & P) $buffer[] = "assist_all";
    if ($off_flags & Q) $buffer[] = "assist_align";
    if ($off_flags & R) $buffer[] = "assist_race";
    if ($off_flags & S) $buffer[] = "assist_players";
    if ($off_flags & T) $buffer[] = "assist_guard";
    if ($off_flags & U) $buffer[] = "assist_vnum";
    if ($off_flags & V) $buffer[] = "charge";
    if ($off_flags & W) $buffer[] = "circle";
    if ($off_flags & X) $buffer[] = "forms";
    if ($off_flags & Y) $buffer[] = "spirit";

    return $buffer;
}

function imm_flags($imm_flags)
{
    $buffer = [];

    if ($imm_flags & A) $buffer[] = "summon";
    if ($imm_flags & B) $buffer[] = "charm";
    if ($imm_flags & C) $buffer[] = "magic";
    if ($imm_flags & D) $buffer[] = "weapon";
    if ($imm_flags & E) $buffer[] = "bash";
    if ($imm_flags & F) $buffer[] = "pierce";
    if ($imm_flags & G) $buffer[] = "slash";
    if ($imm_flags & H) $buffer[] = "fire";
    if ($imm_flags & I) $buffer[] = "cold";
    if ($imm_flags & J) $buffer[] = "lightning";
    if ($imm_flags & K) $buffer[] = "acid";
    if ($imm_flags & L) $buffer[] = "poison";
    if ($imm_flags & M) $buffer[] = "negative";
    if ($imm_flags & N) $buffer[] = "holy";
    if ($imm_flags & O) $buffer[] = "energy";
    if ($imm_flags & P) $buffer[] = "mental";
    if ($imm_flags & Q) $buffer[] = "disease";
    if ($imm_flags & R) $buffer[] = "drowning";
    if ($imm_flags & S) $buffer[] = "light";
    if ($imm_flags & T) $buffer[] = "sound";
    if ($imm_flags & X) $buffer[] = "wood";
    if ($imm_flags & Y) $buffer[] = "silver";
    if ($imm_flags & Z) $buffer[] = "iron";

    return $buffer;
}

function res_flags($res_flags)
{
    $buffer = [];

    if ($res_flags & A) $buffer[] = "summon";
    if ($res_flags & B) $buffer[] = "charm";
    if ($res_flags & C) $buffer[] = "magic";
    if ($res_flags & D) $buffer[] = "weapon";
    if ($res_flags & E) $buffer[] = "bash";
    if ($res_flags & F) $buffer[] = "pierce";
    if ($res_flags & G) $buffer[] = "slash";
    if ($res_flags & H) $buffer[] = "fire";
    if ($res_flags & I) $buffer[] = "cold";
    if ($res_flags & J) $buffer[] = "lightning";
    if ($res_flags & K) $buffer[] = "acid";
    if ($res_flags & L) $buffer[] = "poison";
    if ($res_flags & M) $buffer[] = "negative";
    if ($res_flags & N) $buffer[] = "holy";
    if ($res_flags & O) $buffer[] = "energy";
    if ($res_flags & P) $buffer[] = "mental";
    if ($res_flags & Q) $buffer[] = "disease";
    if ($res_flags & R) $buffer[] = "drowning";
    if ($res_flags & S) $buffer[] = "light";
    if ($res_flags & T) $buffer[] = "sound";
    if ($res_flags & X) $buffer[] = "wood";
    if ($res_flags & Y) $buffer[] = "silver";
    if ($res_flags & Z) $buffer[] = "iron";

    return $buffer;
}


function act_flags($act_flags)
{
    $buffer = [];

    if ($act_flags & A) $buffer[] = "npc";
    if ($act_flags & B) $buffer[] = "sentinel";
    if ($act_flags & C) $buffer[] = "scavenger";

    if ($act_flags & F) $buffer[] = "aggressive";

    if ($act_flags & E) $buffer[] = "mount";

    if ($act_flags & G) $buffer[] = "stay_area";
    if ($act_flags & H) $buffer[] = "wimpy";
    if ($act_flags & I) $buffer[] = "pet";
    if ($act_flags & J) $buffer[] = "trainer";
    if ($act_flags & K) $buffer[] = "practice";
    if ($act_flags & L) $buffer[] = "gambler";
    if ($act_flags & M) $buffer[] = "jeweler";

    if ($act_flags & O) $buffer[] = "undead";
    if ($act_flags & Q) $buffer[] = "cleric";
    if ($act_flags & R) $buffer[] = "mage";
    if ($act_flags & S) $buffer[] = "thief";
    if ($act_flags & T) $buffer[] = "warrior";
    if ($act_flags & U) $buffer[] = "noalign";
    if ($act_flags & V) $buffer[] = "nopurge";
    if ($act_flags & W) $buffer[] = "outdoors";

    if ($act_flags & Y) $buffer[] = "indoors";
    if ($act_flags & aa) $buffer[] = "healer";
    if ($act_flags & bb) $buffer[] = "gain";
    if ($act_flags & cc) $buffer[] = "update_always";
    if ($act_flags & dd) $buffer[] = "changer";
    if ($act_flags & ee) $buffer[] = "stringer";

    return $buffer;
}

function aff_flags($aff_flags)
{
    $buffer = [];

    if ($aff_flags & A) $buffer[] = "blind";
    if ($aff_flags & B) $buffer[] = "invisible";
    if ($aff_flags & C) $buffer[] = "detect_evil";
    if ($aff_flags & D) $buffer[] = "detect_invis";
    if ($aff_flags & E) $buffer[] = "detect_magic";
    if ($aff_flags & F) $buffer[] = "detect_hidden";
    if ($aff_flags & G) $buffer[] = "detect_good";
    if ($aff_flags & H) $buffer[] = "sanctuary";
    if ($aff_flags & I) $buffer[] = "faerie_fire";
    if ($aff_flags & J) $buffer[] = "infrared";
    if ($aff_flags & K) $buffer[] = "curse";
//    if ($aff_flags & L) $buffer[] = "unused";
    if ($aff_flags & M) $buffer[] = "poison";
    if ($aff_flags & N) $buffer[] = "protect_evil";
    if ($aff_flags & O) $buffer[] = "protect_good";
    if ($aff_flags & P) $buffer[] = "sneak";
    if ($aff_flags & Q) $buffer[] = "hide";
    if ($aff_flags & R) $buffer[] = "sleep";
    if ($aff_flags & S) $buffer[] = "charm";
    if ($aff_flags & T) $buffer[] = "flying";
    if ($aff_flags & U) $buffer[] = "pass_door";
    if ($aff_flags & V) $buffer[] = "haste";
    if ($aff_flags & W) $buffer[] = "calm";
    if ($aff_flags & X) $buffer[] = "plague";
    if ($aff_flags & Y) $buffer[] = "weaken";
    if ($aff_flags & Z) $buffer[] = "dark_vision";
    if ($aff_flags & aa) $buffer[] = "berserk";
    if ($aff_flags & bb) $buffer[] = "swim";
    if ($aff_flags & cc) $buffer[] = "regeneration";
    if ($aff_flags & dd) $buffer[] = "slow";
    if ($aff_flags & ee) $buffer[] = "void";

    return $buffer;
}


function get_flags($bits) {

    $flags = [
        'dark'         => A,
        'jail'         => B,
        'no_mobs'       => C,
        'indoors'      => D,
        'stedding'     => E,
        'casino'       => F,
        'camp'         => G,
        'private'      => J,
        'safe'         => K,
        'solitary'     => L,
        'pet_shop'     => M,
        'no_recall'    => N,
        'imp_only'     => O,
        'gods_only'    => P,
        'heroes_only'  => Q,
        'newbie_only'  => R,
        'law'          => S,
        'nowhere'      => T,
        'bank'         => U,
        'pawnshop'     => V,
        'pk'           => W,
        'nogate'       => X,
        'climb'        => Y,
        'home'         => Z,
        'msp'          => aa,
        'mount_shop'   => bb,
        'locker'       => cc,
        'quest'        => dd
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) { return $bits & $f; }));
}

$area = file_get_contents('caemnew.txt');

$start = strpos($area, '#MOBILES')+8;
$end   = strpos($area, '#OBJECTS');

$lines = explode("\n", substr($area, $start, ($end-$start)));

$found = [];
$current = [];

foreach($lines as $pos => $line) {
    if (strpos($line, '#') === 0 && $line != '#0') {

        $current['vnum'] = substr($line, 1);

        $int = $pos;

        $current['name'] = rtrim($lines[++$int], '~');

        $current['die_message'] = '';
        while(($next = $lines[++$int]) !== false) {

            $current['die_message'] .= $next."\n";
            if (substr($next, -1) == '~') { break; }
        }
        $current['die_message'] = trim(rtrim($current['die_message'], '~'));

        if ($current['die_message'] == '(null)') { $current['die_message'] = 'null'; }

        $current['kill_message'] = '';
        while(($next = $lines[++$int]) !== false) {
            $current['kill_message'] .= $next."\n";
            if (substr($next, -1) == '~') { break; }
        }

        $current['kill_message'] = trim(rtrim($current['kill_message'], '~'));

        if ($current['kill_message'] == '(null)') { $current['kill_message'] = 'null'; }

        $current['short_description'] = rtrim($lines[++$int], '~');
        $current['long_description'] = $lines[++$int];


        $current['description'] = '';

        ++$int;

        while(($next = $lines[++$int]) != '~') {
            $current['description'] .= $next."\n";
        }

        $current['race'] = rtrim($lines[++$int], '~'); // dont get the next line, the while loop did that for us

        $parts = explode(' ', $lines[++$int]);

        $current['flag_act'] = act_flags(calculate_flag($parts[0]));
        $current['affected'] = ( ! empty($parts[1]) ? aff_flags(calculate_flag($parts[1])) : null );
        $current['group'] = ($parts[3] ?? null);

        $parts = explode(' ', $lines[++$int]);

        $current['level'] = $parts[0];
        $current['hitroll'] = $parts[1];

        $dice = preg_split('/d|\+/', $parts[2], -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $current['hit_dice'] = $dice[0];
        $current['hit_dice_type'] = ($dice[1] ?? null);
        $current['hit_dice_bonus'] = ($dice[2] ?? null);

        $dice = preg_split('/d|\+/', $parts[3], -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $current['stamina_dice'] = $dice[0];
        $current['stamina_dice_type'] = ($dice[1] ?? null);
        $current['stamina_dice_bonus'] = ($dice[2] ?? null);

        $dice = preg_split('/d|\+/', $parts[4], -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        $current['damage_dice'] = $dice[0];
        $current['damage_dice_type'] = $dice[1];
        $current['damage_dice_bonus'] = $dice[2];

        $current['damage_type'] = $parts[5];

        $parts = explode(' ', $lines[++$int]);
        $current['ac_pierce'] = $parts[0];
        $current['ac_bash'] = $parts[1];
        $current['ac_slash'] = $parts[2];
        $current['ac_exotic'] = $parts[3];

        $parts = explode(' ', $lines[++$int]);
        $current['offensive_flags'] = off_flags(calculate_flag($parts[0]));
        $current['immume_flags'] = imm_flags(calculate_flag($parts[1]));
        $current['resistance_flags'] = res_flags(calculate_flag($parts[2]));
        $current['vulnerable_flags'] = res_flags(calculate_flag($parts[3])); // res and vul flags are the same

        $parts = explode(' ', $lines[++$int]);
        $current['start_position'] = $parts[0];
        $current['default_position'] = $parts[1];
        $current['sex'] = ($parts[2] == 'male' ? 1 : 2);
        $current['wealth'] = $parts[3];

        $parts = explode(' ', $lines[++$int]);
        $current['form'] = form_bit_name(calculate_flag($parts[0]));
        $current['parts'] = part_bit_name(calculate_flag($parts[1]));
        $current['size'] = $parts[2];
        $current['material'] = $parts[3];


        $found[] = $current;
        $current = [];
    }
}

//print_r(array_filter($found, function($f) { return !empty($f['affected']); }));
//die();

$db = new \PDO('mysql:host=localhost;dbname=phpmud', 'root', '123');


foreach($found as $f) {

    $m = $db->prepare('INSERT INTO mobiles
    (area_id, original_vnum, name, die_message, kill_message, short_description, long_description, description, 
     race, flag_action, flag_affected, `group`, damage_type, flag_offensive, flag_immune, flag_resistance, 
     flag_vulnerable, position_start, position_default, sex, flag_form, flag_parts, material, level, hitroll, 
     hit_dice, hit_dice_type, hit_dice_bonus, stamina_dice, stamina_dice_type, stamina_dice_bonus, damage_dice,
     damage_dice_type, damage_dice_bonus, ac_bash, ac_pierce, ac_exotic, ac_slash, wealth, size)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');

    $data = [
        1,
        $f['vnum'],
        $f['name'],
        $f['die_message'],
        $f['kill_message'],
        $f['short_description'],
        $f['long_description'],
        $f['description'],
        $f['race'],
        (is_array($f['flag_act']) ? implode(', ', $f['flag_act']) : null),
        (is_array($f['affected']) ? implode(', ', $f['affected']) : null),
        $f['group'],
        $f['damage_type'],
        (is_array($f['offensive_flags']) ? implode(', ', $f['offensive_flags']) : null),
        (is_array($f['immume_flags']) ? implode(', ', $f['immume_flags']) : null),
        (is_array($f['resistance_flags']) ? implode(', ', $f['resistance_flags']) : null),
        (is_array($f['vulnerable_flags']) ? implode(', ', $f['vulnerable_flags']) : null),
        $f['start_position'],
        $f['default_position'],
        $f['sex'],
        (is_array($f['form']) ? implode(', ', $f['form']) : null),
        (is_array($f['parts']) ? implode(', ', $f['parts']) : null),
        $f['material'],
        $f['level'],
        $f['hitroll'],
        $f['hit_dice'],
        $f['hit_dice_type'],
        $f['hit_dice_bonus'],
        $f['stamina_dice'],
        $f['stamina_dice_type'],
        $f['stamina_dice_bonus'],
        $f['damage_dice'],
        $f['damage_dice_type'],
        $f['damage_dice_bonus'],
        $f['ac_bash'],
        $f['ac_pierce'],
        $f['ac_exotic'],
        $f['ac_slash'],
        $f['wealth'],
        $f['size']
    ];

    if ( ! $m->execute($data)) {
        print_r($m->errorInfo());
    }
}
