<?php

require_once __DIR__ . '/../app/Game/Module/Entity/Interfaces/Table.php';
require_once __DIR__ . '/../app/Modules/World/Table/Weapontype.php';
require_once __DIR__ . '/../app/Modules/World/Table/Furniture.php';
require_once __DIR__ . '/../app/Modules/World/Table/Container.php';
require_once __DIR__ . '/../app/Modules/World/Table/Itemaffect.php';
require_once __DIR__ . '/../app/Modules/World/Table/Wearslot.php';
require_once __DIR__ . '/../app/Modules/World/Table/Itemtype.php';

use Modules\World\Table\Weapontype;
use Modules\World\Table\Furniture;
use Modules\World\Table\Container;
use Modules\World\Table\Itemaffect;
use Modules\World\Table\Wearslot;
use Modules\World\Table\Itemtype;

define('A', 1);
define('B', 2);
define('C', 4);
define('D', 8);
define('E', 16);
define('F', 32);
define('G', 64);
define('H', 128);
define('I', 256);
define('J', 512);
define('K', 1024);
define('L', 2048);
define('M', 4096);
define('N', 8192);
define('O', 16384);
define('P', 32768);
define('Q', 65536);
define('R', 131072);
define('S', 262144);
define('T', 524288);
define('U', 1048576);
define('V', 2097152);
define('W', 4194304);
define('X', 8388608);
define('Y', 16777216);
define('Z', 33554432);
define('aa', 67108864);
define('bb', 134217728);
define('cc', 268435456);
define('dd', 536870912);
define('ee', 1073741824);
define('ff', 2147483648);


function calculate_flag($bits)
{

    $val = 0;
    foreach (str_split($bits) as $char) {
        $val += @constant($char);
    }

    return $val;
}

function get_wear($bits)
{
    $flags = [
        Wearslot::TAKE           => A,
        Wearslot::WEAR_FINGER    => B,
        Wearslot::WEAR_NECK      => C,
        Wearslot::WEAR_BODY      => D,
        Wearslot::WEAR_HEAD      => E,
        Wearslot::WEAR_LEGS      => F,
        Wearslot::WEAR_FEET      => G,
        Wearslot::WEAR_HANDS     => H,
        Wearslot::WEAR_ARMS      => I,
        Wearslot::WEAR_SHIELD    => J,
        Wearslot::WEAR_ABOUT     => K,
        Wearslot::WEAR_WAIST     => L,
        Wearslot::WEAR_WRIST     => M,
        Wearslot::WIELD          => N,
        Wearslot::HOLD           => O,
        Wearslot::WEAR_FLOAT     => P,
        Wearslot::WEAR_NOSE      => Q,
        Wearslot::WEAR_SHOULDERS => R,
        Wearslot::WEAR_BACK      => S,
        Wearslot::WEAR_EYES      => T,
        Wearslot::WEAR_CALF      => U,
        Wearslot::WEAR_THIGH     => V,
        Wearslot::WEAR_FOREARM   => W,
        Wearslot::WEAR_EAR       => X,
        Wearslot::WEAR_ANKLE     => Y,
        Wearslot::WEAR_NEAR_MIND => Z,
        Wearslot::WEAR_FACE      => aa,
        Wearslot::WEAR_ANGREAL   => bb,
        Wearslot::WEAR_LIGHT     => cc
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) {
        return $bits & $f;
    }));
}

function get_apply($bits)
{
    $flags = [
        Itemaffect::NONE          => 0,
        Itemaffect::STR           => 1,
        Itemaffect::DEX           => 2,
        Itemaffect::INT           => 3,
        Itemaffect::WIS           => 4,
        Itemaffect::CON           => 5,
        Itemaffect::SEX           => 6,
        //        Itemaffect::CLASS => 7,
        Itemaffect::LEVEL         => 8,
        Itemaffect::AGE           => 9,
        Itemaffect::HEIGHT        => 10,
        Itemaffect::WEIGHT        => 11,
        Itemaffect::STAMINA       => 12,
        Itemaffect::HIT           => 13,
        Itemaffect::GOLD          => 15,
        Itemaffect::EXP           => 16,
        Itemaffect::AC            => 17,
        Itemaffect::HITROLL       => 18,
        Itemaffect::DAMROLL       => 19,
        Itemaffect::SAVES         => 20,
        Itemaffect::SAVING_ROD    => 21,
        Itemaffect::SAVING_PETRI  => 22,
        Itemaffect::SAVING_BREATH => 23,
        Itemaffect::SAVING_SPELL  => 24,
        Itemaffect::SPELL_AFFECT  => 25,
        Itemaffect::REF           => 26,
        Itemaffect::INS           => 27,
        Itemaffect::CHA           => 28,
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) {
        return $bits & $f;
    }));
}

function get_furnitureflags($bits)
{
    $flags = [
        Furniture::STAND_AT    => A,
        Furniture::STAND_ON    => B,
        Furniture::STAND_IN    => C,
        Furniture::SIT_AT      => D,
        Furniture::SIT_ON      => E,
        Furniture::SIT_IN      => F,
        Furniture::REST_AT     => G,
        Furniture::REST_ON     => H,
        Furniture::REST_IN     => I,
        Furniture::SLEEP_AT    => J,
        Furniture::SLEEP_ON    => K,
        Furniture::SLEEP_IN    => L,
        Furniture::PUT_AT      => M,
        Furniture::PUT_ON      => N,
        Furniture::PUT_IN      => O,
        Furniture::PUT_INSIDE  => P,
        'delete_game_chop'     => Q,
        'delete_game_fives'    => R,
        Furniture::SIT_UNDER   => S,
        Furniture::REST_UNDER  => T,
        Furniture::SLEEP_UNDER => U,
        Furniture::STAND_UNDER => V,
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) {
        return $bits & $f;
    }));
}

function get_flags($bits)
{
    $flags = [
        'glow'     => A,
        'dark'     => C,
        'lock'     => D,
        'evil'     => E,
        'invis'    => F,
        'rotting'  => P,
        'balanced' => aa,
        'remort'   => dd,

        'DELETE_VIS_DEATH'    => Q,
        'DELETE_HUM'          => B,
        'DELETE_BLESS'        => I,
        'DELETE_NOSELL'       => bb,
        'DELETE_NOLOAD'       => cc,
        'DELETE_MAGIC'        => G,
        'DELETE_NODROP'       => H,
        'DELETE_ANTI_GOOD'    => J,
        'DELETE_ANTI_EVIL'    => K,
        'DELETE_ANTI_NEUTRAL' => L,
        'DELETE_NOREMOVE'     => M,
        'DELETE_INVENTORY'    => N,
        'DELETE_NOPURGE'      => O,
        'DELETE_SHIMMER'      => R,
        'DELETE_NONMETAL'     => S,
        'DELETE_NOLOCATE'     => T,
        'DELETE_MELT_DROP'    => U,
        'DELETE_HAD_TIMER'    => V,
        'DELETE_SELL_EXTRACT' => W,
        'DELETE_SPIKED'       => X,
        'DELETE_BURN_PROOF'   => Y,
        'DELETE_NOUNCURSE'    => Z,
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) {
        return $bits & $f;
    }));
}

function get_weapontype($found)
{
    $types = (new Weapontype())->all();
    foreach($types as $id => $t) {
        if ($t->label == $found) {
            return $id;
        }
    }

    return 0;
}

function get_itemtype($found)
{
    $types = (new Itemtype())->all();
    foreach($types as $id => $t) {
        if ($t->label == $found) {
            return $id;
        }
    }

    return 0;
}

function get_containerflags($bits)
{
    $flags = [
        'closeable' => 1,
        'pickproof' => 2,
        'closed'    => 4,
        'locked'    => 8,
        'put_on'    => 16
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) {
        return $bits & $f;
    }));
}

function get_weaponflags($bits)
{
    $flags = [
        'flaming'   => A,
        'frost'     => B,
        'sharp'     => D,
        'two_hands' => F,
        'shocking'  => G,
        'poison'    => H,

        'delete_vampiric'     => C,
        'vorpal'              => E,
        'loaded'              => I,
        'infuse_strength'     => J,
        'infuse_constitution' => K,
        'infuse_dexterity'    => L,
        'infuse_intelligence' => M,
        'infuse_wisdom'       => N,
        'infuse_reflex'       => O,
        'infuse_charisma'     => P,
        'infuse_instinct'     => Q,
        'infuse_hitroll'      => R,
        'infuse_damageroll'   => S,
    ];

    return array_keys(array_filter($flags, function($f) use ($bits) {
        return $bits & $f;
    }));
}

$area = file_get_contents('caemnew.txt');

$start = strpos($area, '#OBJECTS') + 8;
$end   = strpos($area, '#ROOMS');

$lines = explode("\n", substr($area, $start, ($end - $start)));

$found   = [];
$current = [];

$pos = 0;
foreach ($lines as $line) {
    if (strpos($line, '#') === 0 && $line != '#0') {

        $current['vnum'] = substr($line, 1);
        next($lines);
        $current['keywords']   = rtrim(next($lines), '~');
        $current['short_desc'] = rtrim(next($lines), '~');
        $current['long_desc']  = rtrim(next($lines), '~');
        $current['material']   = rtrim(next($lines), '~');
        $current['metadata']   = json_encode(['guild' => rtrim(next($lines), '~')]);

        $parts = explode(' ', next($lines));

        $current['type']       = get_itemtype($parts[0]);
        $current['flags']      = implode(', ', get_flags(calculate_flag($parts[1])));
        $current['wear_flags'] = get_wear(calculate_flag($parts[2]));

        if (count($current['wear_flags']) > 1) {
            $current['wear_flags'] = array_filter($current['wear_flags'], function($f) { return $f !== Wearslot::TAKE; });
        }

        $current['wear_flags'] = array_shift($current['wear_flags']);

        $parts = preg_split("/'(.+?)'|(\S+)| /", next($lines), -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        $current['v0'] = (is_numeric($parts[0]) ? $parts[0] : calculate_flag($parts[0]));
        $current['v1'] = (is_numeric($parts[1]) ? $parts[1] : calculate_flag($parts[1]));
        $current['v2'] = (isset($parts[2]) ? (is_numeric($parts[2]) ? $parts[2] : calculate_flag($parts[2])) : null);
        $current['v3'] = (isset($parts[3]) ? (is_numeric($parts[3]) ? $parts[3] : calculate_flag($parts[3])) : null);
        $current['v4'] = (isset($parts[4]) ? (is_numeric($parts[4]) ? $parts[4] : calculate_flag($parts[4])) : null);

        switch ($current['type']) {

            case Itemtype::WEAPON:
                $current['v0'] = get_weapontype($parts[0]);
                $current['v4'] = implode(', ', get_weaponflags(calculate_flag($parts[4])));
                break;

            case Itemtype::FURNITURE:
                $current['v2'] = implode(', ', get_furnitureflags(calculate_flag($parts[2])));
                break;

            case Itemtype::CONTAINER:
            case Itemtype::SHOP:
                $current['v1'] = implode(', ', get_containerflags(calculate_flag($parts[1])));
                break;
        }

        $parts                = explode(' ', next($lines));
        $current['level']     = $parts[0];
        $current['weight']    = $parts[1];
        $current['cost']      = $parts[2];
        $current['condition'] = 100;

        switch ($parts[3]) {
            case 'G':
                $current['condition'] = 90;
                break;
            case 'A':
                $current['condition'] = 75;
                break;
            case 'W':
                $current['condition'] = 50;
                break;
            case 'D':
                $current['condition'] = 25;
                break;
            case 'B':
                $current['condition'] = 10;
                break;
            case 'R':
                $current['condition'] = 0;
                break;
        }

        while (($next = next($lines)) && $next !== false) {

            // if we run into a new vnum definition, step back
            if (strpos($next, '#') === 0) {
                prev($lines);
                break;
            }

            if ($next == 'E') {
                $extra = [
                    'keyword'     => rtrim(next($lines), '~'),
                    'description' => ''
                ];

                while (($next = next($lines)) !== '~') {
                    $extra['description'] .= $next;
                }
                prev($lines);

                $current['extras'][] = $extra;
            }

            if ($next == 'A') {
                $parts = explode(' ', next($lines));
                try {

                    $current['affect'] = ($parts[0] ?? null);
                    $current['modifier'] = ($parts[1] ?? null);

                } catch (\Exception $e) {

                    printf("Tried to load affect %d on %d\n", $parts[0], $current['vnum']);
                }
            }
        }

        if (isset($current['extras'])) {
            $current['extras'] = json_encode($current['extras']);
        }

        $found[] = $current;
        $current = [];
    }
    $pos++;
}

//print_r(array_slice($lines, 5931));
//
//print_r(array_filter($found, function($f) { return is_array($f['v4']); }));
//die();

$db = new \PDO('mysql:host=localhost;dbname=phpmud', 'root', '123');

foreach($found as $f) {

    $s = $db->prepare(
        'INSERT INTO items 
    (area_id, name, keywords, short_desc, long_desc, type, extra_desc, weight, container, wear_slot, flags, metadata, v0, v1, v2, v3, v4, original_vnum, `condition`) 
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ');

    $data = [
        1, // area_id
        '',
        $f['keywords'],
        $f['short_desc'],
        $f['long_desc'],
        $f['type'],
        ($f['extras'] ?? null),
        $f['weight'],
        (int) ( $f['type'] == Itemtype::CONTAINER),
        $f['wear_flags'],
        $f['flags'],
        $f['metadata'],
        $f['v0'],
        $f['v1'],
        $f['v2'],
        $f['v3'],
        $f['v4'],
        $f['vnum'],
        $f['condition']
    ];

    if ( ! $s->execute($data)) {
        print_r($s->errorInfo());
    }
}
