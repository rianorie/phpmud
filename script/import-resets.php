<?php

$area = file_get_contents('caemnew.txt');

$start = strpos($area, '#RESETS') + 7;
$end   = strpos($area, '#SHOPS');

$lines = explode("\n", substr($area, $start, ($end - $start)));

$found   = [];
$current = [];

$db = new \PDO('mysql:host=localhost;dbname=phpmud', 'root', '123');

$lastMob  = null;
$lastRoom = null;
foreach ($lines as $pos => $line) {

    if ($line == 'S') {
        break;
    }

    $parts = explode(' ', $line);

    switch ($parts[0]) {
        // door resets, skipping these
        case 'D': break;

        // object in room
        case 'O':

            $obj = $db->prepare('SELECT id FROM items WHERE original_vnum = ? LIMIT 1');
            $obj->execute([$parts[2]]);
            $objId = $obj->fetchColumn();

            $room = $db->prepare('SELECT id FROM rooms WHERE original_vnum = ? LIMIT 1');
            $room->execute([$parts[4]]);
            $roomId = $room->fetchColumn();

            $lastRoom = $roomId;

            $os = $db->prepare('INSERT INTO item_resets (area_id, room_id, item_id) VALUES (?, ?, ?)');
            if ( ! $os->execute([1, $roomId, $objId])) {
                printf("%s\n", $line);
                print_r($os->errorInfo());
            }

            break;

        // object in another object in the room
        case 'P':

            $obj = $db->prepare('SELECT id FROM items WHERE original_vnum = ? LIMIT 1');
            $obj->execute([$parts[2]]);
            $objId = $obj->fetchColumn();

            $target = $db->prepare('SELECT id FROM items WHERE original_vnum = ? LIMIT 1');
            $target->execute([$parts[4]]);
            $targetId = $target->fetchColumn();

            $inside = $db->prepare('SELECT id FROM item_resets WHERE item_id = ? AND room_id = ? LIMIT 1');
            $inside->execute([$targetId, $lastRoom]);
            $insideId = $inside->fetchColumn();

            $os = $db->prepare('INSERT INTO item_resets (area_id, room_id, item_id, parent_id) VALUES (?, ?, ?, ?)');
            if ( ! $os->execute([1, $lastRoom, $objId, $insideId])) {
                printf("%s\n", $line);
                print_r($os->errorInfo());
            }

            break;

        // mob in room
        case 'M':


            $mob = $db->prepare('SELECT id FROM mobiles WHERE original_vnum = ? LIMIT 1');
            $mob->execute([$parts[2]]);
            $mobId = $mob->fetchColumn();

            $room = $db->prepare('SELECT id FROM rooms WHERE original_vnum = ? LIMIT 1');
            $room->execute([$parts[4]]);
            $roomId = $room->fetchColumn();

            $lastRoom = $roomId;
            $lastMob  = $mobId;

            $exist = $db->prepare('SELECT id FROM mobile_resets WHERE area_id=? AND room_id=? AND mobile_id=? AND min <= ? AND max <= ? LIMIT 1');
            $exist->execute([1, $roomId, $mobId, $parts[5], $parts[3]]);

            if ($exist->rowCount() > 0) {
                $db->exec('DELETE FROM mobile_resets WHERE id='.$exist->fetchColumn());
                printf("Removed previous reset line because values were higher in the new one.\n");
            }

            $os = $db->prepare('INSERT INTO mobile_resets (area_id, room_id, mobile_id, min, max) VALUES (?, ?, ?, ?, ?)');
            if ( ! $os->execute([1, $roomId, $mobId, $parts[5], $parts[3]])) {
                printf("%s\n", $line);
                print_r($os->errorInfo());
            }

            break;

        // item on mob in room
        case 'G':
        case 'E':

            $obj = $db->prepare('SELECT id FROM items WHERE original_vnum = ? LIMIT 1');
            $obj->execute([$parts[2]]);
            $objId = $obj->fetchColumn();

            $inside = $db->prepare('SELECT id FROM mobile_resets WHERE mobile_id = ? AND room_id = ? LIMIT 1');
            $inside->execute([$lastMob, $lastRoom]);
            $insideId = $inside->fetchColumn();

            $wear = null;

            if (count($parts) == 5) {
                $wear = $parts[4];
            }

            $os = $db->prepare('INSERT INTO item_resets (area_id, room_id, item_id, mobile_reset_id, mobile_wear_loc) VALUES (?, ?, ?, ?, ?)');
            if ( ! $os->execute([1, $lastRoom, $objId, $insideId, $wear])) {
                printf("%s\n", $line);
                print_r($os->errorInfo());
            }


            break;

        default:
            printf("No idea what to do with: %s\n", $line);
    }
}
