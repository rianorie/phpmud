<?php
/**
 * PHP MUD SERVER
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *
 * @package     PHPMUD
 * @copyright   Copyright (c) 2015 Rian Orie (http://rianorie.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Socket wrapper -- this wrapper goes around the game process and keeps the sockets in place for when the game crashes
 *
 * @author  Rian Orie <rian.orie@gmail.com>
 * @version 1.0
 * @created 2015-08-03
 */
class SocketWrapper
{
	/**
	 * Type for external connections
	 */
	const TYPE_EXTERNAL = 1;

	/**
	 * Type for internal connections
	 */
	const TYPE_INTERNAL = 2;

	/**
	 * The game server's unix socket path
	 *
	 * @var string
	 */
	private string $path = '/tmp/game.sock';

	/**
	 * @var Socket
	 */
	private Socket $handle;

	/**
	 * @var resource
	 */
	private $game;

	/**
	 * @var array
	 */
	private array $sockets = [];

	/**
	 * Maps the internal socket ids to the external socket ids
	 *
	 * @var array
	 */
	private array $map = [];

	/**
	 * Saves the socket type, determining either TYPE_INTERNAL or TYPE_EXTERNAL
	 *
	 * @var array
	 */
	private array $type = [];

	/**
	 * Run the socket wrapper
	 *
	 * @throws Exception
	 */
	public function run()
	{
		$this->startGame();
		$this->setupSocketServer();

		while(true) {

			$status = proc_get_status($this->game);
			if ($status['running'] == false) {
				$this->msg('Failed to read output from the game process.');
				$this->msg('Starting process now.');
				$this->startGame();
				sleep(1);
				$this->msg('Re-piping remote sockets through to the game process.');
				$this->bindSockets();

				continue;
			}

			// Setup clients listen socket for reading
			$socketRead = [$this->handle];

			// Loop through the clients to push into the socket reader
			foreach($this->sockets as $socket) { $socketRead[] = $socket; }

			$write = $except = null;

            // Set up a blocking call to socket_select()
            $socketSelect = socket_select($socketRead, $write, $except, 2);

            if ($socketSelect === false) {
                $this->msg("socket_select() failed, reason: " . socket_strerror(socket_last_error()));

            } else if ($socketSelect < 1) {
				continue;
			}

            $handleId = $this->getSocketResourceId($this->handle);
            $readSocketIds = [];
            foreach($socketRead as $readSocket) {
                $readSocketIds[] = $this->getSocketResourceId($readSocket);
            }

			// Create a new connection if requested
            if (in_array($handleId, $readSocketIds)) {
				// add the new connection to the list together with it's internal pipe
				$new = [socket_accept($this->handle), socket_create(AF_UNIX, SOCK_STREAM, 0)];
                socket_connect($new[1], $this->path);

                $this->map[$this->getSocketResourceId($new[0])] = $this->getSocketResourceId($new[1]);
                $this->map[$this->getSocketResourceId($new[1])] = $this->getSocketResourceId($new[0]);

                $this->type[$this->getSocketResourceId($new[0])] = $this::TYPE_EXTERNAL;
                $this->type[$this->getSocketResourceId($new[1])] = $this::TYPE_INTERNAL;

                $this->sockets[$this->getSocketResourceId($new[0])] = $new[0];
                $this->sockets[$this->getSocketResourceId($new[1])] = $new[1];

                $this->msg('New socket connected.');

                $address = $port = null;
                socket_getsockname($new[0], $address, $port);

                $remoteId = socket_read($new[1], 100);

                $this->msg(sprintf("Remote socket id for %d is %s", $this->getSocketResourceId($new[1]), trim($remoteId)));

                $this->writeSocketData($remoteId, ['address' => $address,
                                                   'port' => $port,
                                                   'internal' => $this->getSocketResourceId($new[1]),
                                                   'external' => $this->getSocketResourceId($new[0]),
                                                   'new' => true]);

                // remove the server from the read handle so that it wont end up in the foreach
				unset($socketRead[0]);
			}

            // If a client is trying to write - handle it now
            foreach($socketRead as $socket) {

                $reverse = $this->map[$this->getSocketResourceId($socket)];
                $read    = @socket_read($socket, 1024);
                if ($read !== '{close}') {
                    $write = @socket_write($this->sockets[$reverse], $read);

                } else {
                    $this->msg(sprintf("Received {close} for %d (ext %d)\n", $this->getSocketResourceId($socket), $reverse));
                    $write = $read = false;
                    socket_shutdown($this->sockets[$this->getSocketResourceId($socket)]);
                    socket_shutdown($this->sockets[$reverse]);
                }
                if ($write === false || $read === false) {
                    @socket_set_block($this->sockets[$reverse]);
                    @socket_set_option($this->sockets[$reverse], SOL_SOCKET, SO_LINGER, ['l_onoff' => 1, 'l_linger' => 0]);
                    socket_close($this->sockets[$reverse]);

                    @socket_set_block($this->sockets[$this->getSocketResourceId($socket)]);
                    @socket_set_option($this->sockets[$this->getSocketResourceId($socket)], SOL_SOCKET, SO_LINGER, ['l_onoff' => 1, 'l_linger' => 0]);
                    socket_close($this->sockets[$this->getSocketResourceId($socket)]);

                    $this->msg(sprintf("Called close for %d (ext %d)\n", $this->getSocketResourceId($socket), $reverse));

                    unset($this->map[$reverse], $this->map[$this->getSocketResourceId($socket)]);
                    unset($this->type[$reverse], $this->type[$this->getSocketResourceId($socket)]);
                    unset($this->sockets[$reverse], $this->sockets[$this->getSocketResourceId($socket)]);
                }
			}
		}
	}

	protected function writeSocketData($id, $data)
	{
		file_put_contents(__DIR__.'/var/socketdata/'.$id.'.lock', 1);
		file_put_contents(__DIR__.'/var/socketdata/'.$id.'.json', json_encode($data));
		unlink(__DIR__.'/var/socketdata/'.$id.'.lock');
	}

	/**
	 * Retrieve the system id for a socket resource
	 *
	 * @param Socket $socket
	 *
	 * @return int
	 */
	protected function getSocketResourceId(Socket $socket) : int
    {
	    return spl_object_id($socket);
	}

    /**
     * Setup a socket server
     *
     * @throws Exception
     */
	protected function setupSocketServer()
	{
		// set up the TCP socket
		$this->handle = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

		if ($this->handle == false) {
			throw new Exception('Failed to create socket! Error: '.socket_strerror(socket_last_error()));
		}

		socket_set_option($this->handle, SOL_SOCKET, SO_REUSEPORT, 1);
		socket_bind($this->handle, '0.0.0.0', 6400);
		socket_listen($this->handle);
	}

	/**
	 * Boot up the game process
	 *
	 * @throws Exception
	 */
	protected function startGame()
	{
		$descriptorspec = array(
			0 => array("file", "php://stdin", "r"),  // stdin is a pipe that the child will read from
			1 => array("file", "php://stdout", "w"),  // stdout is a pipe that the child will write to
			2 => array("file", "php://stdout", "w") // stderr is a file to write to
		);

		$this->game = proc_open('php game.php '.$this->path, $descriptorspec, $pipes);

		if ( ! is_resource($this->game)) {
			throw new Exception('Failed to boot game!');
		}
	}

    /**
     * Read the output from the game process
     *
     * @return bool|string
     */
	protected function readGameOutput() : bool|string
    {
		if (is_resource($this->game)) {
            /** @noinspection PhpLoopNeverIteratesInspection */
            while( ! feof($this->game)) {
				return fread($this->game, 1024);
			}
		}

		return false;
	}

	/**
	 * (Re-)bind the external sockets to an internal one
	 */
	protected function bindSockets()
	{
		// first clean up the reference arrays, they'll be rebuilt
		$this->map = [];
		foreach($this->type as $id => $type) {
			if ($type == $this::TYPE_INTERNAL) {
				unset($this->type[$id]);
				socket_close($this->sockets[$id]);
				unset($this->sockets[$id]);
			}
		}

		$types = $this->type;
		foreach($types as $id => $type) {

			// set up the new internal socket
			$socket = socket_create(AF_UNIX, SOCK_STREAM, 0);
			socket_connect($socket, $this->path);

			$this->msg('New socket for ext:'.$id.' added, id: '.$this->getSocketResourceId($socket));

			$address = $port = null;
			socket_getsockname($this->sockets[$id], $address, $port);

			$data = ['address' => $address,
			         'port' => $port,
			         'internal' => $this->getSocketResourceId($socket),
			         'external' => $id,
			         'new' => false];

			$remoteId = socket_read($socket, 100);
			$this->msg(sprintf("Remote socket id for %d is %s", $this->getSocketResourceId($socket), $remoteId));
			$this->writeSocketData($remoteId, $data);

			// set up the bidirectional map
			$this->map[$id] = $this->getSocketResourceId($socket);
			$this->map[$this->getSocketResourceId($socket)] = $id;

			// add the new internal socket to the type list
			$this->type[$this->getSocketResourceId($socket)] = $this::TYPE_INTERNAL;

			// and finally add this new socket to the socket list
			$this->sockets[$this->getSocketResourceId($socket)] = $socket;
		}
	}

	/**
	 * Output messages to the console
	 *
	 * @param $text
	 */
	protected function msg($text)
	{
		printf("[%s] SocketWrapper: %s\n", date('Y-m-d H:i:s'), $text);
	}
}

while(true) {

    try {
        $s = new SocketWrapper();
        $s->run();

    } catch (Exception $e) {
        printf("CRASH: %s\n", $e->getMessage());
    }

}
