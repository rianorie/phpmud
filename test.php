<?php

require_once './vendor/autoload.php';

$server = new Hoa\Socket\Server('tcp://127.0.0.1:4242');
$server->connectAndWait();

while (true) {
    foreach ($server->select() as $node) {
        $line = $server->readLine();

        if (empty(trim($line))) {
            $server->disconnect();
            continue;
        }

        echo '< ', $line, "\n";
        $server->writeLine(strtoupper($line));
    }
}
